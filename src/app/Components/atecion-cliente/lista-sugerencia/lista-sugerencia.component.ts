import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
// import { MensajedialogComponent } from '../mensajedialog/mensajedialog.component';
import { ModalSugerenciasComponent } from './modal-sugerencias/modal-sugerencias.component';
import { ModalDetalleSugerenciaComponent } from './modal-detalle-sugerencia/modal-detalle-sugerencia.component';
import { Router } from '@angular/router';
import { GeneralService } from '../../core/Services/general/general.service';


export interface SugerenciaData {
 
  idSugerencia: string;
  estatus:string;
  fecha: string;
  descripcion: string;
  cliente_cedula: string;
  cliente_nombre: string;
  cliente_apellido: string;
  ver: string;
  color: string;
}

let sugerencias: any[] = [];

@Component({
  selector: 'app-lista-sugerencia',
  templateUrl: './lista-sugerencia.component.html',
  styleUrls: ['./lista-sugerencia.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ListaSugerenciaComponent implements OnInit {
  resultado: any
  displayedColumns: string[] = ['ver', 'fecha', 'estatus'];
  dataSource: MatTableDataSource<SugerenciaData>;
  datos: SugerenciaData;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  value="";
  valueInput(){
    this.value="";
    this.applyFilter(this.value);
  }

  constructor(public generalService: GeneralService, public dialog: MatDialog, private router: Router)
   {
    this.CargarTabla();
     // Assign the data to the data source for the table to render
     
   }

   ngOnInit() {


  }

  openDialog(row) //abre una ventana modal
  {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '600px';
    // PARA PASAR LOS DATOS AL DIALOG
    dialogConfig.data = row;

    const dialogRef = this.dialog.open(ModalSugerenciasComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.CargarTabla()
        }
      }
    );
  }


  openDialog2() // abre una ventana modal
   {
    this.dialog.open(ModalSugerenciasComponent, { width: '600px'});
  }
    openDialogDetalle222() // abre una ventana modal
   {
    this.dialog.open(ModalDetalleSugerenciaComponent, { width: '500px'});
  }

  // openDialog2(){
  //   this.dialog.open(MensajedialogComponent, { width:'500px'})
  // }

  inicio(){
    this.router.navigate(['DashJ']);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  Estatus(est) {
    let estatus;
    if(est == 'A'){
      estatus = 'Aprobado';
    }else if(est == 'R'){
      estatus = 'Rechazado';
    }else{
      estatus = 'Pendiente';
    }
    return estatus;
  }

  CargarTabla()
  {
    let client : any;
    this.generalService.ObtenerUno('cliente/usuario',Number.parseInt(localStorage.getItem('id'))).then((result) => {
      client = result;
      console.log(client);
      console.log(client.data.id);
      this.generalService.ObtenerUno('vista_sugerencia/user',Number.parseInt(client.data.id)).then((result) => {
        this.resultado = result;
        console.log(this.resultado);
        sugerencias = [];


        for (let i = 0; i < this.resultado.data.length; i++) {
        
          if (this.resultado.data[i].estatus != 'I') {
  
            let est = this.Estatus(this.resultado.data[i].estatus);
            let colorEst = this.resultado.data[i].estatus;
            let color;
            let ver;
  
            switch (colorEst) {
              case 'A':
                color = 'green';
                ver = 'check_circle';
                break;
              case 'R':
                color = 'red';
                ver = 'cancel';
                break;
              case 'P':
                color = '#cd9d12';
                ver = 'error';
                break;
            }
          console.log(est);
          this.datos = {
            idSugerencia: this.resultado.data[i].sugerencia_id,
            descripcion: this.resultado.data[i].descripcion,
            fecha: this.resultado.data[i].fecha,
            cliente_nombre: this.resultado.data[i].cliente_nombre,
            cliente_cedula: this.resultado.data[i].cliente_cedula,
            cliente_apellido: this.resultado.data[i].cliente_apellido,
            estatus: est,
            ver: ver,
            color: color,
          };
          sugerencias.push(this.datos);
        }
      }
        this.dataSource = new MatTableDataSource(sugerencias);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }, (err) => {
        console.log(err);
      });
  

    }, (err) => {
      console.log(err);
    });

    
  }

  openDialogDetalle(row) //abre una ventana modal
  {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '600px';
    // PARA PASAR LOS DATOS AL DIALOG
    dialogConfig.data = row;

    const dialogRef = this.dialog.open(ModalDetalleSugerenciaComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.CargarTabla()
        }
      }
    );
  }

}
