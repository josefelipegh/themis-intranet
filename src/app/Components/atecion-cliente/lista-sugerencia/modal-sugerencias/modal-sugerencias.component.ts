import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import {MatSnackBar} from '@angular/material';
import { Router } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';
import { GeneralService } from '../../../core/Services/general/general.service';

@Component({
  selector: 'app-modal-sugerencias',
  templateUrl: './modal-sugerencias.component.html',
  styleUrls: ['./modal-sugerencias.component.scss']
})
export class ModalSugerenciasComponent implements OnInit {
  form_descripcion = { cliente_id: '', fecha_creacion: '', descripcion: '', estatus: 'P'};
  resultado: any;
  listaSugerencia: any[] = [];
  public loading = false;
  fecha = new Date();
  date;

  constructor(public dialogRef: MatDialogRef<ModalSugerenciasComponent>, public generalService: GeneralService, public snackBar: MatSnackBar,private router: Router) {

    this.date = (this.fecha.getDate()+'/'+this.fecha.getMonth()+'/'+this.fecha.getFullYear());
      console.log(this.date);
   }


   descripcionFormControl = new FormControl('', [
    Validators.required
  ]);


  ngOnInit() {
  }

 openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }

  onNoClick(){
    this.dialogRef.close();
  }
  Registrar() {

    if (this.descripcionFormControl.valid) {
      let datos;
      this.generalService.ObtenerUno('cliente/usuario',Number.parseInt(localStorage.getItem('id'))).then((result) => {
        datos = result;
        this.form_descripcion.fecha_creacion = ((this.fecha.getMonth()+1) + '-'+ this.fecha.getDate() + '-' + this.fecha.getFullYear());
        this.form_descripcion.descripcion = this.descripcionFormControl.value;
        this.form_descripcion.cliente_id = datos.data.id;
        console.log(this.form_descripcion);
        
        this.loading = true;
        this.generalService.Registrar(this.form_descripcion, 'sugerencia').then((result) => {
          this.openSnackBar('Sugerencia registrada exitosamente!', "Registro exitoso");
          this.loading = false;
          this.dialogRef.close(this.form_descripcion);
        }, (err) => {
          console.log(err);
          this.loading = false;
        });
      }, (err) => {
        console.log(err);
      });

    }
  }

}
