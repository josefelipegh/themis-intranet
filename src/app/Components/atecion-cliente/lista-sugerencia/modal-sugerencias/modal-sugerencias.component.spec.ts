import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSugerenciasComponent } from './modal-sugerencias.component';

describe('ModalSugerenciasComponent', () => {
  let component: ModalSugerenciasComponent;
  let fixture: ComponentFixture<ModalSugerenciasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalSugerenciasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSugerenciasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
