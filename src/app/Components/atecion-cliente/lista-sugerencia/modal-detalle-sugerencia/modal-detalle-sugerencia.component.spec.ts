import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDetalleSugerenciaComponent } from './modal-detalle-sugerencia.component';

describe('ModalDetalleSugerenciaComponent', () => {
  let component: ModalDetalleSugerenciaComponent;
  let fixture: ComponentFixture<ModalDetalleSugerenciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalDetalleSugerenciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDetalleSugerenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
