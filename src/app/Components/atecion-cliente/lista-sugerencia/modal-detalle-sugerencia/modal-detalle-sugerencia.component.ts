import { Component, OnInit, Inject } from '@angular/core';
import {MatSnackBar} from '@angular/material';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-modal-detalle-sugerencia',
  templateUrl: './modal-detalle-sugerencia.component.html',
  styleUrls: ['./modal-detalle-sugerencia.component.scss']
})
export class ModalDetalleSugerenciaComponent implements OnInit {
  form = { fecha:'', tiporeclamo: '',  descripcion: '', cliente_nombre: '', cliente_apellido:'', cliente_cedula:'', estatus:''};

  constructor(public dialogRef: MatDialogRef<ModalDetalleSugerenciaComponent>,
    public snackBar: MatSnackBar,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public data: any) {

      this.form.fecha = data.fecha;
      this.form.descripcion = data.descripcion;
      this.form.cliente_nombre = data.cliente_nombre;
      this.form.cliente_apellido = data.cliente_apellido;
      this.form.cliente_cedula = data.cliente_cedula;
      this.form.estatus = data.estatus;
    }

  ngOnInit() {
  }
  onNoClick(){
    this.dialogRef.close();
  }

  openSnackBar() {
    this.snackBar.open("Respuesta Enviada!!", "OK", {
      duration: 2000,
    });
    this.router.navigate(['sugerencias']);
  }
  
}
