import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { GeneralService } from '../../../core/Services/general/general.service';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-modal-rechazar-reclamo',
  templateUrl: './modal-rechazar-reclamo.component.html',
  styleUrls: ['./modal-rechazar-reclamo.component.scss']
})
export class ModalRechazarReclamoComponent implements OnInit {

 /*  form = { cliente_nombre: '', cliente_apellido: '', tipo_reclamo: '', descripcion: ''};
  form2 = {estatus: ''};
  resultado: any;
  id;
  listaRespuesta: any[] = [];
  public loading = false;

  constructor(public dialog: MatDialog,
              public snackBar: MatSnackBar,
              public generalService: GeneralService, 
              public dialogRef: MatDialogRef<ModalRechazarReclamoComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {

    this.CargarCombo();
    this.form.cliente_nombre = data.cliente_nombre;
    this.form.cliente_apellido = data.cliente_apellido;
    this.form.tipo_reclamo = data.tipo_reclamo;
    this.form.descripcion = data.descripcion;
    this.id = data.id; */


  
  form = { cliente_id: '', fecha_creacion: '', descripcion: '', tipo_reclamo_id:'' };
  form3 = { cliente_nombre:'', tipo_reclamo:'', cliente_apellido:'', descripcion:'', fecha:'', cliente_cedula:'', garantia_descripcion: '', servicio_nombre: ''}
  abogado;
  
  resultado: any;
  id;
  listaRespuesta: any[] = [];
  public loading = false;
  form2 = { estatus: '' };



  constructor(public dialogRef: MatDialogRef<ModalRechazarReclamoComponent>,
    public generalService: GeneralService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public snackBar: MatSnackBar,
    private router: Router) { 

      this.CargarCombo();
      this.id = data.reclamo_id;
      this.form.cliente_id = data.cliente_id;                
      this.form.descripcion = data.descripcion;
      this.form.tipo_reclamo_id = data.reclamo_id;
      this.form.fecha_creacion = moment().format('MM-DD-YYYY');
      this.form3.cliente_nombre = data.cliente_nombre;
      this.form3.cliente_apellido = data.cliente_apellido;
      this.form3.cliente_cedula = data.cliente_cedula;
      this.form3.descripcion = data.descripcion;
      this.form3.fecha = data.fecha;
      this.form3.tipo_reclamo = data.tipo_reclamo;
      this.form3.garantia_descripcion  = data.garantia_descripcion;
      this.form3.servicio_nombre = data.servicio_nombre;
    }
 
  

  CargarCombo() {
    this.generalService.Obtenertodos('tipo_respuesta').then((result) => {

      this.resultado = result;
      console.log(result);
      for (let i = 0; i < this.resultado.data.length; i++) {
        this.listaRespuesta.push(this.resultado.data[i]);
      }
      console.log(this.listaRespuesta);
    }, (err) => {
      console.log(err);
    });
  }

  respuestaFormControl = new FormControl('', [
    Validators.required
  ]);

  causaFormControl = new FormControl('', [
    Validators.required
  ]);

  disableSelect = new FormControl(true);

  ngOnInit() {
  }

  Rechazar() {

    
    this.generalService.ObtenerUno('cliente',this.data.cliente_id).then((result) => {
      let idcliente: any = result;
      console.log(idcliente.data.usuario_id);

      if (this.respuestaFormControl.valid) {
        this.form2.estatus = 'R';
        this.loading = true;
        this.generalService.Actualizar(this.form2, 'reclamo', this.id).then((result) => {
          this.generalService.ObtenerUno('dispositivo',idcliente.data.usuario_id).then((result) => {
            this.openSnackBar('Reclamo Rechazado con éxito!!', "Rechazar Reclamo");
            let  disp: any = result;
            let datos = 
              {
                dispositivos: [
                  disp.data.token
                ],
                titulo: this.respuestaFormControl.value,
             
              }
            
            console.log(datos);
           /* this.generalService.Registrar(datos, 'notificacion/enviar').then((result) => {
                   
              this.openSnackBar('Reclamo Rechazada con éxito!!', "Rechazar Solicitud");
              this.loading = false;
            },*/  
          }, (err) => {
            console.log(err);
            this.loading = false;
          });
        }, (err) => {
          console.log(err);
          this.loading = false;
        });
  
        this.dialogRef.close(this.data);
      }
    }, (err) => {
      console.log(err);
    });
  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
