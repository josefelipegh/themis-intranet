import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalRechazarReclamoComponent } from './modal-rechazar-reclamo.component';

describe('ModalRechazarReclamoComponent', () => {
  let component: ModalRechazarReclamoComponent;
  let fixture: ComponentFixture<ModalRechazarReclamoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalRechazarReclamoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalRechazarReclamoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
