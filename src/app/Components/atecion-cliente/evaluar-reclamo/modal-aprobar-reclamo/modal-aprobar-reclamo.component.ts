import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { GeneralService } from '../../../core/Services/general/general.service';
import * as moment from 'moment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-modal-aprobar-reclamo',
  templateUrl: './modal-aprobar-reclamo.component.html',
  styleUrls: ['./modal-aprobar-reclamo.component.scss']
})
export class ModalAprobarReclamoComponent implements OnInit {

  form = { cliente_id: '', fecha_creacion: '', descripcion: '', tipo_reclamo_id:'' };
  form3 = { cliente_nombre:'', tipo_reclamo:'', cliente_apellido:'', descripcion:'', fecha:'', cliente_cedula:'', garantia_descripcion: '', servicio_nombre: ''}
  abogado;
  

  form2 = { estatus: '' };

  id;
  public loading = false;

  constructor(public dialogRef: MatDialogRef<ModalAprobarReclamoComponent>,
              public generalService: GeneralService,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public snackBar: MatSnackBar,
              private router: Router) { 

                this.id = data.reclamo_id;
                this.form.cliente_id = data.cliente_id;                
                this.form.descripcion = data.descripcion;
                this.form.tipo_reclamo_id = data.reclamo_id;
                this.form.fecha_creacion = moment().format('MM-DD-YYYY');
                this.form3.cliente_nombre = data.cliente_nombre;
                this.form3.cliente_apellido = data.cliente_apellido;
                this.form3.cliente_cedula = data.cliente_cedula;
                this.form3.descripcion = data.descripcion;
                this.form3.fecha = data.fecha;
                this.form3.tipo_reclamo = data.tipo_reclamo;
                this.form3.garantia_descripcion  = data.garantia_descripcion;
                this.form3.servicio_nombre = data.servicio_nombre;
              }

  ngOnInit() {
  }

  onNoClick() {
    this.dialogRef.close();
  }

  RegistrarServicio() {
  
      console.log(this.form);
      this.loading = true;
      this.generalService.Registrar(this.form, 'servicio').then((result) => {

      this.generalService.ObtenerUno('cliente',this.data.cliente_id).then((result) => {
          let idcliente: any = result;
          console.log(idcliente.data.usuario_id);
    
        this.form2.estatus = 'A';
        console.log(this.data.cliente_id);
        console.log(this.id);
        this.generalService.Actualizar(this.form2, 'reclamo', this.id).then((result) => {
          this.generalService.ObtenerUno('dispositivo',idcliente.data.usuario_id).then((result) => {
            this.openSnackBar('Reclamo Aprobado exitosamente!', "Registro exitoso");
            let  disp: any = result;
            let datos = 
              {
                dispositivos: [
                  disp.data.token
                ],
                titulo: 'Reclamo Aprobado',
                mensaje: 'su reclamo ha sido aprobada'
              }
              
            console.log(datos);
            //this.generalService.Registrar(datos, 'notificacion/enviar').then((result) => {
                   
             // this.openSnackBar('Reclamo Aprobado exitosamente!', "Registro exitoso");
             // this.loading = false;
             this.dialogRef.close(this.form2);
            }, (err) => {
              console.log(err);
              this.loading = false;
            });
  
          }, (err) => {
            console.log(err);
            this.loading = false;
          });

        }, (err) => {
          console.log(err);
          this.loading = false;
        });

      }, (err) => {
        console.log(err);
      });
        
      
   

  }
goToPage(pageName: string){
  this.router.navigate([`${pageName}`])
}

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }
}
