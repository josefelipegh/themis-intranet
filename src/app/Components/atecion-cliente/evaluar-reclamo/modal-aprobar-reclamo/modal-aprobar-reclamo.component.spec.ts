import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAprobarReclamoComponent } from './modal-aprobar-reclamo.component';

describe('ModalAprobarReclamoComponent', () => {
  let component: ModalAprobarReclamoComponent;
  let fixture: ComponentFixture<ModalAprobarReclamoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalAprobarReclamoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAprobarReclamoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
