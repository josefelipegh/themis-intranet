import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalEvaluarReclamoComponent } from './modal-evaluar-reclamo.component';

describe('ModalEvaluarReclamoComponent', () => {
  let component: ModalEvaluarReclamoComponent;
  let fixture: ComponentFixture<ModalEvaluarReclamoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalEvaluarReclamoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEvaluarReclamoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
