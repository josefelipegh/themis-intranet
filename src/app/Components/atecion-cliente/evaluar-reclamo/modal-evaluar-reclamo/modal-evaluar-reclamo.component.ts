import { Component, OnInit, Inject } from '@angular/core';
import {MatSnackBar} from '@angular/material';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


@Component({
  selector: 'app-modal-evaluar-reclamo',
  templateUrl: './modal-evaluar-reclamo.component.html',
  styleUrls: ['./modal-evaluar-reclamo.component.scss']
})
export class ModalEvaluarReclamoComponent implements OnInit {

  form = { fecha:'', tiporeclamo: '',  descripcion: '', cliente_nombre: '', cliente_apellido:'', cliente_cedula:''};

  constructor(public snackBar: MatSnackBar,private router: Router,public dialog: MatDialog, public dialogRef: MatDialogRef<ModalEvaluarReclamoComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {

    this.form.fecha = data.fecha;
    this.form.tiporeclamo = data.tipo_reclamo;
    this.form.descripcion = data.descripcion;
    this.form.cliente_nombre = data.cliente_nombre;
    this.form.cliente_apellido = data.cliente_apellido;
    this.form.cliente_cedula = data.cliente_cedula;

   }

  ngOnInit() {
  }


  openSnackBar() {
    this.snackBar.open("Respuesta Enviada!!", "OK", {
      duration: 2000,
    });
    this.router.navigate(['evaluar_reclamo']);
  }
  onNoClick(){
    this.dialogRef.close();
  }
}
