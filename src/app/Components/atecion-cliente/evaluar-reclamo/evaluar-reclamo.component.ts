import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog, MatDialogConfig} from '@angular/material';
import { Router } from '@angular/router';
import { ModalEvaluarReclamoComponent } from './modal-evaluar-reclamo/modal-evaluar-reclamo.component';
import { GeneralService } from '../../core/Services/general/general.service';
import { ModalRechazarReclamoComponent } from './modal-rechazar-reclamo/modal-rechazar-reclamo.component';
import { ModalAprobarReclamoComponent } from './modal-aprobar-reclamo/modal-aprobar-reclamo.component';


export interface ReclamoData {
  cliente_id: string;
  cliente_cedula: string;
  fecha  : string;
  tipo_reclamo: string;
  descripcion: string;
  cliente_nombre: string;
  cliente_apellido: string;
  reclamo_id: string;
  garantia_descripcion: string;
  servicio_nombre: string;

}

/** Constants used to fill up our data base. */
let reclamos: any[] = [];

@Component({
  selector: 'app-evaluar-reclamo',
  templateUrl: './evaluar-reclamo.component.html',
  styleUrls: ['./evaluar-reclamo.component.scss']
})
export class EvaluarReclamoComponent implements OnInit {

resultado: any;
  datos: ReclamoData;
  displayedColumns: string[] = ['ver', 'cliente_cedula','tipo_reclamo', 'fecha', 'accion'];
  dataSource: MatTableDataSource<ReclamoData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  value="";
  valueInput(){
    this.value="";
    this.applyFilter(this.value);
  }

  constructor(public dialog: MatDialog, private router: Router, public generalService: GeneralService) {
    
    this.CargarTabla();
    
  }
  inicio(){
    this.router.navigate(['DashJ']);
  }

  ngOnInit() {
    
  }

  CargarTabla() {
    this.generalService.Obtenertodos('vista_reclamo').then((result) => {
      this.resultado = result;
      reclamos = [];
      for (let i = 0; i < this.resultado.data.length; i++) {
        if (this.resultado.data[i].estatus=='P') 
        {
        let est = this.Estatus(this.resultado.data[i].estatus);
        this.datos = {
          cliente_id: this.resultado.data[i].cliente_id,
          cliente_cedula: this.resultado.data[i].cliente_cedula,
          tipo_reclamo: this.resultado.data[i].tipo_reclamo,
          fecha: this.resultado.data[i].fecha,
          descripcion: this.resultado.data[i].descripcion,
          cliente_nombre: this.resultado.data[i].cliente_nombre,
          cliente_apellido: this.resultado.data[i].cliente_apellido,
          reclamo_id: this.resultado.data[i].reclamo_id,
          garantia_descripcion: this.resultado.data[i].garantia_descripcion,
          servicio_nombre: this.resultado.data[i].servicio_nombre,
          };
        reclamos.push(this.datos);
      }
    }
      this.dataSource = new MatTableDataSource(reclamos);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, (err) => {
      console.log(err);
    });

  }

  Estatus(est) {
    let estatus;
    return estatus = est == 'P' ? 'Pendiente' : 'Aprobado';
  }

  openDialogAtenderReclamo(row) //abre una ventana modal
  {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '600px';
    // PARA PASAR LOS DATOS AL DIALOG
    dialogConfig.data = row;

    const dialogRef = this.dialog.open(ModalEvaluarReclamoComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.CargarTabla();
        }
      }
    );
  }

  openDialogConfirmar(row){
    const dialogConfig3 = new MatDialogConfig();

    dialogConfig3.disableClose = true;
    dialogConfig3.autoFocus = true;
    dialogConfig3.width = '50%';
    // PARA PASAR LOS DATOS AL DIALOG
    dialogConfig3.data = row;

    const dialogRef3 = this.dialog.open(ModalAprobarReclamoComponent, dialogConfig3);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD
    dialogRef3.afterClosed().subscribe(
      data => {
        if (data) {
          this.CargarTabla();
        }
      }
    );
    
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openDialogRechazar(row){
    const dialogConfig2 = new MatDialogConfig();

    dialogConfig2.disableClose = true;
    dialogConfig2.autoFocus = true;
    dialogConfig2.width = '50%';
    // PARA PASAR LOS DATOS AL DIALOG
    dialogConfig2.data = row;

    const dialogRef2 = this.dialog.open(ModalRechazarReclamoComponent, dialogConfig2);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD
    dialogRef2.afterClosed().subscribe(
      data => {
        if (data) {
          this.CargarTabla();
        }
      }
    );
    
  }


}
