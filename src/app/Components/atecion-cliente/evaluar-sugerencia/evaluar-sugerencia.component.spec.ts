import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EvaluarSugerenciaComponent } from './evaluar-sugerencia.component';

describe('EvaluarSugerenciaComponent', () => {
  let component: EvaluarSugerenciaComponent;
  let fixture: ComponentFixture<EvaluarSugerenciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvaluarSugerenciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvaluarSugerenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
