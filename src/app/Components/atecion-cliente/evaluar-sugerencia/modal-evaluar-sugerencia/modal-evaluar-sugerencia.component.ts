import { Component, OnInit, Inject } from '@angular/core';
import {MatSnackBar} from '@angular/material';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { GeneralService } from '../../../core/Services/general/general.service';

@Component({
  selector: 'app-modal-evaluar-sugerencia',
  templateUrl: './modal-evaluar-sugerencia.component.html',
  styleUrls: ['./modal-evaluar-sugerencia.component.scss']
})
export class ModalEvaluarSugerenciaComponent implements OnInit {
  form = { fecha:'', descripcion: '', cliente_nombre: '', cliente_apellido:'', cliente_cedula:''};
  form2 = {tipo_respuesta_id:'', fecha_creacion:'', sugerencia_id:''}
  form3 = { estatus:''}
  resultado: any;
  id;
  idres;
  listaRespuesta: any[] = [];
  public loading = false;
  fecha = new Date();
  date;

  constructor(
    public snackBar: MatSnackBar,
    private router: Router,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<ModalEvaluarSugerenciaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public generalService: GeneralService) { 

      this.CargarCombo();
      this.date = (this.fecha.getDate()+'/'+this.fecha.getMonth()+'/'+this.fecha.getFullYear());
        console.log(this.date);
      this.form.fecha = data.fecha;
      this.form.descripcion = data.descripcion;
      this.form.cliente_nombre = data.cliente_nombre;
      this.form.cliente_apellido = data.cliente_apellido;
      this.form.cliente_cedula = data.cliente_cedula;
      this.id = data.id;
      console.log(this.id)

    }

    
    respuestaFormControl = new FormControl('',
    [
      Validators.required
    ]);


    CargarCombo() {
      this.generalService.Obtenertodos('tipo_respuesta').then((result) => {
  
        this.resultado = result;
        console.log(result);
        for (let i = 0; i < this.resultado.data.length; i++) {
          this.listaRespuesta.push(this.resultado.data[i]);
        }
        console.log(this.listaRespuesta);
      }, (err) => {
        console.log(err);
      });
    }
    ObtenerIdRespuesta(id) {
      console.log(id);
      return this.idres = id;
  
    }
  ngOnInit() {
  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }

  onNoClick(){
    this.dialogRef.close();
  }

  Registrar() {

    if (this.respuestaFormControl.valid) {
      let datos;
      
        
        this.form2.fecha_creacion= ((this.fecha.getMonth()+1) + '-'+ this.fecha.getDate() + '-' + this.fecha.getFullYear());
        this.form2.tipo_respuesta_id = this.respuestaFormControl.value;
        this.form2.sugerencia_id = this.id;
  
        console.log(this.form2);
        
        this.form3.estatus = 'A'
        this.generalService.Actualizar(this.form3, 'sugerencia', this.id).then((result) => {

        }, (err) => {
          console.log(err);
          this.loading = false;
        });
  
  
        
        this.loading = true;
        this.generalService.Registrar(this.form2, 'respuesta_sugerencia').then((result) => {
          this.openSnackBar('Sugerencia Respondida exitosamente!', "Respuesta exitosa");
          this.loading = false;
          this.dialogRef.close(this.form2);
        }, (err) => {
          console.log(err);
          this.loading = false;
        });
     

    }
  }

}
