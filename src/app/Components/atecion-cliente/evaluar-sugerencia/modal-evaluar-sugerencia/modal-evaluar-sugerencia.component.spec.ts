import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalEvaluarSugerenciaComponent } from './modal-evaluar-sugerencia.component';

describe('ModalEvaluarSugerenciaComponent', () => {
  let component: ModalEvaluarSugerenciaComponent;
  let fixture: ComponentFixture<ModalEvaluarSugerenciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalEvaluarSugerenciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEvaluarSugerenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
