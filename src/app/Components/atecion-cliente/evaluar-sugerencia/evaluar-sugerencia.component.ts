import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog, MatDialogConfig} from '@angular/material';
import { Router } from '@angular/router';
import { ModalEvaluarSugerenciaComponent } from './modal-evaluar-sugerencia/modal-evaluar-sugerencia.component';
import { GeneralService } from '../../core/Services/general/general.service';


export interface SugerenciaData {
  cliente_cedula: string;
  fecha  : string;
  descripcion: string;
  cliente_nombre: string;
  cliente_apellido: string;
  id: string;
}

/** Constants used to fill up our data base. */
let sugerencias: any[] = [];
@Component({
  selector: 'app-evaluar-sugerencia',
  templateUrl: './evaluar-sugerencia.component.html',
  styleUrls: ['./evaluar-sugerencia.component.scss']
})
export class EvaluarSugerenciaComponent implements OnInit {
  resultado: any;
  datos: SugerenciaData;
  displayedColumns: string[] = ['cliente_cedula', 'cliente_nombre', 'fecha', 'ver'];
  dataSource: MatTableDataSource<SugerenciaData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  value="";
  valueInput(){
    this.value="";
    this.applyFilter(this.value);
  }

  constructor(public dialog: MatDialog, private router: Router, public generalService: GeneralService) {

    this.CargarTabla();
  }


  CargarTabla() {
    this.generalService.Obtenertodos('vista_sugerencia').then((result) => {
      this.resultado = result;
      sugerencias = [];
      for (let i = 0; i < this.resultado.data.length; i++) {
        if (this.resultado.data[i].estatus=='P') 
        {
        let est = this.Estatus(this.resultado.data[i].estatus);
        this.datos = {
          cliente_cedula: this.resultado.data[i].cliente_cedula,
          fecha: this.resultado.data[i].fecha,
          descripcion: this.resultado.data[i].descripcion,
          cliente_apellido: this.resultado.data[i].cliente_apellido,
          cliente_nombre: this.resultado.data[i].cliente_nombre,
          id: this.resultado.data[i].sugerencia_id,

        };
        sugerencias.push(this.datos)
        console.log(this.datos.id);        
      }
    }
      this.dataSource = new MatTableDataSource(sugerencias);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, (err) => {
      console.log(err);
    });

  }

  Estatus(est) {
    let estatus;
    return estatus = est == 'P' ? 'Pendiente' : 'Aprobado';
  }
  inicio(){
    this.router.navigate(['DashJ']);
  }

  ngOnInit() {
  
  }

  openDialogAtenderSugerencia(row) //abre una ventana modal
  {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '600px';
    // PARA PASAR LOS DATOS AL DIALOG
    dialogConfig.data = row;

    const dialogRef = this.dialog.open(ModalEvaluarSugerenciaComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.CargarTabla();
        }
      }
    );
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
