import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EvaluarReclamoComponent } from './evaluar-reclamo/evaluar-reclamo.component';
import { ListaReclamoComponent } from './lista-reclamo/lista-reclamo.component';
import { EvaluarSugerenciaComponent} from './evaluar-sugerencia/evaluar-sugerencia.component';
import { ListaSugerenciaComponent } from './lista-sugerencia/lista-sugerencia.component';
import { AuthGuardService as AuthGuard } from './../core/Services/AuthGuard/auth-guard.service';
import { ListaCasosFinalizadosComponent } from './lista-reclamo/lista-casos-finalizados/lista-casos-finalizados.component';

const routes: Routes = [
  { path: 'evaluar_sugerencia', component: EvaluarSugerenciaComponent, canActivate: [AuthGuard]},
  { path: 'evaluar_reclamo', component: EvaluarReclamoComponent, canActivate: [AuthGuard]},
  { path: 'reclamos', component: ListaReclamoComponent, canActivate: [AuthGuard] },
  { path: 'sugerencias', component: ListaSugerenciaComponent, canActivate: [AuthGuard] },
  { path: 'casosfinalizados', component: ListaCasosFinalizadosComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AtecionClienteRoutingModule { }
