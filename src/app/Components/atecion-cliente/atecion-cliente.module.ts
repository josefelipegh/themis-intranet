import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AtecionClienteRoutingModule } from './atecion-cliente-routing.module';

import { ListaReclamoComponent } from './lista-reclamo/lista-reclamo.component';
import { SharedModule } from '../shared/shared.module';
import { ModalEvaluarReclamoComponent } from './evaluar-reclamo/modal-evaluar-reclamo/modal-evaluar-reclamo.component';
import { EvaluarReclamoComponent } from './evaluar-reclamo/evaluar-reclamo.component';
import { ListaSugerenciaComponent } from './lista-sugerencia/lista-sugerencia.component';
import { EvaluarSugerenciaComponent } from './evaluar-sugerencia/evaluar-sugerencia.component';
import { ModalEvaluarSugerenciaComponent } from './evaluar-sugerencia/modal-evaluar-sugerencia/modal-evaluar-sugerencia.component';
import { ModalReclamosComponent } from './lista-reclamo/modal-reclamos/modal-reclamos.component';
import { ModalDetalleReclamoComponent } from './lista-reclamo/modal-detalle-reclamo/modal-detalle-reclamo.component';
import { ModalSugerenciasComponent } from './lista-sugerencia/modal-sugerencias/modal-sugerencias.component';
import { ModalDetalleSugerenciaComponent } from './lista-sugerencia/modal-detalle-sugerencia/modal-detalle-sugerencia.component';
import { ModalRechazarReclamoComponent } from './evaluar-reclamo/modal-rechazar-reclamo/modal-rechazar-reclamo.component';
import { ModalAprobarReclamoComponent } from './evaluar-reclamo/modal-aprobar-reclamo/modal-aprobar-reclamo.component';
import { ListaCasosFinalizadosComponent } from './lista-reclamo/lista-casos-finalizados/lista-casos-finalizados.component';
import { ModalRealizarReclamoComponent } from './lista-reclamo/lista-casos-finalizados/modal-realizar-reclamo/modal-realizar-reclamo.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AtecionClienteRoutingModule
  ],
  declarations: [
    ListaReclamoComponent,
    ModalEvaluarReclamoComponent,
    EvaluarReclamoComponent,
    ListaSugerenciaComponent,
    EvaluarSugerenciaComponent,
    ModalEvaluarSugerenciaComponent,
    ModalReclamosComponent,
    ModalDetalleReclamoComponent,
    ModalSugerenciasComponent,
    ModalDetalleSugerenciaComponent,
    ModalRechazarReclamoComponent,
    ModalAprobarReclamoComponent,
    ListaCasosFinalizadosComponent,
    ModalRealizarReclamoComponent
  ],
  entryComponents: [
    ModalEvaluarReclamoComponent,
    ModalEvaluarSugerenciaComponent,
    ModalDetalleReclamoComponent,
    ModalDetalleSugerenciaComponent,
    ModalSugerenciasComponent,
    ModalReclamosComponent,
    ModalRechazarReclamoComponent,
    ModalAprobarReclamoComponent,
    ListaCasosFinalizadosComponent,
    ModalRealizarReclamoComponent

    
    
  ]
})
export class AtecionClienteModule { }
