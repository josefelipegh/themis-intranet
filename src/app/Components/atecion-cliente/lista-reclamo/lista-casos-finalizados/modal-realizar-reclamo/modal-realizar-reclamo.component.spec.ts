import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalRealizarReclamoComponent } from './modal-realizar-reclamo.component';

describe('ModalRealizarReclamoComponent', () => {
  let component: ModalRealizarReclamoComponent;
  let fixture: ComponentFixture<ModalRealizarReclamoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalRealizarReclamoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalRealizarReclamoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
