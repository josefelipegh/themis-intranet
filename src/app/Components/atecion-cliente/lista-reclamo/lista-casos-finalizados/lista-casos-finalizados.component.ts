import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Router } from '@angular/router';
import { GeneralService } from '../../../core/Services/general/general.service';
import { Location } from '@angular/common';
import * as moment from 'moment';
import { CasoService } from '../../../core/Services/caso/caso.service';
import { ModalRealizarReclamoComponent } from './modal-realizar-reclamo/modal-realizar-reclamo.component';



export interface DetalleCasosClienteData {
  idServicio: any;
  idCliente: any;
  cedulaCliente: any;
  nombreCliente: any;
  apellidoCliente: any;
  idAbogado: any;
  cedulaAbogado:any;
  apellidoAbogado: any;
  nombreAbogado:any;
  tiposervicio: any;
  fechaInicio: any;
  estatus: any;
}
@Component({
  selector: 'app-lista-casos-finalizados',
  templateUrl: './lista-casos-finalizados.component.html',
  styleUrls: ['./lista-casos-finalizados.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})


export class ListaCasosFinalizadosComponent implements OnInit {

 // Atributos usados para cargar servicios vistos por el Cliente
 casosCliente: any = [];
 resultadoCliente: any;
 datoscliente: DetalleCasosClienteData;
 idcliente: any;

 displayedColumns: string[] = [
 'idServicio', 
 'cedulaAbogado', 
 'nombreAbogado',
 'tiposervicio', 
 'fechaInicio',
 'ver'];

 dataSource: MatTableDataSource<DetalleCasosClienteData>;

 @ViewChild(MatPaginator) paginator: MatPaginator;
 @ViewChild(MatSort) sort: MatSort;

 value = '';
 valueInput() {
   this.value = '';
   this.applyFilter(this.value);
 }

 constructor(
   public dialog: MatDialog, 
   private router: Router,
   public generalService: GeneralService,
   private location: Location,
   public casoService: CasoService) {
   // carga la tabla de los servicios activos por rol cliente
   this.CargarTablaCliente();
   }

   // Carga la tabla de casos Activos, unicamente del cliente que esta logueado.
 CargarTablaCliente() {
   let idusuario = Number.parseInt(localStorage.getItem('id'));
   this.generalService.ObtenerUno('cliente/usuario',idusuario).then((result) => {
     this.idcliente = result;
     this.generalService.Obtenertodos('vista_servicio/cliente/'+this.idcliente.data.id+'/estatus/F').then((result) => {
       this.resultadoCliente = result;
       this.casosCliente = [];
       for (let i = 0; i < this.resultadoCliente.data.length; i++) {
        const est = this.Estatus(this.resultadoCliente.data[i].estatus); 
        if (this.resultadoCliente.data[i].estatus == 'F'){
         this.datoscliente = {
           idServicio: this.resultadoCliente.data[i].servicio_id,
           idAbogado: this.resultadoCliente.data[i].abogado_id,
           cedulaAbogado: this.resultadoCliente.data[i].abogado_cedula,
           nombreAbogado: this.resultadoCliente.data[i].abogado_nombre,
           apellidoAbogado: this.resultadoCliente.data[i].abogado_nombre+' '+this.resultadoCliente.data[i].abogado_apellido,
           idCliente: this.resultadoCliente.data[i].cliente_id,
           cedulaCliente: this.resultadoCliente.data[i].cliente_cedula,
           nombreCliente: this.resultadoCliente.data[i].cliente_nombre,
           apellidoCliente: this.resultadoCliente.data[i].cliente_apellido,
           tiposervicio: this.resultadoCliente.data[i].tipo_servicio,
           fechaInicio: moment(this.resultadoCliente.data[i].fecha_creado,['MM-DD-YYYY']).format('DD-MM-YYYY'),
           estatus: est,
         };
         // se le asigna al array casos todos los datos obtenidos
         this.casosCliente.push(this.datoscliente);
       }
      }
       this.dataSource = new MatTableDataSource(this.casosCliente);
       this.dataSource.paginator = this.paginator;
       this.dataSource.sort = this.sort;
     }, (err) => {
       console.log(err);
     });
   }, (err) => {
     console.log(err);
   });

 }

 
 openRealizarReclamo(row) // abre una ventana modal
 {
  const dialogConfig = new MatDialogConfig();

  dialogConfig.disableClose = true;
  dialogConfig.autoFocus = true;
  dialogConfig.width = '80%';
// aqui mandamos los datos al modal del caso cerrado
  dialogConfig.data= row;

  const dialogRef = this.dialog.open(ModalRealizarReclamoComponent, dialogConfig);
   // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD


}

 ngOnInit() {
 }

 // METODO QUE RETORNA ACTIVO O INACTIVO PARA MOSTRAR EN LA TABLA
 Estatus(est) {
   let estatus;
   return estatus = est === 'A' ? 'Activo' : 'Inactivo';
 }

 applyFilter(filterValue: string) {
   this.dataSource.filter = filterValue.trim().toLowerCase();

   if (this.dataSource.paginator) {
     this.dataSource.paginator.firstPage();
   }
 }

 inicio(){
  this.router.navigate(['reclamos']);
}

}
