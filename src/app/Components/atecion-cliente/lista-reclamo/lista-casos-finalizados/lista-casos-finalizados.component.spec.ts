import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaCasosFinalizadosComponent } from './lista-casos-finalizados.component';

describe('ListaCasosFinalizadosComponent', () => {
  let component: ListaCasosFinalizadosComponent;
  let fixture: ComponentFixture<ListaCasosFinalizadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaCasosFinalizadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaCasosFinalizadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
