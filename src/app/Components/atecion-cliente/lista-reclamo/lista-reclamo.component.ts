import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
// import { MensajedialogComponent } from '../mensajedialog/mensajedialog.component';
import { ModalDetalleReclamoComponent } from './modal-detalle-reclamo/modal-detalle-reclamo.component';
import { Router } from '@angular/router';
import { GeneralService } from '../../core/Services/general/general.service';
import { ClassGetter } from '@angular/compiler/src/output/output_ast';
import { ListaCasosFinalizadosComponent } from './lista-casos-finalizados/lista-casos-finalizados.component';




export interface ReclamoData {
  tipo_reclamo: string;
  descripcion:string;
  estatus:string;
  fecha: string;
  cliente_cedula: string;
  cliente_nombre: string;
  cliente_apellido: string;
  color: string;
  ver: string;
}

let reclamos: any[] = [];

@Component({
  selector: 'app-lista-reclamo',
  templateUrl: './lista-reclamo.component.html',
  styleUrls: ['./lista-reclamo.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ListaReclamoComponent implements OnInit {
  resultado: any;
  displayedColumns: string[] = ['ver','tipo_reclamo', 'fecha', 'estatus'];
  dataSource: MatTableDataSource<ReclamoData>;
  datos: ReclamoData;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  value="";
  valueInput(){
    this.value="";
    this.applyFilter(this.value);
  }

  constructor(public generalService: GeneralService, public dialog: MatDialog, private router: Router)
   {
    this.CargarTabla();
   }

   ngOnInit() {

  }

  
  openDialog() // abre una ventana modal
   {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '80%';

    const dialogRef = this.dialog.open(ListaCasosFinalizadosComponent, { width: '80%'});
     // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD
     dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          if(data){
            this.CargarTabla();
          }
        }
      }
    );

  }
 

  // openDialog2(){
  //   this.dialog.open(MensajedialogComponent, { width:'500px'})
  // }

  inicio(){
    this.router.navigate(['DashJ']);
  }

  
  ListaCasosFinalizados(){
    this.router.navigate(['casosfinalizados']);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  Estatus(est) {
    let estatus;
    if(est == 'A'){
      estatus = 'Aprobado';
    }else if(est == 'R'){
      estatus = 'Rechazado';
    }else{
      estatus = 'Pendiente';
    }
    return estatus;
  }

  CargarTabla()
  {
    reclamos = [];
    let client : any;
    this.generalService.ObtenerUno('cliente/usuario',Number.parseInt(localStorage.getItem('id'))).then((result) => {
      client = result;
      console.log(client);
      console.log(client.data.id);
      this.generalService.ObtenerUno('vista_reclamo/user',Number.parseInt(client.data.id)).then((result) => {
        this.resultado = result;
        console.log(this.resultado);
        


        for (let i = 0; i < this.resultado.data.length; i++) {
        
                if (this.resultado.data[i].estatus != 'I') {
        
                  let est = this.Estatus(this.resultado.data[i].estatus);
                  let colorEst = this.resultado.data[i].estatus;
                  let color;
                  let ver;
        
                  switch (colorEst) {
                    case 'A':
                      color = 'green';
                      ver = 'check_circle';
                      break;
                    case 'R':
                      color = 'red';
                      ver = 'cancel';
                      break;
                    case 'P':
                      color = '#cd9d12';
                      ver = 'error';
                      break;
                  }
                  
          console.log(est);
          this.datos = {
            tipo_reclamo: this.resultado.data[i].tipo_reclamo,
            descripcion: this.resultado.data[i].descripcion,
            fecha: this.resultado.data[i].fecha,
            cliente_apellido: this.resultado.data[i].cliente_apellido,
            cliente_cedula: this.resultado.data[i].cliente_cedula,
            cliente_nombre: this.resultado.data[i].cliente_nombre,
            estatus: est,
            color:color,
            ver: ver,
          };
          
          reclamos.push(this.datos);
        }
      }
      console.log(reclamos)
        this.dataSource = new MatTableDataSource(reclamos);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }, (err) => {
        console.log(err);
      });
  

    }, (err) => {
      console.log(err);
    });

    
  }


  

  openDialogDetalle(row) //abre una ventana modal
  {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '600px';
    // PARA PASAR LOS DATOS AL DIALOG
    dialogConfig.data = row;

    const dialogRef = this.dialog.open(ModalDetalleReclamoComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD
    dialogRef.afterClosed().subscribe(
      data => {
        
          this.CargarTabla();
      }
    );
  }

}

