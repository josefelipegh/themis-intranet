import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { MatDialogRef } from '@angular/material';
import {MatSnackBar} from '@angular/material';
import { Router } from '@angular/router';
import { GeneralService } from '../../../core/Services/general/general.service';

@Component({
  selector: 'app-modal-reclamos',
  templateUrl: './modal-reclamos.component.html',
  styleUrls: ['./modal-reclamos.component.scss']
})
export class ModalReclamosComponent implements OnInit {
  form_descripcion = { cliente_id: '', fecha_creacion: '', descripcion: '', tipo_reclamo_id: '', estatus: 'P'};
  resultado: any;
  listaTipoReclamo: any[] = [];
  filteredOptionsReclamo: Observable<string[]>;
  public loading = false;
  fecha = new Date();
  date;
  idreclam;
  constructor(public dialogRef: MatDialogRef<ModalReclamosComponent>, public snackBar: MatSnackBar, public generalService: GeneralService,private router: Router) {

    this.CargarCombo();
    this.date = (this.fecha.getDate()+'/'+this.fecha.getMonth()+'/'+this.fecha.getFullYear());
      console.log(this.date);
   }

   tipoReclamoFormControl = new FormControl('',
   [
     Validators.required
   ]);

   descripcionFormControl = new FormControl('', [
    Validators.required
  ]);

   CargarCombo() {
    this.generalService.Obtenertodos('tipo_reclamo').then((result) => {

      this.resultado = result;
      console.log(result);
      for (let i = 0; i < this.resultado.data.length; i++) {
        this.listaTipoReclamo.push(this.resultado.data[i]);
      }
      console.log(this.listaTipoReclamo);
    }, (err) => {
      console.log(err);
    });
  }




  Registrar() {

    if (this.descripcionFormControl.valid) {
      let datos;
      this.generalService.ObtenerUno('cliente/usuario',Number.parseInt(localStorage.getItem('id'))).then((result) => {
        datos = result;
        this.form_descripcion.fecha_creacion = ((this.fecha.getMonth()+1) + '-'+ this.fecha.getDate() + '-' + this.fecha.getFullYear());
        this.form_descripcion.descripcion = this.descripcionFormControl.value;
        this.form_descripcion.cliente_id = datos.data.id;
        this.form_descripcion.tipo_reclamo_id = this.tipoReclamoFormControl.value;
        console.log(this.form_descripcion);
        
        this.loading = true;
        this.generalService.Registrar(this.form_descripcion, 'reclamo').then((result) => {
          this.openSnackBar('Reclamo registrado exitosamente!', "Registro exitoso");
          this.loading = false;
          this.dialogRef.close(this.form_descripcion);
        }, (err) => {
          console.log(err);
          this.loading = false;
        });
      }, (err) => {
        console.log(err);
      });

    }
  }

  ngOnInit() {
  }
  ObtenerIdReclamo(id) {
    return this.idreclam = id;

  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }
  onNoClick(){
    this.dialogRef.close();
  }


 
}
