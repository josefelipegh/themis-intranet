import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalReclamosComponent } from './modal-reclamos.component';

describe('ModalReclamosComponent', () => {
  let component: ModalReclamosComponent;
  let fixture: ComponentFixture<ModalReclamosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalReclamosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalReclamosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
