import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { GeneralService } from '../../core/Services/general/general.service';

@Component({
  selector: 'app-modal-pregunta-eliminar',
  templateUrl: './modal-pregunta-eliminar.component.html',
  styleUrls: ['./modal-pregunta-eliminar.component.scss']
})
export class ModalPreguntaEliminarComponent implements OnInit {
  id;
  nombreMetodo;
  constructor(public dialogRef: MatDialogRef<ModalPreguntaEliminarComponent>,
    public generalService: GeneralService, public snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.id = data.id;
    this.nombreMetodo = data.nombreMetodo;
  }

  ngOnInit() {
  }

  // CERRAR EL MODAL
  onNoClick() {
    this.dialogRef.close()
  }

  // ELIMINAR ENTIDAD LOGICAMENTE EN LA BD
  Eliminar() {
    this.generalService.Eliminar(this.nombreMetodo, this.id).then((result) => {
      this.openSnackBar('Registro Elimidado!', "Eliminar");
      this.dialogRef.close(this.id);
    }, (err) => {
      console.log(err);
    });
  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }

}
