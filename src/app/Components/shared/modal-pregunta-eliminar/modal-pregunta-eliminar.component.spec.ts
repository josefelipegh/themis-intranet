import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalPreguntaEliminarComponent } from './modal-pregunta-eliminar.component';

describe('ModalPreguntaEliminarComponent', () => {
  let component: ModalPreguntaEliminarComponent;
  let fixture: ComponentFixture<ModalPreguntaEliminarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalPreguntaEliminarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalPreguntaEliminarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
