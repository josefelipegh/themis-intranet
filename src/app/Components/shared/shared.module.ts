import 'hammerjs';
import 'flatpickr/dist/flatpickr.css';
import { NgModule } from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import { AngularMaterialModule } from './angular-material-module';
import { LayoutModule } from '@angular/cdk/layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ChartsModule } from 'ng2-charts';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import localeEs from '@angular/common/locales/es';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { AngularFileUploaderModule } from 'angular-file-uploader';
import { VerticalTimelineModule } from 'angular-vertical-timeline';
import { DragAndDropModule } from 'angular-draggable-droppable';
import { MglTimelineModule } from 'angular-mgl-timeline';
import { FlatpickrModule} from 'angularx-flatpickr';
import { HttpClientModule } from '@angular/common/http';
import { NgxLoadingModule } from 'ngx-loading';
import { AmazingTimePickerModule } from 'amazing-time-picker';
import { OwlDateTimeModule, OwlNativeDateTimeModule, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { OwlColorPickerModule } from 'owl-ng';
import { ModalPreguntaEliminarComponent } from './modal-pregunta-eliminar/modal-pregunta-eliminar.component';
import { MomentModule } from 'angular2-moment';
registerLocaleData(localeEs);

@NgModule({
  imports: [
    CommonModule,
    AngularMaterialModule,
    LayoutModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    HttpClientModule,
    AmazingTimePickerModule,
    ChartsModule,
    NgxLoadingModule.forRoot({}),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    NgbModule,
    NgxMaterialTimepickerModule.forRoot(),
    AngularFileUploaderModule,
    VerticalTimelineModule,
    DragAndDropModule,
    FlatpickrModule.forRoot(),
    MglTimelineModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    OwlColorPickerModule,
    MomentModule
  ],
  declarations: [
    ModalPreguntaEliminarComponent
  ],
  exports: [
    AngularMaterialModule,
    LayoutModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    ChartsModule,
    CalendarModule,
    NgbModule,
    NgxMaterialTimepickerModule,
    AngularFileUploaderModule,
    VerticalTimelineModule,
    DragAndDropModule,
    FlatpickrModule,
    MglTimelineModule,
    NgxLoadingModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    AmazingTimePickerModule,
    OwlColorPickerModule,
    ModalPreguntaEliminarComponent,
    MomentModule
  ],
  providers: [
    // use french locale
    {provide: OWL_DATE_TIME_LOCALE, useValue: 'es'},
  ],
  entryComponents: [
    ModalPreguntaEliminarComponent
  ]
})
export class SharedModule { }
