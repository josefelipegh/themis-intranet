import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RespaldoBDComponent } from './respaldo-bd.component';

describe('RespaldoBDComponent', () => {
  let component: RespaldoBDComponent;
  let fixture: ComponentFixture<RespaldoBDComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RespaldoBDComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RespaldoBDComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
