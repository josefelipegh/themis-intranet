import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Location } from '@angular/common';
import { GeneralService } from '../../../core/Services/general/general.service';
import { ModalEliminarComponent } from '../modal-eliminar/modal-eliminar.component';
// Atributos del objeto TablaData donde el array TipoActuacion esperara esos parametros
export interface TablaData {
  id: number;
  nombre: string;
  descripcion: string;
  color1: string;
  color2: string;
  estatus: string;
}
// Array declarado
let TipoActuacion: any[] = [];

// Component donde se crean los metadatos para ejecutarlos en el html. Yiii
@Component({
  selector: 'app-lista-tipo-actuacion',
  templateUrl: './lista-tipo-actuacion.component.html',
  styleUrls: ['./lista-tipo-actuacion.component.scss'],

  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],

})

export class ListaTipoActuacion2Component implements OnInit {
  displayedColumns: string[] = ['nombre', 'eliminar'];
  dataSource: MatTableDataSource<TablaData>;
  resultado: any;
  // OBJETO PARA CARGAR EN LA TABLA
  datos: TablaData;
  // paginacion
  @ViewChild(MatPaginator) paginator: MatPaginator;
  // ordenar resultados
  @ViewChild(MatSort) sort: MatSort;
  value = '';
  // filtrar y limpiar en tabla
  valueInput() {
    this.value = '';
    this.applyFilter(this.value);
  }
  // contructor donde hace mencion a la funcion CargarTabla que trae los datos de la API y db
  constructor(public dialog: MatDialog, public generalService: GeneralService,
    private location: Location) {
    this.CargarTabla();
  }
// esto no se toca. Yii
   ngOnInit() {

  }


  // PARA ABRIR MODAL PREGUNTAR ESTÁ SEGURO DE ELIMNIAR ESTE MODAL ES GENERICO USEN EL QUE YA ESTA CREADO
  openDialogEliminar(row) 
  {
    // constante creada para un nuevo modal.
    const dialogConfig = new MatDialogConfig();
    // configuracion del modal con sus caracteristicas, ancho...
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    // PARA PASAR LOS DATOS AL DIALOG PREGUNTA ELIMINAR 
    dialogConfig.data = {
      id: row,
      nombreMetodo: 'tipo_actuacion'
    };
    // constante donde se guardan los datos que se ingresan en el modal, los parametros
    // ModalTipoActuacionComponent hace el llamado al componente modal y dialogConfig
    // las caracteristicas del mismo.
    const dialogRef = this.dialog.open(ModalEliminarComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD 
    dialogRef.afterClosed().subscribe(
      data => {
        if(data){
        this.dataSource.data.find(x => x.id == data).estatus = 'Inactivo';
        }
      }
    );
  }
  
  // filtrar busquedas en la tabla y limpiar la pagina
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  
   // METEDO QUE OBTIENE UNA LISTA DE TipoActuacion Y SE CARGA EN LA TABLA, donde
   // se le hace un llamado de generalService donde esta el servicio y colocas 
   // la ultima directiva ejemplo "tipo_actuacion" que te lleva la direccion del
   // servicio en especifico, para obtener los resultados.
   CargarTabla() {
    this.generalService.Obtenertodos('tipo_actuacion').then((result) => {
      this.resultado = result;
      TipoActuacion = [];
      for (let i = 0; i < this.resultado.data.length; i++) {
       let est = this.Estatus(this.resultado.data[i].estatus); 
        this.datos = {
          id: this.resultado.data[i].id,
          nombre: this.resultado.data[i].nombre,
          descripcion: this.resultado.data[i].descripcion,
          color1: this.resultado.data[i].color1,
          color2: this.resultado.data[i].color2,
          estatus: est,
        };
        // se le asigna al array TipoActuacion todos los datos obtenidos
        TipoActuacion.push(this.datos);
      }
      this.dataSource = new MatTableDataSource(TipoActuacion);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, (err) => {
      console.log(err);
    });

  }

  // METODO QUE RETORNA ACTIVO O INACTIVO PARA MOSTRAR EN LA TABLA
  Estatus(est) {
    let estatus;
    return estatus = est == 'A' ? 'Activo' : 'Inactivo';
  }
  volver() {
    this.location.back();
  }
}
