import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Location } from '@angular/common';
import { UrlService } from '../../../core/Services/url/url.service';
import { GeneralService } from '../../../core/Services/general/general.service';
import { ModalEliminarComponent } from '../modal-eliminar/modal-eliminar.component';

// INTERFACE PARA CREAR UN OBJETO DE LA TABLA
export interface TablaData {
  id: number;
  nombre: string;
  descripcion: string;
  imagen: string;
  estatus: string;
  especialidad_id: number;
}

let categoria: any[] = [];

@Component({
  selector: 'app-lista-categoria',
  templateUrl: './lista-categoria.component.html',
  styleUrls: ['./lista-categoria.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})

export class ListaCategoria2Component implements OnInit {

  urlimagen; //varaible que almacena el url base de la imagen
  displayedColumns: string[] = ['imagen', 'nombre', 'eliminar']; //para mostrar columnas en la tabla
  dataSource: MatTableDataSource<TablaData>;
  resultado: any;
  // OBJETO PARA CARGAR EN LA TABLA
  datos: TablaData;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  value = '';

  valueInput() {
    this.value = '';
    this.applyFilter(this.value);
  }

  constructor(public dialog: MatDialog, public generalService: GeneralService, private location: Location) {
    this.CargarTabla();
    this.urlimagen = UrlService.imagenUrl();
  }

  ngOnInit() { }

  openDialogEliminar(row) 
  {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';

    // Pasando los datos al dialog
    dialogConfig.data = {
        id: row,
        nombreMetodo: 'empleado'

      };

    const dialogRef = this.dialog.open(ModalEliminarComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          let index = this.dataSource.data.findIndex(x => x.id == data.id);
          this.dataSource.data.slice(index,1);
        }
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  CargarTabla() {
    this.generalService.Obtenertodos('categoria').then((result) => {
      this.resultado = result;
      categoria = [];
      for (let i = 0; i < this.resultado.data.length; i++) {
        let est = this.Estatus(this.resultado.data[i].estatus);
        this.datos = {
          id: this.resultado.data[i].id,
          imagen: this.resultado.data[i].imagen,
          nombre: this.resultado.data[i].nombre,
          descripcion: this.resultado.data[i].descripcion,
          especialidad_id: this.resultado.data[i].especialidad_id,
          estatus: est,
        };
        categoria.push(this.datos);
      }
      this.dataSource = new MatTableDataSource(categoria);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, (err) => {
      console.log(err);
    });

  }

  // METODO QUE RETORNA ACTIVO O INACTIVO PARA MOSTRAR EN LA TABLA
  Estatus(est) {
    let estatus;
    return estatus = est == 'A' ? 'Activo' : 'Inactivo';
  }
  volver() {
    this.location.back();
  }


}

