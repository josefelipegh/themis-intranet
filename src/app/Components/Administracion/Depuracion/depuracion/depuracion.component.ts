import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-depuracion',
  templateUrl: './depuracion.component.html',
  styleUrls: ['./depuracion.component.scss']
})
export class DepuracionComponent implements OnInit {

  constructor(private router: Router) { }

  goToPage(pageName: string) {
    this.router.navigate([`${pageName}`]);
  }
  ngOnInit() {
  }

}
