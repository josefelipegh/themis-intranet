import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaCaracteristicaComponent } from './lista-caracteristica.component';

describe('ListaCaracteristicaComponent', () => {
  let component: ListaCaracteristicaComponent;
  let fixture: ComponentFixture<ListaCaracteristicaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaCaracteristicaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaCaracteristicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
