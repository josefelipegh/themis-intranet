import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Location } from '@angular/common';
import { GeneralService } from '../../../core/Services/general/general.service';
import { ModalEliminarComponent } from '../modal-eliminar/modal-eliminar.component'

export interface TipoEmpleadoData {
  id: number;
  nombre: string;
  estatus:string;
}

let tipoEmpleado: any[] = [];

@Component({
  selector: 'app-lista-tipo-empleado',
  templateUrl: './lista-tipo-empleado.component.html',
  styleUrls: ['./lista-tipo-empleado.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ListaTipoEmpleado2Component implements OnInit {
  resultado: any;
  displayedColumns: string[] = ['nombre','eliminar'];
  dataSource: MatTableDataSource<TipoEmpleadoData>;
   // OBJETO PARA CARGAR EN LA TABLA
   datos: TipoEmpleadoData;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  value="";
  valueInput(){
    this.value="";
    this.applyFilter(this.value);
  }

  constructor(public dialog: MatDialog, public generalService: GeneralService, private location:Location)
   {
     // Assign the data to the data source for the table to render
     this.CargarTabla();
   }

   ngOnInit() {

  }

  // PARA ABRIR MODAL PREGUNTAR ESTÁ SEGURO DE ELIMNIAR ESTE MODAL ES GENERICO USEN EL QUE YA ESTA CREADO
  openDialogEliminar(row) 
  {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    // PARA PASAR LOS DATOS AL DIALOG PREGUNTA ELIMINAR 
    dialogConfig.data = {
      id: row,
      nombreMetodo: 'tipo_empleado'
    };

    const dialogRef = this.dialog.open(ModalEliminarComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD 
    dialogRef.afterClosed().subscribe(
      data => {
        if(data){
          this.dataSource.data.find(x => x.id == data).estatus = 'Inactivo';
        }
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  
  CargarTabla()
  {
    this.generalService.Obtenertodos('tipo_empleado').then((result) => {
      this.resultado = result;
      tipoEmpleado = [];
      for (let i = 0; i < this.resultado.data.length; i++) {
        let est = this.Estatus(this.resultado.data[i].estatus);
        this.datos = {
          id: this.resultado.data[i].id,
          nombre: this.resultado.data[i].nombre,
          estatus: est,
        };
        tipoEmpleado.push(this.datos);
      }
      this.dataSource = new MatTableDataSource(tipoEmpleado);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, (err) => {
      console.log(err);
    });

  }

   // METODO QUE RETORNA ACTIVO O INACTIVO PARA MOSTRAR EN LA TABLA
   Estatus(est) {
    let estatus;
    return estatus = est == 'A' ? 'Activo' : 'Inactivo';
  }
  volver() {
    this.location.back();
  }

}
