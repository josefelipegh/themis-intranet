import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaTipoGarantiaComponent } from './lista-tipo-garantia.component';

describe('ListaTipoGarantiaComponent', () => {
  let component: ListaTipoGarantiaComponent;
  let fixture: ComponentFixture<ListaTipoGarantiaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaTipoGarantiaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaTipoGarantiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
