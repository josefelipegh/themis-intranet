import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { GeneralService } from '../../../core/Services/general/general.service';
import { ModalEliminarComponent } from '../modal-eliminar/modal-eliminar.component';

 
export interface DiaData {
  id: string;
  nombre: string;
  estatus:string;
}

let dia: any[] = [];

@Component({
  selector: 'app-lista-dia-semana',
  templateUrl: './lista-dia-semana.component.html',
  styleUrls: ['./lista-dia-semana.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ListaDiaSemana2Component implements OnInit {

  displayedColumns: string[] = ['nombre',  'eliminar'];
  dataSource: MatTableDataSource<DiaData>;
  resultado: any;
  datos: DiaData;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  value="";
  valueInput(){
    this.value="";
    this.applyFilter(this.value);
  }

  constructor(public dialog: MatDialog,
              public generalService: GeneralService)
   {
     // Assign the data to the data source for the table to render
     this.CargarTabla();
   }

   ngOnInit() {

  }

  
  CargarTabla() {
    this.generalService.Obtenertodos('dia_semana').then((result) => {
      this.resultado = result;
      dia = [];
      for (let i = 0; i < this.resultado.data.length; i++) {
        let est = this.Estatus(this.resultado.data[i].estatus);
        this.datos = {
          id: this.resultado.data[i].id,
          nombre: this.resultado.data[i].nombre,
          estatus: est,
        };
        dia.push(this.datos);
      }
      this.dataSource = new MatTableDataSource(dia);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, (err) => {
      console.log(err);
    });

  }
  Estatus(est) {
    let estatus;
    return estatus = est == 'A' ? 'Activo' : 'Inactivo';
  }

    openDialogEliminar(row) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    // PARA PASAR LOS DATOS AL DIALOG PREGUNTA ELIMINAR
    dialogConfig.data = {
      id: row,
      nombreMetodo: 'dia_semana'
    };

    const dialogRef = this.dialog.open(ModalEliminarComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE ELIMINA EN LA BD
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.dataSource.data.find(x => x.id == data).estatus = 'Inactivo';
        }
      }
    );
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
