import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaTipoClienteComponent } from './lista-tipo-cliente.component';

describe('ListaTipoClienteComponent', () => {
  let component: ListaTipoClienteComponent;
  let fixture: ComponentFixture<ListaTipoClienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaTipoClienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaTipoClienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
