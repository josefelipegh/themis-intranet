import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaTipoValoracionComponent } from './lista-tipo-valoracion.component';

describe('ListaTipoValoracionComponent', () => {
  let component: ListaTipoValoracionComponent;
  let fixture: ComponentFixture<ListaTipoValoracionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaTipoValoracionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaTipoValoracionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
