import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaTipoReclamoComponent } from './lista-tipo-reclamo.component';

describe('ListaTipoReclamoComponent', () => {
  let component: ListaTipoReclamoComponent;
  let fixture: ComponentFixture<ListaTipoReclamoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaTipoReclamoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaTipoReclamoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
