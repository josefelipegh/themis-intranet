import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaActuacionComponent } from './lista-actuacion.component';

describe('ListaActuacionComponent', () => {
  let component: ListaActuacionComponent;
  let fixture: ComponentFixture<ListaActuacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaActuacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaActuacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
