import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaRedSocialComponent } from './lista-red-social.component';

describe('ListaRedSocialComponent', () => {
  let component: ListaRedSocialComponent;
  let fixture: ComponentFixture<ListaRedSocialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaRedSocialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaRedSocialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
