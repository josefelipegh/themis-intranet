import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaTipoIncidenciaComponent } from './lista-tipo-incidencia.component';

describe('ListaTipoIncidenciaComponent', () => {
  let component: ListaTipoIncidenciaComponent;
  let fixture: ComponentFixture<ListaTipoIncidenciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaTipoIncidenciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaTipoIncidenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
