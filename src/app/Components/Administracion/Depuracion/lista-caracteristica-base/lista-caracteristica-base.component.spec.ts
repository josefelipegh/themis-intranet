import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaCaracteristicaBaseComponent } from './lista-caracteristica-base.component';

describe('ListaCaracteristicaBaseComponent', () => {
  let component: ListaCaracteristicaBaseComponent;
  let fixture: ComponentFixture<ListaCaracteristicaBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaCaracteristicaBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaCaracteristicaBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
