import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Location } from '@angular/common';
import { GeneralService } from '../../../core/Services/general/general.service';
import { ModalEliminarComponent } from '../modal-eliminar/modal-eliminar.component';

export interface TablaData {
  id: number;
  nombre: string;
  descripcion: string;
  estatus: string;
}

let caracteristicabase: any[] = [];

@Component({
  selector: 'app-lista-caracteristica-base',
  templateUrl: './lista-caracteristica-base.component.html',
  styleUrls: ['./lista-caracteristica-base.component.scss'],

  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ListaCaracteristicaBase2Component implements OnInit {

  displayedColumns: string[] = ['nombre', 'eliminar'];
  dataSource: MatTableDataSource<TablaData>;
  resultado: any;
  // OBJETO PARA CARGAR EN LA TABLA
  datos: TablaData;


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  value = '';
  valueInput() {
    this.value = '';
    this.applyFilter(this.value);
  }
  constructor(public dialog: MatDialog, public generalService: GeneralService,
    private location: Location)
   {
    this.CargarTabla();
   }

    openDialogEliminar(row) 
  {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    // PARA PASAR LOS DATOS AL DIALOG PREGUNTA ELIMINAR 
    dialogConfig.data = {
      id: row,
      nombreMetodo: 'caracteristica_base'
    };
    
    const dialogRef = this.dialog.open(ModalEliminarComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD 
    dialogRef.afterClosed().subscribe(
      data => {
        if(data){
          this.dataSource.data.find(x => x.id == data).estatus = 'Inactivo';
        }
      }
    );
  }


  CargarTabla() {
    this.generalService.Obtenertodos('caracteristica_base').then((result) => {
      this.resultado = result;
      caracteristicabase = [];
      for (let i = 0; i < this.resultado.data.length; i++) {
        let est = this.Estatus(this.resultado.data[i].estatus);
        this.datos = {
          id: this.resultado.data[i].id,
          nombre: this.resultado.data[i].nombre,
          descripcion: this.resultado.data[i].descripcion,
          estatus: est,
        };
        caracteristicabase.push(this.datos);
      }
      this.dataSource = new MatTableDataSource(caracteristicabase);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, (err) => {
      console.log(err);
    });

  }

  Estatus(est) {
    let estatus;
    return estatus = est == 'A' ? 'Activo' : 'Inactivo';
  }
  volver() {
    this.location.back();
  }

   ngOnInit() {
    //this.dataSource.paginator = this.paginator;
  //  this.dataSource.sort = this.sort;
   // this.Obtener();
  }

  
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


}
