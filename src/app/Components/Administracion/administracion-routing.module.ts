import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BitacoraComponent } from './bitacora/bitacora.component';
import { DepuracionComponent } from './Depuracion/depuracion/depuracion.component'
import { RespaldoBDComponent } from './respaldo-bd/respaldo-bd.component';
import { SesionComponent } from './sesion/sesion.component';
import { AuthGuardService as AuthGuard } from './../core/Services/AuthGuard/auth-guard.service';
import { ListaCategoria2Component } from './Depuracion/lista-categoria/lista-categoria2.component'
import { ListaActuacion2Component } from './Depuracion/lista-actuacion/lista-actuacion2.component';
import { ListaEspecialidad2Component } from './Depuracion/lista-especialidad/lista-especialidad2.component';
import { ListaTipoIncidencia2Component } from './Depuracion/lista-tipo-incidencia/lista-tipo-incidencia2.component';
import { ListaTipoEmpleado2Component } from './Depuracion/lista-tipo-empleado/lista-tipo-empleado2.component';
import { ListaBloqueHora2Component } from './Depuracion/lista-bloque-hora/lista-bloque-hora2.component';
import { ListaDiaSemana2Component } from './Depuracion/lista-dia-semana/lista-dia-semana2.component';
import { ListaRedSocial2Component } from './Depuracion/lista-red-social/lista-red-social2.component';
import { ListaRol2Component } from './Depuracion/lista-rol/crudrol2.component';
import { ListaRuta2Component } from './Depuracion/lista-ruta/lista-ruta2.component';
import { ListaFuncion2Component } from './Depuracion/lista-funcion/lista-funcion2.component';
import { ListaCaracteristicaBase2Component } from './Depuracion/lista-caracteristica-base/lista-caracteristica-base2.component';
import { ListaCaracteristica2Component } from './Depuracion/lista-caracteristica/lista-caracteristica2.component';
import { ListaTipoCliente2Component } from './Depuracion/lista-tipo-cliente/lista-tipo-cliente2.component';
import { ListaTipoActuacion2Component } from './Depuracion/lista-tipo-actuacion/lista-tipo-actuacion2.component';
import { ListaTipoDocumento2Component } from './Depuracion/lista-tipo-documento/lista-tipo-documento2.component';
import { ListaTipoValoracion2Component } from './Depuracion/lista-tipo-valoracion/lista-tipo-valoracion2.component';
import { ListaRangoValoracion2Component } from './Depuracion/lista-rango-valoracion/lista-rango-valoracion2.component';
import { ListaTipoReclamo2Component } from './Depuracion/lista-tipo-reclamo/lista-tipo-reclamo2.component';
import { ListaTipoGarantia2Component } from './Depuracion/lista-tipo-garantia/lista-tipo-garantia2.component';


const routes: Routes = [

  {path: 'bitacora', component: BitacoraComponent, canActivate: [AuthGuard]},
  {path: 'depuracion', component: DepuracionComponent, canActivate: [AuthGuard]},
  {path: 'respaldo_bd', component: RespaldoBDComponent, canActivate: [AuthGuard]},
  {path: 'sesion', component: SesionComponent, canActivate: [AuthGuard]},
  {path: 'depuracion-actuacion', component: ListaActuacion2Component},
  {path: 'depuracion-categoria', component: ListaCategoria2Component},
  {path: 'depuracion-especialidad', component: ListaEspecialidad2Component},
  {path: 'depuracion-tipo_incidencia', component: ListaTipoIncidencia2Component},
  {path: 'depuracion-tipo_empleado', component: ListaTipoEmpleado2Component},
  {path: 'depuracion-bloque_hora', component: ListaBloqueHora2Component},
  {path: 'depuracion-dia_semana', component: ListaDiaSemana2Component},
  {path: 'depuracion-red_social', component: ListaRedSocial2Component},
  {path: 'depuracion-rol', component: ListaRol2Component},
  {path: 'depuracion-ruta', component: ListaRuta2Component},
  {path: 'depuracion-funcion', component: ListaFuncion2Component},
  {path: 'depuracion-caracteristica_base', component: ListaCaracteristicaBase2Component},
  {path: 'depuracion-caracteristica', component: ListaCaracteristica2Component},
  {path: 'depuracion-tipo_cliente', component: ListaTipoCliente2Component},
  {path: 'depuracion-tipo_actuacion', component: ListaTipoActuacion2Component},
  {path: 'depuracion-tipo_documento', component: ListaTipoDocumento2Component},
  {path: 'depuracion-tipo_valoracion', component: ListaTipoValoracion2Component},
  {path: 'depuracion-rango_valoracion', component: ListaRangoValoracion2Component},
  {path: 'depuracion-tipo_reclamo', component: ListaTipoReclamo2Component},
  {path: 'depuracion-tipo_garantia', component: ListaTipoGarantia2Component},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministracionRoutingModule { }
