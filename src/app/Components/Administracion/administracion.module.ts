import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { AdministracionRoutingModule } from './administracion-routing.module';
import { SharedModule } from '../shared/shared.module';
import { BitacoraComponent } from './bitacora/bitacora.component';
import { RespaldoBDComponent } from './respaldo-bd/respaldo-bd.component';
import { SesionComponent } from './sesion/sesion.component';
import { ListaActuacion2Component } from './Depuracion/lista-actuacion/lista-actuacion2.component';
import { DepuracionComponent } from './Depuracion/depuracion/depuracion.component';
import { ListaCategoria2Component } from './Depuracion/lista-categoria/lista-categoria2.component'
import { ModalEliminarComponent } from './Depuracion/modal-eliminar/modal-eliminar.component';
import { ListaEspecialidad2Component } from './Depuracion/lista-especialidad/lista-especialidad2.component';
import { ListaTipoIncidencia2Component } from './Depuracion/lista-tipo-incidencia/lista-tipo-incidencia2.component';
import { ListaTipoEmpleado2Component } from './Depuracion/lista-tipo-empleado/lista-tipo-empleado2.component';
import { ListaBloqueHora2Component } from './Depuracion/lista-bloque-hora/lista-bloque-hora2.component';
import { ListaDiaSemana2Component } from './Depuracion/lista-dia-semana/lista-dia-semana2.component';
import { ListaRedSocial2Component } from './Depuracion/lista-red-social/lista-red-social2.component';
import { ListaRol2Component } from './Depuracion/lista-rol/crudrol2.component';
import { ListaRuta2Component } from './Depuracion/lista-ruta/lista-ruta2.component';
import { ListaFuncion2Component } from './Depuracion/lista-funcion/lista-funcion2.component';
import { ListaCaracteristicaBase2Component } from './Depuracion/lista-caracteristica-base/lista-caracteristica-base2.component'
import {ListaCaracteristica2Component} from './Depuracion/lista-caracteristica/lista-caracteristica2.component';
import { ListaTipoCliente2Component } from './Depuracion/lista-tipo-cliente/lista-tipo-cliente2.component';
import { ListaTipoActuacion2Component } from './Depuracion/lista-tipo-actuacion/lista-tipo-actuacion2.component';
import { ListaTipoDocumento2Component } from './Depuracion/lista-tipo-documento/lista-tipo-documento2.component';
import { ListaTipoValoracion2Component } from './Depuracion/lista-tipo-valoracion/lista-tipo-valoracion2.component';
import { ListaRangoValoracion2Component } from './Depuracion/lista-rango-valoracion/lista-rango-valoracion2.component';
import { ListaTipoReclamo2Component } from './Depuracion/lista-tipo-reclamo/lista-tipo-reclamo2.component';
import { ListaTipoGarantia2Component } from './Depuracion/lista-tipo-garantia/lista-tipo-garantia2.component';

@NgModule({
  imports: [
    CommonModule,
    AdministracionRoutingModule,
    SharedModule,
  ],
  declarations: [
    BitacoraComponent,
    RespaldoBDComponent,
    SesionComponent,
    DepuracionComponent,
    ModalEliminarComponent,
    ListaActuacion2Component,
    ListaCategoria2Component,
    ListaEspecialidad2Component,
    ListaTipoIncidencia2Component,
    ListaTipoEmpleado2Component,
    ListaBloqueHora2Component,
    ListaDiaSemana2Component,
    ListaRedSocial2Component,
    ListaRol2Component,
    ListaRuta2Component,
    ListaFuncion2Component,
    ListaCaracteristicaBase2Component,
    ListaCaracteristica2Component,
    ListaTipoCliente2Component,
    ListaTipoActuacion2Component,
    ListaTipoDocumento2Component,
    ListaTipoValoracion2Component,
    ListaRangoValoracion2Component,
    ListaTipoReclamo2Component,
    ListaTipoGarantia2Component,
  ],
  entryComponents: [
    ModalEliminarComponent    
  ],
  providers: [

  ]
})
export class AdministracionModule { }
