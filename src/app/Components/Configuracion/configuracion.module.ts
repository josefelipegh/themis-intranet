import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalServiciosComponent } from './confiuracion-empresa/Modal-servicios/modal-servicios.component'
import { ConfiguracionRoutingModule } from './configuracion-routing.module';
import { ConfigInfoAdicionalComponent } from './config-info-adicional/config-info-adicional.component';
import { ConfigValoracionComponent } from './config-valoracion/config-valoracion.component';
import { ConfiuracionEmpresaComponent } from './confiuracion-empresa/confiuracion-empresa.component';
import { EmpleadoComponent } from './empleado/empleado.component';
import { EmpresaComponent } from './empresa/empresa.component';
import { FuncionalidadesComponent } from './funcionalidades/funcionalidades.component';
import { HorarioEmpleadoComponent } from './horario-empleado/horario-empleado.component';
import { SharedModule } from '../shared/shared.module';
import {ListaUsuarioComponent} from './lista-usuario/lista-usuario.component';
import { PerfilClienteComponent } from './perfil-cliente/perfil-cliente.component';
import { PerfilUsuarioComponent } from './perfil-usuario/perfil-usuario.component';
import { ConfiguracionComponent } from './configuracion/configuracion.component';
import { GaleriaImagenesComponent } from './galeria-imagenes/galeria-imagenes.component';
import { HorarioAgendaComponent } from './horario-agenda/horario-agenda.component';
import { ModalHorarioEmrpesaComponent } from './horario-agenda/modal-horario-emrpesa/modal-horario-emrpesa.component';
import { ClienteComponent } from './cliente/cliente.component';
import { ColorPickerModule } from 'ngx-color-picker';
import { ModalEliminarComponent } from './modal-eliminar/modal-eliminar.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ConfiguracionRoutingModule,
    ColorPickerModule,
  ],
  declarations: [
    ConfigInfoAdicionalComponent,
    ConfigValoracionComponent,
    ConfiuracionEmpresaComponent,
    EmpleadoComponent,
    EmpresaComponent,
    FuncionalidadesComponent,
    HorarioEmpleadoComponent,
    PerfilClienteComponent,
    ListaUsuarioComponent,
    GaleriaImagenesComponent,
    ConfiguracionComponent,
    PerfilUsuarioComponent,
    HorarioAgendaComponent,
    ModalHorarioEmrpesaComponent,
    ClienteComponent,
    ModalServiciosComponent, 
    ModalEliminarComponent,
  ],
  entryComponents: [
    ModalHorarioEmrpesaComponent,ModalServiciosComponent,
    ModalEliminarComponent,
  ],
  providers: [

  ]
})
export class ConfiguracionModule { }
