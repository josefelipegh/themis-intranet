import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatSnackBar } from '@angular/material';
import * as Chart from "chart.js";
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { GeneralService } from '../../core/Services/general/general.service';
export interface Parametros {
  id: number;
  nombre: string;
  checked: boolean;
  color: string;
}
// INTERFACE PARA CREAR UN OBJETO DE LA TABLA
export interface TablaData {
  id: number;
  caracteristica: string;
  valor: string;
}
let datosEjemplo: TablaData[] = [];

@Component({
  selector: 'app-perfil-usuario',
  templateUrl: './perfil-usuario.component.html',
  styleUrls: ['./perfil-usuario.component.scss']
})
export class PerfilUsuarioComponent implements OnInit {
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  public loading = false;
  datosCliente = {
    nombre: '', apellido: '', cedula: '', direccion: '', sexo: '', fecha_nac: '',
    pais_id: '', estado_id: '', ciudad: '', telefono: ''
  }
  nacionalidad = [
    { value: 1, viewValue: 'Venezolana' },
    { value: 2, viewValue: 'Extranjera' }
  ];
  resultadoObtenido: any;
  comboEstados: any[] = [];
  sexo = [
    { value: 'F', viewValue: 'Femenino' },
    { value: 'M', viewValue: 'Masculino' }
  ];

  displayedColumns: string[] = ['caracteristica', 'valor'];
  dataSource: MatTableDataSource<TablaData>;
  // OBJETO PARA CARGAR EN LA TABLA
  datos: TablaData;
  elementoTabla = false;
  ocupacion: string[] = ['Albañil', 'Ingeniero', 'Profesor', 'Deportista'];
  porcentaje = 0;
  completitud: string = "Completitud del Perfil";
  mostrar: boolean = false;
  chart: any;
  por2 = 100;
  listaParametros: Parametros[] = [];
  listaValores: any[];
  agregar: boolean = true;
  idCaracBase;
  completar: boolean = false;
  carac = {
    id: 0,
    caracteristica: "",
    valor: ""
  }
  imagen;//variable para mostrar las imágenes
  imagenSeleccionada: File = null; // variable tipo File que se utiliza para mostrar una vista previa de la imagen  
  imagenDefecto: string = "assets/images/default.jpg";//imagen por defecto que se le pasa al incluir
  ocultarPassword = false; // Oculta el campo contraseña para el incluir
  confirmar: boolean = false; //confirmar contraseña
  hide = true; // para la contraseña
  hide2 = true;// para el confirmar contraseña
  datosUsuarioFoto = new FormData();
  constructor(private _formBuilder: FormBuilder, public generalService: GeneralService, public snackBar: MatSnackBar, ) {
    this.cargarCombos('estado', this.comboEstados);
    this.cargarlistaCaractBase();
    this.CargarTabla();
    this.CargarDatos();
    this.myControlCorreo.disable();
  }
  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
    this.CargarChart();
  }


  myControlCorreo = new FormControl('',
    [
      Validators.required,
      Validators.email,
    ]);
  imagenFormControl = new FormControl();

  myControlCedula = new FormControl('',
    [
      Validators.required
    ]);

  myControlNombre = new FormControl('',
    [
      Validators.required
    ]);


  myControlApellido = new FormControl('',
    [
      Validators.required
    ]);
  myControlFecha = new FormControl('',
    [
      Validators.required
    ]);

  myControlNacionalidad = new FormControl('',
    [
      Validators.required
    ]);
  myControlEstado = new FormControl('',
    [
      Validators.required
    ]);

  myControlCiudad = new FormControl('',
    [
      Validators.required
    ]);
  myControlSexo = new FormControl('',
    [
      Validators.required
    ]);
  myControlTelefono = new FormControl('',
    [
      Validators.required
    ]);

  myControlDireccion = new FormControl('',
    [
      Validators.required
    ]);
  myControlContrasena = new FormControl('',
    [
      Validators.required
    ]);
  myControlConfirmar = new FormControl('');
  //Carga cualquier combo desde la base de datos
  cargarCombos(nombreEntidad, combo: any[]) {
    this.generalService.Obtenertodos(nombreEntidad).then((result) => {
      this.resultadoObtenido = result;
      for (let i = 0; i < this.resultadoObtenido.data.length; i++) {
        combo.push(
          {
            id: this.resultadoObtenido.data[i].id,
            nombre: this.resultadoObtenido.data[i].nombre
          }
        );
      }
    }, (err) => {
      console.log(err);
    });

  }

  //Metodo para validar el confirmar las contraseña
  validarPassWord() {
    if (this.myControlContrasena.value != this.myControlConfirmar.value) {
      this.confirmar = true;
    } else {
      this.confirmar = false;
    }
  }
  //CARGAR LISTA DE CARACTERISTICA BASE
  cargarlistaCaractBase() {
    this.generalService.Obtenertodos('caracteristica_base').then((result) => {
      this.resultadoObtenido = result;
      for (let i = 0; i < this.resultadoObtenido.data.length; i++) {
        this.listaParametros.push(
          {
            id: this.resultadoObtenido.data[i].id,
            nombre: this.resultadoObtenido.data[i].nombre,
            checked: false,
            color: 'none'
          }
        );
      }
    }, (err) => {
      console.log(err);
    });

  }

  // PARA CARGAR EL CHART Y ACTUALIZARLO
  CargarChart() {
    this.chart = new Chart("canvas-dona", {
      type: 'doughnut',

      data: {
        datasets: [{

          yAxisID: 'A',
          data: [this.porcentaje, this.por2],
          backgroundColor: ['#E0A801', '#D8D8D8'],
          hoverBorderColor: ['#E0A801', '#D8D8D8'],

        }]
      },
      options: {

        legend: {
          display: false,
        },
        tooltips: {
          enabled: false,
          display: false
        },
        hover: { mode: null },
        cutoutPercentage: 70

      }
    });
  }
  // METEDO QUE OBTIENE UNA LISTA Y SE CARGA EN LA TABLA
  CargarTabla() {
    let caracCliente;
    let idcliente;
    this.generalService.ObtenerUno('cliente/usuario', localStorage.getItem('id')).then((result) => {
      idcliente = result;
      this.generalService.ObtenerUno('vista_perfil/cliente', idcliente.data.id).then((result) => {
        caracCliente = result;
        console.log(caracCliente);
        for (let i = 0; i < caracCliente.data.length; i++) {
          datosEjemplo.push({
            id: caracCliente.data[i].id,
            caracteristica: caracCliente.data[i].caracteristica_base_nombre,
            valor: caracCliente.data[i].caracteristica_nombre
          });
        }
        this.dataSource = new MatTableDataSource(datosEjemplo);
        console.log(datosEjemplo);
        if (datosEjemplo.length > 0) {
          this.elementoTabla = true;
          let list;
          this.generalService.Obtenertodos('caracteristica_base').then((result) => {
            list = result;
            this.porcentaje = (this.listaParametros.length / list.data.length * 100);

            this.por2 = 100 - this.porcentaje;
            this.CargarChart();
            for (let i = 0; i < datosEjemplo.length; i++) {
              let index = this.listaParametros.findIndex(x => x.nombre == datosEjemplo[i].caracteristica);
              if (index != -1) {
                this.listaParametros[index].checked = true;
              }
            }
          }, (err) => {
            console.log(err);
            this.loading = false;
          });
        }
      }, (err) => {
        console.log(err);
      });
    }, (err) => {
      console.log(err);
    });

  }

  // PARA AGREGAR UNA CARCACTERISTICA NUEVA A LA TABLA
  AgregarCaracteristica() {
    let index = datosEjemplo.findIndex(x => x.caracteristica == this.carac.caracteristica);
    this.mostrar = this.mostrar = !this.mostrar;
    if (index != -1) {
      datosEjemplo[index].valor = this.carac.valor;
      datosEjemplo[index].id = this.carac.id;
    } else {
      datosEjemplo.push({
        id: this.carac.id,
        caracteristica: this.carac.caracteristica,
        valor: this.carac.valor
      });
      this.Cambiar();
    }
    console.log(datosEjemplo);
    this.agregar = true;
  }

  // METODO PARA CARGAR LA LISTA DE VALOR DE CARACTERISTICA
  CargarValor(idBase: number, nombre, check) {

    let index = this.listaParametros.findIndex(x => x.id == idBase);
    this.listaParametros[index].color = 'gray';
    let i = 0;
    while (i < this.listaParametros.length) {
      if (i != index) {
        this.listaParametros[i].color = 'white';
      }
      i++;
    }
    this.mostrar = !this.mostrar;
    this.carac.caracteristica = nombre;
    this.idCaracBase = idBase;
    let resultado: any;
    this.generalService.ObtenerUno('caracteristica/base', idBase).then((result) => {
      resultado = result;
      this.listaValores = resultado.data;
    }, (err) => {
      console.log(err);
    });
  }
  // METODO PARA OBTENER EL VALOR DE LA CARACTERISTICA QUE SE VA A INCLUIR EN LA TABLA
  SeleccionarCaracValor(id, nombre) {
    this.carac.id = id;
    this.carac.valor = nombre;
    this.agregar = false;
  }
  // PARA ACTUALIZAR Y COLOCAR EL EL CHECKED DEL ITEM DE LA IZQUIERDA EN TRUE
  Cambiar() {
    if (datosEjemplo.length > 0) {
      this.elementoTabla = true;
    }
    let index = this.listaParametros.findIndex(x => x.id == this.idCaracBase);
    this.listaParametros[index].checked = true;
    this.porcentaje = this.porcentaje + (1 / this.listaParametros.length * 100);
    this.por2 = 100 - this.porcentaje;
    this.CargarChart();
  }

  // Funcion para las imágenes 
  imagenEntrada(img: FileList) {
    this.imagenSeleccionada = img.item(0);
    //Para mostrar un preview de la imagen
    var lector = new FileReader();
    lector.onload = (event: any) => {
      this.imagen = event.target.result;
      this.imagenDefecto = event.target.result;
    }
    lector.readAsDataURL(this.imagenSeleccionada);
  }

  CargarDatos() {
    let obtCliente;
    this.generalService.ObtenerUno('cliente/usuario', localStorage.getItem('id')).then((result) => {
      obtCliente = result;
      this.myControlCedula.setValue(obtCliente.data.cedula);
      this.myControlNombre.setValue(obtCliente.data.nombre);
      this.myControlApellido.setValue(obtCliente.data.apellido);
      this.myControlFecha.setValue(obtCliente.data.fecha_nac);
      this.myControlNacionalidad.setValue(obtCliente.data.pais_id);
      this.myControlEstado.setValue(obtCliente.data.estado_id);
      this.myControlCiudad.setValue(obtCliente.data.ciudad);
      this.myControlSexo.setValue(obtCliente.data.sexo);
      this.myControlTelefono.setValue(obtCliente.data.telefono);
      this.myControlDireccion.setValue(obtCliente.data.direccion);
      this.generalService.ObtenerUno('usuario', localStorage.getItem('correo')).then((result) => {
        obtCliente = result;
        this.imagen = obtCliente.data.imagen;
        this.myControlCorreo.setValue(obtCliente.data.correo);

      }, (err) => {
        console.log(err);
      });
    }, (err) => {
      console.log(err);
    });
  }
  // ACTUALIZAR LOS DATOS DEL CLIENTE EN LA BD
  Actualizar() {
    if (this.porcentaje < 100) {
      this.completar = true;
    }
    else {
      this.completar = false;
    }


    if (this.myControlNacionalidad.valid && this.myControlCedula.valid && this.myControlNombre.valid
      && this.myControlSexo.valid && this.myControlTelefono.valid && this.myControlApellido.valid && this.porcentaje > 99) {
      this.datosCliente.nombre = this.myControlNombre.value;
      this.datosCliente.apellido = this.myControlApellido.value;
      this.datosCliente.cedula = this.myControlCedula.value;
      this.datosCliente.telefono = this.myControlTelefono.value;
      this.datosCliente.direccion = this.myControlDireccion.value;
      this.datosCliente.sexo = this.myControlSexo.value;
      this.datosCliente.fecha_nac = this.myControlFecha.value;
      this.datosCliente.pais_id = this.myControlNacionalidad.value;
      this.datosCliente.estado_id = this.myControlEstado.value;
      this.datosCliente.ciudad = this.myControlCiudad.value;
      console.log(this.datosCliente);
      let idclient;
      this.loading = true;


      this.generalService.ObtenerUno('cliente/usuario', Number.parseInt(localStorage.getItem('id'))).then((result) => {
        idclient = result;

        this.generalService.Actualizar(this.datosCliente, 'cliente', idclient.data.id).then((result) => {


          let res;
          this.generalService.ObtenerUno('cliente/usuario', Number.parseInt(localStorage.getItem('id'))).then((result) => {
            res = result;
            for (let i = 0; i < datosEjemplo.length; i++) {
              let perfil = {
                caracteristica_id: datosEjemplo[i].id,
                cliente_id: res.data.id,
                estatus: 'A'
              }
              if (this.imagenSeleccionada != null) {
                this.datosUsuarioFoto.append('imagen', this.imagenSeleccionada, this.imagenSeleccionada.name);
              }

              // this.datosUsuarioFoto.append('correo', this.myControlCorreo.value);
              if (this.myControlContrasena.value != "") {
                this.datosUsuarioFoto.append('contrasenia', this.myControlContrasena.value);
              }

              this.generalService.ActualizarConImagen(this.datosUsuarioFoto, 'usuario', localStorage.getItem('id')).then((result) => {
                // let perfiles;
                // this.generalService.ObtenerUno('perfil/cliente', idclient.data.id).then((result) => {
                //   perfiles = result;
                //   for(let i = 0; i< perfiles.data.length; i++){
                //     this.generalService.EliminarFisico('perfil',perfiles.data[i].id).then((result) => {
                //     }, (err) => {
                //       console.log(err);
                //       this.loading = false;
                //     });

                //   }
                //   for (let i = 0; i < datosEjemplo.length; i++) {
                //     let perfil = {
                //       caracteristica_id: datosEjemplo[i].id,
                //       cliente_id: idclient.data.id,
                //       estatus: 'A'
                //     }
                //     console.log(perfil);
                //     this.generalService.Registrar(perfil, 'perfil').then((result) => {

                //     }, (err) => {
                //       console.log(err);
                //       this.loading = false;
                //     });
                //   }

                //   this.loading = false;
                //   this.openSnackBar('Datos actualizados con exito!', "Actualizar Datos");
                // }, (err) => {
                //   console.log(err);
                //   this.loading = false;
                // });
                this.loading = false;
                this.openSnackBar('Datos actualizados con exito!', "Actualizar Datos");
              }, (err) => {
                console.log(err);
                this.loading = false;
              });

            }

          }, (err) => {
            console.log(err);
            this.loading = false;
          });


        }, (err) => {
          console.log(err);
          this.loading = false;
        });
      }, (err) => {
        console.log(err);
        this.loading = false;
      });

    }

  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }

}
