import { Component, OnInit, ViewChild } from '@angular/core';
import { GeneralService } from '../../core/Services/general/general.service';
import { FormControl, Validators } from '@angular/forms';
import { MatSnackBar, MatSelectionList } from '@angular/material';

export interface Original {
  funcion: any,
}
@Component({
  selector: 'app-funcionalidades',
  templateUrl: './funcionalidades.component.html',
  styleUrls: ['./funcionalidades.component.scss']
})
export class FuncionalidadesComponent implements OnInit {
  listaConfiguracion1: string[] = [];
  listaConfiguracion2: string[] = [];
  form_rolAsociado = { id_funcion: '', id_rol: '', estatus: 'A' };
  listaRol: any[];
  listaFuncion: any[] = [];
  resultado: any;
  resultado2: any[];
  bloquear: boolean = true;
  cambios: boolean = false;
  verdadero: boolean[] = []; //se utiliza para que la lista venga selecionada
  funcionSelecionada: any[] = []; // arreglo que almacena los valores seleccionados de la lista 1
  funcionSelecionada2: any[] = [];// arreglo que almacena los valores seleccionados de la lista 2
  listaRegistrar: any[] = []; //concatena lo seleccionado en las dos listas 
  datosOriginales: any[] = [];
  original: Original;
  @ViewChild('funcion') lista1: MatSelectionList;
  @ViewChild('funcion2') lista2: MatSelectionList;


  constructor(private generalService: GeneralService, public snackBar: MatSnackBar) {
    this.cargarCombo();
    this.CargarFuncion();
  }
  rolFormControl = new FormControl('',
    [
      Validators.required
    ]);


  //CARGAR COMBO ROLES
  cargarCombo() {
    this.generalService.Obtenertodos('rol').then((result) => {
      this.resultado = result;
      this.listaRol = this.resultado.data;
    }, (err) => {
      console.log(err);
    });
  }

  //Limpiar Arreglo Verdadero

  limpiarArreglo() {
    for (let j = 0; j < this.verdadero.length; j++) {
      this.verdadero[j] = false;
    }
    this.lista1.deselectAll();//deselecciona las listas
    this.lista2.deselectAll();
    this.bloquear = true;
  }

  cancelar() {
    this.limpiarArreglo();
    this.rolFormControl.setValue(-1);
  }

  //Carga el Acceso A ROL desde la bd

  cargarAccesoRol() {
    this.limpiarArreglo();
    this.datosOriginales = [];
    this.bloquear = false;
    this.generalService.Obtenertodos('acceso_rol').then((result) => {
      this.resultado = result;
      for (let i = 0; i < this.resultado.data.length; i++) {
        if (this.rolFormControl.value == this.resultado.data[i].rol_id) {
          this.verdadero[this.resultado.data[i].funcion_id] = true;
          this.original = { funcion:this.resultado.data[i].funcion_id}
          this.datosOriginales.push(this.original);
        }
      }

      console.log(this.datosOriginales,'cargando ==');
    }, (err) => {
      console.log(err);
    });
  }

  //carga todas las funciones en la lista
  CargarFuncion() {
    this.generalService.Obtenertodos('funcion').then((result) => {
      this.resultado = result;
      for (let i = 0; i < this.resultado.data.length; i++) {
        this.listaFuncion.push({
          id: this.resultado.data[i].id,
          nombre: this.resultado.data[i].nombre,
          seleccionado: false
        });
        this.verdadero[this.resultado.data[i].id] = false; //Pone todo en falso en la posición exacta del id de cada funcionalidad del sistema
      }
      console.log(this.listaFuncion);
      let valor = Math.round(this.listaFuncion.length / 2);
      for (let i = 0; i < valor; i++) {
        this.listaConfiguracion1.push(this.listaFuncion[i]);
      }
      for (let i = valor; i < this.listaFuncion.length; i++) {
        this.listaConfiguracion2.push(this.listaFuncion[i])
      }
    }, (err) => {
      console.log(err);
    });
    console.log(this.verdadero);
  }

  //SOLO ELIMA SI SE LECCIONAN LAS DOS LISTAS NO FUNCIONA BIEN
  Eliminar()
  {
    console.log(this.datosOriginales, 'ORIGINAL1')
    console.log(this.listaRegistrar,'LISTA REGISTRAR')
    for(let i=0; i<this.listaRegistrar.length; i++)
    {
      let index =this.datosOriginales.findIndex(x => x.funcion === this.listaRegistrar[i]);
      console.log(index,'index');
      if(index)
      {
        this.datosOriginales.splice(index,1);
      }
    }
    console.log(this.datosOriginales,'ORIGINAL2')

    for(let i=0; i<this.datosOriginales.length; i++)
    {
      this.generalService.EliminarFisico('acceso_rol/delete',this.rolFormControl.value +"/"+this.datosOriginales[i].funcion).then((result) => {
        this.lista1.deselectAll();//deselecciona las listas
        this.lista2.deselectAll();
        this.rolFormControl.setValue(-1);
      }, (err) => {
        console.log(err);
      });
    }
  }

  // inserta o realiza un delete en el caso de que se deseleccione una función
  Registrar() {
    let i = 0;
    while (i < this.funcionSelecionada.length) {
      this.listaRegistrar[i] = this.funcionSelecionada[i];
      i++;
    }
    let k = 0;
    while (k < this.funcionSelecionada2.length) {
      this.listaRegistrar[i] = this.funcionSelecionada2[k];
      i++;
      k++;
    }

    if (this.rolFormControl.valid) {
      let j = 0;
      this.form_rolAsociado.id_rol = this.rolFormControl.value;
      while (j <= this.listaRegistrar.length) {
        //console.log(this.verdadero[this.funcionSelecionada[j]]);
        if (this.verdadero[this.listaRegistrar[j]] == false) {
          this.form_rolAsociado.id_funcion = this.listaRegistrar[j];
          this.generalService.Registrar(this.form_rolAsociado, 'rol/asociar').then((result) => {
            this.rolFormControl.setValue(-1);
            this.lista1.deselectAll();//deselecciona las listas
            this.lista2.deselectAll();
          }, (err) => {
            console.log(err);
          });
        }
        j++;
      }
      if (this.cambios == false) {
        this.openSnackBar('No se realizó Ningún cambio!', "registrar");
      }
      else {
        this.openSnackBar('Cambio exitoso!', "registrar");
      }
      this.Eliminar();
    }


  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }
  ngOnInit() {
  }
  //evento change de las listas
  onAreaListControlChanged(list) {
    // https://stackblitz.com/edit/base-angular-material-jyycr7?file=app%2Fapp.component.ts
    this.funcionSelecionada = list.selectedOptions.selected.map(item => item.value);
    console.log(this.funcionSelecionada);
    this.cambios = true;
  }

  onAreaListControlChanged2(list) {
    // https://stackblitz.com/edit/base-angular-material-jyycr7?file=app%2Fapp.component.ts
    this.funcionSelecionada2 = list.selectedOptions.selected.map(item => item.value);
    console.log(this.funcionSelecionada2);
    this.cambios = true;
  }

}
