import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { GeneralService } from '../../core/Services/general/general.service';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.scss']
})
export class ClienteComponent implements OnInit {
  nacionalidad = [
    { value: 1, viewValue: 'Venezolana' },
    { value: 2, viewValue: 'Extranjera' }
  ];
  resultadoObtenido: any;
  comboEstados: any[] =[];
  sexo = [
    { value: 'f', viewValue: 'Femenino' },
    { value: 'm', viewValue: 'Masculino' }
];

  constructor(public generalService: GeneralService,) {
    this.cargarCombos('estado',this.comboEstados)
   }

  ngOnInit() {
  }

  myControlCedula = new FormControl('',
    [
      Validators.required
    ]);

  myControlNombre = new FormControl('',
    [
      Validators.required
    ]);


  myControlApellido = new FormControl('',
    [
      Validators.required
    ]);
  myControlFecha = new FormControl('',
    [
      Validators.required
    ]);

  myControlNacionalidad = new FormControl('',
    [
      Validators.required
    ]);
    myControlEstado = new FormControl('',
    [
        Validators.required
    ]);

    myControlCiudad = new FormControl('',
    [
      Validators.required
    ]);
    myControlSexo = new FormControl('',
    [
        Validators.required
    ]);
    myControlTelefono = new FormControl('',
    [
        Validators.required
    ]);

    myControlDireccion = new FormControl('',
    [
        Validators.required
    ]);
    //Carga cualquier combo desde la base de datos
    cargarCombos(nombreEntidad, combo: any[]) {
      this.generalService.Obtenertodos(nombreEntidad).then((result) => {
          this.resultadoObtenido = result;
          for (let i = 0; i < this.resultadoObtenido.data.length; i++) {
              combo.push(
                  {
                      id: this.resultadoObtenido.data[i].id,
                      nombre: this.resultadoObtenido.data[i].nombre
                  }
              );
          }
      }, (err) => {
          console.log(err);
      });

  }
}
