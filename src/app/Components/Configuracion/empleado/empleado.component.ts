import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { GeneralService } from '../../core/Services/general/general.service';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute } from '@angular/router';


@Component({
    selector: 'app-empleado',
    templateUrl: './empleado.component.html',
    styleUrls: ['./empleado.component.scss'],
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'es-VE' },
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },

    ]
})


export class EmpleadoComponent implements OnInit {
    resultado: any; // almacena los datos de un empleado
    resultadoUser: any; // almacena los datos de un usuario
    incluir = true;
    confirmar: boolean = false; //confirmar contraseña
    hide = true; // para la contraseña
    hide2 = true;// para el confirmar contraseña
    ocultarPassword = false; // Oculta el campo contraseña para el incluir
    resultadoObtenido: any;
    comboTipoEmpleado: any[] = []; // Tipo Empleado
    comboEstados: any[] = []; // Combo Estados
    comboRol: any[] = []; //Roles
    comboEspecialidad: any[] = []; //especialidad
    estadoCivil: any[] = []; //estadoCivil
    nacionalidad: any[] = [];
    public loading = false; //Para Utilizar el componente loading
    imagen;//variable para mostrar las imágenes
    imagenDefecto: string = "assets/images/default.jpg";//imagen por defecto que se le pasa al incluir
    imagenSeleccionada: File = null; // variable tipo File que se utiliza para mostrar una vista previa de la imagen
    continuar: boolean = true; // variable booleana para continuar de llenar los campos
    mover = 'Siguiente';
    iconoMoverS = 'navigate_next';
    iconoMoverR = '';
    datosEmpleado = {
        correo: '', rol_id: '', ultimo_acceso: '', nombre1: '', nombre2: '', apellido1: '', apellido2: '',
        cedula: '', direccion: '', sexo: '', fecha_nac: '', pais_id: '', estado_id: '', ciudad: '', empresa_id: '', tipo_empleado_id: '', especialidad_id: '', telefono: '', estado_civil_id: ''
    }

    datosEmpleado2 = {
        nombre1: '', nombre2: '',
        apellido1: '', apellido2: '', cedula: '', direccion: '', sexo: '', fecha_nac: '', pais_id: '', estado_id: '', ciudad: '', tipo_empleado_id: '', especialidad_id: '', telefono: '', estado_civil_id: ''
    }
    idEmpleado: any;
    sexo = [
        { value: 'f', viewValue: 'Femenino' },
        { value: 'm', viewValue: 'Masculino' }
    ];

    myControlTipoEmpleado = new FormControl('',
        [
            Validators.required
        ]);
    myControlEstado = new FormControl('',
        [
            Validators.required
        ]);

    myControlCedula = new FormControl('',
        [
            Validators.required
        ]);

    myControlNombre = new FormControl('',
        [
            Validators.required
        ]);

    myControlSegundoNomb = new FormControl();

    imagenFormControl = new FormControl();

    myControlPrimerApellido = new FormControl('',
        [
            Validators.required
        ]);

    myControlSegundoApellido = new FormControl();

    myControlSexo = new FormControl('',
        [
            Validators.required
        ]);

    myControlEstadoCivil = new FormControl('',
        [
            Validators.required
        ]);

    myControlTelefono = new FormControl('',
        [
            Validators.required
        ]);

    myControlCorreo = new FormControl('',
        [
            Validators.required,
            Validators.email,
        ]);

    myControlFecha = new FormControl('',
        [
            Validators.required
        ]);

    myControlNacionalidad = new FormControl('',
        [
            Validators.required
        ]);

    myControlEspecialidad = new FormControl();

    myControlCiudad = new FormControl('',
        [
            Validators.required
        ]);
    myControlDireccion = new FormControl('',
        [
            Validators.required
        ]);
    myControlContrasena = new FormControl('',
        [
            Validators.required
        ]);
    myControlConfirmar = new FormControl('');
    myControlRol = new FormControl('', [
        Validators.required
    ]);

    constructor(public generalService: GeneralService, public snackBar: MatSnackBar, private route: ActivatedRoute) {
        this.cargarCombos('tipo_empleado', this.comboTipoEmpleado);
        this.cargarCombos('estado', this.comboEstados);
        this.cargarCombos('rol', this.comboRol);
        this.cargarCombos('especialidad', this.comboEspecialidad);
        this.cargarCombos('estado_civil', this.estadoCivil);
        this.cargarCombos('pais', this.nacionalidad)

        this.incluir = true;
        this.route.params.subscribe(params => {
            if (params['datos'] != null) {
                this.obtenerEmpleado(params['datos'])
                this.incluir = false;
                this.idEmpleado = params['datos'];
                this.ocultarPassword = true;
                this.myControlCorreo.disable()
            }
            else {
                this.incluir = true;
                this.myControlCorreo.enable()
            }

        });


    }

    ngOnInit() {
        this.myControlEspecialidad.disable();
        this.onChanges();
    }

    //funcion para obtener datos de un empleado

    obtenerEmpleado(idEmpleado) {
        this.generalService.ObtenerUno('empleado', idEmpleado).then((result) => {
            this.resultado = result;
            this.myControlTipoEmpleado.setValue(this.resultado.data.tipo_empleado_id);
            this.myControlNombre.setValue(this.resultado.data.nombre1)
            this.myControlSegundoNomb.setValue(this.resultado.data.nombre2)
            this.myControlPrimerApellido.setValue(this.resultado.data.apellido1)
            this.myControlSegundoApellido.setValue(this.resultado.data.apellido2)
            this.myControlEspecialidad.setValue(this.resultado.data.especialidad_id)
            this.myControlCedula.setValue(this.resultado.data.cedula)
            this.myControlSexo.setValue(this.resultado.data.sexo)
            this.myControlTelefono.setValue(this.resultado.data.telefono)
            this.myControlNacionalidad.setValue(this.resultado.data.pais_id)
            this.myControlEstadoCivil.setValue(this.resultado.data.estado_civil_id)
            this.myControlFecha.setValue(this.resultado.data.fecha_nac)
            this.myControlEstado.setValue(this.resultado.data.estado_id)
            this.myControlDireccion.setValue(this.resultado.data.direccion)
            this.myControlCiudad.setValue(this.resultado.data.ciudad)
            this.myControlCorreo.setValue(this.resultado.data.correo)
            //Obtener Usuario
            this.generalService.ObtenerUno('usuario', this.resultado.data.correo).then((result) => {
                this.resultadoUser = result;
                this.myControlRol.setValue(this.resultadoUser.data.rol_id)
                this.imagen = this.resultadoUser.data.imagen
            }, (err) => {
                console.log(err);
            });

        }, (err) => {
            console.log(err);
        });

    }

    //Funcion para deshabilitar el campo Especialidad si no selecciona un abogado
    onChanges() {
        this.myControlTipoEmpleado.valueChanges
            .subscribe(valorSeleccionado => {
                if (valorSeleccionado != '2') {
                    this.myControlEspecialidad.reset();
                    this.myControlEspecialidad.disable();
                }
                else {
                    this.myControlEspecialidad.enable();
                }
            });
    }



    // Funcion para las imágenes 
    imagenEntrada(img: FileList) {
        this.imagenSeleccionada = img.item(0);
        //Para mostrar un preview de la imagen
        var lector = new FileReader();
        lector.onload = (event: any) => {
            this.imagen = event.target.result;
            this.imagenDefecto = event.target.result;
        }
        lector.readAsDataURL(this.imagenSeleccionada);
    }

    //Carga cualquier combo desde la base de datos
    cargarCombos(nombreEntidad, combo: any[]) {
        this.generalService.Obtenertodos(nombreEntidad).then((result) => {
            this.resultadoObtenido = result;
            for (let i = 0; i < this.resultadoObtenido.data.length; i++) {
                combo.push(
                    {
                        id: this.resultadoObtenido.data[i].id,
                        nombre: this.resultadoObtenido.data[i].nombre
                    }
                );
            }
        }, (err) => {
            console.log(err);
        });

    }
    selected = new FormControl(0);


    //Metodo para validar el confirmar las contraseña
    validarPassWord() {
        if (this.myControlContrasena.value != this.myControlConfirmar.value) {
            this.confirmar = true;
        } else {
            this.confirmar = false;
        }
    }

    //  INSERTA EN LA BASE DE DATOS
    Registrar() {
        this.validarPassWord()

        if (this.myControlTipoEmpleado.valid && this.myControlNacionalidad.valid && this.myControlCedula.valid && this.myControlNombre.valid
            && this.myControlPrimerApellido.valid && this.myControlSexo.valid && this.myControlTelefono.valid && this.myControlEstadoCivil.valid
            && this.myControlFecha.valid && this.myControlEstado.valid && this.myControlCiudad.valid && this.myControlDireccion.valid && this.myControlRol.valid
            && this.myControlCorreo.valid && this.myControlConfirmar.valid && !this.confirmar) {
            this.datosEmpleado.correo = this.myControlCorreo.value;
            this.datosEmpleado.rol_id = this.myControlRol.value;
            this.datosEmpleado.nombre1 = this.myControlNombre.value;
            if (this.myControlSegundoNomb.value != null) {
                this.datosEmpleado.nombre2 = this.myControlSegundoNomb.value;
            }
            this.datosEmpleado.apellido1 = this.myControlPrimerApellido.value;
            if (this.myControlSegundoApellido.value != null) {
                this.datosEmpleado.apellido2 = this.myControlSegundoApellido.value;
            }
            this.datosEmpleado.cedula = this.myControlCedula.value;
            this.datosEmpleado.direccion = this.myControlDireccion.value;
            this.datosEmpleado.sexo = this.myControlSexo.value;
            this.datosEmpleado.fecha_nac = this.myControlFecha.value;
            this.datosEmpleado.pais_id = this.myControlNacionalidad.value;
            this.datosEmpleado.estado_id = this.myControlEstado.value;
            this.datosEmpleado.ciudad = this.myControlCiudad.value;
            this.datosEmpleado.empresa_id = '1';
            this.datosEmpleado.tipo_empleado_id = this.myControlTipoEmpleado.value;
            this.datosEmpleado.especialidad_id = this.myControlEspecialidad.value;
            this.datosEmpleado.estado_civil_id = this.myControlEstadoCivil.value;
            this.datosEmpleado.telefono = this.myControlTelefono.value;
            let iduser;
            this.generalService.Registrar(this.datosEmpleado, 'empleado').then((result) => {
                this.generalService.ObtenerUno('usuario', this.datosEmpleado.correo).then((result) => {
                    iduser = result;
                    this.generalService.Actualizar({ estatus: 'A' }, 'usuario', iduser.data.id).then((result) => {
                        this.openSnackBar('Registro exitoso!', "registrar");

                    }, (err) => {
                        console.log(err);
                    });
                }, (err) => {
                    console.log(err);
                });

            }, (err) => {
                console.log(err);
            });
            console.log(this.datosEmpleado);
        }
        else {
            this.continuar = true;
        }

    }

    // PARA MOVERSE ENTRE LOS TABS DE LA VISTA DE EMPLEADO
    Mover() {
        if (this.selected.value == 0) {
            this.selected.setValue(1);
            this.mover = 'Regresar';
            this.iconoMoverR = "navigate_before";
            this.iconoMoverS = "";
        } else
            if (this.selected.value == 1) {
                this.selected.setValue(0);
                this.mover = 'Siguiente';
                this.iconoMoverS = "navigate_next";
                this.iconoMoverR = "";
            }
    }
    Mover2(evt) {
        this.selected.setValue(evt);
        if (this.selected.value == 0) {
            this.mover = 'Siguiente';
            this.iconoMoverS = "navigate_next";
            this.iconoMoverR = "";
        } else
            if (this.selected.value == 1) {
                this.mover = 'Regresar';
                this.iconoMoverR = "navigate_before";
                this.iconoMoverS = "";
            }
    }
    openSnackBar(message: string, action: string) {
        this.snackBar.open(message, action, {
            duration: 1000,
        });
    }

    Actualizar() {
        let iduser;
        if (this.myControlTipoEmpleado.valid && this.myControlNacionalidad.valid && this.myControlCedula.valid && this.myControlNombre.valid
            && this.myControlPrimerApellido.valid && this.myControlSexo.valid && this.myControlTelefono.valid && this.myControlEstadoCivil.valid
            && this.myControlFecha.valid && this.myControlEstado.valid && this.myControlCiudad.valid && this.myControlDireccion.valid && this.myControlRol.valid
            && !this.confirmar) {
            //ACTUALIZAR DATOS EMPLEADO
            this.datosEmpleado2.nombre1 = this.myControlNombre.value;
            if (this.myControlSegundoNomb.value != null) {
                this.datosEmpleado2.nombre2 = this.myControlSegundoNomb.value;
            }
            this.datosEmpleado2.apellido1 = this.myControlPrimerApellido.value;
            if (this.myControlSegundoApellido.value != null) {
                this.datosEmpleado2.apellido2 = this.myControlSegundoApellido.value;
            }
            this.datosEmpleado2.cedula = this.myControlCedula.value;
            this.datosEmpleado2.direccion = this.myControlDireccion.value;
            this.datosEmpleado2.sexo = this.myControlSexo.value;
            this.datosEmpleado2.fecha_nac = this.myControlFecha.value;
            this.datosEmpleado2.pais_id = this.myControlNacionalidad.value;
            this.datosEmpleado2.estado_id = this.myControlEstado.value;
            this.datosEmpleado2.ciudad = this.myControlCiudad.value;
            this.datosEmpleado2.tipo_empleado_id = this.myControlTipoEmpleado.value;
            this.datosEmpleado2.especialidad_id = this.myControlEspecialidad.value;
            this.datosEmpleado2.estado_civil_id = this.myControlEstadoCivil.value;
            this.datosEmpleado2.telefono = this.myControlTelefono.value;
            //ACTUALIZAR USUARIO
            const datosUsuario = new FormData();
            if (this.imagenSeleccionada != null) {
                datosUsuario.append('imagen', this.imagenSeleccionada, this.imagenSeleccionada.name);
            }

            if (this.myControlContrasena.value != null) {
                datosUsuario.append('contrasenia', this.myControlContrasena.value)
            }
            datosUsuario.append('rol_id', this.myControlRol.value)
            this.loading = true;
            this.generalService.Actualizar(this.datosEmpleado2, 'empleado', Number.parseInt(this.idEmpleado)).then(result => {
                this.generalService.ObtenerUno('usuario', this.myControlCorreo.value).then((result) => {
                    iduser = result;
                    this.generalService.ActualizarConImagen(datosUsuario, 'usuario', iduser.data.id).then((result) => {
                        this.openSnackBar('Registro Actualizado!', "Actualizar");
                        this.loading = false;

                    }, (err) => {
                        console.log(err);
                        this.loading = false;
                    });
                }, (err) => {
                    console.log(err);
                    this.loading = false;
                });

            }, (err) => {
                this.loading = false;
                console.log(err);
            });

        }

    }
}
