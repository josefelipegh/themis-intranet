import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConfigInfoAdicionalComponent } from './config-info-adicional/config-info-adicional.component';
import { ConfigValoracionComponent } from './config-valoracion/config-valoracion.component';
import { ConfiuracionEmpresaComponent } from './confiuracion-empresa/confiuracion-empresa.component';
import { EmpleadoComponent } from './empleado/empleado.component';
import { EmpresaComponent } from './empresa/empresa.component';
import { FuncionalidadesComponent } from './funcionalidades/funcionalidades.component';
import { HorarioEmpleadoComponent } from './horario-empleado/horario-empleado.component';
import { PerfilClienteComponent } from './perfil-cliente/perfil-cliente.component';
import { PerfilUsuarioComponent } from './perfil-usuario/perfil-usuario.component';
import { ListaUsuarioComponent } from './lista-usuario/lista-usuario.component';
import { ConfiguracionComponent } from './configuracion/configuracion.component';
import { GaleriaImagenesComponent } from './galeria-imagenes/galeria-imagenes.component';
import { HorarioAgendaComponent } from './horario-agenda/horario-agenda.component';
import { ClienteComponent } from './cliente/cliente.component';
import { AuthGuardService as AuthGuard } from './../core/Services/AuthGuard/auth-guard.service';

const routes: Routes = [
  {path: 'config_info' , component: ConfigInfoAdicionalComponent, canActivate: [AuthGuard]},
  {path: 'config_valoracion' , component: ConfigValoracionComponent, canActivate: [AuthGuard]},
  {path: 'config_web' , component: ConfiuracionEmpresaComponent, canActivate: [AuthGuard]},
  {path: 'empleado', component: EmpleadoComponent, canActivate: [AuthGuard]},
  {path: 'empleado/:datos', component: EmpleadoComponent, canActivate: [AuthGuard]},
  {path: 'empresa', component: EmpresaComponent, canActivate: [AuthGuard]},
  {path: 'config_access', component: FuncionalidadesComponent, canActivate: [AuthGuard]},
  {path: 'horario_empleado', component: HorarioEmpleadoComponent, canActivate: [AuthGuard]},
  {path: 'horario_empresa', component: HorarioAgendaComponent, canActivate: [AuthGuard]},
  {path: 'perfil', component: PerfilClienteComponent, canActivate: [AuthGuard]},
  {path: 'datos_perfil', component: PerfilUsuarioComponent, canActivate: [AuthGuard]},
  {path: 'listaUsuario', component: ListaUsuarioComponent, canActivate: [AuthGuard]},
  {path: 'configuracion', component: ConfiguracionComponent, canActivate: [AuthGuard]},
  {path: 'galeria', component: GaleriaImagenesComponent, canActivate: [AuthGuard]},
  {path: 'cliente', component: ClienteComponent, canActivate: [AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfiguracionRoutingModule { }
