import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigValoracionComponent } from './config-valoracion.component';

describe('ConfigValoracionComponent', () => {
  let component: ConfigValoracionComponent;
  let fixture: ComponentFixture<ConfigValoracionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigValoracionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigValoracionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
