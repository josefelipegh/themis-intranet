import { Component, OnInit } from '@angular/core';
import {coerceNumberProperty} from '@angular/cdk/coercion';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-config-valoracion',
  templateUrl: './config-valoracion.component.html',
  styleUrls: ['./config-valoracion.component.scss']
})
export class ConfigValoracionComponent implements OnInit {
  
  myControl = new FormControl();
  options: string[] = ['One', 'Two', 'Three'];
  
  max = 5;
  min = 0;
  showTicks = false;
  step = 1;
  thumbLabel = true;
  value = 0;
  selOpc = '';
  selEval = '';
  selCat = '';
  disableServ = true;
  disableEval = true;
  disableRango = true;
  
habilitarServicios(selCat){
  if(selCat != ''){
  this.disableServ = false;}
}

habilitarEvaluaciones(selOpc){
  if(selOpc != ''){
  this.disableEval = false;}
}

habilitarRango(selEval){
  if(selEval != ''){
  this.disableRango = false;}
}

  constructor() { }

  ngOnInit() {
  }


}


