import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { GeneralService } from '../../../core/Services/general/general.service';
import { FormControl, Validators } from '@angular/forms';
import * as moment from 'moment';

export interface Dia_semana {
  id: any;
  nombre: any;
}
export interface Horario {
  id: any;
  nombre: any;
}

@Component({
  selector: 'app-modal-horario-emrpesa',
  templateUrl: './modal-horario-emrpesa.component.html',
  styleUrls: ['./modal-horario-emrpesa.component.scss']
})
export class ModalHorarioEmrpesaComponent implements OnInit {

  resultado: any;
  dia: Dia_semana;
  dias: any[] = [];
  hora: Horario;
  horario: any[] = [];
  form_descripcion = { dia_semana_id: '', bloque_hora_id: '', estatus: 'A' };
  incluir = false;
  id;
  titulo = '';
  opcionDia: any;
  opcionBloque: any;
  constructor(public modal: MatDialog,
              public generalService: GeneralService,
              public dialogRef: MatDialogRef<ModalHorarioEmrpesaComponent>,
              public snackBar: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) public data: any) {
                this.Cargardias();
                this.Cargarhorario();
                this.incluir = true;
                this.titulo = 'Nuevo';
               }

  Cargardias() {
    this.generalService.Obtenertodos('dia_semana').then((result) => {
      this.resultado = result;
      this.dias = [];
      for (let i = 0; i < this.resultado.data.length; i++) {
        this.dia = {
          id: this.resultado.data[i].id,
          nombre: this.resultado.data[i].nombre,
        };
        this.dias.push(this.dia);
      }
    }, (err) => {
      console.log(err);
    });

  }

  Cargarhorario() {
    this.generalService.Obtenertodos('bloque_hora').then((result) => {
      this.resultado = result;
      this.horario = [];
      for (let i = 0; i < this.resultado.data.length; i++) {
        let bloque = this.Hora(this.resultado.data[i].hora_inicio,
                               this.resultado.data[i].hora_fin);
        this.hora = {
          id: this.resultado.data[i].id,
          nombre: bloque,
        };
        this.horario.push(this.hora);
      }
    }, (err) => {
      console.log(err);
    });

  }

  Hora(inicio, fin) {
    let bloque;
    return bloque = 'Desde ' + moment(inicio, ['HH:mm:ss']).format('LT') + ' hasta ' + moment(fin, ['HH:mm:ss']).format('LT');
  }

  Registrar() {

    if (this.opcionBloque && this.opcionDia) {
      this.form_descripcion.dia_semana_id = this.opcionDia;
      this.form_descripcion.bloque_hora_id = this.opcionBloque;
      console.log(this.opcionBloque, this.opcionDia);
      this.generalService.Registrar(this.form_descripcion, 'horario').then((result) => {
        this.openSnackBar('Registro Exitoso!', 'registrar');
      }, (err) => {
        console.log(err);
      });
      this.data.result = true;
      this.dialogRef.close(this.data);
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }

  ngOnInit() {
  }
  onNoClick() {
    this.dialogRef.close();
  }

}
