import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalHorarioEmrpesaComponent } from './modal-horario-emrpesa.component';

describe('ModalHorarioEmrpesaComponent', () => {
  let component: ModalHorarioEmrpesaComponent;
  let fixture: ComponentFixture<ModalHorarioEmrpesaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalHorarioEmrpesaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalHorarioEmrpesaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
