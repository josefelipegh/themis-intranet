
import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Location } from '@angular/common';
import { GeneralService } from '../../core/Services/general/general.service';
import { ModalPreguntaEliminarComponent } from '../../shared/modal-pregunta-eliminar/modal-pregunta-eliminar.component';
import { ModalHorarioEmrpesaComponent } from './modal-horario-emrpesa/modal-horario-emrpesa.component';
import * as moment from 'moment';
export interface Horario {
  id_dia: any;
  nombre: any;
  horario: any;
}

export interface Bloque {
  id: any;
  bcolor: any;
  inicio: any;
  fin: any;
}

@Component({
  selector: 'app-horario-agenda',
  templateUrl: './horario-agenda.component.html',
  styleUrls: ['./horario-agenda.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class HorarioAgendaComponent implements OnInit {

  resultado: any;
  // OBJETO PARA CARGAR EN LA TABLA
  hora: Horario;
  horario: any[] = [];
  bloque: Bloque;
  bloques: any[] = [];
  color = 1;

  constructor(public dialog: MatDialog,
              public generalService: GeneralService,
              private location: Location) {
    this.Cargarhorario();
  }

   openDialogRegistrar() { // abre una ventana modal

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';

    const dialogRef = this.dialog.open(ModalHorarioEmrpesaComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.Cargarhorario();
        }
      }
    );
  }

  openDialogActualizar(hora, bloque) {// abre una ventana modal

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    // PARA PASAR LOS DATOS AL DIALOG
    const datos = {id: hora.id, dia_semana: hora.id_dia, bloque_hora: bloque};
    dialogConfig.data = datos;

    const dialogRef = this.dialog.open(ModalHorarioEmrpesaComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
           this.Cargarhorario();
        }
      }
    );
  }
  openDialogEliminar(row) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    // PARA PASAR LOS DATOS AL DIALOG PREGUNTA ELIMINAR
    dialogConfig.data = {
      id: row,
      nombreMetodo: 'horario'
    };

    const dialogRef = this.dialog.open(ModalPreguntaEliminarComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.Cargarhorario();
        }
      }
    );
  }

  Cargarhorario() {
    this.generalService.Obtenertodos('vista_horario').then((result) => {
      this.resultado = result;
      this.horario = [];
      for (let i = 0; i < this.resultado.data.length; i++) {
        let val;
        val = this.horario.filter( hora => hora.nombre === this.resultado.data[i].dia_semana_nombre);
        if ( val.length === 0 ) {
          this.bloques = [];
          if (this.resultado.data[i].horario_estatus === 'A') {
            for (let j = 0; j < this.resultado.data.length; j ++) {
              if (this.resultado.data[j].dia_semana_id === this.resultado.data[i].dia_semana_id) {
                if (this.resultado.data[j].horario_estatus === 'A') {
                  this.bloque = {
                    id: this.resultado.data[j].horario_id,
                    bcolor: this.Color(),
                    inicio: moment(this.resultado.data[j].hora_inicio, ['HH:mm:ss']).format('LT'),
                    fin: moment(this.resultado.data[j].hora_fin, ['HH:mm:ss']).format('LT')
                  };
                  this.bloques.push(this.bloque);
                }
              }
            }
            this.hora = {
              id_dia: this.resultado.data[i].dia_semana_id,
              nombre: this.resultado.data[i].dia_semana_nombre,
              horario: this.bloques
            };
            this.horario.push(this.hora);
          }
        }
      }
      console.log(this.horario);
    }, (err) => {
      console.log(err);
    });

  }

  Color() {
    if (this.color > 2) {
      this.color = 1;
    }
    switch (this.color) {
      case 1:
        this.color += 1;
        return '#5e2129';
      case 2:
        this.color += 1;
        return '#e0a801';
    }
  }

   ngOnInit() {

  }

}
