import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HorarioAgendaComponent } from './horario-agenda.component';

describe('HorarioAgendaComponent', () => {
  let component: HorarioAgendaComponent;
  let fixture: ComponentFixture<HorarioAgendaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HorarioAgendaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HorarioAgendaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
