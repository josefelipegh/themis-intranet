import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigInfoAdicionalComponent } from './config-info-adicional.component';

describe('ConfigInfoAdicionalComponent', () => {
  let component: ConfigInfoAdicionalComponent;
  let fixture: ComponentFixture<ConfigInfoAdicionalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigInfoAdicionalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigInfoAdicionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
