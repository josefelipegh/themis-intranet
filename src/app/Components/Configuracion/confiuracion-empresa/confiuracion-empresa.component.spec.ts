import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfiuracionEmpresaComponent } from './confiuracion-empresa.component';

describe('ConfiuracionEmpresaComponent', () => {
  let component: ConfiuracionEmpresaComponent;
  let fixture: ComponentFixture<ConfiuracionEmpresaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfiuracionEmpresaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfiuracionEmpresaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
