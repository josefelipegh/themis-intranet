import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatPaginator, MatSort} from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { GeneralService } from '../../../core/Services/general/general.service';


export interface ServiciosData {
    idc: number;
    id: number;
    imagen: string;
    nombre: string;
    descripcion: string;
    visible: boolean;
    
  }
  let listaservicios: any[] = [];
@Component({
  selector: 'app-MServicios',
  templateUrl: './modal-servicios.component.html',
  styleUrls: ['./modal-servicios.component.scss']
})

export class ModalServiciosComponent implements OnInit {
    displayedColumns: string[] = ['imagenServicio','nombre','descripcion','accion']
    dataSource: MatTableDataSource<ServiciosData>;
  titulo = '';
  datos: ServiciosData;
  listaServicios : any[];
  listaServicios2 : any[];
  catalogo : any[];
  resultado:any;
  idc:number ;
  lleno : boolean = true;
  existe: boolean = false;
  @ViewChild(MatPaginator) paginator : MatPaginator;
  @ViewChild(MatSort) sort : MatSort;
  public loading = false; //para mostrar los tres puntitos al incluir o modificar la imagen
  constructor(public dialogRef: MatDialogRef<ModalServiciosComponent>, public generalService: GeneralService, public snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.titulo = data.titulo;
      this.CargarTablaServicios(data.id)

   
  }
  
  ngOnInit() {

  }
  onNoClick() {
    this.dialogRef.close();
    listaservicios = [];
  }

  cambiarV(id,visible,idc){
      
    if(visible==true){
      this.loading = true;
      this.generalService.CambiarFalse('catalogo_servicio', id).then((result) => {
        
        this.CargarTablaServicios(idc);
        listaservicios=[];
        this.loading = false;
      },(err) => {
        console.log(err);
        this.loading = false;
      });   
    }
      else{
        this.loading = true;
        this.generalService.CambiarTrue('catalogo_servicio', id).then((result) => {
          this.CargarTablaServicios(idc);
          listaservicios=[];
          this.loading = false;
          },(err) => {
          console.log(err);
          this.loading = false;
        });   
      }
      
    }

    CargarTablaServicios(idc) {
      
      this.generalService.ObtenerUno('categoria', idc).then((result) => {
        let resultado : any = result
        if(resultado.data.catalogo_servicio.length==0){
          this.lleno = false
        }else{
          
                
        listaservicios = [];
        
        this.datos = null;
        for (let i = 0; i < resultado.data.catalogo_servicio.length; i++) {
          if(resultado.data.catalogo_servicio[i].estatus=='A'){
            this.existe=true;
          this.datos = {
            idc: resultado.data.id,
            id: resultado.data.catalogo_servicio[i].id,
            imagen: resultado.data.catalogo_servicio[i].imagen,
            nombre: resultado.data.catalogo_servicio[i].nombre,
            descripcion: resultado.data.catalogo_servicio[i].descripcion,
            visible: resultado.data.catalogo_servicio[i].visible
          };
          listaservicios.push(this.datos);
        }
        }
        if(this.existe==true){
          this.lleno=true;
        }else{
          this.lleno = false;
        }
        this.dataSource = new MatTableDataSource(listaservicios);
        this.dataSource.paginator = this.paginator
        this.dataSource.sort = this.sort
      }
      }, (err) => {
        console.log(err);
      });
  
    }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  
}