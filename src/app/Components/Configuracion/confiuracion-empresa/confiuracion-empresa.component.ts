import { Component, OnInit, ViewChild, ViewChildren, AfterViewInit, QueryList } from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource, MatDialogConfig} from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { GeneralService } from '../../core/Services/general/general.service';
import { FormControl, Validators } from '@angular/forms';
import { ModalEliminarComponent } from '../modal-eliminar/modal-eliminar.component';
import { MatSnackBar } from '@angular/material';
import { ModalServiciosComponent} from '../confiuracion-empresa/Modal-servicios/modal-servicios.component'
export interface PromocionData {
  idPromocion: Int32Array;
  imagenPromocion: string;
  nombrePromocion: string;
  DescPromocion: string;
  visible: boolean;
}

let listaPromocion: any[] = [];

export interface CarouselData {
  id: number;
  imagenCarousel: string;
  textoCarousel: string;
  estatus: string;
   
}
let carousel: any[] = [];


export interface CategoriaData {
  id: number;
  imagenCategoria: string;
  nombreCategoria: string;
  descripcionCategoria: string;
  visibleCategoria : boolean;
  estatus: string;
  servicios : any[];
}
let categoria: any[] = [];
let servicios: any[] = [];
export interface ServicioData {
  id: number;
  imagen: string;
  nombre: string;
  descripcion: string;
  servicios: any [];
  visible: boolean;
  
}
export interface IAdicionalData {
  id: number;
  imagenApp: string;
  textoApp: string;
  tituloApp: string;
  tituloE : string;
  
}
let IAdicional: any[] = [];
export interface IEmpresaData {
  id: number;
  imagenE: string;
  texto1: string;
  tituloE : string;
  texto2 : string;
    
}
let IEmpleado: any[] = [];
export interface NosotrosData {
  id: number;
  mision: string;
  vision: string;
  valor1: string;
  valor2 : string;    
  valor3 : string;
  imagenmi: string;
  imagenvi: string;
  imagenva: string;
}
let IEmpresa: any[] = [];
export interface EmpresaData {
  id: number;
  telefono1: number;
  telefono2: number;
  facebook: string;
  twitter : string;    
  instagram : string;
  correo: string;
  direccion: string;
}
let Nosotros: any[] = [];
export interface UsuarioData {
  id:number;
  imagenU:string; 
  nombreUsuario: string;
  visibilidad: boolean;
  rolUsuario:string;
}
let listaUsuario: any[] = [];
@Component({
  selector: 'app-confiuracion-empresa',
  templateUrl: './confiuracion-empresa.component.html',
  styleUrls: ['./confiuracion-empresa.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ConfiuracionEmpresaComponent implements OnInit {
  displayedColumns: string[] = ['imagenUsuario','nombreUsuario','rolUsuario','accion'];
  displayedColumns2: string[] = ['imagenCarousel','textoCarousel','eliminar'];
  displayedColumns3: string[] = ['imagenPromocion', 'nombrePromocion', 'DescPromocion', 'accion'];
  displayedColumns4: string[] = ['imagenCategoria','nombreCategoria','descripcionCategoria','accion','accion2']
  dataSource: MatTableDataSource<UsuarioData>;
  dataSource2: MatTableDataSource<CarouselData>;
  dataSource3: MatTableDataSource<PromocionData>;
  datasource4: MatTableDataSource<CategoriaData>;
  dataSource5: MatTableDataSource<ServicioData>
  imagenSeleccionada: any = [];
  imagenSeleccionada2: any = [];
  imagenSeleccionada3: any = [];
  imagenSeleccionada4: any = [];
  imagenSeleccionada5: any = [];
  imagenSeleccionada6: any = [];
  imagenDefecto: string = "assets/images/default.jpg";
  imagenDefecto2: string = "assets/images/default.jpg";
  imagenDefecto3: string = "assets/images/default.jpg";
  imagenDefecto4: string = "assets/images/default.jpg";
  imagenDefecto5: string = "assets/images/default.jpg";
  imagenDefecto6: string = "assets/images/default.jpg";
  imagen;
  imagen2;
  imagen3;
  imagen4;
  imagen5;
  imagen6;
  resultado: any;
  resultado2: any;
  resultado3: any;
  incluir: boolean = false;
  existeAd: boolean = false;
  existeNo: boolean = false;
  existeFo: boolean = false;
  datos: CarouselData;
  datos2: CategoriaData;
  datos3: IAdicionalData;
  datos4: NosotrosData;
  datos5: IEmpresaData;
  datos6: UsuarioData;
  datos7: ServicioData;
  datos8: PromocionData;
  datos9: EmpresaData;
  user: any;
  idA : number;
  idN : number;
  idE : number;
  public loading = false;
 

  @ViewChild('paginator') paginator : MatPaginator;
  @ViewChild('sort') sort : MatSort ;
  @ViewChild('paginator2') paginator2 : MatPaginator;
  @ViewChild('sort2') sort2 : MatSort ;
  @ViewChild('paginator3') paginator3 : MatPaginator;
  @ViewChild('sort3') sort3 : MatSort ;
  @ViewChild('paginator4') paginator4 : MatPaginator;
  @ViewChild('sort4') sort4 : MatSort ;
  @ViewChild('paginator5') paginator5 : MatPaginator;
  @ViewChild('sort5') sort5 : MatSort ;
    value="";
    value2="";
    value3="";
    valueInput(){
    this.value="";
    this.value2="";
    this.applyFilter(this.value);
    this.applyFilter2(this.value2);
    this.applyFilter3(this.value3);
   
  }
  


  constructor(public generalService: GeneralService,public dialog: MatDialog, public snackBar: MatSnackBar) { 
    
      
  }
  ngOnInit() {
    this.incluir = true;
      this.CargarIAdicional();
      this.CargarIEmpleado();
      this.CargarNosotros();
      this.CargarTablaCarousel();
      this.CargarTablaCategoria();
      this.CargarTablaEmpleados();
      this.CargarTablaPromociones();
      this.CargarEmpresa();
    
  }
RegistrarICarousel(){
  const datos = new FormData();
  
  if(this.imagenFormControl2.valid && this.textoFormControl2.valid){
    datos.append('imagen', this.imagenSeleccionada2);
    datos.append('texto', this.textoFormControl2.value);
    datos.append('estatus', 'A');
    this.loading = true;
  }
  this.generalService.RegistrarConImagen(datos, 'imagen_carrusel').then((result) => {
    this.imagenDefecto2 = "assets/images/default.jpg";
    this.CargarTablaCarousel();
    this.textoFormControl2.setValue("");
    this.openSnackBar('Registro exitoso!', "registrar");
    this.loading = false;
     
  },(err) => {
    console.log(err);
    this.loading = false;
  });

}
RegistrarIAdicional(){
  
  if(this.existeAd==false){
    const datos = new FormData();
  if(this.imagenFormControl3.valid && this.textoAppFormControl.valid && this.tituloAppFormControl && this.tituloEFormControl){
    datos.append('imagen_app', this.imagenSeleccionada3);
    datos.append('texto_app', this.textoAppFormControl.value);
    datos.append('titulo_equipo', this.tituloEFormControl.value);
    datos.append('titulo_app', this.tituloAppFormControl.value);
    this.loading = true;
  }
  this.generalService.RegistrarConImagen(datos, 'inicio_web').then((result) => {
    this.openSnackBar('Registro exitoso!', "registrar");
    this.loading = false;
     
  },(err) => {
    console.log(err);
    this.loading = false;
  });
  
}else{
  {
    if(this.imagenFormControl3.valid && this.textoAppFormControl.valid && this.tituloAppFormControl && this.tituloEFormControl){
      const datos = new FormData();
      datos.append('imagen_app', this.imagenSeleccionada3);
      datos.append('texto_app', this.textoAppFormControl.value);
      datos.append('titulo_equipo', this.tituloEFormControl.value);
      datos.append('titulo_app', this.tituloAppFormControl.value);
      
    this.loading = true;
            
      this.generalService.ActualizarConImagen(datos,'inicio_web',this.idA).then((result) => {

        this.generalService.ObtenerUno('inicio_web', IAdicional[0].id).then((result) => {
          this.resultado2 = result;
          this.loading = false;
          this.openSnackBar('Registro Actualizado!', "Actualizar");
          }, (err) => {
          this.loading = false;
          console.log(err);
        });

      }, (err) => {
        console.log(err);
        this.loading = false;
      });
    }

  }
}
  

}
RegistrarIEmpresa(){
  
  if(this.existeAd==false){
    const datos = new FormData();
    
  if(this.tituloEmFormControl.valid && this.texto1EFormControl.valid && this.texto2EFormControl.valid){
    datos.append('imagen_nosotros', this.imagenSeleccionada);
    datos.append('texto1_nosotros', this.texto1EFormControl.value);
    datos.append('titulo_nosotros', this.tituloEmFormControl.value);
    datos.append('texto2_nosotros', this.texto2EFormControl.value);
    this.loading = true;
    
  }
  this.generalService.RegistrarConImagen(datos, 'inicio_web').then((result) => {
    
    this.openSnackBar('Registro exitoso!', "registrar");
    this.loading = false;
     
  },(err) => {
    console.log(err);
    this.loading = false;
  });
  
}else{
  { 
    
    if(this.tituloEmFormControl.valid && this.texto1EFormControl.valid && this.texto2EFormControl.valid){
      const datos = new FormData();
      datos.append('imagen_nosotros', this.imagenSeleccionada);
      datos.append('texto1_nosotros', this.texto1EFormControl.value);
      datos.append('titulo_nosotros', this.tituloEmFormControl.value);
      datos.append('texto2_nosotros', this.texto2EFormControl.value);
      
    this.loading = true;
            
      this.generalService.ActualizarConImagen(datos,'inicio_web',this.idA).then((result) => {

        this.generalService.ObtenerUno('inicio_web', this.idA).then((result) => {
          this.resultado2 = result;
          this.loading = false;
          this.openSnackBar('Registro Actualizado!', "Actualizar");
          }, (err) => {
          this.loading = false;
          console.log(err);
        });

      }, (err) => {
        console.log(err);
        this.loading = false;
      });
    }

  }
}
   
}
RegistrarMision(){
  
  if(this.existeNo==false){
    const datos = new FormData();
  if(this.misionFormControl.valid){
    datos.append('mision', this.misionFormControl.value);
    datos.append('imagen_mision', this.imagenSeleccionada4);
    this.loading = true;
  }
  this.generalService.RegistrarConImagen(datos, 'filosofia').then((result) => {
    this.openSnackBar('Registro exitoso!', "registrar");
    console.log(result);
    this.loading = false;
     
  },(err) => {
    console.log(err);
    this.loading = false;
  });
this.CargarNosotros();
}else{
  {
    const datos = new FormData();
    if(this.misionFormControl.valid){
      datos.append('mision', this.misionFormControl.value);
      datos.append('imagen_mision', this.imagenSeleccionada4)
          
      this.loading = true;
            
      this.generalService.ActualizarConImagen(datos,'filosofia',this.idN).then((result) => {

        this.generalService.ObtenerUno('filosofia', this.idN).then((result) => {
          this.resultado2 = result;
          this.loading = false;
          this.openSnackBar('Registro Actualizado!', "Actualizar");
          }, (err) => {
          this.loading = false;
          console.log(err);
        });

      }, (err) => {
        console.log(err);
        this.loading = false;
      });
    }

  }
}
 
}
RegistrarVision(){
  
  if(this.existeNo==false){
    const datos = new FormData();
  if(this.visionFormControl.valid){
    datos.append('vision', this.visionFormControl.value);
    datos.append('imagen_vision', this.imagenSeleccionada5);
    this.loading = true;
  }
  this.generalService.RegistrarConImagen(datos, 'filosofia').then((result) => {
    this.openSnackBar('Registro exitoso!', "registrar");
    console.log(result);
    this.loading = false;
     
  },(err) => {
    console.log(err);
    this.loading = false;
  });
this.CargarNosotros();
}else{
  {
    const datos = new FormData();
    if(this.misionFormControl.valid){
      datos.append('vision', this.visionFormControl.value);
      datos.append('imagen_vision', this.imagenSeleccionada5)
          
      this.loading = true;
            
      this.generalService.ActualizarConImagen(datos,'filosofia',this.idN).then((result) => {

        this.generalService.ObtenerUno('filosofia', this.idN).then((result) => {
          this.resultado2 = result;
          this.loading = false;
          this.openSnackBar('Registro Actualizado!', "Actualizar");
          }, (err) => {
          this.loading = false;
          console.log(err);
        });

      }, (err) => {
        console.log(err);
        this.loading = false;
      });
    }

  }
}
}
RegistrarValores(){
  
  if(this.existeNo==false){
    const datos = new FormData();
  if(this.valor1FormControl.valid && this.valor2FormControl.valid && this.valor3FormControl.valid){
    datos.append('valor1', this.valor1FormControl.value);
    datos.append('valor2', this.valor2FormControl.value);
    datos.append('valor3', this.valor3FormControl.value);
    datos.append('imagen_valores', this.imagenSeleccionada6);
    this.loading = true;
  }
  this.generalService.RegistrarConImagen(datos, 'filosofia').then((result) => {
    this.openSnackBar('Registro exitoso!', "registrar");
    console.log(result);
    this.loading = false;
     
  },(err) => {
    console.log(err);
    this.loading = false;
  });
this.CargarNosotros();
}else{
  {
    const datos = new FormData();
    if(this.valor1FormControl.valid && this.valor2FormControl.valid && this.valor3FormControl.valid){
      datos.append('valor1', this.valor1FormControl.value);
      datos.append('valor2', this.valor2FormControl.value);
      datos.append('valor3', this.valor3FormControl.value); 
      datos.append('imagen_valores', this.imagenSeleccionada6);
          
      this.loading = true;
            
      this.generalService.ActualizarConImagen(datos,'filosofia',this.idN).then((result) => {

        this.generalService.ObtenerUno('filosofia', this.idN).then((result) => {
          this.resultado2 = result;
          this.loading = false;
          this.openSnackBar('Registro Actualizado!', "Actualizar");
          }, (err) => {
          this.loading = false;
          console.log(err);
        });

      }, (err) => {
        console.log(err);
        this.loading = false;
      });
    }

  }
}
}
RegistrarFooter(){
  
  if(this.existeFo==false){
    const datos = new FormData();
  if(this.instagramFormControl.valid && this.facebookFormControl.valid && this.twitterFormControl && this.telefono1FormControl && this.telefono2FormControl && this.correoFormControl && this.direccionFormControl){
    datos.append('instagram', this.instagramFormControl.value);
    datos.append('facebook', this.facebookFormControl.value);
    datos.append('twitter', this.twitterFormControl.value);
    datos.append('telefono1', this.telefono1FormControl.value);
    datos.append('telefono2', this.telefono2FormControl.value);
    datos.append('correo', this.correoFormControl.value);
    datos.append('direccion', this.direccionFormControl.value);
    this.loading = true;
  }
  this.generalService.Registrar(datos, 'empresa').then((result) => {
    this.openSnackBar('Registro exitoso!', "registrar");
    console.log(result);
    this.loading = false;
     
  },(err) => {
    console.log(err);
    this.loading = false;
  });
this.CargarEmpresa();
}else{
  {
    const datos = new FormData();
    if(this.instagramFormControl.valid && this.facebookFormControl.valid && this.twitterFormControl && this.telefono1FormControl && this.telefono2FormControl && this.correoFormControl && this.direccionFormControl){
      datos.append('instagram', this.instagramFormControl.value);
    datos.append('facebook', this.facebookFormControl.value);
    datos.append('twitter', this.twitterFormControl.value);
    datos.append('telefono1', this.telefono1FormControl.value);
    datos.append('telefono2', this.telefono2FormControl.value);
    datos.append('correo', this.correoFormControl.value);
    datos.append('direccion', this.direccionFormControl.value);
          
      this.loading = true;
            
      this.generalService.ActualizarConImagen(datos,'empresa',this.idE).then((result) => {

        this.generalService.ObtenerUno('empresa', this.idE).then((result) => {
          this.resultado2 = result;
          this.loading = false;
          this.openSnackBar('Registro Actualizado!', "Actualizar");
          }, (err) => {
          this.loading = false;
          console.log(err);
        });

      }, (err) => {
        console.log(err);
        this.loading = false;
      });
    }

  }
}
}
OpenDialogServicios(row){
    
  this.dataSource5 = new MatTableDataSource(servicios)
  this.dataSource5.paginator = this.paginator5
  this.dataSource5.sort = this.sort5
  let dialogRef = this.dialog.open(ModalServiciosComponent, {
    width: '80%',
    data: {id: row.id, data2: row.servicios, dataS: this.dataSource5, titulo: row.nombreCategoria, pag: this.dataSource5.paginator, sor: this.dataSource5.sort}
    });
 }
cambiarVCategoria(id,visible){
      
  if(visible==true){
    this.loading = true;
    this.generalService.CambiarFalse('categoria', id).then((result) => {
      this.CargarTablaCategoria();
      
      this.loading = false;
    },(err) => {
      console.log(err);
      this.loading = false;
    });   
  }
    else{
      this.loading = true;
      
      this.generalService.CambiarTrue('categoria', id).then((result) => {
        this.CargarTablaCategoria();
        this.loading = false;
        
       
      },(err) => {
        console.log(err);
        this.loading = false;
      });   
    }
    
  }
  cambiarVEmpleado(id,visible){
      
    if(visible==true){
      this.loading = true;
      this.generalService.CambiarFalse('empleado', id).then((result) => {
        this.CargarTablaEmpleados();
        
        this.loading = false;
      },(err) => {
        console.log(err);
        this.loading = false;
      });   
    }
      else{
        this.loading = true;
        this.generalService.CambiarTrue('empleado', id).then((result) => {
          this.CargarTablaEmpleados();
          this.loading = false;
        },(err) => {
          console.log(err);
          this.loading = false;
        });   
      }
      
    }
    cambiarVPromocion(id,visible){
      
      if(visible==true){
        this.loading = true;
        this.generalService.CambiarFalse('promocion', id).then((result) => {
          this.CargarTablaPromociones();
          
          this.loading = false;
        },(err) => {
          console.log(err);
          this.loading = false;
        });   
      }
        else{
          this.loading = true;
          this.generalService.CambiarTrue('promocion', id).then((result) => {
            this.CargarTablaPromociones();
            this.loading = false;
          },(err) => {
            console.log(err);
            this.loading = false;
          });   
        }
        
      }
openSnackBar(message: string, action: string) {
  this.snackBar.open(message, action, {
    duration: 1000,
  });
}

imagenFormControl = new FormControl('',
[
  Validators.required
]);
imagen4FormControl = new FormControl('',
[
  Validators.required
]);
imagen5FormControl = new FormControl('',
[
  Validators.required
]);
imagen6FormControl = new FormControl('',
[
  Validators.required
]);

  imagenFormControl2 = new FormControl('',
    [
      Validators.required
    ]);
    imagenFormControl3 = new FormControl('',
    [
      Validators.required
    ]);
    textoFormControl2 = new FormControl('',
    [
      Validators.required
    ]
    );
    tituloEFormControl = new FormControl('',
    [
      Validators.required
    ]
    );
    tituloAppFormControl = new FormControl('',
    [
      Validators.required
    ]
    );
    textoAppFormControl = new FormControl('',
    [
      Validators.required
    ]
    );
    misionFormControl = new FormControl('',
    [
      Validators.required
    ]
    );
    visionFormControl = new FormControl('',
    [
      Validators.required
    ]
    );
    valor1FormControl = new FormControl('',
    [
      Validators.required
    ]
    );
    valor2FormControl = new FormControl('',
    [
      Validators.required
    ]
    );
    valor3FormControl = new FormControl('',
    [
      Validators.required
    ]
    );
    tituloEmFormControl = new FormControl('',
    [
      Validators.required
    ]
    );
    texto1EFormControl = new FormControl('',
    [
      Validators.required
    ]
    );
    texto2EFormControl = new FormControl('',
    [
      Validators.required
    ]
    );
    facebookFormControl = new FormControl('',
    [
      Validators.required
    ]
    );
    instagramFormControl = new FormControl('',
    [
      Validators.required
    ]
    );
    twitterFormControl = new FormControl('',
    [
      Validators.required
    ]
    );
    telefono1FormControl = new FormControl('',
    [
      Validators.required
    ]
    );
    telefono2FormControl = new FormControl('',
    [
      Validators.required
    ]
    );
    correoFormControl = new FormControl('',
    [
      Validators.required
    ]
    );
    direccionFormControl = new FormControl('',
    [
      Validators.required
    ]
    );
    imagenEntrada(img: FileList) {
      this.imagenSeleccionada = img.item(0);
      //Para mostrar un preview de la imagen
      var lector = new FileReader();
      lector.onload = (event: any) => {
        this.imagen = event.target.result;
        this.imagenDefecto = event.target.result;
      }
      lector.readAsDataURL(this.imagenSeleccionada);
    }
  imagenEntrada2(img2: FileList){
    this.imagenSeleccionada2 = img2.item(0);
    //Para mostrar un preview de la imagen
    var lector = new FileReader();
    lector.onload = (event: any) => {
      this.imagen2 = event.target.result;
      this.imagenDefecto2 = event.target.result;
    }
    lector.readAsDataURL(this.imagenSeleccionada2);
  }
  imagenEntrada3(img3: FileList){
    this.imagenSeleccionada3 = img3.item(0);
    
    //Para mostrar un preview de la imagen
    var lector = new FileReader();
    lector.onload = (event: any) => {
      this.imagen3 = event.target.result;
      this.imagenDefecto3 = event.target.result;
    }
    lector.readAsDataURL(this.imagenSeleccionada3);
  }
  imagenEntrada4(img4: FileList){
    this.imagenSeleccionada4 = img4.item(0);
    
    //Para mostrar un preview de la imagen
    var lector = new FileReader();
    lector.onload = (event: any) => {
      this.imagen4 = event.target.result;
      this.imagenDefecto4 = event.target.result;
    }
    lector.readAsDataURL(this.imagenSeleccionada4);
  }
  imagenEntrada5(img5: FileList){
    this.imagenSeleccionada5 = img5.item(0);
    
    //Para mostrar un preview de la imagen
    var lector = new FileReader();
    lector.onload = (event: any) => {
      this.imagen5 = event.target.result;
      this.imagenDefecto5 = event.target.result;
    }
    lector.readAsDataURL(this.imagenSeleccionada5);
  }
  imagenEntrada6(img6: FileList){
    this.imagenSeleccionada6 = img6.item(0);
    
    //Para mostrar un preview de la imagen
    var lector = new FileReader();
    lector.onload = (event: any) => {
      this.imagen6 = event.target.result;
      this.imagenDefecto6 = event.target.result;
    }
    lector.readAsDataURL(this.imagenSeleccionada6);
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  applyFilter2(filterValue: string) {
    this.datasource4.filter = filterValue.trim().toLowerCase();

    if (this.datasource4.paginator) {
      this.datasource4.paginator.firstPage();
    }
  }
  applyFilter3(filterValue: string) {
    this.dataSource3.filter = filterValue.trim().toLowerCase();

    if (this.dataSource3.paginator) {
      this.dataSource3.paginator.firstPage();
    }
  }
  Estatus(est) {
    let estatus;
    return estatus = est == 'A' ? 'Activo' : 'Inactivo';
  }
  CargarIAdicional() {
    IAdicional = [];
    this.generalService.Obtenertodos('inicio_web').then((result) => {
      this.resultado = result;
      for (let i = 0; i < this.resultado.data.length; i++) {
        
        this.datos3={
          id: this.resultado.data[i].id,
          tituloApp: this.resultado.data[i].titulo_app,
          tituloE: this.resultado.data[i].titulo_equipo,
          textoApp: this.resultado.data[i].texto_app,
          imagenApp: this.resultado.data[i].imagen_app
        };
        if(this.datos3.id==null){
        this.existeAd = false;
        }else
        if(this.datos3.imagenApp==null){
          this.llenarCIAdicional(this.datos3);
          this.existeAd=true;
          this.idA = this.datos3.id;
        }else{
          this.llenarCIAdicional(this.datos3);
          this.existeAd=true;
          this.idA = this.datos3.id;
          this.imagenDefecto3 = this.datos3.imagenApp;
        }
        
        IAdicional.push(this.datos3)
      }
    }, (err) => {
      console.log(err);
    });
  }
  CargarNosotros() {
    
    this.generalService.Obtenertodos('filosofia').then((result) => {
      this.resultado = result;
      for (let i = 0; i < this.resultado.data.length; i++) {
        
        this.datos4={
          id: this.resultado.data[i].id,
          mision: this.resultado.data[i].mision,
          vision: this.resultado.data[i].vision,
          valor1: this.resultado.data[i].valor1,
          valor2: this.resultado.data[i].valor2,
          valor3: this.resultado.data[i].valor3,
          imagenmi: this.resultado.data[i].imagen_mision,
          imagenvi: this.resultado.data[i].imagen_vision,
          imagenva: this.resultado.data[i].imagen_valores
        };
        if(this.datos4.id==null){
        this.existeNo = false;
        }else {
          this.existeNo=true;
          this.idN = this.datos4.id;
          this.llenarCNosotros(this.datos4);
        }
        if(this.datos4.imagenmi!= null){
          this.imagenDefecto4 = this.datos4.imagenmi;
        }
        if(this.datos4.imagenvi!=null){
          this.imagenDefecto5 = this.datos4.imagenvi;
        }
        if(this.datos4.imagenva != null){
          this.imagenDefecto6 = this.datos4.imagenva;
        }
        Nosotros.push(this.datos4)
      }
    }, (err) => {
      console.log(err);
    });
  }
  CargarEmpresa() {
    IEmpresa = [];
    this.generalService.Obtenertodos('empresa').then((result) => {
      this.resultado = result;
      for (let i = 0; i < this.resultado.data.length; i++) {
        
        this.datos9={
          id: this.resultado.data[i].id,
          telefono1: this.resultado.data[i].telefono1,
          telefono2: this.resultado.data[i].telefono2,
          instagram: this.resultado.data[i].instagram,
          twitter: this.resultado.data[i].twitter,
          facebook: this.resultado.data[i].facebook,
          correo: this.resultado.data[i].correo,
          direccion: this.resultado.data[i].direccion          
        };
        if(this.datos9.id==null){
        this.existeFo = false;
        }else{
          this.existeFo=true;
          this.idE = this.datos9.id;
          this.llenarCFooter(this.datos9);
        }
        
        Nosotros.push(this.datos4)
      }
    }, (err) => {
      console.log(err);
    });
  }
  CargarIEmpleado() {
    IEmpleado = [];
    this.generalService.Obtenertodos('inicio_web').then((result) => {
      this.resultado = result;
      for (let i = 0; i < this.resultado.data.length; i++) {
        
        this.datos5={
          id: this.resultado.data[i].id,
          texto1: this.resultado.data[i].texto1_nosotros,
          texto2: this.resultado.data[i].texto2_nosotros,
          tituloE: this.resultado.data[i].titulo_nosotros,
          imagenE: this.resultado.data[i].imagen_nosotros
        };
        if(this.existeAd==true && this.datos5.imagenE!=null){
          this.llenarCIEmpresa(this.datos5);
          this.imagenDefecto = this.datos5.imagenE;
          
        }else{
          this.llenarCIEmpresa(this.datos5);
          
        }     
        IEmpleado.push(this.datos5)
      }
    }, (err) => {
      console.log(err);
    });
  }
  llenarCNosotros(datos4){
    this.misionFormControl.setValue(datos4.mision);
    this.visionFormControl.setValue(datos4.vision);
    this.valor1FormControl.setValue(datos4.valor1);
    this.valor2FormControl.setValue(datos4.valor2);
    this.valor3FormControl.setValue(datos4.valor3);
  }
  llenarCFooter(datos9){
    this.telefono1FormControl.setValue(datos9.telefono1);
    this.telefono2FormControl.setValue(datos9.telefono2);
    this.correoFormControl.setValue(datos9.correo);
    this.instagramFormControl.setValue(datos9.instagram);
    this.facebookFormControl.setValue(datos9.facebook);
    this.twitterFormControl.setValue(datos9.twitter);
    this.direccionFormControl.setValue(datos9.direccion);
  }
  llenarCIAdicional(datos3){
    this.tituloAppFormControl.setValue(datos3.tituloApp);
    this.tituloEFormControl.setValue(datos3.tituloE);
    this.textoAppFormControl.setValue(datos3.textoApp);
    }
  llenarCIEmpresa(datos5){
    this.tituloEmFormControl.setValue(datos5.tituloE);
    this.texto1EFormControl.setValue(datos5.texto1);
    this.texto2EFormControl.setValue(datos5.texto2);
  }
  CargarTablaCarousel() {
    this.generalService.Obtenertodos('imagen_carrusel').then((result) => {
      this.resultado = result;
      carousel = [];
      for (let i = 0; i < this.resultado.data.length; i++) {
        let est = this.Estatus(this.resultado.data[i].estatus);
        this.datos = {
          id: this.resultado.data[i].id,
          imagenCarousel: this.resultado.data[i].imagen,
          textoCarousel: this.resultado.data[i].texto,
          estatus:this.resultado.data[i].texto
        };
        carousel.push(this.datos);
      }
      this.dataSource2 = new MatTableDataSource(carousel);
      this.dataSource2.paginator = this.paginator2
      this.dataSource2.sort = this.sort2


    }, (err) => {
      console.log(err);
    });

  }
  CargarTablaEmpleados() {
    this.generalService.Obtenertodos('vista_empleado').then((result) => {
      this.resultado = result;
      listaUsuario = [];
      for (let i = 0; i < this.resultado.data.length; i++) {
        
        this.datos6 = {
          id: this.resultado.data[i].empleado_id,
          imagenU: this.resultado.data[i].usuario_foto,
          nombreUsuario: this.resultado.data[i].empleado_nombre + " "+ this.resultado.data[i].empleado_apellido,
          rolUsuario: this.resultado.data[i].rol_usuario,
          visibilidad: this.resultado.data[i].visibilidad
        };
        listaUsuario.push(this.datos6);
      }
      this.dataSource = new MatTableDataSource(listaUsuario);
      this.dataSource.paginator = this.paginator
    this.dataSource.sort = this.sort

    }, (err) => {
      console.log(err);
    });

  }
  CargarTablaPromociones() {
    this.generalService.Obtenertodos('promocion').then((result) => {
      this.resultado3 = result;
      listaPromocion = [];
      
      for (let i = 0; i < this.resultado3.data.length; i++) {
        
        this.datos8 = {
          idPromocion: this.resultado3.data[i].id,
          imagenPromocion: this.resultado3.data[i].imagen,
          DescPromocion: this.resultado3.data[i].descripcion,
          nombrePromocion: this.resultado3.data[i].nombre,
          visible: this.resultado3.data[i].visible
        };
        listaPromocion.push(this.datos8);
      }
      
      this.dataSource3 = new MatTableDataSource(listaPromocion);
      this.dataSource3.paginator = this.paginator3
    this.dataSource3.sort = this.sort3

    }, (err) => {
      console.log(err);
    });

  }
  CargarTablaCategoria() {
    this.generalService.Obtenertodos('categoria').then((result) => {
      this.resultado2 = result;
      categoria = [];
      for (let i = 0; i < this.resultado2.data.length; i++) {
        let est = this.Estatus(this.resultado2.data[i].estatus);
        this.datos2 = {
          id: this.resultado2.data[i].id,
          imagenCategoria: this.resultado2.data[i].imagen,
          nombreCategoria: this.resultado2.data[i].nombre,
          descripcionCategoria: this.resultado2.data[i].descripcion,
          visibleCategoria: this.resultado2.data[i].visible,
          estatus:this.resultado2.data[i].estatus,
          servicios: this.resultado2.data[i].catalogo_servicio
          
        };
        
        categoria.push(this.datos2);
      }
      this.datasource4 = new MatTableDataSource(categoria);
      this.datasource4.paginator = this.paginator4;
      this.datasource4.sort = this.sort4;

    }, (err) => {
      console.log(err);
    });

  }
  
  openDialogEliminar(row) 
  {
    // constante creada para un nuevo modal.
    const dialogConfig = new MatDialogConfig();
    // configuracion del modal con sus caracteristicas, ancho...
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    // PARA PASAR LOS DATOS AL DIALOG PREGUNTA ELIMINAR 
    dialogConfig.data = {
      id: row,
      nombreMetodo: 'imagen_carrusel'
    };
    // constante donde se guardan los datos que se ingresan en el modal, los parametros
    // ModalTipoActuacionComponent hace el llamado al componente modal y dialogConfig
    // las caracteristicas del mismo.
    const dialogRef = this.dialog.open(ModalEliminarComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD 
    dialogRef.afterClosed().subscribe(
      data => {
        if(data){
        //this.dataSource2.data.find(x => x.id == data).estatus = 'Inactivo';
        this.CargarTablaCarousel()
        }
      }
    );
  }
}