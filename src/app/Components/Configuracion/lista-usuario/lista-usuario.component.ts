import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { GeneralService } from '../../core/Services/general/general.service';
import { Router, NavigationExtras } from '@angular/router';
import { ModalPreguntaEliminarComponent } from '../../shared/modal-pregunta-eliminar/modal-pregunta-eliminar.component';

export interface UsuarioData {
  idUsuario: Int32Array;
  imagenUsuario: string;
  nombreUsuario: string;
  cedulaUsuario: string;
  correoUsuario: string;
  rolUsuario: string;
}

let listaEmpleado: any[] = [];


@Component({
  selector: 'app-lista-usuario',
  templateUrl: './lista-usuario.component.html',
  styleUrls: ['./lista-usuario.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ListaUsuarioComponent implements OnInit {
 
  resultado: any;
  resultado2: any;
  usuarioEmpleado: any; //para pasar los datos del empleado a la vista del empleado
  empleado: UsuarioData;
  displayedColumns: string[] = ['ver', 'imagenUsuario', 'nombreUsuario', 'cedulaUsuario', 'correoUsuario', 'rolUsuario', 'eliminar'];
  dataSource: MatTableDataSource<UsuarioData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  value = "";
  valueInput() {
    this.value = "";
    this.applyFilter(this.value);
  }

  constructor(public dialog: MatDialog, public generalService: GeneralService, private _router: Router) {
    // Assign the data to the data source for the table to render
    this.CargarTabla();
  }

  ngOnInit() {}

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  // METEDO QUE OBTIENE UNA LISTA DE empleados Y SE CARGA EN LA TABLA
  CargarTabla() {
    this.generalService.Obtenertodos('vista_empleado').then((result) => {
      this.resultado = result;
      listaEmpleado = [];
      for (let i = 0; i < this.resultado.data.length; i++) {
        this.empleado = {
          idUsuario: this.resultado.data[i].empleado_id,
          imagenUsuario: this.resultado.data[i].usuario_foto,
          nombreUsuario: this.resultado.data[i].empleado_nombre + ' ' + this.resultado.data[i].empleado_apellido,
          cedulaUsuario: this.resultado.data[i].empleado_cedula,
          correoUsuario: this.resultado.data[i].correo_usuario,
          rolUsuario: this.resultado.data[i].rol_usuario,
        };
        listaEmpleado.push(this.empleado);
      }
      this.dataSource = new MatTableDataSource(listaEmpleado);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, (err) => {
      console.log(err);
    });

  }

}
