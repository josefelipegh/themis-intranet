import { Component, OnInit,Inject } from '@angular/core';
import {MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { EmpleadoComponent } from '../empleado/empleado.component';



@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.css']
})
export class EmpresaComponent implements OnInit {

  RedSocial: string[] = ['Facebook','Instagram','Skype', 'Twitter'];

  constructor(public dialog: MatDialog) { }

  openDialog() {
    this.dialog.open(EmpleadoComponent);
  }

  ngOnInit() {
  }

}


