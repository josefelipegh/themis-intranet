import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-galeria-imagenes',
  templateUrl: './galeria-imagenes.component.html',
  styleUrls: ['./galeria-imagenes.component.scss']
})
export class GaleriaImagenesComponent implements OnInit {

  opciones = [
    {value: 'nac-0', viewValue: 'Inicio'},
    {value: 'nac-1', viewValue: 'Servicios'}
  ];
  pictures = [
    {
      id: 1,
      title: 'Deportivas',
      img: 'assets/images/big/img1.jpg'
    },
    {
      id: 2,
      title: 'Paisajes',
      img: 'assets/images/big/img2.jpg'
    },
    {
      id: 3,
      title: '',
      img: 'assets/images/big/img3.jpg'
    },
    {
      id: 4,
      title: 'Abstract design',
      img: 'assets/images/big/img4.jpg'
    },
    {
      id: 5,
      title: 'Tech',
      img: 'assets/images/big/img5.jpg'
    },
    {
      id: 6,
      title: 'Nightlife',
      img: 'assets/images/big/img6.jpg'
    },
  ];
  constructor() { }

  ngOnInit() {
  }



}
