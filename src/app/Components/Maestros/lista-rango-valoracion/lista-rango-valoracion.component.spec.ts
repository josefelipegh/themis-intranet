import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaRangoValoracionComponent } from './lista-rango-valoracion.component';

describe('ListaRangoValoracionComponent', () => {
  let component: ListaRangoValoracionComponent;
  let fixture: ComponentFixture<ListaRangoValoracionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaRangoValoracionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaRangoValoracionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
