import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { GeneralService } from '../../../core/Services/general/general.service';

@Component({
  selector: 'app-modal-rango-valoracion',
  templateUrl: './modal-rango-valoracion.component.html',
  styleUrls: ['./modal-rango-valoracion.component.scss']
})
export class ModalRangoValoracionComponent implements OnInit {
  [x: string]: any;

   
//codigo
descripcionFormControl = new FormControl();
onCommentChange() {
  console.log(this.descripcionFormControl.value);
}
//codigo

form_descripcion = { valor: '',  descripcion: '',  estatus: 'A' };
incluir: boolean = false;
id;
idCat;


constructor(public dialogRef: MatDialogRef<ModalRangoValoracionComponent>,public generalService: GeneralService, public snackBar: MatSnackBar,
  @Inject(MAT_DIALOG_DATA) public data: any) {

    if (data) {
      this.valorFormControl.setValue(data.valor);
      this.descripcionFormControl.setValue(data.descripcion);
      this.id = data.id;
    } else {
      this.incluir = true;
    }

   }
   valorFormControl = new FormControl('', [
    Validators.required
  ]);




  Registrar() {
    this.form_descripcion.valor = this.valorFormControl.value;
    //agregado
    this.form_descripcion.descripcion = this.descripcionFormControl.value;
    if (this.valorFormControl.valid) {
      this.generalService.Registrar(this.form_descripcion, 'rango_valoracion').then((result) => {
        this.openSnackBar('Registro Exitoso!', "registrar");
      }, (err) => {
        console.log(err);
      });
      this.dialogRef.close(this.form_descripcion);
    }
  }

  Actualizar() {

    if (this.valorFormControl.valid) {
      this.form_descripcion.valor = this.valorFormControl.value;
      this.form_descripcion.descripcion = this.descripcionFormControl.value;
      this.generalService.Actualizar(this.form_descripcion, 'rango_valoracion', this.id).then((result) => {
        this.openSnackBar('Registro Actualizado!', "Actualizar");
      }, (err) => {
        console.log(err);
      });
      this.data.valor = this.valorFormControl.value
      this.data.descripcion = this.descripcionFormControl.value
      this.dialogRef.close(this.data);
    }
  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }
ngOnInit() {
}
onNoClick() {
  this.dialogRef.close();
}
}
