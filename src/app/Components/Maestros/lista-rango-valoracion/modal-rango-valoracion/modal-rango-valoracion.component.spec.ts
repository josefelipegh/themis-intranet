import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalRangoValoracionComponent } from './modal-rango-valoracion.component';

describe('ModalRangoValoracionComponent', () => {
  let component: ModalRangoValoracionComponent;
  let fixture: ComponentFixture<ModalRangoValoracionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalRangoValoracionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalRangoValoracionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
