import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalTipoGarantiaComponent } from './modal-tipo-garantia.component';

describe('ModalTipoGarantiaComponent', () => {
  let component: ModalTipoGarantiaComponent;
  let fixture: ComponentFixture<ModalTipoGarantiaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalTipoGarantiaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalTipoGarantiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
