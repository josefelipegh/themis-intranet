import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { GeneralService } from '../../../core/Services/general/general.service';


@Component({
  selector: 'app-modal-caracteristica',
  templateUrl: './modal-caracteristica.component.html',
  styleUrls: ['./modal-caracteristica.component.scss']
})
export class ModalCaracteristicaComponent implements OnInit {

  caracteristica: any[] = [];
  resultado: any;
  form_descripcion = { nombre: '', descripcion: '', caracteristica_base_id: 0, estatus: 'A' };
  incluir: boolean = false;
  id;
  caracteristica_base_id: number;

  descripcionFormControl = new FormControl('', [
    Validators.required
  ]);
  nombreFormControl = new FormControl('', [
    Validators.required
  ]);
  caracteristicaFormControl = new FormControl('', [
    Validators.required
  ]);
  constructor(public dialogRef: MatDialogRef<ModalCaracteristicaComponent>,
    public generalService: GeneralService, public snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    // PARA CARGAR LA CAJA DE TEXTO CUANDO SE VA ACTUALIZAR DATOS
    this.CargarCombo();
    if (data) {
      this.id = data.idCarac;
      this.nombreFormControl.setValue(data.nombreCarac);
      this.descripcionFormControl.setValue(data.descripcion);
      this.caracteristicaFormControl.setValue(data.nombreBase);
      console.log(data);
    } else {
      this.incluir = true;
    }
  }

  ObtenerIdCar(id) {
    this.caracteristica_base_id = id;
  }


  ngOnInit() {
  }
  // CERRAR EL MODAL
  onNoClick() {
    this.dialogRef.close()
  }
  // REGISTRAR UN tipo actuacion EN LA BD
  Registrar() {
    this.form_descripcion.nombre = this.nombreFormControl.value;
    this.form_descripcion.descripcion = this.descripcionFormControl.value;
    this.form_descripcion.caracteristica_base_id = this.caracteristica_base_id;
    console.log(this.form_descripcion);
    if (this.nombreFormControl.valid && this.descripcionFormControl.valid) {
      this.generalService.Registrar(this.form_descripcion, 'caracteristica').then((result) => {
        this.dialogRef.close(this.form_descripcion);
        this.openSnackBar('Registro exitoso!', "Registrar");
      }, (err) => {
        console.log(err);
      });
    }
  }
  // ACTUALIZAR NOMBRE DEL valor caracteristica EN LA BD
  Actualizar() {

    if (this.nombreFormControl.valid) {
      this.form_descripcion.nombre = this.nombreFormControl.value;
      this.form_descripcion.descripcion = this.descripcionFormControl.value;
      this.generalService.Actualizar(this.form_descripcion, 'caracteristica', this.id).then((result) => {
        this.openSnackBar('Registro Actualizado!', "Actualizar");
      }, (err) => {
        console.log(err);
      });
      this.data.nombre = this.nombreFormControl.value;
      this.data.descripcion = this.descripcionFormControl.value;
      this.dialogRef.close(this.data);
    }
  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }

  CargarCombo() {
    this.generalService.Obtenertodos('caracteristica_base').then((result) => {
      this.resultado = result;
      for (let i = 0; i < this.resultado.data.length; i++) {
        this.caracteristica.push({
          id: this.resultado.data[i].id,
          nombre: this.resultado.data[i].nombre,
        });
      }
    }, (err) => {
      console.log(err);
    });
  }



}