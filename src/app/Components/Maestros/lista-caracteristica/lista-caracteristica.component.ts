import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { ModalCaracteristicaComponent } from './modal-caracteristica/modal-caracteristica.component';
import { GeneralService } from '../../core/Services/general/general.service';
import { ModalPreguntaEliminarComponent } from '../../shared/modal-pregunta-eliminar/modal-pregunta-eliminar.component';
import { Location } from '@angular/common';

export interface TablaData {
  idCarac: number;
  idBase: number;
  nombreCarac: string;
  nombreBase: string;
  descripcion: string;
  estatus: string;
  
}

// Array declarado
let Caracteristica: any[] = [];

@Component({
  selector: 'app-lista-caracteristica',
  templateUrl: './lista-caracteristica.component.html',
  styleUrls: ['./lista-caracteristica.component.scss'],

  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ListaCaracteristicaComponent implements OnInit {

  displayedColumns: string[] = ['ver','nombre', 'estatus','eliminar'];
  dataSource: MatTableDataSource<TablaData>;
  resultado: any;
  datos: TablaData;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  value = '';
  valueInput() {
    this.value = '';
    this.applyFilter(this.value);
  }
  constructor(public dialog: MatDialog, public generalService: GeneralService,
    private location: Location)
   {
    this.CargarTabla();

   }

   // esto no se toca. Yii
   ngOnInit() {

  }

  openDialogRegistrar() // abre una ventana modal para el registro de nuevos tipo Actuacion
  {
    // constante creada para un nuevo modal.
    const dialogConfig = new MatDialogConfig();
    // configuracion del modal con sus caracteristicas, ancho...
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    // constante donde se guardan los datos que se ingresan en el modal, los parametros
    // ModalTipoActuacionComponent hace el llamado al componente modal y dialogConfig
    // las caracteristicas del mismo.
    const dialogRef = this.dialog.open(ModalCaracteristicaComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD, luego de ser cerrado el modal.
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.CargarTabla();
        }
      }
    );
  }

  openDialogActualizar(row) // abre una ventana modal
  {
    // constante creada para un nuevo modal.
    const dialogConfig = new MatDialogConfig();
    // configuracion del modal con sus caracteristicas, ancho...
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    // PARA PASAR LOS DATOS AL DIALOG
    dialogConfig.data = row;
    // constante donde se guardan los datos que se ingresan en el modal, los parametros
    // ModalTipoActuacionComponent hace el llamado al componente modal y dialogConfig
    // las caracteristicas del mismo.
    const dialogRef = this.dialog.open(ModalCaracteristicaComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD, luego de ser cerrado el modal.
    dialogRef.afterClosed().subscribe(
      data => {
        if(data){
           this.dataSource.data.find(x => x.idCarac == data.id).nombreCarac = data.nombreCarac;
        }
      }
    );
  }

  // PARA ABRIR MODAL PREGUNTAR ESTÁ SEGURO DE ELIMNIAR ESTE MODAL ES GENERICO USEN EL QUE YA ESTA CREADO
  openDialogEliminar(row) 
  {
    // constante creada para un nuevo modal.
    const dialogConfig = new MatDialogConfig();
    // configuracion del modal con sus caracteristicas, ancho...
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    // PARA PASAR LOS DATOS AL DIALOG PREGUNTA ELIMINAR 
    dialogConfig.data = {
      id: row,
      nombreMetodo: 'caracteristica'
    };
    // constante donde se guardan los datos que se ingresan en el modal, los parametros
    // ModalTipoActuacionComponent hace el llamado al componente modal y dialogConfig
    // las caracteristicas del mismo.
    const dialogRef = this.dialog.open(ModalPreguntaEliminarComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD 
    dialogRef.afterClosed().subscribe(
      data => {
        if(data){
        this.dataSource.data.find(x => x.idCarac == data).estatus = 'Inactivo';
        }
      }
    );
  }
  
  // filtrar busquedas en la tabla y limpiar la pagina
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  
   // METEDO QUE OBTIENE UNA LISTA DE TipoActuacion Y SE CARGA EN LA TABLA, donde
   // se le hace un llamado de generalService donde esta el servicio y colocas 
   // la ultima directiva ejemplo "tipo_actuacion" que te lleva la direccion del
   // servicio en especifico, para obtener los resultados.
   CargarTabla() {
    this.generalService.Obtenertodos('vista_caracteristicas').then((result) => {
      this.resultado = result;
      Caracteristica = [];
      for (let i = 0; i < this.resultado.data.length; i++) {
       let est = this.Estatus(this.resultado.data[i].estatus); 
        this.datos = {
          idCarac: this.resultado.data[i].caracteristica_id,
          idBase: this.resultado.data[i].caracteristica_base_id,
          nombreCarac: this.resultado.data[i].caracteristica,
          nombreBase: this.resultado.data[i].caracteristica_base,
          descripcion: this.resultado.data[i].descripcion,
          estatus: est,
          
        };
        // se le asigna al array TipoActuacion todos los datos obtenidos
        Caracteristica.push(this.datos);
      }
      this.dataSource = new MatTableDataSource(Caracteristica);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, (err) => {
      console.log(err);
    });

  }

  // METODO QUE RETORNA ACTIVO O INACTIVO PARA MOSTRAR EN LA TABLA
  Estatus(est) {
    let estatus;
    return estatus = est == 'A' ? 'Activo' : 'Inactivo';
  }
  volver() {
    this.location.back();
  }


}
