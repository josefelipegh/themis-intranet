import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalFuncionComponent } from './modal-funcion.component';

describe('ModalFuncionComponent', () => {
  let component: ModalFuncionComponent;
  let fixture: ComponentFixture<ModalFuncionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalFuncionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalFuncionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
