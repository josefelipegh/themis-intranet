import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { GeneralService } from '../../../core/Services/general/general.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-modal-funcion',
  templateUrl: './modal-funcion.component.html',
  styleUrls: ['./modal-funcion.component.scss']
})
export class ModalFuncionComponent implements OnInit {
  rutas: any[] = [];
  padre: any[] = [];
  form_descripcion = { nombre: '', nombre_logo: '', funcion_id: null, ruta_id: null, posicion:'', estatus: 'A' };
  incluir: boolean = false;
  id;
  titulo = '';
  resultado: any;

  iconoCtrl = new FormControl();
  filteredIconos: Observable<any[]>;

  iconos: any[] = [
    {
      nombre: 'Casa',
      imagen: 'home',
    },
    {
      nombre: 'cuadros',
      imagen: 'view_comfy',
    },
    {
      nombre: 'campana servicio',
      imagen: 'room_service',
    },
    {
      nombre: 'persona',
      imagen: 'person',
    },
    {
      nombre: 'notificación',
      imagen: 'notification_important',
    },
    {
      nombre: 'vista compacta',
      imagen: 'view_compact',
    },
    {
      nombre: 'lista',
      imagen: 'view_list',
    },
    {
      nombre: 'configuración',
      imagen: 'settings',
    }
  ];
  public loading = false;
  constructor(public dialogRef: MatDialogRef<ModalFuncionComponent>,
    public generalService: GeneralService, public snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any) {

    this.CargarPadre();
    this.CargarRuta();
    console.log(this.rutas);
    console.log(this.padre);
    // PARA CARGAR LA CAJA DE TEXTO CUANDO SE VA ACTUALIZAR DATOS
    if (data) {
      this.nombreFormControl.setValue(data.nombre);
      if(data.icono){
        let icono = this.iconos.find(x => x.imagen == data.icono).nombre;
        this.iconoCtrl.setValue(icono);
      }
      this.funcionFormControl.setValue(data.funcion);
      this.rutaFormControl.setValue(data.ruta);
      this.posicionFormControl.setValue(data.posicion);
      this.id = data.id;
      this.titulo = 'Actualizar';
    } else {
      this.incluir = true;
      this.titulo = 'Nueva';
    }
    this.filteredIconos = this.iconoCtrl.valueChanges
      .pipe(
        startWith(''),
        map(icono => icono ? this._filterIconos(icono) : this.iconos.slice())
      );
  }
  private _filterIconos(value: string): any[] {
    const filterValue = value.toLowerCase();

    return this.iconos.filter(icono => icono.nombre.toLowerCase().indexOf(filterValue) === 0);
  }
  nombreFormControl = new FormControl('', [
    Validators.required
  ]);
  posicionFormControl = new FormControl('', [
    Validators.required
  ]);
  iconoFormControl = new FormControl('', [
    Validators.required
  ]);
  funcionFormControl = new FormControl('', [
  ]);
  rutaFormControl = new FormControl('', [
  ]);

  ngOnInit() {
  }
  // CERRAR EL MODAL
  onNoClick() {
    this.dialogRef.close();
  }
  // REGISTRAR UN ROL EN LA BD
  Registrar() {
    if (this.nombreFormControl.valid) {
      this.form_descripcion.nombre = this.nombreFormControl.value;
 
      if(this.iconoCtrl.value !=null){
        let icono = this.iconos.find(x => x.nombre == this.iconoCtrl.value).imagen;
        this.form_descripcion.nombre_logo = icono;
      }
      if(this.funcionFormControl.value){
        this.form_descripcion.funcion_id = this.funcionFormControl.value;
      }
      if(this.rutaFormControl.value){
        this.form_descripcion.ruta_id = this.rutaFormControl.value;
      }
      
      this.form_descripcion.posicion = this.posicionFormControl.value;
      this.loading = true;
      console.log(this.form_descripcion);
      this.generalService.Registrar(this.form_descripcion, 'funcion').then((result) => {
        this.loading = false;
        this.dialogRef.close(this.form_descripcion);
        this.openSnackBar('Registro exitoso!', 'registrar');
      }, (err) => {
        console.log(err);
        this.loading = false;
      });
    }
  }
  // ACTUALIZAR NOMBRE DEL ROL EN LA BD
  Actualizar() {

    if (this.nombreFormControl.valid) {
      this.form_descripcion.nombre = this.nombreFormControl.value;
 
      let icono;
      if(this.iconoCtrl.value !=null){
        icono = this.iconos.find(x => x.nombre == this.iconoCtrl.value).imagen;
        this.form_descripcion.nombre_logo = icono;
      }
      if(this.funcionFormControl.value){
        this.form_descripcion.funcion_id = this.funcionFormControl.value;
      }
      if(this.rutaFormControl.value){
        this.form_descripcion.ruta_id = this.rutaFormControl.value;
      }
      this.form_descripcion.posicion = this.posicionFormControl.value;
      console.log(this.form_descripcion);
      this.generalService.Actualizar(this.form_descripcion, 'funcion', this.id).then((result) => {
        this.openSnackBar('Registro Actualizado!', 'Actualizar');
      }, (err) => {
        console.log(err);
      });
      this.data.nombre = this.nombreFormControl.value;
      this.data.icono = icono;
      this.data.funcion = this.funcionFormControl.value;
      this.data.ruta = this.rutaFormControl.value;
      this.data.posicion = this.posicionFormControl.value;
      this.dialogRef.close(this.data);
    }
  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }

  CargarRuta() {
    this.generalService.Obtenertodos('ruta').then((result) => {
      this.resultado = result;
      this.rutas = [];
      for (let i = 0; i < this.resultado.data.length; i++) {
        let datos = {
          id: this.resultado.data[i].id,
          nombre: this.resultado.data[i].nombre,
        };
        this.rutas.push(datos);
      }
    }, (err) => {
      console.log(err);
    });
  }
  CargarPadre() {
    this.generalService.Obtenertodos('funcion').then((result) => {
      this.resultado = result;
      this.padre = [];
      for (let i = 0; i < this.resultado.data.length; i++) {
        let datos = {
          id: this.resultado.data[i].id,
          nombre: this.resultado.data[i].nombre,
        };
        this.padre.push(datos);
      }
    }, (err) => {
      console.log(err);
    });
  }
}
