import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { GeneralService } from '../../../core/Services/general/general.service';

@Component({
  selector: 'app-modal-documento',
  templateUrl: './modal-documento.component.html',
  styleUrls: ['./modal-documento.component.scss']
})
export class ModalDocumentoComponent implements OnInit {
   
  resultado: any;
  listaDocumento: any[] = [];
  form_descripcion = { nombre: '', tipo_documento_id: '', estatus: 'A' };
  incluir: boolean = false;
  id;
  titulo = '';
  idCat;
  public loading = false;
  constructor(public dialogRef: MatDialogRef<ModalDocumentoComponent>,
    public generalService: GeneralService, public snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.CargarCombo();
    // PARA CARGAR LA CAJA DE TEXTO CUANDO SE VA ACTUALIZAR DATOS
    if (data) {
      this.nombreFormControl.setValue(data.nombre);
      this.tipodocumentoFormControl.setValue(data.tipo_documento_id);
      this.id = data.id;
      this.titulo = 'Actualizar';
    } else {
      this.incluir = true;
      this.titulo = 'Nuevo';
    }
  }

  nombreFormControl = new FormControl('', [
    Validators.required
  ]);

  tipodocumentoFormControl = new FormControl('',
    [
      Validators.required
    ]);
  ngOnInit() {
  }
  // CERRAR EL MODAL
  onNoClick() {
    this.dialogRef.close()
  }

   // OBTIENE EL ID DE LA CATEGORIA DEL COMBO
   ObtenerIdCat(id) {
    console.log(this.idCat);
    return this.idCat = id;
   
  }
  // REGISTRAR UN ROL EN LA BD
  Registrar() {
    this.form_descripcion.nombre = this.nombreFormControl.value;
     this.form_descripcion.tipo_documento_id = this.idCat;
    if (this.nombreFormControl.valid) {
      this.generalService.Registrar(this.form_descripcion, 'documento').then((result) => {
        this.openSnackBar('Registro exitoso!', "registrar");
      }, (err) => {
        console.log(err);
      });
      this.dialogRef.close(this.form_descripcion);
    }
  }
  // ACTUALIZAR NOMBRE DEL ROL EN LA BD
  Actualizar() {



    if (this.nombreFormControl.valid) {
      this.form_descripcion.nombre = this.nombreFormControl.value;
      this.form_descripcion.tipo_documento_id = this.tipodocumentoFormControl.value;
      this.generalService.Actualizar(this.form_descripcion, 'documento', this.id).then((result) => {
        this.openSnackBar('Registro Actualizado!', "Actualizar");
      }, (err) => {
        console.log(err);
      });
      this.data.nombre = this.nombreFormControl.value;
      this.data.tipo_documento_id = this.tipodocumentoFormControl.value;
      this.dialogRef.close(this.data);
    }
  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }

  CargarCombo() {
    this.generalService.Obtenertodos('tipo_documento').then((result) => {
      this.resultado = result;
      for (let i = 0; i < this.resultado.data.length; i++) {
        this.listaDocumento.push({
          
          id: this.resultado.data[i].id,
          nombre: this.resultado.data[i].nombre,
        });
      
        console.log( this.resultado.data[i].id);
      }
    }, (err) => {
      console.log(err);
    });
  }

}
