import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalTipoClienteComponent } from './modal-tipo-cliente.component';

describe('ModalTipoClienteComponent', () => {
  let component: ModalTipoClienteComponent;
  let fixture: ComponentFixture<ModalTipoClienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalTipoClienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalTipoClienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
