import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { ModalTipoClienteComponent } from './modal-tipo-cliente/modal-tipo-cliente.component';
import { Location } from '@angular/common';
import { GeneralService } from '../../core/Services/general/general.service';
import { ModalPreguntaEliminarComponent } from '../../shared/modal-pregunta-eliminar/modal-pregunta-eliminar.component';

export interface TipoClienteData {
  id: number;
  nombre: string;
  descripcion: string;
  estatus: string;
}
let TipoCliente: any[] = [];

@Component({
  selector: 'app-lista-tipo-cliente',
  templateUrl: './lista-tipo-cliente.component.html',
  styleUrls: ['./lista-tipo-cliente.component.scss'],

  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],

})
export class ListaTipoClienteComponent implements OnInit {

  displayedColumns: string[] = ['ver','nombre', 'eliminar'];
  dataSource: MatTableDataSource<TipoClienteData>;
  resultado: any;
// OBJETO PARA CARGAR EN LA TABLA
datos: TipoClienteData;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  value = '';
  valueInput() {
    this.value = '';
    this.applyFilter(this.value);
  }
  constructor(public dialog: MatDialog, public generalService: GeneralService,
    private location: Location)
   {
    this.CargarTabla();
   }

   ngOnInit() {
    //this.dataSource.paginator = this.paginator;
  //  this.dataSource.sort = this.sort;
   // this.Obtener();
  }

  openDialogRegistrar() // abre una ventana modal
   {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    const dialogRef = this.dialog.open(ModalTipoClienteComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD 
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.CargarTabla()
        }
      }
    );
  }
  openDialogActualizar(row) // abre una ventana modal
  {
    // constante creada para un nuevo modal.
    const dialogConfig = new MatDialogConfig();
    // configuracion del modal con sus caracteristicas, ancho...
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    // PARA PASAR LOS DATOS AL DIALOG
    dialogConfig.data = row;
    // constante donde se guardan los datos que se ingresan en el modal, los parametros
    // ModalTipoActuacionComponent hace el llamado al componente modal y dialogConfig
    // las caracteristicas del mismo.
    const dialogRef = this.dialog.open(ModalTipoClienteComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD, luego de ser cerrado el modal.
    dialogRef.afterClosed().subscribe(
      data => {
        if(data){
           this.dataSource.data.find(x => x.id == data.id).nombre = data.nombre;
        }
      }
    );
  }
// PARA ABRIR MODAL PREGUNTAR ESTÁ SEGURO DE ELIMNIAR ESTE MODAL ES GENERICO USEN EL QUE YA ESTA CREADO
  openDialogEliminar(row) 
  {
    // constante creada para un nuevo modal.
    const dialogConfig = new MatDialogConfig();
    // configuracion del modal con sus caracteristicas, ancho...
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    // PARA PASAR LOS DATOS AL DIALOG PREGUNTA ELIMINAR 
    dialogConfig.data = {
      id: row,
      nombreMetodo: 'tipo_cliente'
    };
// constante donde se guardan los datos que se ingresan en el modal, los parametros
    // ModalTipoActuacionComponent hace el llamado al componente modal y dialogConfig
    // las caracteristicas del mismo.
    const dialogRef = this.dialog.open(ModalPreguntaEliminarComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD 
    dialogRef.afterClosed().subscribe(
      data => {
        if(data){
          //this.dataSource.data.find(x => x.id == data).estatus = 'Inactivo';
        }
      }
    );
  }
  

  
// METEDO QUE OBTIENE UNA LISTA DE TipoActuacion Y SE CARGA EN LA TABLA, donde
   // se le hace un llamado de generalService donde esta el servicio y colocas 
   // la ultima directiva ejemplo "tipo_actuacion" que te lleva la direccion del
   // servicio en especifico, para obtener los resultados.
   CargarTabla() {
    this.generalService.Obtenertodos('tipo_cliente').then((result) => {
      this.resultado = result;
      TipoCliente= [];
      for (let i = 0; i < this.resultado.data.length; i++) {
        let est = this.Estatus(this.resultado.data[i].estatus);
        this.datos = {
          id: this.resultado.data[i].id,
          nombre: this.resultado.data[i].nombre,
          descripcion: this.resultado.data[i].descripcion,
          estatus: est,
        };
        TipoCliente.push(this.datos);
      }
      this.dataSource = new MatTableDataSource(TipoCliente);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, (err) => {
      console.log(err);
    });

  }

  Estatus(est) {
    let estatus;
    return estatus = est == 'A' ? 'Activo' : 'Inactivo';
  }
  volver() {
    this.location.back();
  }


  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


}
