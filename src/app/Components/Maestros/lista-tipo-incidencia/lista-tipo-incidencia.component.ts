import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { ModalTipoIncidenciaComponent } from './modal-tipo-incidencia/modal-tipo-incidencia.component';
import {MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Location } from '@angular/common';
import { GeneralService } from '../../core/Services/general/general.service';
import { ModalPreguntaEliminarComponent } from '../../shared/modal-pregunta-eliminar/modal-pregunta-eliminar.component';

export interface TipoIncidenciaData {
  id: number;
  nombre: string;
  estatus: string;
}

let tipoincidencia: any[] = [];
@Component({
  selector: 'app-lista-tipo-incidencia',
  templateUrl: './lista-tipo-incidencia.component.html',
  styleUrls: ['./lista-tipo-incidencia.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ListaTipoIncidenciaComponent implements OnInit {

  displayedColumns: string[] = ['ver','nombre','eliminar'];
  dataSource: MatTableDataSource<TipoIncidenciaData>;
  resultado: any;
  // OBJETO PARA CARGAR EN LA TABLA
  datos: TipoIncidenciaData;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  value="";
  valueInput(){
    this.value="";
    this.applyFilter(this.value);
  }

  constructor(public dialog: MatDialog, public generalService: GeneralService,
    private location: Location)
   {
    this.CargarTabla();
   }

   ngOnInit() {

  }
  openDialogRegistrar() // abre una ventana modal
  {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';

    const dialogRef = this.dialog.open(ModalTipoIncidenciaComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD 
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.CargarTabla()
        }
      }
    );
  }


  openDialogActualizar(row) // abre una ventana modal
  {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    // PARA PASAR LOS DATOS AL DIALOG
    dialogConfig.data = row;

    const dialogRef = this.dialog.open(ModalTipoIncidenciaComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD 
    dialogRef.afterClosed().subscribe(
      data => {
        if(data){
           this.dataSource.data.find(x => x.id == data.id).nombre = data.nombre;
        }
      }
    );
  }

  openDialogEliminar(row) 
  {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    // PARA PASAR LOS DATOS AL DIALOG PREGUNTA ELIMINAR 
    dialogConfig.data = {
      id: row,
      nombreMetodo: 'tipo_incidencia'
    };

    const dialogRef = this.dialog.open(ModalPreguntaEliminarComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD 
    dialogRef.afterClosed().subscribe(
      data => {
        if(data){
          this.dataSource.data.find(x => x.id == data).estatus = 'Inactivo';
        }
      }
    );
  }

  CargarTabla() {
    this.generalService.Obtenertodos('tipo_incidencia').then((result) => {
      this.resultado = result;
      tipoincidencia = [];
      for (let i = 0; i < this.resultado.data.length; i++) {
        let est = this.Estatus(this.resultado.data[i].estatus);
        this.datos = {
          id: this.resultado.data[i].id,
          nombre: this.resultado.data[i].nombre,
          estatus: est,
        };
        tipoincidencia.push(this.datos);
      }
      this.dataSource = new MatTableDataSource(tipoincidencia);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, (err) => {
      console.log(err);
    });

  }

  Estatus(est) {
    let estatus;
    return estatus = est == 'A' ? 'Activo' : 'Inactivo';
  }
  volver() {
    this.location.back();
  }

  openDialog() //abre una ventana modal
   {
    this.dialog.open(ModalTipoIncidenciaComponent, { width: '50%'});
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  
}
