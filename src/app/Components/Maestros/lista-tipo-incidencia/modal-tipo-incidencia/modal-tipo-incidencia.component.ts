import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { GeneralService } from '../../../core/Services/general/general.service';

@Component({
  selector: 'app-modal-tipo-incidencia',
  templateUrl: './modal-tipo-incidencia.component.html',
  styleUrls: ['./modal-tipo-incidencia.component.scss']
})
export class ModalTipoIncidenciaComponent implements OnInit {
  form_descripcion = { nombre: '', estatus: 'A' };
  incluir: boolean = false;
  id;
  titulo = '';
  constructor(public dialogRef: MatDialogRef<ModalTipoIncidenciaComponent>, public generalService: GeneralService, public snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any) { 

      if (data) {
        this.nombreFormControl.setValue(data.nombre);
        this.id = data.id;
        this.titulo = 'Actualizar';
      } else {
        this.incluir = true;
        this.titulo = 'Nuevo';
      }
    }   
    
    nombreFormControl = new FormControl('', [
      Validators.required
    ]);



    Registrar() {
      this.form_descripcion.nombre = this.nombreFormControl.value;
      if (this.nombreFormControl.valid) {
        this.generalService.Registrar(this.form_descripcion, 'tipo_incidencia').then((result) => {
          this.openSnackBar('Registro Exitoso!', "registrar");
        }, (err) => {
          console.log(err);
        });
        this.dialogRef.close(this.form_descripcion);

  

      }
    }

    Actualizar() {

      if (this.nombreFormControl.valid) {
        this.form_descripcion.nombre = this.nombreFormControl.value;
        this.generalService.Actualizar(this.form_descripcion, 'tipo_incidencia', this.id).then((result) => {
          this.openSnackBar('Registro Actualizado!', "Actualizar");
        }, (err) => {
          console.log(err);
        });
        this.data.nombre = this.nombreFormControl.value
        this.dialogRef.close(this.data);
      }
    }
    openSnackBar(message: string, action: string) {
      this.snackBar.open(message, action, {
        duration: 1000,
      });
    }


  ngOnInit() {
  }
  onNoClick() {
    this.dialogRef.close();
  }

}
