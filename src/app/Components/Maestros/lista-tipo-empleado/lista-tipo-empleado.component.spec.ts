import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaTipoEmpleadoComponent } from './lista-tipo-empleado.component';

describe('ListaTipoEmpleadoComponent', () => {
  let component: ListaTipoEmpleadoComponent;
  let fixture: ComponentFixture<ListaTipoEmpleadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaTipoEmpleadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaTipoEmpleadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
