import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalTipoEmpleadoComponent } from './modal-tipo-empleado.component';

describe('ModalTipoEmpleadoComponent', () => {
  let component: ModalTipoEmpleadoComponent;
  let fixture: ComponentFixture<ModalTipoEmpleadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalTipoEmpleadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalTipoEmpleadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
