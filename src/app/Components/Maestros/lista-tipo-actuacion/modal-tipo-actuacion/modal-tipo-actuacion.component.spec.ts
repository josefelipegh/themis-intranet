import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalTipoActuacionComponent } from './modal-tipo-actuacion.component';

describe('ModalTipoActuacionComponent', () => {
  let component: ModalTipoActuacionComponent;
  let fixture: ComponentFixture<ModalTipoActuacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalTipoActuacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalTipoActuacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
