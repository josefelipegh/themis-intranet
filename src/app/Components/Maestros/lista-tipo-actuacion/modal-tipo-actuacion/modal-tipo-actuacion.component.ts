import { Component, OnInit, Inject,ChangeDetectionStrategy } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { GeneralService } from '../../../core/Services/general/general.service';


@Component({
  selector: 'app-modal-tipo-actuacion',
  templateUrl: './modal-tipo-actuacion.component.html',
  styleUrls: ['./modal-tipo-actuacion.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModalTipoActuacionComponent implements OnInit {

  public Color1: string;
  public Color2: string;
  form_descripcion = { nombre: '', descripcion: '', color_primario: '', color_secundario: '', estatus: 'A' };
  incluir: boolean = false;
  id;
  titulo = '';
  descripcionFormControl = new FormControl('', [
    Validators.required
  ]);
  nombreFormControl = new FormControl('', [
    Validators.required
  ]);
  constructor(public dialogRef: MatDialogRef<ModalTipoActuacionComponent>,
              public generalService: GeneralService, 
              public snackBar: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    // PARA CARGAR LA CAJA DE TEXTO CUANDO SE VA ACTUALIZAR DATOS
    if (data) {
      this.id = data.id;
      this.nombreFormControl.setValue(data.nombre);
      this.descripcionFormControl.setValue(data.descripcion);
      this.Color1 = data.color1;
      this.Color2 = data.color2;
      this.titulo = 'Actualizar';
    } else {
      this.incluir = true;
      this.titulo = 'Nuevo';
    }
  }

  

  ngOnInit() {
  }
  // CERRAR EL MODAL
  onNoClick() {
    this.dialogRef.close()
  }
  // REGISTRAR UN tipo actuacion EN LA BD
  Registrar() {
    this.form_descripcion.nombre = this.nombreFormControl.value;
    this.form_descripcion.descripcion = this.descripcionFormControl.value;
    this.form_descripcion.color_primario = this.Color1;
    this.form_descripcion.color_secundario = this.Color2;
    console.log(this.form_descripcion);
    if (this.nombreFormControl.valid && this.descripcionFormControl.valid) {
      this.generalService.Registrar(this.form_descripcion, 'tipo_actuacion').then((result) => {
        this.openSnackBar('Registro exitoso!', "registrar");
      }, (err) => {
        console.log(err);
      });
      this.dialogRef.close(this.form_descripcion);
    }
  }
  // ACTUALIZAR NOMBRE DEL tipo de actuacion EN LA BD
  Actualizar() {

    if (this.nombreFormControl.valid) {
      this.form_descripcion.nombre = this.nombreFormControl.value;
      this.form_descripcion.descripcion = this.descripcionFormControl.value;
      this.form_descripcion.color_primario = this.Color1;
      this.form_descripcion.color_secundario = this.Color2;
      this.generalService.Actualizar(this.form_descripcion, 'tipo_actuacion', this.id).then((result) => {
        this.openSnackBar('Registro Actualizado!', "Actualizar");
      }, (err) => {
        console.log(err);
      });
      this.data.nombre = this.nombreFormControl.value;
      this.data.descripcion = this.descripcionFormControl.value;
      this.dialogRef.close(this.data);
    }
  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }


}
