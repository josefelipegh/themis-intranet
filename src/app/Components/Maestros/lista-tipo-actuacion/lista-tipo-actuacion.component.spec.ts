import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaTipoActuacionComponent } from './lista-tipo-actuacion.component';

describe('ListaTipoActuacionComponent', () => {
  let component: ListaTipoActuacionComponent;
  let fixture: ComponentFixture<ListaTipoActuacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaTipoActuacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaTipoActuacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
