import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { TipoDocumentoComponent } from './tipo-documento/tipo-documento.component';
import {MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Location } from '@angular/common';
import { GeneralService } from '../../core/Services/general/general.service';
import { ModalPreguntaEliminarComponent } from '../../shared/modal-pregunta-eliminar/modal-pregunta-eliminar.component';


export interface TipoDocumentoData {
  id: number;
  nombre: string;
  estatus: string;
}
  
  

@Component({
  selector: 'app-lista-tipo-documento',
  templateUrl: './lista-tipo-documento.component.html',
  styleUrls: ['./lista-tipo-documento.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],

})
export class ListaTipoDocumentoComponent implements OnInit {

  displayedColumns: string[] = ['ver','nombre', 'eliminar'];
  dataSource: MatTableDataSource<TipoDocumentoData>;
  resultado: any;
  // OBJETO PARA CARGAR EN LA TABLA
  datos: TipoDocumentoData;
  tipodocumento: any[] = [];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  value="";
  valueInput(){
    this.value="";
    this.applyFilter(this.value);
  }

  constructor(public dialog: MatDialog,
              public generalService: GeneralService,
              private location: Location)
   {
    this.CargarTabla();
   }

   ngOnInit() {
  }

  openDialogRegistrar() // abre una ventana modal
  {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';

    const dialogRef = this.dialog.open(TipoDocumentoComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD 
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.CargarTabla()
        }
      }
    );
  }
  openDialog() //abre una ventana modal
  {
   this.dialog.open(TipoDocumentoComponent, { width: '50%'});
 }
  openDialogActualizar(row) // abre una ventana modal
  {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    // PARA PASAR LOS DATOS AL DIALOG
    dialogConfig.data = row;

    const dialogRef = this.dialog.open(TipoDocumentoComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD 
    dialogRef.afterClosed().subscribe(
      data => {
        if(data){
           this.dataSource.data.find(x => x.id == data.id).nombre = data.nombre;
        }
      }
    );
  }

  openDialogEliminar(row) 
  {
    console.log(this.tipodocumento);
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    // PARA PASAR LOS DATOS AL DIALOG PREGUNTA ELIMINAR 
    dialogConfig.data = {
      id: row,
      nombreMetodo: 'tipo_documento'
    };

    const dialogRef = this.dialog.open(ModalPreguntaEliminarComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD 
    dialogRef.afterClosed().subscribe(
      data => {
        if(data){
          let index = this.tipodocumento.findIndex(x => x.id == data);
          this.tipodocumento.splice(index, 1);
          console.log(this.tipodocumento); 
        }
      }
    );
    
  }

  CargarTabla() {
    this.generalService.Obtenertodos('tipo_documento').then((result) => {
      this.resultado = result;
      this.tipodocumento = [];
      for (let i = 0; i < this.resultado.data.length; i++) {
        let est = this.Estatus(this.resultado.data[i].estatus);
        this.datos = {
          id: this.resultado.data[i].id,
          nombre: this.resultado.data[i].nombre,
          estatus: est,
        };
        this.tipodocumento.push(this.datos);
      }
      this.dataSource = new MatTableDataSource(this.tipodocumento);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, (err) => {
      console.log(err);
    });

  }

  Estatus(est) {
    let estatus;
    return estatus = est == 'A' ? 'Activo' : 'Inactivo';
  }
  volver() {
    this.location.back();
  }


  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


}
