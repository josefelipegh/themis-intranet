import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalTipoValoracionComponent } from './modal-tipo-valoracion.component';

describe('ModalTipoValoracionComponent', () => {
  let component: ModalTipoValoracionComponent;
  let fixture: ComponentFixture<ModalTipoValoracionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalTipoValoracionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalTipoValoracionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
