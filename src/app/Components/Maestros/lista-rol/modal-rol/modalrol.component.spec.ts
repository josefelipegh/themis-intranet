import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalrolComponent } from './modalrol.component';

describe('ModalrolComponent', () => {
  let component: ModalrolComponent;
  let fixture: ComponentFixture<ModalrolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalrolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalrolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
