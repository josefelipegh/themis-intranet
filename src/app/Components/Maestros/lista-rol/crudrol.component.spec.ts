import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaRolComponent } from './crudrol.component';

describe('CrudrolComponent', () => {
  let component: ListaRolComponent;
  let fixture: ComponentFixture<ListaRolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaRolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaRolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
