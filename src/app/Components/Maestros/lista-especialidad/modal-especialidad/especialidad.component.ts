import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { GeneralService } from '../../../core/Services/general/general.service';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-especialidad',
  templateUrl: './especialidad.component.html',
  styleUrls: ['./especialidad.component.scss']
})
export class ModalEspecialidadComponent implements OnInit {

  form_descripcion = { nombre: '', estatus: 'A' };
  incluir: boolean = false;
  id;
  titulo = '';
  constructor(public dialogRef: MatDialogRef<ModalEspecialidadComponent>,
    public generalService: GeneralService, public snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    if (data) {
      this.nombreFormControl.setValue(data.nombre);
      this.id = data.id;
      this.titulo = 'Actualizar';
    } else {
      this.incluir = true;
      this.titulo = 'Nueva';
    }
  }

  nombreFormControl = new FormControl('', [
    Validators.required
  ]);

  ngOnInit() {
  }
  onNoClick() {
    this.dialogRef.close();
  }

    // REGISTRAR UN ROL EN LA BD
    Registrar() {
      this.form_descripcion.nombre = this.nombreFormControl.value;
      if (this.nombreFormControl.valid) {
        this.generalService.Registrar(this.form_descripcion, 'especialidad').then((result) => {
          this.openSnackBar('Registro exitoso!', "registrar");
        }, (err) => {
          console.log(err);
        });
        this.dialogRef.close(this.form_descripcion);
      }
    }
    // ACTUALIZAR NOMBRE DEL ROL EN LA BD
    Actualizar() {
  
      if (this.nombreFormControl.valid) {
        this.form_descripcion.nombre = this.nombreFormControl.value;
        this.generalService.Actualizar(this.form_descripcion, 'especialidad', this.id).then((result) => {
          this.openSnackBar('Registro Actualizado!', "Actualizar");
        }, (err) => {
          console.log(err);
        });
        this.data.nombre = this.nombreFormControl.value
        this.dialogRef.close(this.data);
      }
    }
    openSnackBar(message: string, action: string) {
      this.snackBar.open(message, action, {
        duration: 1000,
      });
    }

}
