import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { GeneralService } from '../../../core/Services/general/general.service';

@Component({
  selector: 'app-maestroactuacion',
  templateUrl: './maestroactuacion.component.html',
  styleUrls: ['./maestroactuacion.component.scss']
  
})
export class ModalActuacionComponent implements OnInit {
 
   //codigo
   tipoactuaciones: any[] = [];
   resultado: any;
   form_descripcion = { nombre: '', estatus: 'A', tipo_actuacion_id: 0, descripcion: '', };
   incluir: boolean = false;
   id;
   tipo_actuacion_id;
   titulo = '';


//codigo
  descripcionFormControl = new FormControl();
  onCommentChange() {
    console.log(this.descripcionFormControl.value);
  }

  nombreFormControl = new FormControl('', [
    Validators.required
  ]);

  tipoactuacionFormControl = new FormControl('', [
    Validators.required
  ]);

  constructor(public dialogRef: MatDialogRef<ModalActuacionComponent>,public generalService: GeneralService, public snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.CargarCombo();
      if (data) {
        this.nombreFormControl.setValue(data.nombre);
        this.descripcionFormControl.setValue(data.descripcion);
        this.id = data.id;
        this.tipoactuacionFormControl.setValue(data.tipo_actuacion_id);
        this.titulo = 'Actualizar';
      } else {
        this.incluir = true;
        this.titulo = 'Nueva';
      }

     }


    Registrar() {
      this.form_descripcion.nombre = this.nombreFormControl.value;
      //agregado
      this.form_descripcion.descripcion = this.descripcionFormControl.value;
      this.form_descripcion.tipo_actuacion_id = this.tipo_actuacion_id;
      if (this.nombreFormControl.valid) {
        this.generalService.Registrar(this.form_descripcion, 'actuacion').then((result) => {
          this.openSnackBar('Registro Exitoso!', "registrar");
        }, (err) => {
          console.log(err);
        });
        this.dialogRef.close(this.form_descripcion);

  

      }
    }

    Actualizar() {

      if (this.nombreFormControl.valid) {
        this.form_descripcion.nombre = this.nombreFormControl.value;
        this.form_descripcion.descripcion = this.descripcionFormControl.value;
        this.generalService.Actualizar(this.form_descripcion, 'actuacion', this.id).then((result) => {
          this.openSnackBar('Registro Actualizado!', "Actualizar");
        }, (err) => {
          console.log(err);
        });
        this.data.nombre = this.nombreFormControl.value
        this.data.descripcion = this.descripcionFormControl.value
        this.dialogRef.close(this.data);
      }
    }

    ObtenerIdTipoActuacion(id) {
      this.tipo_actuacion_id = id;
    }

    CargarCombo() {
      this.generalService.Obtenertodos('tipo_actuacion').then((result) => {
        this.resultado = result;
        for (let i = 0; i < this.resultado.data.length; i++) {
          this.tipoactuaciones.push({
            id: this.resultado.data[i].id,
            nombre: this.resultado.data[i].nombre,
            descripcion: this.resultado.data[i].descripcion,
          });
        }
      }, (err) => {
        console.log(err);
      });
    }


    openSnackBar(message: string, action: string) {
      this.snackBar.open(message, action, {
        duration: 1000,
      });
    }
  ngOnInit() {
  }
  onNoClick() {
    this.dialogRef.close();
  }
    

  

}
