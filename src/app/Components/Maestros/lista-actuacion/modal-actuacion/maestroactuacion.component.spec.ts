import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaestroactuacionComponent } from './maestroactuacion.component';

describe('MaestroactuacionComponent', () => {
  let component: MaestroactuacionComponent;
  let fixture: ComponentFixture<MaestroactuacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaestroactuacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaestroactuacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
