import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCaracteristicaBaseComponent } from './modal-caracteristica-base.component';

describe('ModalCaracteristicaBaseComponent', () => {
  let component: ModalCaracteristicaBaseComponent;
  let fixture: ComponentFixture<ModalCaracteristicaBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCaracteristicaBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCaracteristicaBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
