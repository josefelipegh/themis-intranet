import { Component, OnInit, Inject} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { GeneralService } from '../../../core/Services/general/general.service';


export interface DialogData {
 
}

@Component({
  selector: 'app-modal-caracteristica-base',
  templateUrl: './modal-caracteristica-base.component.html',
  styleUrls: ['./modal-caracteristica-base.component.scss']
})
export class ModalCaracteristicaBaseComponent implements OnInit {
  form_descripcion = { nombre: '', descripcion: '', estatus: 'A' };
  incluir: boolean = false;
  id;
  titulo = '';
  constructor(public dialogRef: MatDialogRef<ModalCaracteristicaBaseComponent>, public generalService: GeneralService, public snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any) {

      
      if (data) {
        this.nombreFormControl.setValue(data.nombre);
        this.descripcionFormControl.setValue(data.descripcion);
        this.id = data.id;
        this.titulo = 'Actualizar'
      } else {
        this.incluir = true;
        this.titulo = 'Nueva';
      }

     }

    descripcionFormControl = new FormControl('', [
      Validators.required
    ]);

    nombreFormControl = new FormControl('', [
      Validators.required
    ]);

    Registrar() {
      this.form_descripcion.nombre = this.nombreFormControl.value;
      this.form_descripcion.descripcion = this.descripcionFormControl.value;
      if (this.nombreFormControl.valid, this.descripcionFormControl.valid) {
        this.generalService.Registrar(this.form_descripcion, 'caracteristica_base').then((result) => {
          this.openSnackBar('Registro Exitoso!', "registrar");
        }, (err) => {
          console.log(err);
        });
        this.dialogRef.close(this.form_descripcion);
      }
    }

    Actualizar() {

      if (this.nombreFormControl.valid, this.descripcionFormControl.valid) {
        this.form_descripcion.nombre = this.nombreFormControl.value;
        this.form_descripcion.descripcion = this.descripcionFormControl.value;
        this.generalService.Actualizar(this.form_descripcion, 'caracteristica_base', this.id).then((result) => {
          this.openSnackBar('Registro Actualizado!', "Actualizar");
        }, (err) => {
          console.log(err);
        });
        this.data.nombre = this.nombreFormControl.value
        this.dialogRef.close(this.data);
      }
    }

    openSnackBar(message: string, action: string) {
      this.snackBar.open(message, action, {
        duration: 1000,
      });
    }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
