import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalValoracionComponent } from './modal-valoracion.component';

describe('ModalValoracionComponent', () => {
  let component: ModalValoracionComponent;
  let fixture: ComponentFixture<ModalValoracionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalValoracionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalValoracionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
