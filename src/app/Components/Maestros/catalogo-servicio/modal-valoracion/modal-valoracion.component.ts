import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { GeneralService } from '../../../core/Services/general/general.service';
import { trigger, state, style, transition, animate } from '@angular/animations';

export interface preguntaServicio {
  tipo_valoracion: string;
  rango_descripcion: string;
  tipo_valoracion_id: number;
  rango_valoracion_id: number;
  tipo_servicio_id: number;
  tr_valoracion_id: number;

}

let tablaPregunta: any[] = [];

@Component({
  selector: 'app-modal-valoracion',
  templateUrl: './modal-valoracion.component.html',
  styleUrls: ['./modal-valoracion.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ModalValoracionComponent implements OnInit {

  displayedColumns: string[] = ['pregunta', 'nombreResp', 'eliminar'];
  dataSource: MatTableDataSource<preguntaServicio>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  resultado2: any;
  resultado: any;
  resultado1: any;
  resultado3: any;
  pregunta: preguntaServicio;
  listaPregunta: any[] = [];
  listaRespuesta: any[] = [];
  form_respuesta_tipo_servicio = { tipo_valoracion_id: '', rango_valoracion_id: '', catalogo_servicio_id: '' };
  incluir: boolean = false;
  id;
  titulo = '';
  idCat;
  idPreg;
  public loading = false;
  constructor(public dialogRef: MatDialogRef<ModalValoracionComponent>,
    public generalService: GeneralService, public snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data1: any) {
    this.CargarCombo();
    this.CargarComboRespuestas();
    this.CargarTabla();
  }

  preguntaFormControl = new FormControl('',
    [
      Validators.required
    ]);

  respuestaFormControl = new FormControl('',
    [
      Validators.required
    ]);

  ngOnInit() {
  }

  // CERRAR EL MODAL
  onclick() {
    this.dialogRef.close()
  }

  ObtenerIdPreg(id) {
    return this.idPreg = id;

  }

  // OBTIENE EL ID DE LA CATEGORIA DEL COMBO
  ObtenerIdCat(id) {
    return this.idCat = id;

  }
  CargarTabla() {
    this.generalService.ObtenerUno('vista_valoracion/tipo_servicio', this.data1).then((result) => {
      this.resultado3 = result;
      tablaPregunta = [];
      for (let i = 0; i < this.resultado3.data.length; i++) {
        this.pregunta = {
          tipo_servicio_id: this.resultado3.data[i].tipo_servicio_id,
          tipo_valoracion_id: this.resultado3.data[i].tipo_valoracion_id,
          tipo_valoracion: this.resultado3.data[i].tipo_valoracion,
          rango_valoracion_id: this.resultado3.data[i].rango_valoracion_id,
          rango_descripcion: this.resultado3.data[i].rango_descripcion,
          tr_valoracion_id: this.resultado3.data[i].tr_valoracion_id,
        };
        tablaPregunta.push(this.pregunta);
      }
      console.log(tablaPregunta);
      this.dataSource = new MatTableDataSource(tablaPregunta);
      this.dataSource.paginator = this.paginator;
    }, (err) => {
      console.log(err);
    });

  }





  Registrar() {
    this.form_respuesta_tipo_servicio.catalogo_servicio_id = this.data1;
    this.form_respuesta_tipo_servicio.tipo_valoracion_id = this.preguntaFormControl.value
    this.form_respuesta_tipo_servicio.rango_valoracion_id = this.respuestaFormControl.value
    if (this.respuestaFormControl.valid && this.preguntaFormControl.valid) {
      this.generalService.Registrar(this.form_respuesta_tipo_servicio, 'tr_valoracion').then((result) => {
        this.generalService.Registrar({catalogo_servicio_id: this.data1, tipo_valoracion_id: this.preguntaFormControl.value}, 'valoracion_catalogo').then((result) => {
          this.openSnackBar('Registro exitoso!', "registrar");
          this.CargarTabla();
          this.respuestaFormControl.setValue('-1');
          this.preguntaFormControl.setValue('-1');
        }, (err) => {
          console.log(err);
        });
      }, (err) => {
        console.log(err);
      });


    }
  }


  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }

  CargarCombo() {
    this.generalService.Obtenertodos('tipo_valoracion').then((result) => {
      this.resultado = result;
      for (let i = 0; i < this.resultado.data.length; i++) {
        this.listaPregunta.push({

          id: this.resultado.data[i].id,
          nombre: this.resultado.data[i].nombre,
        });
      }
    }, (err) => {
      console.log(err);
    });
  }

  CargarComboRespuestas() {
    this.generalService.Obtenertodos('rango_valoracion').then((result) => {
      this.resultado1 = result;
      for (let i = 0; i < this.resultado1.data.length; i++) {
        this.listaRespuesta.push({

          id: this.resultado1.data[i].id,
          descripcion: this.resultado1.data[i].descripcion,
        });
      }
    }, (err) => {
      console.log(err);
    });
  }

  // ELIMINAR ENTIDAD FISICAMENTE EN LA BD
  Eliminar(id) {
    this.generalService.EliminarFisico('tr_valoracion', id).then((result) => {
      this.openSnackBar('Registro Elimidado!', "Eliminar");
      this.CargarTabla();
    }, (err) => {
      console.log(err);
    });
  }


}

