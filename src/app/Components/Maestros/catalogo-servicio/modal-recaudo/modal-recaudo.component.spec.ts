import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalRecaudoComponent } from './modal-recaudo.component';

describe('ModalRecaudoComponent', () => {
  let component: ModalRecaudoComponent;
  let fixture: ComponentFixture<ModalRecaudoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalRecaudoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalRecaudoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
