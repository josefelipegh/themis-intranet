import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { GeneralService } from '../../../core/Services/general/general.service';
import { trigger, state, style, transition, animate } from '@angular/animations';

export interface documentoServicio {
  nombreDocumento: string;
  recaudo_catalogo_id: number;
  recaudo_id: number;

}

let tablaDocumento: any[] = [];


@Component({
  selector: 'app-modal-recaudo',
  templateUrl: './modal-recaudo.component.html',
  styleUrls: ['./modal-recaudo.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ModalRecaudoComponent implements OnInit {

  displayedColumns: string[] = ['nombreDoc', 'eliminar'];
  dataSource: MatTableDataSource<documentoServicio>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  resultado2: any;
  resultado: any;
  documento: documentoServicio;
  listaDocumento: any[] = [];
  form_documento_catalogo = { catalogo_servicio_id: '', documento_id: '', estatus: 'A' };
  incluir: boolean = false;
  id;
  titulo = '';
  idCat;
  public loading = false;
  constructor(public dialogRef: MatDialogRef<ModalRecaudoComponent>,
    public generalService: GeneralService, public snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    console.log(data)
    this.CargarCombo();
    this.CargarTabla();
  }

  documentoFormControl = new FormControl('',
    [
      Validators.required
    ]);


  ngOnInit() {
  }
  // CERRAR EL MODAL
  onclick() {
    this.dialogRef.close()
  }



  // OBTIENE EL ID DE LA CATEGORIA DEL COMBO
  ObtenerIdCat(id) {
    return this.idCat = id;

  }



  Registrar() {
    this.form_documento_catalogo.catalogo_servicio_id = this.data;
    this.form_documento_catalogo.documento_id = this.documentoFormControl.value
    if (this.documentoFormControl.valid) {
      if (!tablaDocumento.find(x => x.recaudo_id == this.documentoFormControl.value)) {
        this.generalService.Registrar(this.form_documento_catalogo, 'recaudo_catalogo').then((result) => {
          this.openSnackBar('Registro exitoso!', "registrar");
          this.CargarTabla();
        }, (err) => {
          console.log(err);
        });
      } else {
        this.openSnackBar('El documento seleccionado está asociado a este servicio', "Seleccione otra documento");
      }

    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }

  // METEDO QUE OBTIENE UNA LISTA DE DOCUMENTO Y SE CARGA EN LA TABLA
  CargarTabla() {
    this.generalService.ObtenerUno('vista_recaudo_catalogo/catalogo', this.data).then((result) => {
      this.resultado2 = result;
      tablaDocumento = [];
      for (let i = 0; i < this.resultado2.data.length; i++) {
        this.documento = {
          nombreDocumento: this.resultado2.data[i].nombre,
          recaudo_catalogo_id: this.resultado2.data[i].recaudo_catalogo_id,
          recaudo_id: this.resultado2.data[i].recaudo_id,
        };
        tablaDocumento.push(this.documento);
      }
      this.dataSource = new MatTableDataSource(tablaDocumento);
      this.dataSource.paginator = this.paginator;
    }, (err) => {
      console.log(err);
    });

  }

  CargarCombo() {
    this.generalService.Obtenertodos('documento').then((result) => {
      this.resultado = result;
      for (let i = 0; i < this.resultado.data.length; i++) {
        this.listaDocumento.push({

          id: this.resultado.data[i].id,
          nombre: this.resultado.data[i].nombre,
        });
      }
    }, (err) => {
      console.log(err);
    });
  }

    // ELIMINAR ENTIDAD FISICAMENTE EN LA BD
    Eliminar(id) {
      this.generalService.EliminarFisico('recaudo_catalogo',id).then((result) => {
        this.openSnackBar('Registro Elimidado!', "Eliminar");
        this.CargarTabla();
      }, (err) => {
        console.log(err);
      });
    }

}
