import { Component, OnInit, ElementRef, ViewChild, Inject } from '@angular/core';
import { MatDialogRef, MatAutocompleteSelectedEvent, MatChipInputEvent, MatAutocomplete, MAT_DIALOG_DATA } from '@angular/material';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { GeneralService } from '../../../core/Services/general/general.service';

let resultado2: any;
@Component({
  selector: 'app-maestro',
  templateUrl: './maestro.component.html',
  styleUrls: ['./maestro.component.scss']
})
export class ModalCatalogoComponent implements OnInit {

  titulo = '';
  listaCategoria: any[] = [];
  resultado: any;
  form_cat = { nombre: '', descripcion: '', imagen: null, estatus: 'A', fecha_creacion: '11-22-2018', categoria_id: '' };
  incluir: boolean = false;
  imagen;//variable para mostrar las imágenes
  imagenDefecto: string = "assets/images/default.jpg";//imagen por defecto que se le pasa al incluir
  imagenSeleccionada: any = null; // variable tipo File que se utiliza para mostrar una vista previa de la imagen
  idCat;
  id;
  public loading = false;

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  fruitCtrl = new FormControl();
  filteredFruits: Observable<string[]>;
  fruits: string[] = [];
  Actuacion: string[] = [''];
  allFruits: string[] = ['Acta de matrimonio', 'titulo de propiedad', 'carta de solteria'];
  listaActuaciones: string[] = ['Apertura Expediente', 'Cierre Expediente', 'Revisar Expediente', 'Declaración Imputado', ' Diligencias Previas'];
  @ViewChild('fruitInput') fruitInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;

  constructor(public dialogRef: MatDialogRef<ModalCatalogoComponent>, public generalService: GeneralService, public snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.CargarCombo();
    if (data) {
      this.nombreFormControl.setValue(data.titulo);
      this.descripcionFormControl.setValue(data.contenido);
      this.categoriaFormControl.setValue(data.categoria_id);
      this.imagen = data.imagen;
      this.titulo = 'Actualizar';
      this.id = data.id;
    }
    else {
      this.incluir = true;
      this.titulo = 'Nuevo';
    }

    this.filteredFruits = this.fruitCtrl.valueChanges.pipe(
      startWith(null),
      map((fruit: string | null) => fruit ? this._filter(fruit) : this.allFruits.slice()));
  }

  categoriaFormControl = new FormControl('',
    [
      Validators.required
    ]);
  nombreFormControl = new FormControl('',
    [
      Validators.required
    ]);

  descripcionFormControl = new FormControl('',
    [
      Validators.required
    ]);

  imagenFormControl = new FormControl('',
    [
      Validators.required
    ]);
  // Funcion para las imágenes 
  imagenEntrada(img: FileList) {
    this.imagenSeleccionada = img.item(0);
    //Para mostrar un preview de la imagen
    var lector = new FileReader();
    lector.onload = (event: any) => {
      this.imagen = event.target.result;
      this.imagenDefecto = event.target.result;
    }
    lector.readAsDataURL(this.imagenSeleccionada);
  }

  // OBTIENE EL ID DE LA CATEGORIA DEL COMBO
  ObtenerIdCat(id) {
    return this.idCat = id;
  }

  // REGISTRAR UN NUEVO SERVICIO EN LA BASE DE DATOS
  Registrar() {
    let fecha = new Date();
    let hoy = fecha.getMonth() + '-' + fecha.getDate() + '-' + fecha.getFullYear();

    if (this.nombreFormControl.valid && this.descripcionFormControl.valid && this.categoriaFormControl.valid && this.imagenFormControl.valid) {
      const datos = new FormData();
      datos.append('imagen', this.imagenSeleccionada, this.imagenSeleccionada.name);
      datos.append('nombre', this.nombreFormControl.value);
      datos.append('descripcion', this.descripcionFormControl.value);
      datos.append('categoria_id', this.categoriaFormControl.value);
      datos.append('fecha_creacion', hoy);
      datos.append('estatus', 'A');
      this.loading = true;

      this.generalService.RegistrarConImagen(datos, 'catalogo_servicio').then((result) => {
        this.openSnackBar('Registro exitoso!', "registrar");
        this.imagenDefecto = "assets/images/default.jpg";
        this.loading = false;
        this.dialogRef.close(datos);
      }, (err) => {
        console.log(err);
        this.loading = false;
      });
    }
  }

  // ACTUALIZAR LOS DATOS DEL SERVICIO EN LA BD
  Actualizar() {

    if (this.nombreFormControl.valid && this.descripcionFormControl.valid && this.categoriaFormControl.valid) {
      const datos = new FormData();
      if (this.imagenSeleccionada != null) {
        datos.append('imagen', this.imagenSeleccionada, this.imagenSeleccionada.name);
      }
      datos.append('nombre', this.nombreFormControl.value);
      datos.append('descripcion', this.descripcionFormControl.value);
      datos.append('categoria_id', this.categoriaFormControl.value);
      this.loading = true;
      this.generalService.ActualizarConImagen(datos,'catalogo_servicio', this.id).then(result => {
        console.log(result);
        this.generalService.ObtenerUno('catalogo_servicio', this.id).then((result) => {
          resultado2 = result;
          this.loading = false;
          this.openSnackBar('Registro Actualizado!', "Actualizar");
          this.dialogRef.close(resultado2.data);
        }, (err) => {
          this.loading = false;
          console.log(err);
        });
      }, (err) => {
        this.loading = false;
        console.log(err);
      });

    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }

  add(event: MatChipInputEvent): void {
    // Add fruit only when MatAutocomplete is not open
    // To make sure this does not conflict with OptionSelected Event
    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const value = event.value;

      // Add our fruit
      if ((value || '').trim()) {
        this.fruits.push(value.trim());
      }

      // Reset the input value
      if (input) {
        input.value = '';
      }

      this.fruitCtrl.setValue(null);
    }
  }

  remove(fruit: string): void {
    const index = this.fruits.indexOf(fruit);

    if (index >= 0) {
      this.fruits.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.fruits.push(event.option.viewValue);
    this.fruitInput.nativeElement.value = '';
    this.fruitCtrl.setValue(null);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.allFruits.filter(fruit => fruit.toLowerCase().indexOf(filterValue) === 0);
  }

  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  CargarCombo() {
    this.generalService.Obtenertodos('categoria').then((result) => {
      this.resultado = result;
      for (let i = 0; i < this.resultado.data.length; i++) {
        this.listaCategoria.push({
          id: this.resultado.data[i].id,
          nombre: this.resultado.data[i].nombre,
        });
      }
    }, (err) => {
      console.log(err);
    });
  }
}
