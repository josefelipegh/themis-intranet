import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalActuacionRecaudoComponent } from './modal-actuacion-recaudo.component';

describe('ModalActuacionRecaudoComponent', () => {
  let component: ModalActuacionRecaudoComponent;
  let fixture: ComponentFixture<ModalActuacionRecaudoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalActuacionRecaudoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalActuacionRecaudoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
