import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MatSnackBar, MAT_DIALOG_DATA, MatTableDataSource, MatPaginator } from '@angular/material';
import { GeneralService } from '../../../core/Services/general/general.service';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Validators, FormControl } from '@angular/forms';

export interface ActuacionServicio {
  id:number;
  nombre: string;
  actuacion_id: number;
  actuacion_catalogo_id :number,
}

let tablaActuacion: any[] = [];

@Component({
  selector: 'app-modal-actuacion-recaudo',
  templateUrl: './modal-actuacion-recaudo.component.html',
  styleUrls: ['./modal-actuacion-recaudo.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ModalActuacionRecaudoComponent implements OnInit {

  displayedColumns: string[] = ['nombre', 'eliminar'];
  dataSource: MatTableDataSource<ActuacionServicio>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  resultado2:any;
  resultado: any;
  actuacion: ActuacionServicio; //permite llenar la tabla
  listaActuacion: any[] = []; //almacena actuaciones
  form_actuacion_catalogo = { actuacion_id: '', catalogo_servicio_id: '', estatus: 'A' };
  idAct;
  constructor(public dialogRef: MatDialogRef<ModalActuacionRecaudoComponent>,
    public generalService: GeneralService, public snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.CargarCombo();
      this.CargarTabla();
      console.log(this.data);
     }

     actuacionFormControl = new FormControl('',
    [
      Validators.required
    ]);

  ngOnInit() {
  }

  // CERRAR EL MODAL
  onclick() {
    this.dialogRef.close()
  }

   // OBTIENE EL ID DE LA ACTUACION DEL COMBO
   ObtenerId(id) {
    return this.idAct = id;
   
   }

    // REGISTRAR UNA ACTUACION POR SERVICIO EN LA BD
  Registrar()
  {
      this.form_actuacion_catalogo.catalogo_servicio_id = this.data;
      this.form_actuacion_catalogo.actuacion_id = this.actuacionFormControl.value
     if (this.actuacionFormControl.valid) 
     {
      if(! tablaActuacion.find(x => x.actuacion_id == this.actuacionFormControl.value))
      {
       this.generalService.Registrar(this.form_actuacion_catalogo, 'actuacion_catalogo').then((result) => {
         this.openSnackBar('Registro exitoso!', "registrar");
         this.CargarTabla();
       }, (err) => {
         console.log(err);
       });
     }else{
      this.openSnackBar('La actuación seleccionada está asociada a este servicio', "Seleccione otra actuación");
     }
    }
   }

  // ELIMINAR ENTIDAD FISICAMENTE EN LA BD
  Eliminar(id) {
    this.generalService.EliminarFisico('actuacion_catalogo',id).then((result) => {
      this.openSnackBar('Registro Elimidado!', "Eliminar");
      this.CargarTabla();
    }, (err) => {
      console.log(err);
    });
  }
 
   openSnackBar(message: string, action: string) {
     this.snackBar.open(message, action, {
       duration: 1000,
     });
   }

    // METEDO QUE OBTIENE UNA LISTA DE ACTUACIONES POR CATALOGO Y SE CARGA EN LA TABLA
    CargarTabla() {
      this.generalService.ObtenerUno('vista_actuacion_catalogo/catalogo',this.data).then((result) => {
        this.resultado2 = result;
        tablaActuacion = [];
        for (let i = 0; i < this.resultado2.data.length; i++) {
          this.actuacion = { 
            id: this.resultado2.data[i].id,
            nombre: this.resultado2.data[i].nombre,
            actuacion_id: this.resultado2.data[i].actuacion_id,
            actuacion_catalogo_id: this.resultado2.data[i].actuacion_catalogo_id
          };
          tablaActuacion.push(this.actuacion);
        }
        this.dataSource = new MatTableDataSource(tablaActuacion);
        this.dataSource.paginator = this.paginator;
      }, (err) => {
        console.log(err);
      });
  
    }
  
    CargarCombo() {
      this.generalService.Obtenertodos('actuacion').then((result) => {
        this.resultado = result;
        for (let i = 0; i < this.resultado.data.length; i++) {
          this.listaActuacion.push({
            
            id: this.resultado.data[i].id,
            nombre: this.resultado.data[i].nombre,
          });
        }
      }, (err) => {
        console.log(err);
      });
    }

}
