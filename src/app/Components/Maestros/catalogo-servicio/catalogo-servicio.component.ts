import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ModalCatalogoComponent } from './modal-catalogo/maestro.component';
import { GeneralService } from '../../core/Services/general/general.service';
import { ModalPreguntaEliminarComponent } from '../../shared/modal-pregunta-eliminar/modal-pregunta-eliminar.component';
import { ModalRecaudoComponent } from './modal-recaudo/modal-recaudo.component';
import { ModalActuacionRecaudoComponent } from './modal-actuacion-recaudo/modal-actuacion-recaudo.component';
import { ModalValoracionComponent } from './modal-valoracion/modal-valoracion.component';

export interface categoria {
  id: number;
  nombre: string;
}

@Component({
  selector: 'app-catalogo-servicio',
  templateUrl: './catalogo-servicio.component.html',
  styleUrls: ['./catalogo-servicio.component.scss'],
})
export class CatalogoServicioComponent implements OnInit {

  // variable que se usa para limpiar los valores de la caja de texto
  // con el atributo en la etiqueta input vista que se llama ngModel
  value = '';

  // lista para cargar el combo de categoria
  listaCategoria: categoria[] = [];

  resultado: any;

  nombre: any;

  categoria: categoria;

  listaServicios: any[] = [];
  listaServicios2: any[] = [];

  constructor(public dialog: MatDialog, public generalService: GeneralService ) {
    this.CargarCombo();
  }

  openDialogRegistrar() //abre una ventana modal
  {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    

    const dialogRef = this.dialog.open(ModalCatalogoComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD 
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.CargarCatalogo()
        }
      }
    );
  }
  openDialogRecaudo(row) //abreRecaudo una ventana modal
  {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    dialogConfig.data = row;
    const dialogRef = this.dialog.open(ModalRecaudoComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD 
    dialogRef.afterClosed().subscribe(
      data => {
 
      }
    );
  }

  openDialogActuacion(row) //abreRecaudo una ventana modal
  {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    dialogConfig.data = row;
    const dialogRef = this.dialog.open(ModalActuacionRecaudoComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD 
    dialogRef.afterClosed().subscribe(
      data => {
 
      }
    );
  }

  openDialogValoracion(row) //abreRecaudo una ventana modal
  {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '80%';
    dialogConfig.data = row;
    const dialogRef = this.dialog.open(ModalValoracionComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD 
    dialogRef.afterClosed().subscribe(
      data => {
 
      }
    );
  }


  openDialogActualizar(row) //abre una ventana modal con datos
  {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    // PARA PASAR LOS DATOS AL DIALOG
    dialogConfig.data = row;

    const dialogRef = this.dialog.open(ModalCatalogoComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD 
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          let index = this.listaServicios.findIndex(x => x.id == data.id);
          let sub = this.nombreCateroria(data.categoria_id);
          this.listaServicios[index] = {
            id: data.id,
            titulo: data.nombre,
            subtitulo: sub,
            contenido: data.descripcion,
            categoria_id: data.categoria_id,
            imagen: data.imagen
          }
        }
      }
    );
  }

  // PARA ABRIR MODAL PREGUNTAR ESTÁ SEGURO DE ELIMNIAR ESTE MODAL ES GENERICO USEN EL QUE YA ESTA CREADO
  openDialogEliminar(row) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    // PARA PASAR LOS DATOS AL DIALOG PREGUNTA ELIMINAR 
    dialogConfig.data = {
      id: row,
      nombreMetodo: 'catalogo_servicio'
    };

    const dialogRef = this.dialog.open(ModalPreguntaEliminarComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD 
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          let index = this.listaServicios.findIndex(x => x.id == data);
          this.listaServicios.splice(index, 1);
        }
      }
    );
  }

  ngOnInit() {

  }

  CargarCombo() {
    this.generalService.Obtenertodos('categoria').then((result) => {
      this.resultado = result;
      for (let i = 0; i < this.resultado.data.length; i++) {
        this.categoria = {
          id: this.resultado.data[i].id,
          nombre: this.resultado.data[i].nombre,
        }
        this.listaCategoria.push(this.categoria);
      }
      this.CargarCatalogo();
    }, (err) => {
      console.log(err);
    });
  }

  FiltrarCatalogo(idcat) {
    console.log(this.listaServicios2);
    this.listaServicios = this.listaServicios2.filter(x => x.categoria_id == idcat);
  }

  FiltrarCatalogoNombre() {
    if (this.value != "") {
      this.listaServicios = this.listaServicios2
        .filter(x => x.titulo.trim()
          .toLowerCase().startsWith(this.value.trim().toLowerCase()) == true);
    } else {
      this.ReiniciarCatalogo();
    }
  }

  valueInput() {
    this.value = '';
    this.ReiniciarCatalogo();
  }

  ReiniciarCatalogo() {
    this.listaServicios = this.listaServicios2;
  }

  nombreCateroria(id2) {
    let nom = this.listaCategoria.find(x => x.id == id2);
    return nom.nombre;
  }
  CargarCatalogo() {
    this.listaServicios = [];
    this.generalService.Obtenertodos('catalogo_servicio').then((result) => {
      this.resultado = result;
      for (let i = 0; i < this.resultado.data.length; i++) {
        let sub = this.nombreCateroria(this.resultado.data[i].categoria_id);
        this.listaServicios.push({
          id: this.resultado.data[i].id,
          titulo: this.resultado.data[i].nombre,
          subtitulo: sub,
          contenido: this.resultado.data[i].descripcion,
          categoria_id: this.resultado.data[i].categoria_id,
          imagen: this.resultado.data[i].imagen
        });
      }
      this.listaServicios2 = this.listaServicios;
    }, (err) => {
      console.log(err);
    });
  }
}