import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { GeneralService } from '../../../core/Services/general/general.service';


@Component({
  selector: 'app-modal-tipo-respuesta',
  templateUrl: './modal-tipo-respuesta.component.html',
  styleUrls: ['./modal-tipo-respuesta.component.scss']
})
export class ModalTipoRespuestaComponent implements OnInit {

 
//codigo
descripcionFormControl = new FormControl();
onCommentChange() {
  console.log(this.descripcionFormControl.value);
}
//codigo

form_descripcion = { nombre: '', descripcion: '',  estatus: 'A' };
incluir: boolean = false;
id;

constructor(public dialogRef: MatDialogRef<ModalTipoRespuestaComponent>,public generalService: GeneralService, public snackBar: MatSnackBar,
  @Inject(MAT_DIALOG_DATA) public data: any) {

    if (data) {
      this.nombreFormControl.setValue(data.nombre);
      this.descripcionFormControl.setValue(data.descripcion);
      this.id = data.id;
    } else {
      this.incluir = true;
    }

   }
 

   nombreFormControl = new FormControl('', [
    Validators.required
  ]);
  



  Registrar() {
    this.form_descripcion.nombre = this.nombreFormControl.value;
    //agregado
    this.form_descripcion.descripcion = this.descripcionFormControl.value;
    if (this.nombreFormControl.valid) {
      this.generalService.Registrar(this.form_descripcion, 'tipo_respuesta').then((result) => {
        this.openSnackBar('Registro Exitoso!', "registrar");
      }, (err) => {
        console.log(err);
      });
      this.dialogRef.close(this.form_descripcion);



    }
  }

  Actualizar() {

    if (this.nombreFormControl.valid) {
      this.form_descripcion.nombre = this.nombreFormControl.value;
      this.form_descripcion.descripcion = this.descripcionFormControl.value;
      this.generalService.Actualizar(this.form_descripcion, 'tipo_respuesta', this.id).then((result) => {
        this.openSnackBar('Registro Actualizado!', "Actualizar");
      }, (err) => {
        console.log(err);
      });
      this.data.nombre = this.nombreFormControl.value
      this.data.descripcion = this.descripcionFormControl.value
      this.dialogRef.close(this.data);
    }
  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }
ngOnInit() {
}
onNoClick() {
  this.dialogRef.close();
}
  

}
