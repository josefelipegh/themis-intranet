import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalTipoRespuestaComponent } from './modal-tipo-respuesta.component';

describe('ModalTipoRespuestaComponent', () => {
  let component: ModalTipoRespuestaComponent;
  let fixture: ComponentFixture<ModalTipoRespuestaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalTipoRespuestaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalTipoRespuestaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
