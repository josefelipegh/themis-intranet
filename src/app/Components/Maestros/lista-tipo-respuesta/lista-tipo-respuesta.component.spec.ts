import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaTipoRespuestaComponent } from './lista-tipo-respuesta.component';

describe('ListaTipoRespuestaComponent', () => {
  let component: ListaTipoRespuestaComponent;
  let fixture: ComponentFixture<ListaTipoRespuestaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaTipoRespuestaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaTipoRespuestaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
