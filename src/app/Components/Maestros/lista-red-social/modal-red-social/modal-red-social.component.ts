import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { GeneralService } from '../../../core/Services/general/general.service';
import { Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-modal-red-social',
  templateUrl: './modal-red-social.component.html',
  styleUrls: ['./modal-red-social.component.scss']
})
export class ModalRedSocialComponent implements OnInit {

  form_descripcion = { nombre: '', estatus: 'A' };
  incluir: boolean = false;
  id;
  titulo = '';
  constructor(public dialogRef: MatDialogRef<ModalRedSocialComponent>,
    public generalService: GeneralService, public snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    // PARA CARGAR LA CAJA DE TEXTO CUANDO SE VA ACTUALIZAR DATOS
    if (data) {
      this.nombreFormControl.setValue(data.nombre);
      this.id = data.id;
      this.titulo = 'Actualizar';
    } else {
      this.incluir = true;
      this.titulo = 'Nueva';
    }
  }

  nombreFormControl = new FormControl('', [
    Validators.required
  ]);

  ngOnInit() {
  }
  onNoClick() {
    this.dialogRef.close();
  }

  // REGISTRAR UN ROL EN LA BD
  Registrar() {
    this.form_descripcion.nombre = this.nombreFormControl.value;
    if (this.nombreFormControl.valid) {
      this.generalService.Registrar(this.form_descripcion, 'red_social').then((result) => {
        this.openSnackBar('Registro exitoso!', "registrar");
      }, (err) => {
        console.log(err);
      });
      this.dialogRef.close(this.form_descripcion);
    }
  }
  // ACTUALIZAR NOMBRE DEL ROL EN LA BD
  Actualizar() {

    if (this.nombreFormControl.valid) {
      this.form_descripcion.nombre = this.nombreFormControl.value;
      this.generalService.Actualizar(this.form_descripcion, 'red_social', this.id).then((result) => {
        this.openSnackBar('Registro Actualizado!', "Actualizar");
      }, (err) => {
        console.log(err);
      });
      this.data.nombre = this.nombreFormControl.value
      this.dialogRef.close(this.data);
    }
  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }
}
