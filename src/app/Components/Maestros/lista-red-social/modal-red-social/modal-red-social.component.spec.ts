import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalRedSocialComponent } from './modal-red-social.component';

describe('ModalRedSocialComponent', () => {
  let component: ModalRedSocialComponent;
  let fixture: ComponentFixture<ModalRedSocialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalRedSocialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalRedSocialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
