import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { GeneralService } from '../../../core/Services/general/general.service';
import { UrlService } from '../../../core/Services/url/url.service';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';


@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.scss']
})

export class ModalCategoriaComponent implements OnInit {
  titulo = '';
  especialidad:  any[]=[];
  resultado: any;
  resultado2: any;
  incluir: boolean = false;
  imagen;//variable para mostrar las imágenes
  imagenDefecto: string = "assets/images/default.jpg";//imagen por defecto que se le pasa al incluir
  imagenSeleccionada: File = null; // variable tipo File que se utiliza para mostrar una vista previa de la imagen
  public loading = false; //para mostrar los tres puntitos al incluir o modificar la imagen
  idCategoria; //IDCATEGORIA DE SERVICIO
  idEspecialidad = '';// ID ESPECIALIDAD
  constructor(public dialogRef: MatDialogRef<ModalCategoriaComponent>, public generalService: GeneralService, public snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.cargarEspecialidad();

    if (data) {
      this.nombreFormControl.setValue(data.nombre);
      this.descripcionFormControl.setValue(data.descripcion);
      this.especialidadFormControl.setValue(data.especialidad_id);
      this.imagen = data.imagen;
      this.titulo='Actualizar';
      this.idCategoria = data.id;
      console.log(data);
    }
    else {
      this.incluir = true;
      this.titulo='Nueva';
    }
  }

  especialidadFormControl = new FormControl('',
    [
      Validators.required
    ]);
  nombreFormControl = new FormControl('',
    [
      Validators.required
    ]);

  descripcionFormControl = new FormControl('',
    [
      Validators.required
    ]);

    imagenFormControl= new FormControl('',
    [
      Validators.required
    ]);

  // Funcion para las imágenes 
  imagenEntrada(img: FileList) {
    this.imagenSeleccionada = img.item(0);
    //Para mostrar un preview de la imagen
    var lector = new FileReader();
    lector.onload = (event: any) => {
      this.imagen = event.target.result;
      this.imagenDefecto = event.target.result;
    }
    lector.readAsDataURL(this.imagenSeleccionada);
  }

  filteredEspecialidad: Observable<string[]>;
  ngOnInit() {

  }
  onNoClick() {
    this.dialogRef.close();
  }


  //retorna una especialidad
  ObtenerIdEspec(id)
  {
    return this.idEspecialidad=id;
  }
  

  //REGISTRA UNA NUEVA CATEGORIA EN LA BD


  Registrar() {

    if (this.nombreFormControl.valid && this.descripcionFormControl.valid && this.especialidadFormControl.valid && this.imagenFormControl.valid) {
      const datos = new FormData();
      datos.append('nombre',this.nombreFormControl.value);
      datos.append('descripcion',this.descripcionFormControl.value);
      datos.append('imagen',this.imagenSeleccionada, this.imagenSeleccionada.name);
      datos.append('especialidad_id',this.idEspecialidad);
      datos.append('estatus', 'A');
      this.loading = true;
      console.log(this.idEspecialidad);
      this.generalService.RegistrarConImagen(datos, 'categoria').then((result) => {
        this.openSnackBar('Registro exitoso!', "registrar");
        this.imagenDefecto = "assets/images/default.jpg";
        this.loading = false;
        this.dialogRef.close(datos);
      }, (err) => {
        console.log(err);
        this.loading = false;
      });
    }
  }

  //MODIFICAR UNA CATEGORIA DE LA BASE DE DATOS

  Actualizar()
  {
    if (this.nombreFormControl.valid && this.descripcionFormControl.valid && this.especialidadFormControl.valid) {
      const datos = new FormData();
      datos.append('nombre',this.nombreFormControl.value);
      datos.append('descripcion',this.descripcionFormControl.value);
      if (this.imagenSeleccionada != null) 
      {
        datos.append('imagen', this.imagenSeleccionada, this.imagenSeleccionada.name);
      }
      console.log(this.idEspecialidad);
      if (this.idEspecialidad != '')
      {
        console.log(this.idEspecialidad);
        datos.append('especialidad_id', this.idEspecialidad);
      }
      this.loading = true;

      this.generalService.ActualizarConImagen(datos,'categoria',this.idCategoria).then((result) => {

        this.generalService.ObtenerUno('categoria', this.idCategoria).then((result) => {
          this.resultado2 = result;
          this.loading = false;
          this.openSnackBar('Registro Actualizado!', "Actualizar");
          this.dialogRef.close(this.resultado2.data);
        }, (err) => {
          this.loading = false;
          console.log(err);
        });

      }, (err) => {
        console.log(err);
        this.loading = false;
      });
    }

  }
 
  //CargaCombo especialidad
  cargarEspecialidad() {
    this.generalService.Obtenertodos('especialidad').then((result) => {
      this.resultado = result;
      for (let i = 0; i < this.resultado.data.length; i++) {
        this.especialidad.push(
          {
            id: this.resultado.data[i].id,
            nombre: this.resultado.data[i].nombre,
          }
        );
      }
    }, (err) => {
      console.log(err);
    });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }

}
