import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MaestrosComponent } from './maestros/maestros.component';
import { CatalogoServicioComponent } from './catalogo-servicio/catalogo-servicio.component';
import { ListaActuacionComponent } from './lista-actuacion/lista-actuacion.component';
import { ListaBloqueHoraComponent } from './lista-bloque-hora/lista-bloque-hora.component';
import { ListaCaracteristicaComponent } from './lista-caracteristica/lista-caracteristica.component';
import { ListaCaracteristicaBaseComponent } from './lista-caracteristica-base/lista-caracteristica-base.component';
import { ListaCategoriaComponent } from './lista-categoria/lista-categoria.component';
import { ListaDiaSemanaComponent } from './lista-dia-semana/lista-dia-semana.component';
import { ListaDocumentoComponent } from './lista-documento/lista-documento.component';
import { ListaEspecialidadComponent } from './lista-especialidad/lista-especialidad.component';
import { ListaFuncionComponent } from './lista-funcion/lista-funcion.component';
import { ListaRangoValoracionComponent } from './lista-rango-valoracion/lista-rango-valoracion.component';
import { ListaRedSocialComponent } from './lista-red-social/lista-red-social.component';
import { ListaRolComponent } from './lista-rol/crudrol.component';
import { ListaTipoClienteComponent } from './lista-tipo-cliente/lista-tipo-cliente.component';
import { ListaTipoDocumentoComponent } from './lista-tipo-documento/lista-tipo-documento.component';
import { ListaTipoEmpleadoComponent } from './lista-tipo-empleado/lista-tipo-empleado.component';
import { ListaTipoGarantiaComponent } from './lista-tipo-garantia/lista-tipo-garantia.component';
import { ListaTipoIncidenciaComponent } from './lista-tipo-incidencia/lista-tipo-incidencia.component';
import { ListaTipoReclamoComponent } from './lista-tipo-reclamo/lista-tipo-reclamo.component';
import { ListaTipoValoracionComponent } from './lista-tipo-valoracion/lista-tipo-valoracion.component';
import { ListaTipoActuacionComponent } from './lista-tipo-actuacion/lista-tipo-actuacion.component';
import { ListaRutaComponent } from './lista-ruta/lista-ruta.component';
import { ListaTipoRespuestaComponent } from './lista-tipo-respuesta/lista-tipo-respuesta.component';
import { AuthGuardService as AuthGuard } from './../core/Services/AuthGuard/auth-guard.service';
const routes: Routes = [
{ path: 'maestros', component: MaestrosComponent, canActivate: [AuthGuard]},
{ path: 'catalogo', component: CatalogoServicioComponent, canActivate: [AuthGuard] },
{ path: 'actuacion', component: ListaActuacionComponent, canActivate: [AuthGuard] },
{ path: 'bloque_hora', component: ListaBloqueHoraComponent, canActivate: [AuthGuard] },
{ path: 'caracteristica', component: ListaCaracteristicaComponent, canActivate: [AuthGuard] },
{ path: 'caracteristica_base', component: ListaCaracteristicaBaseComponent, canActivate: [AuthGuard] },
{ path: 'categoria', component: ListaCategoriaComponent, canActivate: [AuthGuard] },
{ path: 'dia_semana', component: ListaDiaSemanaComponent, canActivate: [AuthGuard] },
{ path: 'documento', component: ListaDocumentoComponent, canActivate: [AuthGuard] },
{ path: 'especialidad', component: ListaEspecialidadComponent, canActivate: [AuthGuard] },
{ path: 'funcion', component: ListaFuncionComponent, canActivate: [AuthGuard] },
{ path: 'rango_valoracion', component: ListaRangoValoracionComponent, canActivate: [AuthGuard] },
{ path: 'red_social', component: ListaRedSocialComponent, canActivate: [AuthGuard] },
{ path: 'rol', component: ListaRolComponent, canActivate: [AuthGuard] },
{ path: 'ruta', component: ListaRutaComponent, canActivate: [AuthGuard] },
{ path: 'tipo_cliente', component: ListaTipoClienteComponent, canActivate: [AuthGuard] },
{ path: 'tipo_documento', component: ListaTipoDocumentoComponent, canActivate: [AuthGuard] },
{ path: 'documento', component: ListaDocumentoComponent, canActivate: [AuthGuard]},
{ path: 'tipo_empleado', component: ListaTipoEmpleadoComponent, canActivate: [AuthGuard] },
{ path: 'tipo_garantia', component: ListaTipoGarantiaComponent, canActivate: [AuthGuard] },
{ path: 'tipo_incidencia', component: ListaTipoIncidenciaComponent, canActivate: [AuthGuard] },
{ path: 'tipo_reclamo', component: ListaTipoReclamoComponent, canActivate: [AuthGuard] },
{ path: 'tipo_valoracion', component: ListaTipoValoracionComponent, canActivate: [AuthGuard] },
{ path: 'tipo_actuacion', component: ListaTipoActuacionComponent, canActivate: [AuthGuard] },
{ path: 'tipo_respuesta', component: ListaTipoRespuestaComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaestrosRoutingModule { }
