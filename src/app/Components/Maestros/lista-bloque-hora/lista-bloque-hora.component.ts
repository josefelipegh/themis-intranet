import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { ModalBloqueHoraComponent } from './modal-bloque-hora/modal-bloque-hora.component';
import {MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Location } from '@angular/common';
import { GeneralService } from '../../core/Services/general/general.service';
import { ModalPreguntaEliminarComponent } from '../../shared/modal-pregunta-eliminar/modal-pregunta-eliminar.component';
import * as moment from 'moment';
export interface BloqueData {
  id: string;
  hora_inicio: string;
  hora_fin: string;
  estatus: string;
}

let bloque: any[] = [];

@Component({
  selector: 'app-lista-bloque-hora',
  templateUrl: './lista-bloque-hora.component.html',
  styleUrls: ['./lista-bloque-hora.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ListaBloqueHoraComponent implements OnInit {
  displayedColumns: string[] = ['ver','hora_inicio', 'hora_fin', 'eliminar'];
  dataSource: MatTableDataSource<BloqueData>;
  resultado: any;
  // OBJETO PARA CARGAR EN LA TABLA
  datos: BloqueData;
  myDate: Date;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  value="";
  valueInput(){
    this.value="";
    this.applyFilter(this.value);
  }

  constructor(public dialog: MatDialog, public generalService: GeneralService,
    private location: Location)
   {
    this.CargarTabla();
    this.myDate = new Date();
   }

   openDialogRegistrar() // abre una ventana modal
  {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';

    const dialogRef = this.dialog.open(ModalBloqueHoraComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD 
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.CargarTabla();
        }
      }
    );
  }

   ngOnInit() {

  }

  openDialogActualizar(row) // abre una ventana modal
  {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    // PARA PASAR LOS DATOS AL DIALOG
    dialogConfig.data = row;

    const dialogRef = this.dialog.open(ModalBloqueHoraComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD 
    dialogRef.afterClosed().subscribe(
      data => {
        if(data){
           this.dataSource.data.find(x => x.id == data.id).hora_inicio = data.hora_inicio;
           this.dataSource.data.find(y => y.id == data.id).hora_fin = data.hora_fin;
        }
      }
    );
  }
  openDialogEliminar(row) 
  {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    // PARA PASAR LOS DATOS AL DIALOG PREGUNTA ELIMINAR 
    dialogConfig.data = {
      id: row,
      nombreMetodo: 'bloque_hora'
    };

    const dialogRef = this.dialog.open(ModalPreguntaEliminarComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD 
    dialogRef.afterClosed().subscribe(
      data => {
        if(data){
          this.dataSource.data.find(x => x.id == data).estatus = 'Inactivo';
        }
      }
    );
  }

  CargarTabla() {
    this.generalService.Obtenertodos('bloque_hora').then((result) => {
      this.resultado = result;
      bloque = [];
      for (let i = 0; i < this.resultado.data.length; i++) {
        let est = this.Estatus(this.resultado.data[i].estatus);
        this.datos = {
          id: this.resultado.data[i].id,
          hora_inicio: moment(this.resultado.data[i].hora_inicio, ['HH:mm:ss']).format('LT'),
          hora_fin: moment(this.resultado.data[i].hora_fin, ['HH:mm:ss']).format('LT'),
          estatus: est,
        };
        bloque.push(this.datos);
      }
      this.dataSource = new MatTableDataSource(bloque);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, (err) => {
      console.log(err);
    });

  }

  Estatus(est) {
    let estatus;
    return estatus = est == 'A' ? 'Activo' : 'Inactivo';
  }
  volver() {
    this.location.back();
  }
  openDialog() //abre una ventana modal
   {
    this.dialog.open(ModalBloqueHoraComponent, { width: '50%'});
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  

}
