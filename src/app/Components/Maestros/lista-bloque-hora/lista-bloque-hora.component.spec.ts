import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaBloqueHoraComponent } from './lista-bloque-hora.component';

describe('ListaBloqueHoraComponent', () => {
  let component: ListaBloqueHoraComponent;
  let fixture: ComponentFixture<ListaBloqueHoraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaBloqueHoraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaBloqueHoraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
