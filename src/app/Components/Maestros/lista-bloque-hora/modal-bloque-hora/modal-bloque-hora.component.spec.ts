import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalBloqueHoraComponent } from './modal-bloque-hora.component';

describe('ModalBloqueHoraComponent', () => {
  let component: ModalBloqueHoraComponent;
  let fixture: ComponentFixture<ModalBloqueHoraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalBloqueHoraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalBloqueHoraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
