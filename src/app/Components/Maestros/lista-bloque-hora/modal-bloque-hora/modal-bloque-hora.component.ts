import { Component, OnInit, Inject} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { GeneralService } from '../../../core/Services/general/general.service';
import * as moment from 'moment';

@Component({
  selector: 'app-modal-bloque-hora',
  templateUrl: './modal-bloque-hora.component.html',
  styleUrls: ['./modal-bloque-hora.component.scss']
})
export class ModalBloqueHoraComponent implements OnInit {
  form_descripcion = { hora_inicio: '', hora_fin: '', estatus: 'A' };
  incluir = false;
  id;
  titulo = '';
  t = new Date();
  t1 = new Date(this.t.getFullYear(), this.t.getMonth() + 1, this.t.getDate(), this.t.getHours(), this.t.getMinutes(), this.t.getSeconds());
  t2 = new Date(this.t.getFullYear(), this.t.getMonth() + 1, this.t.getDate(), this.t.getHours(), this.t.getMinutes(), this.t.getSeconds());
  constructor(public dialogRef: MatDialogRef<ModalBloqueHoraComponent>,
              public generalService: GeneralService,
              public snackBar: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) public data: any) {
      if (data) {
        this.t1 = new Date(this.t.getFullYear(),
                           this.t.getMonth() + 1,
                           this.t.getDate(),
                           parseInt(moment(data.hora_inicio, ['LT']).format('HH'), 10),
                           parseInt(moment(data.hora_inicio, ['LT']).format('mm'), 10),
                           parseInt(moment(data.hora_inicio, ['LT']).format('ss'), 10));
        this.t2 = new Date(this.t.getFullYear(),
                          this.t.getMonth() + 1,
                          this.t.getDate(),
                          parseInt(moment(data.hora_fin, ['LT']).format('HH'), 10),
                          parseInt(moment(data.hora_fin, ['LT']).format('mm'), 10),
                          parseInt(moment(data.hora_fin, ['LT']).format('ss'), 10));
        this.id = data.id;
        this.titulo = 'Actualizar';
      } else {
        this.incluir = true;
        this.titulo = 'Nuevo';
      }
      console.log(data, this.t1);
    }

    Registrar() {
      this.form_descripcion.hora_inicio = moment(this.t1).format('HH:mm:ss');
      this.form_descripcion.hora_fin = moment(this.t2).format('HH:mm:ss');
        this.generalService.Registrar(this.form_descripcion, 'bloque_hora').then((result) => {
          this.openSnackBar('Registro Exitoso!', 'registrar');
        }, (err) => {
          console.log(err);
        });
        this.dialogRef.close(this.form_descripcion);
    }

    Actualizar() {
        this.form_descripcion.hora_inicio = moment(this.t1).format('HH:mm:ss');
        this.form_descripcion.hora_fin = moment(this.t2).format('HH:mm:ss');
        this.generalService.Actualizar(this.form_descripcion, 'bloque_hora', this.id).then((result) => {
          this.openSnackBar('Registro Actualizado!', 'Actualizar');
        }, (err) => {
          console.log(err);
        });
        this.data.hora_inicio = this.t1;
        this.data.hora_fin = this.t2;
        this.dialogRef.close(this.data);
    }

    openSnackBar(message: string, action: string) {
      this.snackBar.open(message, action, {
        duration: 1000,
      });
    }

  ngOnInit() {
  }

  onNoClick() {
    this.dialogRef.close();
  }

}
