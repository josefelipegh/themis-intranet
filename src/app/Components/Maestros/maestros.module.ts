import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaestrosRoutingModule } from './maestros-routing.module';
import { CatalogoServicioComponent } from './catalogo-servicio/catalogo-servicio.component';
import { ListaActuacionComponent } from './lista-actuacion/lista-actuacion.component';
import { ListaBloqueHoraComponent } from './lista-bloque-hora/lista-bloque-hora.component';
import { ListaCaracteristicaBaseComponent } from './lista-caracteristica-base/lista-caracteristica-base.component';
import { ListaCaracteristicaComponent } from './lista-caracteristica/lista-caracteristica.component';
import { ListaCategoriaComponent } from './lista-categoria/lista-categoria.component';
import { ListaDiaSemanaComponent } from './lista-dia-semana/lista-dia-semana.component';
import { ListaDocumentoComponent } from './lista-documento/lista-documento.component';
import { ListaEspecialidadComponent } from './lista-especialidad/lista-especialidad.component';
import { ListaFuncionComponent } from './lista-funcion/lista-funcion.component';
import { ListaRangoValoracionComponent } from './lista-rango-valoracion/lista-rango-valoracion.component';
import { ListaRedSocialComponent } from './lista-red-social/lista-red-social.component';
import { ListaRolComponent } from './lista-rol/crudrol.component';
import { ListaTipoClienteComponent } from './lista-tipo-cliente/lista-tipo-cliente.component';
import { ListaTipoDocumentoComponent } from './lista-tipo-documento/lista-tipo-documento.component';
import { ListaTipoEmpleadoComponent } from './lista-tipo-empleado/lista-tipo-empleado.component';
import { ListaTipoGarantiaComponent } from './lista-tipo-garantia/lista-tipo-garantia.component';
import { ListaTipoIncidenciaComponent } from './lista-tipo-incidencia/lista-tipo-incidencia.component';
import { ListaTipoReclamoComponent } from './lista-tipo-reclamo/lista-tipo-reclamo.component';
import { ModalActuacionComponent } from './lista-actuacion/modal-actuacion/maestroactuacion.component';
import { ModalCatalogoComponent } from './catalogo-servicio/modal-catalogo/maestro.component';
import { ModalCategoriaComponent } from './lista-categoria/modal-categoria/categoria.component';
import { ModalEspecialidadComponent } from './lista-especialidad/modal-especialidad/especialidad.component';
import { ModalrolComponent } from './lista-rol/modal-rol/modalrol.component';
import { SharedModule } from '../shared/shared.module';
import { TipoDocumentoComponent } from './lista-tipo-documento/tipo-documento/tipo-documento.component';
import { MaestrosComponent } from './maestros/maestros.component';
import { ModalFuncionComponent } from './lista-funcion/modal-funcion/modal-funcion.component';
import { ModalTipoEmpleadoComponent } from './lista-tipo-empleado/modal-tipo-empleado/modal-tipo-empleado.component';
import { ModalBloqueHoraComponent } from './lista-bloque-hora/modal-bloque-hora/modal-bloque-hora.component';
import { ModalDiaSemanaComponent } from './lista-dia-semana/modal-dia-semana/modal-dia-semana.component';
import { ModalRedSocialComponent } from './lista-red-social/modal-red-social/modal-red-social.component';
import { ModalTipoReclamoComponent } from './lista-tipo-reclamo/modal-tipo-reclamo/modal-tipo-reclamo.component';
import { ModalTipoIncidenciaComponent } from './lista-tipo-incidencia/modal-tipo-incidencia/modal-tipo-incidencia.component';
import { ModalTipoGarantiaComponent } from './lista-tipo-garantia/modal-tipo-garantia/modal-tipo-garantia.component';
import { ModalTipoClienteComponent } from './lista-tipo-cliente/modal-tipo-cliente/modal-tipo-cliente.component';
import {
  ModalCaracteristicaBaseComponent
 } from './lista-caracteristica-base/modal-caracteristica-base/modal-caracteristica-base.component';
import { ModalCaracteristicaComponent } from './lista-caracteristica/modal-caracteristica/modal-caracteristica.component';
import { ListaTipoValoracionComponent } from './lista-tipo-valoracion/lista-tipo-valoracion.component';
import { ModalTipoValoracionComponent } from './lista-tipo-valoracion/modal-tipo-valoracion/modal-tipo-valoracion.component';
import { ListaTipoActuacionComponent } from './lista-tipo-actuacion/lista-tipo-actuacion.component';
import { ModalTipoActuacionComponent } from './lista-tipo-actuacion/modal-tipo-actuacion/modal-tipo-actuacion.component';
import { HttpClientModule } from '@angular/common/http';

import { ListaRutaComponent } from '../Maestros/lista-ruta/lista-ruta.component';
import { ModalRutaComponent } from '../Maestros/lista-ruta/modal-ruta/modal-ruta.component';
import { ListaTipoRespuestaComponent } from './lista-tipo-respuesta/lista-tipo-respuesta.component';
import { ModalTipoRespuestaComponent } from './lista-tipo-respuesta/modal-tipo-respuesta/modal-tipo-respuesta.component';
import { ModalRangoValoracionComponent } from './lista-rango-valoracion/modal-rango-valoracion/modal-rango-valoracion.component';
import { ModalDocumentoComponent } from './lista-documento/modal-documento/modal-documento.component';
import { ModalRecaudoComponent } from './catalogo-servicio/modal-recaudo/modal-recaudo.component';
import { ModalActuacionRecaudoComponent } from './catalogo-servicio/modal-actuacion-recaudo/modal-actuacion-recaudo.component';
import { ModalValoracionComponent } from './catalogo-servicio/modal-valoracion/modal-valoracion.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MaestrosRoutingModule,
    HttpClientModule
  ],
  declarations: [
    CatalogoServicioComponent,
    ListaActuacionComponent,
    ListaBloqueHoraComponent,
    ListaCaracteristicaBaseComponent,
    ListaCaracteristicaComponent,
    ListaCategoriaComponent,
    ListaDiaSemanaComponent,
    ListaDocumentoComponent,
    ListaEspecialidadComponent,
    ListaFuncionComponent,
    ListaRangoValoracionComponent,
    ListaRedSocialComponent,
    ListaRolComponent,
    ListaTipoActuacionComponent,
    ListaTipoClienteComponent,
    ListaTipoDocumentoComponent,
    ListaTipoEmpleadoComponent,
    ListaTipoGarantiaComponent,
    ListaTipoIncidenciaComponent,
    ListaTipoReclamoComponent,
    ListaTipoValoracionComponent,
    MaestrosComponent,
    CatalogoServicioComponent,
    ListaRutaComponent,
    ListaTipoRespuestaComponent,
    // modales
    ModalActuacionComponent,
    ModalBloqueHoraComponent,
    ModalCaracteristicaBaseComponent,
    ModalCaracteristicaComponent,
    ModalCatalogoComponent,
    ModalCategoriaComponent,
    ModalDiaSemanaComponent,
    ModalEspecialidadComponent,
    ModalFuncionComponent,
    ModalRedSocialComponent,
    ModalrolComponent,
    ModalTipoActuacionComponent,
    ModalTipoClienteComponent,
    ModalTipoEmpleadoComponent,
    ModalTipoGarantiaComponent,
    ModalTipoIncidenciaComponent,
    ModalTipoReclamoComponent,
    ModalTipoValoracionComponent,
    TipoDocumentoComponent,
    ModalRutaComponent,
    ModalTipoRespuestaComponent,
    ModalRangoValoracionComponent,
    ModalDocumentoComponent,
    ModalRecaudoComponent,
    ModalActuacionRecaudoComponent,
    ModalValoracionComponent,
  ],
  entryComponents: [
    ModalActuacionComponent,
    ModalBloqueHoraComponent,
    ModalCaracteristicaBaseComponent,
    ModalCaracteristicaComponent,
    ModalCatalogoComponent,
    ModalCategoriaComponent,
    ModalDiaSemanaComponent,
    ModalEspecialidadComponent,
    ModalFuncionComponent,
    ModalRedSocialComponent,
    ModalrolComponent,
    ModalTipoActuacionComponent,
    ModalTipoClienteComponent,
    ModalTipoEmpleadoComponent,
    ModalTipoGarantiaComponent,
    ModalTipoIncidenciaComponent,
    ModalTipoReclamoComponent,
    ModalTipoValoracionComponent,
    TipoDocumentoComponent,
    ModalRutaComponent,
    ModalTipoRespuestaComponent,
    ModalRangoValoracionComponent,
    ModalDocumentoComponent,
    ModalRecaudoComponent,
    ModalActuacionRecaudoComponent,
    ModalValoracionComponent
  ],
  providers: [
  ]
})
export class MaestrosModule { }
