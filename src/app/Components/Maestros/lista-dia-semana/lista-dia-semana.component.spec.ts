import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaDiaSemanaComponent } from './lista-dia-semana.component';

describe('ListaDiaSemanaComponent', () => {
  let component: ListaDiaSemanaComponent;
  let fixture: ComponentFixture<ListaDiaSemanaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaDiaSemanaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaDiaSemanaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
