import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { GeneralService } from '../../../core/Services/general/general.service';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-modal-dia-semana',
  templateUrl: './modal-dia-semana.component.html',
  styleUrls: ['./modal-dia-semana.component.scss']
})
export class ModalDiaSemanaComponent implements OnInit {

  form_descripcion = { nombre: '', estatus: 'A' };
  incluir: boolean = false;
  id;
  titulo = '';
  nombreFormControl = new FormControl('', [
    Validators.required
  ]);
  constructor(public dialogRef: MatDialogRef<ModalDiaSemanaComponent>,
              public generalService: GeneralService,
              public snackBar: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) public data: any) {
          if (data) {
          this.nombreFormControl.setValue(data.nombre);
          this.id = data.id;
          this.titulo = 'Actualizar';
          } else {
          this.incluir = true;
          this.titulo = 'Nuevo';
          }
        }
    Registrar() {
      this.form_descripcion.nombre = this.nombreFormControl.value;
      if (this.nombreFormControl.valid) {
        this.generalService.Registrar(this.form_descripcion, 'dia_semana').then((result) => {
          this.openSnackBar('Registro Exitoso!', "registrar");
        }, (err) => {
          console.log(err);
        });
        this.dialogRef.close(this.form_descripcion);
      }
    }
    Actualizar() {

        if (this.nombreFormControl.valid) {
          this.form_descripcion.nombre = this.nombreFormControl.value;
          this.generalService.Actualizar(this.form_descripcion, 'dia_semana', this.id).then((result) => {
            this.openSnackBar('Registro Actualizado!', "Actualizar");
          }, (err) => {
            console.log(err);
          });
          this.data.nombre = this.nombreFormControl.value
          this.dialogRef.close(this.data);
        }
      }
      openSnackBar(message: string, action: string) {
        this.snackBar.open(message, action, {
          duration: 1000,
        });
    }

      ngOnInit() {
      }

      onNoClick() {
        this.dialogRef.close();
      }
}
