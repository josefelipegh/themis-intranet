import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDiaSemanaComponent } from './modal-dia-semana.component';

describe('ModalDiaSemanaComponent', () => {
  let component: ModalDiaSemanaComponent;
  let fixture: ComponentFixture<ModalDiaSemanaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalDiaSemanaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDiaSemanaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
