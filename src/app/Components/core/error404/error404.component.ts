import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-error404',
  templateUrl: './error404.component.html',
  styleUrls: ['./error404.component.scss']
})
export class Error404Component implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  inicio(){
    let rol = JSON.parse(localStorage.getItem('rol'));
        if(rol!= null){
            switch(rol.id){
                case 1:
                this.router.navigate(['/DashC']);
                break;
                case 2:
                this.router.navigate(['/DashAbo']);
                break;
                case 37:
                this.router.navigate(['/DashJ']);
                break;
                case 9:
                this.router.navigate(['/DashSec']);
                break;
                case 38:
                this.router.navigate(['/DashA']);
                break;
            }
        }
  }
}
