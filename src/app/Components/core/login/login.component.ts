import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { LoginService } from '../../core/Services/login/login.service';
import { Router } from '@angular/router';
import { GeneralService } from '../Services/general/general.service';
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    loginData = { correo: '', contrasenia: '' };
    usuarioValido = false;
    datos: any;
    ojo = 'visibility';
    datosUsuario: any;
    constructor(public loginService: LoginService, public router: Router, public generalService: GeneralService) {
      let rol = JSON.parse(localStorage.getItem('rol'));
      console.log(rol);
        if(rol!= null && rol !=''){
            switch(rol.id){
                case 1:
                this.router.navigate(['/DashC']);
                break;
                case 2:
                this.router.navigate(['/DashAbo']);
                break;
                case 37:
                this.router.navigate(['/DashJ']);
                break;
                case 9:
                this.router.navigate(['/DashSec']);
                break;
                case 38:
                this.router.navigate(['/DashA']);
                break;
            }
        }
    }
    emailFormControl = new FormControl('', [
        Validators.required,
        Validators.email,
    ]);
    contrasenaFormControl = new FormControl('', [
        Validators.required,
    ]);
    doLogin() {
        this.loginData = {
            correo: this.emailFormControl.value,
            contrasenia: this.contrasenaFormControl.value
        }
        this.loginService.login(this.loginData).then((result) => {
            this.datos = result;
            // this.loginService.obtenerUsuario(localStorage.getItem("correo")).then((result2) => { 
            //     this.datos = result2  
            //     localStorage.setItem('rol',JSON.stringify(this.datos.data.rol));
            //   console.log(this.datos.data);
            //   }, (err) => {

            //   });
            localStorage.setItem('rol', JSON.stringify(this.datos.data.usuario.rol));
            let rol = this.datos.data.usuario.rol.id;
            switch(rol){
                case 1:
                this.router.navigate(['/DashC']);
                break;
                case 2:
                this.router.navigate(['/DashAbo']);
                break;
                case 37:
                this.router.navigate(['/DashJ']);
                break;
                case 9:
                this.router.navigate(['/DashSec']);
                break;
                case 38:
                this.router.navigate(['/DashA']);
                break;
            }
            this.Datos();
        }, (err) => {
            this.usuarioValido = true;
        });
        
    }
      // PARA CARGAR EL NOMBRE Y LA FOTO DEL USUARIO
  Datos(){
    this.generalService.ObtenerUno('vista_usuario', Number.parseInt(localStorage.getItem('id'))).then((result) => {
      this.datosUsuario = result;
      localStorage.setItem('datosUsuario', JSON.stringify(this.datosUsuario.data));
    }, (err) => {
      console.log(err);
    });
  }
    iniciar() {
        if (this.emailFormControl.valid && this.contrasenaFormControl.valid) {
            console.log(this.loginData.correo + "  " + this.loginData.contrasenia);
            this.doLogin();
        }

    }
    showPassword(input: any): any {
        if (input.type === 'password') {
            input.type = 'text';
            this.ojo = 'visibility_off';
        } else {
            input.type = 'password';
            this.ojo = 'visibility';
        }
    }

    ngOnInit() {
    }

}
