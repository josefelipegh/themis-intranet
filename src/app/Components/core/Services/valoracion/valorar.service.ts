import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ValorarService {

  private dataArray: any;
  private dataArray2: any [];


  insertData(data) {
    this.dataArray = data;
  }

  getData() {
    return this.dataArray;
  }
  insertResp(data) {
    this.dataArray2 = data;
  }

  getResp() {
    return this.dataArray2;
  }
}
