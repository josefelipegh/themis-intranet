import { TestBed } from '@angular/core/testing';

import { ValorarService } from './valorar.service';

describe('ValorarService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ValorarService = TestBed.get(ValorarService);
    expect(service).toBeTruthy();
  });
});
