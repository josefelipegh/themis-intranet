import { Injectable } from '@angular/core';
import { UrlService } from '../url/url.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  url;
  public token: any; //para guardar el token

  constructor(public http: HttpClient) {
    this.url = UrlService.apiUrl();
  }
  headers: HttpHeaders = new HttpHeaders({
    'Content-Type': 'application/json',
    // 'Authorization': "token " + localStorage.getItem("token")
  });
  //login Usuario envia un post para la API
  login(credentials) {
    return new Promise((resolve, reject) => {

      this.http.post(this.url + 'signin', JSON.stringify(credentials), { headers: this.headers })
        .subscribe(res => {
          let data = JSON.parse(JSON.stringify(res));
          
          console.log(data);
          localStorage.setItem('id', data.data.usuario.id);
          localStorage.setItem('correo', credentials.correo);
          localStorage.setItem('token', data.data.token);
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });

  }

  isAuthenticated() {
    let token = localStorage.getItem('token');

    if (token) {
      return true;
    }
    else {
      return false;
    }
  }

  // FILTRA USUARIO POR CORREO
  obtenerUsuario(correo) {

    return new Promise(resolve => {

      this.http.get(this.url + "usuario/" + correo, { headers: this.headers }).subscribe(data => {
        let datos = resolve(data);

      }, err => {
        console.log(err);
      });
    });

  }
}

