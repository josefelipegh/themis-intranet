import { Injectable } from '@angular/core';
import { LoginService } from '../login/login.service';
import { Router,ActivatedRouteSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  constructor(private authentication: LoginService, private router: Router) { }

  canActivate(): boolean | Promise<boolean> {
    let token = localStorage.getItem('token');
    if (!token) {
        console.error("User is not authenticated.");
        this.redirectToLoginPage();
        return false;
    }else if (this.authentication.isAuthenticated()) {
        return true;
    }    

}
canActivatePermiso(ruta): boolean | Promise<boolean> {
    let token = localStorage.getItem('token');
    let rol = JSON.parse(localStorage.getItem('token'));
    if (!token) {
        console.error("User is not authenticated.");
        this.redirectToLoginPage();
        return false;
    }
    else if ( rol.funciones.findIndex(x => x.ruta.nombre == ruta) == -1) {
        this.redirectToPermisoPage();
        return false;
    }else if (this.authentication.isAuthenticated()) {
        return true;
    }   

}


redirectToLoginPage() {
    this.router.navigate(['/login']);
}
redirectToPermisoPage() {
    this.router.navigate(['/Error403']);
}

}
