import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UrlService {

  constructor(public http: HttpClient) { }
  public static  apiUrl(){
    return 'https://eos-themis.herokuapp.com/api/';
  }

  public static  imagenUrl(){
    return 'https://eos-themis.herokuapp.com';
  }
}
