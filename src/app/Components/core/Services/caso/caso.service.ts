import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CasoService {
  private DatosAbogado: any;
  private DatosCliente: any;
  private DatosServicio: any;

  constructor() { }

  public setAbogado(dato) {
    this.DatosAbogado = dato;
  }

  getAbogado() {
    return this.DatosAbogado;
  }

  setCliente(dato) {
    this.DatosCliente = dato;
  }

  getCliente() {
    return this.DatosCliente;
  }

  setServicio(dato) {
    this.DatosServicio = dato;
  }

  getServicio() {
    return this.DatosServicio;
  }

}

