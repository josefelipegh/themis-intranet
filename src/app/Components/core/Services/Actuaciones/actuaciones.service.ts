import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ActuacionesService {
  private  dataArray: any;
  private datosAbogados: any;
  private datosCita: any;
  private servicio: any;

  setServicio(data){
    this.servicio = data;
  }

  getServicio(){
    return this.servicio;
  }

  setDatosCita (data) {
    this.datosCita = data;
  }

  getDatosCita () {
    return this.datosCita;
  }

  setDatosAbogado (data) {
    this.datosAbogados = data;
  }

  getDatosAbogado () {
    return this.datosAbogados;
  }

  insertData(data) {
      this.dataArray = data;
  }

  getData() {
    return this.dataArray;
  }

}
