import { TestBed } from '@angular/core/testing';

import { ActuacionesService } from './actuaciones.service';

describe('ActuacionesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ActuacionesService = TestBed.get(ActuacionesService);
    expect(service).toBeTruthy();
  });
});
