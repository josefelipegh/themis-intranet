import { Injectable } from '@angular/core';
import { UrlService } from '../url/url.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {

  url;
  constructor(public http: HttpClient) {
    this.url = UrlService.apiUrl();
  }

  headers: HttpHeaders = new HttpHeaders({
    'Content-Type': 'application/json',
    // 'Authorization': "token " + localStorage.getItem("token")
  });
  headers2: HttpHeaders = new HttpHeaders({
    // 'Content-Type': 'text/plain;charset=UTF-8',
    // 'Authorization': "token " + localStorage.getItem("token")
  });
  headersMensaje: HttpHeaders = new HttpHeaders();
  

  //METODO POST PARA REGISTRAR UNA ENTIDAD EN LA BD
  Registrar(datos, nombreMetodo: string) {
    return new Promise((resolve, reject) => {
      this.http.post(this.url + nombreMetodo, JSON.stringify(datos), { headers: this.headers })
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });

  }
  
   //METODO POST PARA REGISTRAR UNA ENTIDAD EN LA BD
   RegistrarMensaje(datos) {
     console.log(datos);
     this.headersMensaje.append('Content-Type', 'application/json');
     this.headersMensaje.append('Authorization', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhZG1pbiIsImlhdCI6MTU0ODI1NjA5OSwiZXhwIjo0MTAyNDQ0ODAwLCJ1aWQiOjY3MTY4LCJyb2xlcyI6WyJST0xFX1VTRVIiXX0.lWQnPCs1c_3RpZSl_PEfBBC8IZrvRNr7f_eDuEjIVBA');
     this.headersMensaje.append('Access-Control-Allow-Origin', '*');
    return new Promise((resolve, reject) => {
      this.http.post('https://smsgateway.me/api/v4/message/send', datos, { headers: this.headersMensaje })
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });

  }
  //METODO POST PARA REGISTRAR UNA ENTIDAD EN LA BD
  RegistrarConImagen(datos, nombreMetodo: string) {
    return new Promise((resolve, reject) => {
      this.http.post(this.url + nombreMetodo, (datos))
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });

  }

  //METODO PUT PARA ACTUALIZAR UNA ENTIDAD EN LA BD
  Actualizar(datos, nombreMetodo: string, id) {
    return new Promise((resolve, reject) => {
      this.http.put(this.url + nombreMetodo + '/' + id, JSON.stringify(datos), { headers: this.headers })
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });

  }

  //METODO PUT PARA ACTUALIZAR UNA ENTIDAD EN LA BD
  ActualizarConImagen(datos, nombreMetodo: string, id) {
    return new Promise((resolve, reject) => {
      this.http.put(this.url + nombreMetodo + '/' + id, (datos), { headers: this.headers2 })
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });

  }

  //METODO PUT PARA ELIMINAR LOGICAMENTE UNA ENTIDAD EN LA BD
  Eliminar( nombreMetodo: string, id) {
    return new Promise((resolve, reject) => {
      this.http.put(this.url + nombreMetodo + '/' + id, {estatus: 'I'}, { headers: this.headers })
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });

  }

  //METODO DELETE PARA ELIMINAR FISICAMENTE UNA ENTIDAD EN LA BD

  EliminarFisico( nombreMetodo: string, id)
  {
    return new Promise((resolve, reject) => {
      this.http.delete(this.url + nombreMetodo + '/' + 'borrar' + '/'+ id, { headers: this.headers })
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });

  }


  // METODO GET PARA OBTENER TODAS LAS ENTIDADES DE UNA DE UNA TABLA 
  ObtenerUno(nombreMetodo: string,id: any) {

    return new Promise(resolve => {
      this.http.get(this.url + nombreMetodo + '/' + id, { headers: this.headers }).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });

  }

  // METODO GET PARA OBTENER TODAS LAS ENTIDADES DE UNA DE UNA TABLA 
  Obtenertodos(nombreMetodo: string) {
    return new Promise(resolve => {
      this.http.get(this.url + nombreMetodo + '/', { headers: this.headers }).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });

  }

  // METODOS DE CONFIGURACION WEB

CambiarFalse( nombreMetodo: string, id) {
  return new Promise((resolve, reject) => {
    this.http.put(this.url + nombreMetodo + '/' + id, {visible: false}, { headers: this.headers })
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });

}

CambiarTrue( nombreMetodo: string, id) {
  return new Promise((resolve, reject) => {
    this.http.put(this.url + nombreMetodo + '/' + id, {visible: true}, { headers: this.headers })
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });

}

CambiarFalseE( nombreMetodo: string, id) {
  return new Promise((resolve, reject) => {
    this.http.put(this.url + nombreMetodo + '/' + id, {visibilidad: false}, { headers: this.headers })
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });

}

CambiarTrueE( nombreMetodo: string, id) {
  return new Promise((resolve, reject) => {
    this.http.put(this.url + nombreMetodo + '/' + id, {visibilidad: true}, { headers: this.headers })
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });

}


}

