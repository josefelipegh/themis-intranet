import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatSnackBar } from '@angular/material';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { GeneralService } from '../../Services/general/general.service';
import * as Chart from "chart.js";

export interface Parametros {
  id: number;
  nombre: string;
  checked: boolean;
}
// INTERFACE PARA CREAR UN OBJETO DE LA TABLA
export interface TablaData {
  id: number;
  caracteristica: string;
  valor: string;
}
let datosEjemplo: TablaData[] = [];

@Component({
  selector: 'app-perfil-primera-vez',
  templateUrl: './perfil-primera-vez.component.html',
  styleUrls: ['./perfil-primera-vez.component.scss']
})
export class PerfilPrimeraVezComponent implements OnInit {

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  public loading = false;
  datosCliente = {
    nombre: '', apellido: '', cedula: '', direccion: '', sexo: '', fecha_nac: '',
    pais_id: '', estado_id: '', ciudad: '', telefono: ''
  }
  nacionalidad = [
    { value: 1, viewValue: 'Venezolana' },
    { value: 2, viewValue: 'Extranjera' }
  ];
  resultadoObtenido: any;
  comboEstados: any[] = [];
  sexo = [
    { value: 'F', viewValue: 'Femenino' },
    { value: 'M', viewValue: 'Masculino' }
  ];

  displayedColumns: string[] = ['caracteristica', 'valor'];
  dataSource: MatTableDataSource<TablaData>;
  // OBJETO PARA CARGAR EN LA TABLA
  datos: TablaData;
  elementoTabla = false;
  ocupacion: string[] = ['Albañil', 'Ingeniero', 'Profesor', 'Deportista'];
  porcentaje = 0;
  completitud: string = "Completitud del Perfil";
  mostrar: boolean = false;
  chart: any;
  por2 = 100;
  listaParametros: Parametros[] = [];
  listaValores: any[];
  agregar: boolean = true;
  idCaracBase;
  completar: boolean = false;
  carac = {
    id: 0,
    caracteristica: "",
    valor: ""
  }

  constructor(public dialogRef: MatDialogRef<PerfilPrimeraVezComponent>,
    private _formBuilder: FormBuilder, public generalService: GeneralService, public snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.myControlNombre.setValue(data.nombre);
    this.myControlApellido.setValue(data.apellido);
    this.myControlTelefono.setValue(data.telefono);
    this.myControlSexo.setValue(data.sexo);
    this.cargarCombos('estado', this.comboEstados);
    this.cargarlistaCaractBase();
    this.CargarTabla();
  }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
    this.CargarChart();
  }

  myControlCedula = new FormControl('',
    [
      Validators.required
    ]);

  myControlNombre = new FormControl('',
    [
      Validators.required
    ]);


  myControlApellido = new FormControl('',
    [
      Validators.required
    ]);
  myControlFecha = new FormControl('',
    [
      Validators.required
    ]);

  myControlNacionalidad = new FormControl('',
    [
      Validators.required
    ]);
  myControlEstado = new FormControl('',
    [
      Validators.required
    ]);

  myControlCiudad = new FormControl('',
    [
      Validators.required
    ]);
  myControlSexo = new FormControl('',
    [
      Validators.required
    ]);
  myControlTelefono = new FormControl('',
    [
      Validators.required
    ]);

  myControlDireccion = new FormControl('',
    [
      Validators.required
    ]);
  //Carga cualquier combo desde la base de datos
  cargarCombos(nombreEntidad, combo: any[]) {
    this.generalService.Obtenertodos(nombreEntidad).then((result) => {
      this.resultadoObtenido = result;
      for (let i = 0; i < this.resultadoObtenido.data.length; i++) {
        combo.push(
          {
            id: this.resultadoObtenido.data[i].id,
            nombre: this.resultadoObtenido.data[i].nombre
          }
        );
      }
    }, (err) => {
      console.log(err);
    });

  }
  //CARGAR LISTA DE CARACTERISTICA BASE
  cargarlistaCaractBase() {
    this.generalService.Obtenertodos('caracteristica_base').then((result) => {
      this.resultadoObtenido = result;
      for (let i = 0; i < this.resultadoObtenido.data.length; i++) {
        this.listaParametros.push(
          {
            id: this.resultadoObtenido.data[i].id,
            nombre: this.resultadoObtenido.data[i].nombre,
            checked: false
          }
        );
      }
    }, (err) => {
      console.log(err);
    });

  }

  // PARA CARGAR EL CHART Y ACTUALIZARLO
  CargarChart() {
    this.chart = new Chart("canvas-dona", {
      type: 'doughnut',

      data: {
        datasets: [{

          yAxisID: 'A',
          data: [this.porcentaje, this.por2],
          backgroundColor: ['#E0A801', '#D8D8D8'],
          hoverBorderColor: ['#E0A801', '#D8D8D8'],

        }]
      },
      options: {

        legend: {
          display: false,
        },
        tooltips: {
          enabled: false,
          display: false
        },
        hover: { mode: null },
        cutoutPercentage: 70

      }
    });
  }
  // METEDO QUE OBTIENE UNA LISTA Y SE CARGA EN LA TABLA
  CargarTabla() {
    //   this.generalService.Obtenertodos('rol').then((result) => {
    //     this.resultado = result;
    //     roles = [];
    //     for (let i = 0; i < this.resultado.data.length; i++) {
    //       let est = this.Estatus(this.resultado.data[i].estatus);
    //       this.datos = {
    //         id: this.resultado.data[i].id,
    //         nombre: this.resultado.data[i].nombre,
    //         estatus: est,
    //       };
    //       roles.push(this.datos);
    //     }
    //     this.dataSource = new MatTableDataSource(roles);
    //   }, (err) => {
    //     console.log(err);
    //   });
    // let datosEjemplo: TablaData[] = [
    //   {
    //     id: 1,
    //     caracteristica: "Ocupación",
    //     valor: "Estudiante"
    //   }, {
    //     id: 2,
    //     caracteristica: "Pasatiempo",
    //     valor: "ver pelíca"
    //   }, {
    //     id: 3,
    //     caracteristica: "Estado civil",
    //     valor: "soltero"
    //   }
    // ];
    if (datosEjemplo.length > 0) {
      this.elementoTabla = true;
    }
    this.dataSource = new MatTableDataSource(datosEjemplo);
  }

  // PARA AGREGAR UNA CARCACTERISTICA NUEVA A LA TABLA
  AgregarCaracteristica() {
    let index = datosEjemplo.findIndex(x => x.caracteristica == this.carac.caracteristica);
    this.mostrar = this.mostrar = !this.mostrar;
    if (index != -1) {
      datosEjemplo[index].valor = this.carac.valor;
      datosEjemplo[index].id = this.carac.id;
    } else {
      datosEjemplo.push({
        id: this.carac.id,
        caracteristica: this.carac.caracteristica,
        valor: this.carac.valor
      });
      this.Cambiar();
    }
    this.agregar = true;
  }

  // METODO PARA CARGAR LA LISTA DE VALOR DE CARACTERISTICA
  CargarValor(idBase: number, nombre) {
    this.mostrar = !this.mostrar;
    this.carac.caracteristica = nombre;
    this.idCaracBase = idBase;
    let resultado: any;
    this.generalService.ObtenerUno('caracteristica/base', idBase).then((result) => {
      resultado = result;
      this.listaValores = resultado.data;
    }, (err) => {
      console.log(err);
    });
  }
  // METODO PARA OBTENER EL VALOR DE LA CARACTERISTICA QUE SE VA A INCLUIR EN LA TABLA
  SeleccionarCaracValor(id, nombre) {
    this.carac.id = id;
    this.carac.valor = nombre;
    
    this.AgregarCaracteristica();
  }
  // PARA ACTUALIZAR Y COLOCAR EL EL CHECKED DEL ITEM DE LA IZQUIERDA EN TRUE
  Cambiar() {
    if (datosEjemplo.length > 0) {
      this.elementoTabla = true;
    }
    let index = this.listaParametros.findIndex(x => x.id == this.idCaracBase);
    this.listaParametros[index].checked = true;
    this.porcentaje = this.porcentaje + (1 / this.listaParametros.length * 100);
    this.por2 = 100 - this.porcentaje;
    this.CargarChart();

  }

  // ACTUALIZAR LOS DATOS DEL CLIENTE EN LA BD
  Actualizar() {
    if (this.porcentaje < 100) {
      this.completar = true;
    }
    else {
      this.completar = false;
    }


    if (this.myControlNacionalidad.valid && this.myControlCedula.valid && this.myControlNombre.valid
      && this.myControlSexo.valid && this.myControlTelefono.valid && this.myControlApellido.valid && this.porcentaje > 99) {
      this.datosCliente.nombre = this.myControlNombre.value;
      this.datosCliente.apellido = this.myControlApellido.value;
      this.datosCliente.cedula = this.myControlCedula.value;
      this.datosCliente.telefono = this.myControlTelefono.value;
      this.datosCliente.direccion = this.myControlDireccion.value;
      this.datosCliente.sexo = this.myControlSexo.value;
      this.datosCliente.fecha_nac = this.myControlFecha.value;
      this.datosCliente.pais_id = this.myControlNacionalidad.value;
      this.datosCliente.estado_id = this.myControlEstado.value;
      this.datosCliente.ciudad = this.myControlCiudad.value;
      console.log(this.datosCliente);
      let idclient;
      this.loading = true;
      this.generalService.ObtenerUno('cliente/usuario', Number.parseInt(localStorage.getItem('id'))).then((result) => {
        idclient = result;

        this.generalService.Actualizar(this.datosCliente, 'cliente', idclient.data.id).then((result) => {


          let res;
          this.generalService.ObtenerUno('cliente/usuario', Number.parseInt(localStorage.getItem('id'))).then((result) => {
            res = result;
            for (let i = 0; i < datosEjemplo.length; i++) {
              let perfil = {
                caracteristica_id: datosEjemplo[i].id,
                cliente_id: res.data.id,
                estatus: 'A'
              }
              this.generalService.Registrar(perfil, 'perfil').then((result) => {
                this.loading = false;
                this.openSnackBar('Datos actualizados con exito!', "Actualizar Datos");
                this.dialogRef.close();
              }, (err) => {
                console.log(err);
                this.loading = false;
              });
            }

          }, (err) => {
            console.log(err);
            this.loading = false;
          });


        }, (err) => {
          console.log(err);
          this.loading = false;
        });
      }, (err) => {
        console.log(err);
        this.loading = false;
      });


    }

  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }
}
