import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerfilPrimeraVezComponent } from './perfil-primera-vez.component';

describe('PerfilPrimeraVezComponent', () => {
  let component: PerfilPrimeraVezComponent;
  let fixture: ComponentFixture<PerfilPrimeraVezComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerfilPrimeraVezComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerfilPrimeraVezComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
