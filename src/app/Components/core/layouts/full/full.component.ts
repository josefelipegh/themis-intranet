import * as $ from 'jquery';
import { MediaMatcher } from '@angular/cdk/layout';
import { Router } from '@angular/router';
import {
  ChangeDetectorRef,
  Component,
  NgZone,
  OnDestroy,
  ViewChild,
  HostListener,
  Directive,
  AfterViewInit,
  ElementRef,
  ViewEncapsulation
} from '@angular/core';
import { NavItem } from './nav-items';
import { NavService } from './nav.service';
import { AppHeaderComponent } from './header/header.component';
import { AppSidebarComponent } from './sidebar/sidebar.component';
import { forEach } from '@angular/router/src/utils/collection';
import { GeneralService } from '../../Services/general/general.service';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { PerfilPrimeraVezComponent } from '../../pefilCliente/perfil-primera-vez/perfil-primera-vez.component';
import Swal from 'sweetalert2';
/** @title Responsive sidenav */
@Component({
  selector: 'app-full-layout',
  templateUrl: 'full.component.html',
  styleUrls: [],
  encapsulation: ViewEncapsulation.None
})
export class FullComponent implements OnDestroy, AfterViewInit {
  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;

  navItems2: NavItem[] = [];
  navItems: NavItem[] = [];
  children1: any[] = [];
  item: NavItem;
  usuario;

  // METODO PARA CARGAR EL MENU LATERAL 
  cargarMenu() {
    let ir;
    switch (this.rol.id) {
      case 1:
        ir = '/DashC';
        break;
      case 2:
        ir = '/DashAbo';
        break;
      case 37:
        ir = '/DashJ';
        break;
      case 9:
        ir = '/DashSec';
        break;
      case 38:
        ir = '/DashA';
        break;
    }
    this.navItems.push(
      {
        displayName: 'Inicio',
        iconName: 'home',
        route: ir,
        ver: true,
      });
    let ruta = '';
    let ruta2 = '';
    for (let i = 0; i < this.rol.funciones.length; i++) {

      if (this.rol.funciones[i].funcion_id == "" || this.rol.funciones[i].funcion_id == null) {
        for (let k = 0; k < this.rol.funciones.length; k++) {
          if (this.rol.funciones[k].funcion_id != null && this.rol.funciones[i].id == this.rol.funciones[k].funcion_id) {

            if (this.rol.funciones[k].ruta != null) {
              ruta = this.rol.funciones[k].ruta.nombre;
            }
            this.item = {
              displayName: this.rol.funciones[k].nombre,
              posicion: this.rol.funciones[k].posicion,
              route: ruta,
              ver: true,
            }
            this.children1.push(this.item);
          }
        }

        if(this.rol.funciones[i].nombre== 'Reportes'){
          this.navItems.push(
            {
              displayName: this.rol.funciones[i].nombre,
              iconName: this.rol.funciones[i].nombre_logo,
              posicion: this.rol.funciones[i].posicion,
              ver: true,
              route: 'reportes',
              children: this.children1
            }
          );
        }else{
          this.navItems.push(
            {
              displayName: this.rol.funciones[i].nombre,
              iconName: this.rol.funciones[i].nombre_logo,
              posicion: this.rol.funciones[i].posicion,
              ver: true,
              children: this.children1
            }
          );

        }
        this.children1 = [];
      }
    }
    this.navItems.sort((a, b) => a.posicion - b.posicion);
    for (let i = 0; i < this.navItems.length; i++) {
      if (this.navItems[i].children != null) {
        this.navItems[i].children.sort((a, b) => a.posicion - b.posicion);
      }
    }
  }

  cargarMenudeMentira() {
    this.navItems = [
      {
        displayName: 'Inicio',
        iconName: 'home',
        route: 'DashJ',
        ver: true,
      },
      {
        displayName: 'Información Inicial',
        iconName: 'view_comfy',
        ver: true,
        children:
          [{
            displayName: 'Básica',
            route: 'maestros',
            ver: true
          },
          {
            displayName: 'Configuración',
            route: 'configuracion',
            ver: true
          }
          ]
      },
      {
        displayName: 'Servicio',
        iconName: 'room_service',
        ver: true,
        children: [
          {
            displayName: 'Solicitud',
            route: 'solicitudes',
            ver: true
          },
          {
            displayName: 'Evaluar solicitud',
            route: 'evaluar',
            ver: true
          },
          {
            displayName: 'Citas',
            route: 'citas',
            ver: true
          },
          {
            displayName: 'Casos',
            route: 'casos',
            ver: true
          },
          {
            displayName: 'Calificar',
            route: 'calificar',
            ver: true
          },
        ]
      },
      {
        displayName: 'Atención al cliente',
        iconName: 'person',
        ver: true,
        children: [
          {
            displayName: 'Reclamo',
            route: 'reclamos',
            ver: true
          },
          {
            displayName: 'Evaluar reclamos',
            route: 'evaluar_reclamo',
            ver: true
          },
          {
            displayName: 'Sugerencia',
            route: 'sugerencias',
            ver: true
          },
          {
            displayName: 'Evaluar sugerencia',
            route: 'evaluar_sugerencia',
            ver: true
          },
        ]
      },
      {
        displayName: 'Incidencia',
        iconName: 'notification_important',

        ver: true,
        children: [
          {
            displayName: 'Registrar',
            route: 'incidencia',
            ver: true
          },
          {
            displayName: 'Evaluar',
            route: 'evaluar-incidencia',
            ver: true
          },
        ]
      },
      {
        displayName: 'Promociones',
        iconName: 'view_compact',
        ver: true,
        children: [
          {
            displayName: 'Configurar',
            route: 'config_promocion',
            ver: true
          },
          {
            displayName: 'Difusión',
            route: 'difusion',
            ver: true
          },
        ]
      },
      {
        displayName: 'Reportes',
        iconName: 'view_list',
        route: 'reportes',
        ver: true,
      },
      {
        displayName: 'Administración del sistema',
        iconName: 'settings',
        ver: true,
        children: [
          {
            displayName: 'Bitacora',
            route: 'bitacora',
            ver: true,
          },
          {
            displayName: 'Respaldo Base de Datos',
            route: 'respaldo_bd',
            ver: true
          },
          {
            displayName: 'Depuracion Base de Datos',
            route: 'depuracion_bd',
            ver: true
          },
          {
            displayName: 'Sesion',
            route: 'sesion',
            ver: true
          },
        ]
      },
    ];
  }

  rol: any;
  constructor(changeDetectorRef: ChangeDetectorRef, 
              media: MediaMatcher, 
              public navService: NavService, 
              public generalService: GeneralService,
              public dialog: MatDialog) {
    this.mobileQuery = media.matchMedia('(min-width: 768px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);

    this.rol = JSON.parse(localStorage.getItem("rol"));
    this.usuario = JSON.parse(localStorage.getItem("datosUsuario"));
    // esto no va solo para cargar el menu de mentira
    // this.rol = (localStorage.getItem("correo"));
    this.cargarMenu();
    // this.cargarMenudeMentira();
    this.PreguntarUsuario();

  }

  preguntaUsuario;
  PreguntarUsuario() {
    console.log(this.rol.id);
    if (this.rol.id == 1) {
      this.generalService.ObtenerUno('cliente/usuario', Number.parseInt(localStorage.getItem('id'))).then((result) => {
        this.preguntaUsuario = result;
        if (this.preguntaUsuario.data.cedula == null || this.preguntaUsuario.data.direccion == null || this.preguntaUsuario.data.fecha_nac == null
          || this.preguntaUsuario.data.direccion == '' || this.preguntaUsuario.data.cedula == "") {
          console.log('hola mundo');
          Swal({
            title: 'Gracias por preferir Themis',
            text: 'Por favor, completa su perfil',
            imageUrl: 'assets/images/themis404.png',
            imageWidth: 400,
            imageHeight: 200,
            timer: 4000,
            showConfirmButton: false
          }).then((result) => {
            if (result) {
              const dialogConfig = new MatDialogConfig();
              dialogConfig.disableClose = true;
              dialogConfig.autoFocus = true;
              dialogConfig.width = '65%';
              dialogConfig.height = '90%';
              dialogConfig.data = this.preguntaUsuario.data;
              const dialogRef = this.dialog.open(PerfilPrimeraVezComponent, dialogConfig);
            }
          })

        }
      }, (err) => {
        console.log(err);
      });

    }
  }
  

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }
  ngAfterViewInit() {
   
  }
}
