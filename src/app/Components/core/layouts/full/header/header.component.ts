import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { GeneralService } from '../../../Services/general/general.service';

export interface Notifications {
  icon: string;
  color: string;
  title: string;
  subTitle: string;
  time: string;
}

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class AppHeaderComponent {

  constructor(private router: Router, public generalService: GeneralService) {
    this.CargarNotificacion();
  }

  notificaciones: Notifications[] = [
    // {
    //   icon: 'today',
    //   color: '#fc4b6c',
    //   title: 'Cita pendiente',
    //   subTitle: 'Tienes una cita el 18/01/19',
    //   time: 'Hace 2 min',
    // },
    // {
    //   icon: 'thumb_up',
    //   color: '#26c6da',
    //   title: 'Solicitud rechazada',
    //   subTitle: 'Estimado usuario, su solicitud ha sido rechazada',
    //   time: 'Hace 4h',
    // },
    // {
    //   icon: 'thumb_down',
    //   color: '#1e88e5',
    //   title: 'Solicitud aprobada',
    //   subTitle: 'Estimado usuario, su solicitud ha sido aprobada',
    //   time: 'Ayer',
    // }
  ];


  CargarNotificacion() {
    let resultado;
    let hola;
    let icon;
    let color;
    this.generalService.ObtenerUno('notificacion/findAllByUser', localStorage.getItem('id')).then((result) => {
      resultado = result;
      console.log(resultado.data);
      for (let i = 0; i < resultado.data.length; i++) {
        hola = resultado.data[i].tipo_notificacion_id;
        if (hola == 1) {
          icon = 'thumb_up';
          color = '#26c6da';
        } else if (hola == 2) {
          icon = 'today';
          color = '#fc4b6c';
        } else if (hola == 3) {
          icon = 'thumb_down';
          color = '#1e88e5';   
        }
        this.notificaciones.push(
          {
            icon: icon,
            color: color,
            title: resultado.data[i].titulo,
            subTitle: resultado.data[i].mensaje,
            time: resultado.data[i].fecha_creacion,
          }
        )
      }
    }, (err) => {
      console.log(err);
    });
  }

  CerrarSesion() {
    localStorage.clear();
    this.router.navigate(['/login']);
  }

  AbrirPerfil() {
    let resultado;
    let rol = JSON.parse(localStorage.getItem('rol'));
    if (rol.id == 1) {
      this.router.navigate(['/datos_perfil']);
    } else if (rol.id == 38 || rol.id == 37 || rol.id == 2 || rol.id == 9) {
      this.generalService.ObtenerUno("empleado/user", Number.parseInt(localStorage.getItem("id"))).then((result) => {
        resultado = result
        this.router.navigate(['/empleado', resultado.data.id]);
      }, (err) => {
        console.log(err);
      });
    }
  }
}
