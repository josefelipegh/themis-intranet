export interface NavItem {
  displayName: string;
  disabled?: boolean;
  iconName?: string;
  route?: string;
  ver?: boolean;
  posicion?: number;
  children?: NavItem[];
}

