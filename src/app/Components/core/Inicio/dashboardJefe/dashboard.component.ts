import { Component } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointState, BreakpointObserver } from '@angular/cdk/layout';
import { Router } from '@angular/router';
import { GeneralService } from '../../Services/general/general.service';

export interface Servicio {
  nombre: string;
  fecha: string;
  desc: string;
}

export interface Reclamo {
  nombre: string;
  fecha: string;
  desc: string;
}

export interface Valorado {
  nombre: string;
  posi: number;
  nega: number;/*  */
}



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {

  servicios: any[] = [];
  reclamos: any[] = [];
  
  resultado: any;
  resultado2: any;
  resultado3: any;

  valorados: Valorado[] = [
    {nombre: 'Indemnización ante expropiación', posi: 9, nega: 1},
    {nombre: 'Denuncia por delitos', posi: 7, nega: 1},
    {nombre: 'Contrato de seguros', posi: 9, nega: 1},
    {nombre: 'Traspaso de vehículos', posi: 2, nega: 1},
    {nombre: 'Pensiones alimenticias', posi: 9, nega: 1},
    {nombre: 'Representación legal', posi: 9, nega: 1},
    {nombre: 'Denuncia por maltrato infantil', posi: 9, nega: 1},
    {nombre: 'Asesorías coon cierre fiscal para empresas', posi: 6, nega: 1},
    {nombre: 'Formación de sociedades mercantiles', posi: 4, nega: 1},    
  ];

  datos: Servicio;
  datos2: Reclamo;

  constructor(private breakpointObserver: BreakpointObserver, private router: Router, public generalService: GeneralService) {
    this.CargarCatalago();
    this.CargarReclamo();
  }

  goToPage(pageName: string) {
    this.router.navigate([`${pageName}`]);
  }

  CargarCatalago() {
    this.generalService.Obtenertodos('catalogo_servicio').then((result) => {
      this.resultado = result;
      this.servicios = [];
      
      for (let i = 0; i < this.resultado.data.length; i++) {
        if (this.resultado.data[i].estatus == 'A') {

          this.datos = {
            nombre: this.resultado.data[i].nombre,
            fecha: this.resultado.data[i].fecha_creacion,
            desc: this.resultado.data[i].descripcion,
          };
          this.servicios.push(this.datos);
        }
      }

    }, (err) => {
      console.log(err);
    });

  }

  CargarReclamo() {

    let idTipoRec;

    this.generalService.Obtenertodos('reclamo').then((result) => {
      this.resultado2 = result;
      this.reclamos = [];
      

      for (let i = 0; i < this.resultado2.data.length; i++) {
        if (this.resultado2.data[i].estatus == 'P') {
          idTipoRec = this.resultado2.data[i].tipo_reclamo_id;

          
          this.generalService.ObtenerUno('tipo_reclamo', idTipoRec).then((result2) => {
            this.resultado3 = result2;
            
            this.datos2 = {
              nombre: this.resultado3.data.nombre,
              fecha:  this.resultado2.data[i].fecha_creacion,
              desc:  this.resultado2.data[i].descripcion,
            };

            this.reclamos.push(this.datos2);

          }, (err) => {
            console.log(err);
          });

        }
      }

    }, (err) => {
      console.log(err);
    });

  }

}




