import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-chart-dona',
  templateUrl: './chart-dona.component.html',
  styleUrls: ['./chart-dona.component.css']
})
export class ChartDonaComponent implements OnInit {

   // Doughnut
   public donChartOptions:any = {
  
    responsive: true,
    maintainAspectRatio: false

  };
   public doughnutChartLabels:string[] = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'];
   public doughnutChartData:number[] = [350, 450, 100];   
   public doughnutChartType:string = 'doughnut';
   public lineChartColors:Array<any> = [
    { 
      backgroundColor: ["#E0A801", "#5e2129", "#1dcaff"]
    },
    
    
  ];
  
   // events
   public chartClicked(e:any):void {
     console.log(e);
   }
  
   public chartHovered(e:any):void {
     console.log(e);
   }
   
  constructor() { }

  ngOnInit() {
  }

}
