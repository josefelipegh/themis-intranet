import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-char-linea',
  templateUrl: './char-linea.component.html',
  styleUrls: ['./char-linea.component.scss']
})
export class CharLineaComponent implements OnInit {

  public lineChartData:Array<any> = [
    {data: [65, 59, 80, 81, 56, 55, 40], label: 'Divorcio de mutuo acuerdo'},
    {data: [28, 48, 40, 19, 86, 27, 90], label: 'Trámite de documentos'},
    {data: [18, 48, 77, 9, 10, 27, 40], label: 'Asesoría Jurídica'}
  ];
  public lineChartLabels:Array<any> = ['Enero', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChartOptions:any = {
    responsive: true
  };
  public lineChartColors:Array<any> = [
    { // grey
      backgroundColor: 'rgba(238, 215, 147, 0.2)',
      borderColor: '#E0A801',
      pointBackgroundColor: '#E0A801',
      pointBorderColor: '#E0A801',
      pointHoverBackgroundColor: '#E0A801',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(42, 29, 31, 0.2)',
      borderColor: '#5e2129',
      pointBackgroundColor: '#5e2129',
      pointBorderColor: '#5e2129',
      pointHoverBackgroundColor: '#5e2129',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // grey
      backgroundColor: 'rgba(206,234,242,0.2)',
      borderColor: '#1dcaff',
      pointBackgroundColor: '#1dcaff',
      pointBorderColor: '#1dcaff',
      pointHoverBackgroundColor: '#1dcaff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend:boolean = false;
  public lineChartType:string = 'line';
 
  
 
  // events
  public chartClicked(e:any):void {
    console.log(e);
  }
 
  public chartHovered(e:any):void {
    console.log(e);
  }
  constructor() { }

  ngOnInit() {
  }

}
