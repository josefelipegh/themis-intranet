import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharLineaComponent } from './char-linea.component';

describe('CharLineaComponent', () => {
  let component: CharLineaComponent;
  let fixture: ComponentFixture<CharLineaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharLineaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharLineaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
