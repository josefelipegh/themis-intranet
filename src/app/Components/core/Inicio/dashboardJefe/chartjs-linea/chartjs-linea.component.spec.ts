import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartjsLineaComponent } from './chartjs-linea.component';

describe('ChartjsLineaComponent', () => {
  let component: ChartjsLineaComponent;
  let fixture: ComponentFixture<ChartjsLineaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartjsLineaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartjsLineaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
