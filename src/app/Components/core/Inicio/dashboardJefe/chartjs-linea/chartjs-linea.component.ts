import { Component, OnInit } from '@angular/core';
import * as Chart from "chart.js";

@Component({
  selector: 'app-chartjs-linea',
  templateUrl: './chartjs-linea.component.html',
  styleUrls: ['./chartjs-linea.component.scss']
})
export class ChartjsLineaComponent implements OnInit {
chart: any;
  constructor() { }

  ngOnInit() {
    this.chart = new Chart("canvas-linea", {
      type: 'line',
     
    data: {
      labels: ['Enero', 'Febrero', 'Marzo', 'Abril'],
   
      datasets: [
          {
              label: "Divorcio de mutuo acuerdo",
              backgroundColor: 'rgba(224, 168, 1, 0.3)',
              borderColor: '#E0A801',
              pointBackgroundColor: '#E0A801',
              pointHoverBorderColor: '#E0A801',
             
              data: [2, 3, 5, 11]
          },
          {
              label: "Trámite de documentos",
              backgroundColor: 'rgba(94, 33, 41, 0.3)',
              borderColor: '#5e2129',
              pointBackgroundColor: '#5e2129',
              pointHoverBorderColor: '#5e2129',
              pointHighlightStroke: "#5e2129",
              data: [0, 1, 1, 7] 
          },
          {
              label: "Asesoría Jurídica",              
              backgroundColor: 'rgba(29, 202, 255, 0.3)',
              borderColor: '#1dcaff',
              pointBackgroundColor: '#1dcaff',
              pointHoverBorderColor: '#1dcaff',
              pointHighlightStroke: "#1dcaff",
              data: [0, 1, 5, 4]
          }
      ]
    },
    options: {
      legend: false,
    }
    
  });
  
  }

}
