import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartjsDonaComponent } from './chartjs-dona.component';

describe('ChartjsDonaComponent', () => {
  let component: ChartjsDonaComponent;
  let fixture: ComponentFixture<ChartjsDonaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartjsDonaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartjsDonaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
