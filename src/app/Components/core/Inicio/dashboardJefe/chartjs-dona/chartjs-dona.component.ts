import { Component, OnInit } from '@angular/core';
import * as Chart from "chart.js";

@Component({
  selector: 'app-chartjs-dona',
  templateUrl: './chartjs-dona.component.html',
  styleUrls: ['./chartjs-dona.component.scss']
})
export class ChartjsDonaComponent implements OnInit {

  chart: any;

  constructor() { }

  ngOnInit() {
    this.chart = new Chart("canvas-dona", {
      type: 'doughnut',
     
    data: {
      labels: ['Exitosamente', 'Con Reclamo', 'Pendientes'],
      datasets: [{
        
        yAxisID: 'A',
        data: [10, 96, 84],
         backgroundColor: ['#1dcaff','#E0A801', '#5e2129' ],
         hoverBorderColor: ['#1dcaff','#E0A801', '#5e2129' ],
         
      }]
    },
    options: {
     
        legend: {
          display: false,
        },
        cutoutPercentage: 80
    
  }
  });
}

}
