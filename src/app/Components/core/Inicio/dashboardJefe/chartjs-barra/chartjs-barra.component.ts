import { Component, OnInit } from '@angular/core';
import * as Chart from "chart.js";

@Component({
  selector: 'app-chartjs-barra',
  templateUrl: './chartjs-barra.component.html',
  styleUrls: ['./chartjs-barra.component.scss']
})
export class ChartjsBarraComponent implements OnInit {

    chart: any;
  constructor() { }

  ngOnInit() {
   this.chart = new Chart("canvas-barra", {
    type: 'bar',
   
  data: {
    labels: ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'],
    datasets: [{
      label: 'Divorcio de mutuo acuerdo',
      yAxisID: 'A',
      data: [10, 96, 84, 76, 6, 67, 33],
      
        backgroundColor: 'rgba(224, 168, 1, 0.7)',
        borderColor: '#E0A801',
        borderWidth: 3
       
    }, {
      label: 'Trámite de documentos',
      yAxisID: 'B',
      data: [56, 6, 34, 16, 1, 4, 12],
      backgroundColor: 'rgba(94, 33, 41, 0.7)',
      borderColor: '#5e2129',
      borderWidth: 3

    }]
  },
  options: {
    scales: {
      yAxes: [{
        id: 'A',
        
        position: 'left',
      }, {
        id: 'B',
        
        position: 'right',
        display: false
        
      }]
    },
    xAxes: [{ barPercentage: 10 }],
    legend: {
      display: false,
    }
  }
});


  

  
  }

}
