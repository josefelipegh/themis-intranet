import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartjsBarraComponent } from './chartjs-barra.component';

describe('ChartjsBarraComponent', () => {
  let component: ChartjsBarraComponent;
  let fixture: ComponentFixture<ChartjsBarraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartjsBarraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartjsBarraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
