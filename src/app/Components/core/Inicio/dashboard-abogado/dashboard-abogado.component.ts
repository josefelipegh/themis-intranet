import { Component, OnInit } from '@angular/core';
import { Breakpoints, BreakpointState, BreakpointObserver } from '@angular/cdk/layout';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { GeneralService } from '../../Services/general/general.service';

export interface Caso {
  cedula: string;
  nombre: string;
  fecha: string;
  cliente: string;
}

export interface Cita {
  nombre: string;
  cliente: string;
  fecha: string;
}

@Component({
  selector: 'app-dashboard-abogado',
  templateUrl: './dashboard-abogado.component.html',
  styleUrls: ['./dashboard-abogado.component.scss']
})
export class DashboardAbogadoComponent implements OnInit {

  casos: any[] = [];
  citas: any[] = [];
  resultado: any;
  datos: Caso;
  resultado2: any;
  resultado3: any;
  resultado4: any;
  datos2: Cita;

  constructor(private breakpointObserver: BreakpointObserver, private router: Router, public generalService: GeneralService) {
    this.CargarCitas();
    this.CargarCasos();
  }

  CargarCasos() {

    let idUser = Number.parseInt(localStorage.getItem('id'));
    let idAbogado;
    this.generalService.ObtenerUno('empleado/user', idUser).then((result2) => {
      idAbogado = result2;


      this.generalService.Obtenertodos('vista_servicio_abogado/' + idAbogado.data.id).then((result2) => {
        this.resultado = result2;
        this.casos = [];

        for (let i = 0; i < this.resultado.data.length; i++) {

          if (this.resultado.data[i].estatus == 'A') {

            this.datos = {
              cedula: this.resultado.data[i].cliente_cedula,
              nombre: this.resultado.data[i].tipo_servicio,
              cliente: this.resultado.data[i].cliente_nombre + ' ' + this.resultado.data[i].cliente_apellido,
              fecha: this.resultado.data[i].fecha_creado,
            };
            this.casos.push(this.datos);
          }
        }

      }, (err) => {
        console.log(err);
      });
    }, (err) => {
      console.log(err);
    });


  }

  CargarCitas() {

    let idUser = Number.parseInt(localStorage.getItem('id'));
    let idActServ;
    let idAct;
    let idAbogado;

    this.generalService.ObtenerUno('empleado/user', idUser).then((result2) => {
      idAbogado = result2;

      this.generalService.Obtenertodos('vista_cita/abogado/' + idAbogado.data.id + '/estatus/P').then((result) => {
        this.resultado2 = result;
        this.citas = [];

        for (let i = 0; i < this.resultado2.data.length; i++) {
          idActServ = this.resultado2.data[i].actuacion_servicio_id;

          this.generalService.ObtenerUno('actuacion_servicio', idActServ).then((result2) => {
            idAct = result2;

            this.generalService.ObtenerUno('actuacion', idAct.data.actuacion_id).then((result3) => {
              this.resultado3 = result3;

              this.datos2 = {
                nombre: this.resultado3.data.nombre,
                cliente: this.resultado2.data[i].cliente_nombre + ' ' + this.resultado2.data[i].cliente_apellido,
                fecha: this.resultado2.data[i].fecha,
              }
              this.citas.push(this.datos2);

            }, (err) => {
              console.log(err);
            });

          }, (err) => {
            console.log(err);
          });
        }
      }, (err) => {
        console.log(err);
      });
    }, (err) => {
      console.log(err);
    });
  }

  ngOnInit() {

  }

}
