import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardAbogadoComponent } from './dashboard-abogado.component';

describe('DashboardAbogadoComponent', () => {
  let component: DashboardAbogadoComponent;
  let fixture: ComponentFixture<DashboardAbogadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardAbogadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardAbogadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
