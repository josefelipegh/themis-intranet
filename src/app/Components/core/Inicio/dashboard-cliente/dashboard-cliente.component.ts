import { Component, OnInit } from '@angular/core';
import { Breakpoints, BreakpointState, BreakpointObserver } from '@angular/cdk/layout';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { GeneralService } from '../../Services/general/general.service';

export interface Cita {
  nombre: string;
  fecha: string;
  abogado: string;
}

export interface Actuacion {
  nombre: string;
  abogado: string;
  fecha: string;
}

@Component({
  selector: 'app-dashboard-cliente',
  templateUrl: './dashboard-cliente.component.html',
  styleUrls: ['./dashboard-cliente.component.scss']
})
export class DashboardClienteComponent implements OnInit {

  actuaciones: any[] = [];
  citas: any[] = [];
  resultado: any;
  datos: Actuacion;
  resultado2: any;
  resultado3: any;
  resultado4: any;
  datos2: Cita;



  constructor(private breakpointObserver: BreakpointObserver, private router: Router, public generalService: GeneralService) {
    this.CargarServicios();
    this.CargarCitas();
  }

  ngOnInit() {
  }

  CargarServicios() {

    let idUser = Number.parseInt(localStorage.getItem('id'));
let idCliente;
    this.generalService.ObtenerUno('cliente/usuario', idUser).then((result2) => {
      idCliente = result2;


    this.generalService.Obtenertodos('vista_servicio/cliente/' + idCliente.data.id + '/estatus/A').then((result2) => {
      this.resultado = result2;
      this.actuaciones = [];

      

      for (let i = 0; i < this.resultado.data.length; i++) {
        

          this.datos = {
            nombre: this.resultado.data[i].tipo_servicio,
            abogado: this.resultado.data[i].abogado_nombre + ' ' + this.resultado.data[i].abogado_apellido,
            fecha: this.resultado.data[i].fecha_creado,
          };
          this.actuaciones.push(this.datos);
        
      }

    }, (err) => {
      console.log(err);
    });
    }, (err) => {
      console.log(err);
    });


  }

  CargarCitas() {

    let idUser = Number.parseInt(localStorage.getItem('id'));
    let idActServ;
    let idAct;
    let idCliente;
    
    this.generalService.ObtenerUno('cliente/usuario', idUser).then((result2) => {
      idCliente = result2;

    this.generalService.Obtenertodos('vista_cita/cliente/' + idCliente.data.id + '/estatus/P').then((result) => {
    this.resultado2 = result;
    this.citas = [];

      for (let i = 0; i < this.resultado2.data.length; i++) {
        idActServ = this.resultado2.data[i].actuacion_servicio_id;

        this.generalService.ObtenerUno('actuacion_servicio', idActServ).then((result2) => {
          idAct = result2;

          this.generalService.ObtenerUno('actuacion', idAct.data.actuacion_id).then((result3) => {
            this.resultado3 = result3;

            this.datos2 = {
              nombre: this.resultado3.data.nombre,
              abogado: this.resultado2.data[i].abogado_nombre + ' ' + this.resultado2.data[i].abogado_apellido,
              fecha: this.resultado2.data[i].fecha,
            }
            this.citas.push(this.datos2);

          }, (err) => {
            console.log(err);
          });

        }, (err) => {
          console.log(err);
        });
      }
    }, (err) => {
      console.log(err);
    });
  }, (err) => {
    console.log(err);
  });
  }
}
