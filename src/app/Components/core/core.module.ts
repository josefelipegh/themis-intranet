import { NgModule } from '@angular/core';
import { CommonModule, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { CoreRoutingModule } from './core-routing.module';
import { LoginComponent } from './login/login.component';
import { RouterModule } from '@angular/router';
import { FullComponent } from './layouts/full/full.component';
import { AppHeaderComponent } from './layouts/full/header/header.component';
import { AppSidebarComponent } from './layouts/full/sidebar/sidebar.component';
import { SpinnerComponent } from './layouts/spinner.component';
import { NavService } from './layouts/full/nav.service';
import { UrlService } from './Services/url/url.service';
import { LoginService } from './Services/login/login.service';
import { SharedModule } from '../shared/shared.module';
import { DashboardComponent } from './Inicio/dashboardJefe/dashboard.component';
import { DashboardAbogadoComponent } from './Inicio/dashboard-abogado/dashboard-abogado.component';
import { DashboardAsistenteComponent } from './Inicio/dashboard-asistente/dashboard-asistente.component';
import { DashboardClienteComponent } from './Inicio/dashboard-cliente/dashboard-cliente.component';
import { DashboardSecretariaComponent } from './Inicio/dashboard-secretaria/dashboard-secretaria.component';
import { ChartjsLineaComponent } from './Inicio/dashboardJefe/chartjs-linea/chartjs-linea.component';
import { ChartjsBarraComponent } from './Inicio/dashboardJefe/chartjs-barra/chartjs-barra.component';
import { ChartjsDonaComponent } from './Inicio/dashboardJefe/chartjs-dona/chartjs-dona.component';
import { Error404Component } from './error404/error404.component';
import { Error401Component } from './error401/error401.component';
import { Error403Component } from './error403/error403.component';
import { GeneralService } from './Services/general/general.service';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { NgxLoadingModule } from 'ngx-loading';
import { PerfilPrimeraVezComponent } from './pefilCliente/perfil-primera-vez/perfil-primera-vez.component';
import { CasoService } from './Services/caso/caso.service';
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    CoreRoutingModule,
    SweetAlert2Module.forRoot(),
    NgxLoadingModule.forRoot({})
  ],
  declarations: [
    LoginComponent,
    FullComponent,
    AppHeaderComponent,
    AppSidebarComponent,
    SpinnerComponent,
    DashboardComponent,
    DashboardAbogadoComponent,
    DashboardAsistenteComponent,
    DashboardClienteComponent,
    DashboardSecretariaComponent,
    ChartjsLineaComponent,
    ChartjsBarraComponent,
    ChartjsDonaComponent,
    Error404Component,
    Error401Component,
    Error403Component,
    PerfilPrimeraVezComponent
  ],
  exports: [
    RouterModule,
    FullComponent,
    AppHeaderComponent,
    AppSidebarComponent,
    SpinnerComponent
  ],
  providers: [
    {
      provide: LocationStrategy,
      useClass: PathLocationStrategy,
    },
    NavService,
    UrlService,
    LoginService,
    GeneralService,
    CasoService,
    //AutenticacionService
    // SolicitudService,

  ],
  entryComponents: [
    PerfilPrimeraVezComponent
  ]
})
export class CoreModule { }
