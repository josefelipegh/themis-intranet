import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { FullComponent } from './layouts/full/full.component';
import { DashboardComponent } from './Inicio/dashboardJefe/dashboard.component';
import { DashboardClienteComponent } from './Inicio/dashboard-cliente/dashboard-cliente.component';
import { DashboardAsistenteComponent } from './Inicio/dashboard-asistente/dashboard-asistente.component';
import { DashboardAbogadoComponent } from './Inicio/dashboard-abogado/dashboard-abogado.component';
import { Error404Component } from './error404/error404.component';
import { DashboardSecretariaComponent } from './Inicio/dashboard-secretaria/dashboard-secretaria.component';
import { AuthGuardService as AuthGuard } from './../core/Services/AuthGuard/auth-guard.service';
import { Error403Component } from './error403/error403.component';
const routes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  }, {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '',
    component: FullComponent,
    children: [
      {
        path: '',
        loadChildren: '../Configuracion/configuracion.module#ConfiguracionModule'
      },
      {
        path: '',
        loadChildren: '../Administracion/administracion.module#AdministracionModule'
      },
      {
        path: '',
        loadChildren: '../Maestros/maestros.module#MaestrosModule'
      },
      {
        path: '',
        loadChildren: '../Reportes/reportes.module#ReportesModule'
      },
      {
        path: '',
        loadChildren: '../atecion-cliente/atecion-cliente.module#AtecionClienteModule'
      },
      {
        path: '',
        loadChildren: '../Administracion/administracion.module#AdministracionModule'
      },
      {
        path: '',
        loadChildren: '../promociones/promociones.module#PromocionesModule'
      },
      {
        path: '',
        loadChildren: '../Servicios/servicios.module#ServiciosModule'
      }, {
        path: 'DashJ',
        component: DashboardComponent, 
        canActivate: [AuthGuard]
      },
      {
        path: 'DashC',
        component: DashboardClienteComponent, 
        canActivate: [AuthGuard]
      },
      {
        path: 'DashA',
        component: DashboardAsistenteComponent, 
        canActivate: [AuthGuard]
      },
      {
        path: 'DashAbo',
        component: DashboardAbogadoComponent, 
        canActivate: [AuthGuard]
      }, {
        path: 'DashSec',
        component: DashboardSecretariaComponent, 
        canActivate: [AuthGuard]
      },
      
    ]
  },
  {
    path: '**',
    component: Error404Component
  },{
    path: 'Error403',
    component: Error403Component
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule { }
