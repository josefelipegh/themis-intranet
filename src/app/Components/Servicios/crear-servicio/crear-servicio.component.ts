import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { RecaudoAbogadoComponent, Actuaciones } from '../recaudo-abogado/recaudo-abogado.component';
import { trigger } from '@angular/animations';
import { FormControl, Validators } from '@angular/forms';
import { GeneralService } from '../../core/Services/general/general.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ActuacionesService } from '../../core/Services/Actuaciones/actuaciones.service';
import * as moment from 'moment';
export interface Food {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-crear-servicio',
  templateUrl: './crear-servicio.component.html',
  styleUrls: ['./crear-servicio.component.scss'],

})
export class CrearServicioComponent implements OnInit {

  resultado: any;
  resultado2: any;
  listaCategoria: any[] = [];
  Abogado: any[] = [];
  idCat;
  id;
  idRadioButton = '';
  radioButton: boolean = false;
  listaCatalogo: any[] = [];
  form_descripcion = { descripcion: '',
                       pagado: '',
                       abogado_id: '',
                       catalogo_servicio_id: '',
                       estatus: 'A'};
  cancelarServicio: any[] = [
    {
      nombre: 'Servicio Cancelado',
      id: 's'
    },
    {
      nombre: 'Servicio no Cancelado',
      id: 'n'
    }
  ];

  cliente_id: any;
  abogado_id: any;
  servicio_id: any;
  catalogo_id: any;
  especialidad_id: any;
  categoria_id: any;
  categoriaFormControl = new FormControl('',
  [
    Validators.required
  ]);
  tipoServFormControl = new FormControl('',
  [
    Validators.required
  ]);

  catalogoFormControl = new FormControl('',
    [
      Validators.required
    ]);
  descripcion = new FormControl('',
    [
      Validators.required
    ]);

  nombre = new FormControl();
  telefono = new FormControl();
  cedula = new FormControl();
  telefonoA = new FormControl();
  cedulaA = new FormControl('',
  [
    Validators.required
  ]);
  correoA = new FormControl();
  datos_actuacion = {id: '', fecha_realizada: '', hora_realizada: '', estatus: 'R'};

  constructor(public generalService: GeneralService,
              private route: ActivatedRoute,
              public snackBar: MatSnackBar,
              private routerLink: Router,
              private act: ActuacionesService) {
    this.cedula.disable();
    this.nombre.disable();
    this.telefono.disable();
    this.telefonoA.disable();
    this.correoA.disable();
    this.CargarCombo();
    this.route.params.subscribe(params => {

      this.obtenerCliente(params['cliente_id']);
      this.obtenerAbogado(params['abogado_id']);
      this.obtenerDatosServicio(params['catalogo_servicio_id']);
      this.id = params['servicio_id'];
      this.catalogo_id = params['catalogo_servicio_id'];
      this.datos_actuacion.id = params['actuacion_id'];
    });
    console.log(this.catalogo_id);

  }


  ngOnInit() {
  }

  // OBTIENE EL ID DE LA CATEGORIA DEL COMBO
  // ObtenerIdCat(id) {
  //   console.log(id);
  //   this.ComboCatalogo(id);
  //   return this.idCat = id;
  // }

  obtenerCliente(idEmpleado) {
    this.cliente_id = idEmpleado;
    this.generalService.ObtenerUno('cliente', idEmpleado).then((result) => {
      this.resultado2 = result;
      this.cedula.setValue(this.resultado2.data.cedula);
      this.nombre.setValue(this.resultado2.data.nombre + ' ' + this.resultado2.data.apellido);
      this.telefono.setValue(this.resultado2.data.telefono);
    }, (err) => {
      console.log(err);
    });
  }

  obtenerAbogado(idEmpleado) {
    this.abogado_id = idEmpleado;
    this.generalService.ObtenerUno('empleado', idEmpleado).then((result) => {
      this.resultado2 = result;
      this.correoA.setValue(this.resultado2.data.correo);
      this.telefonoA.setValue(this.resultado2.data.telefono);
      this.cedulaA.setValue(this.resultado2.data.id);
    }, (err) => {
      console.log(err);
    });
  }

  obtenerAbogadosCategoria(id){
    this.generalService.ObtenerUno('categoria', id).then((result2) => {
      this.resultado2 = result2;
      this.especialidad_id = this.resultado2.data.especialidad_id;
      console.log('especialidad', this.especialidad_id);
      this.generalService.Obtenertodos('vista_abogado/categoria/' + this.especialidad_id ).then((result3) => {
        this.resultado = result3;
        this.Abogado = [];
        for (let i = 0; i < this.resultado.data.length; i++) {
          this.Abogado.push(
            {
              id: this.resultado.data[i].abogado_id,
              nombre: this.resultado.data[i].abogado_nombre,
              apellido: this.resultado.data[i].abogadio_apellido,
              cedula: this.resultado.data[i].abogado_cedula
            }
          );
        }
      }, (err) => {
        console.log(err);
      });
    }, (err) => {
      console.log(err);
    });
  }

  obtenerDatosServicio(idCatalogo) {
    console.log(' catalogo combo ', idCatalogo);
    this.generalService.ObtenerUno('catalogo_servicio', idCatalogo).then((result) => {
      this.resultado2 = result;
      console.log('result categoria', this.resultado2 );
      this.categoria_id = this.resultado2.data.categoria_id;
      console.log('categoria', this.categoria_id);
      this.generalService.ObtenerUno('categoria', this.resultado2.data.categoria_id).then((result2) => {
        this.resultado2 = result2;
        this.especialidad_id = this.resultado2.data.especialidad_id;
        console.log('especialidad', this.especialidad_id);
        this.generalService.Obtenertodos('vista_abogado/categoria/' + this.especialidad_id ).then((result3) => {
          this.resultado = result3;
          this.Abogado = [];
          for (let i = 0; i < this.resultado.data.length; i++) {
            this.Abogado.push(
              {
                id: this.resultado.data[i].abogado_id,
                nombre: this.resultado.data[i].abogado_nombre,
                apellido: this.resultado.data[i].abogadio_apellido,
                cedula: this.resultado.data[i].abogado_cedula
              }
            );
          }
        }, (err) => {
          console.log(err);
        });
      }, (err) => {
        console.log(err);
      });
    }, (err) => {
      console.log(err);
    });
    console.log(this.Abogado);

  }

  // Carga cualquier combo desde la base de datos
  cargarComboEmpleado() {
  }

  CargarCombo() {
    console.log(this.categoria_id);
    this.generalService.Obtenertodos('categoria').then((result) => {

      this.resultado = result;
      console.log(result);
      for (let i = 0; i < this.resultado.data.length; i++) {
        this.listaCategoria.push(this.resultado.data[i]);
      }
      console.log(this.listaCategoria);
      this.ComboCatalogo(this.categoria_id);
      
    }, (err) => {
      console.log(err);
    });
    console.log(this.listaCategoria);
  }

  ObtenerIdCat(id) {
    this.ComboCatalogo(id);
    this.obtenerAbogadosCategoria(id);
  }

  ObtenerIdCatalogo(id) {
   console.log('combo');
    this.obtenerDatosServicio(id);
  }

  ComboCatalogo(id) {
    this.listaCatalogo = [];
    const index = this.listaCategoria.findIndex(x => x.id === id);
    let i = 0;
    console.log('index', this.listaCategoria[index], 'todo', this.listaCategoria);
    while (i < this.listaCategoria[index].catalogo_servicio.length) {
      if (this.listaCategoria[index].catalogo_servicio[i].estatus === 'A') {
        this.listaCatalogo.push(this.listaCategoria[index].catalogo_servicio[i]);
      }
      i++;
    }
  
    console.log(this.listaCatalogo);
  }
  // ACTUALIZAR SERVICIO EN LA BD
  Actualizar(id) {
    const servicio = {id: id, catalogo_servicio_id: this.catalogo_id};
    const index = this.Abogado.findIndex( x => x.id === this.abogado_id);
    const Abo = [{id: this.abogado_id , nombre: this.Abogado[index].nombre + ' ' + this.Abogado[index].apellido }];
    if (this.idRadioButton === '') {
      this.radioButton = true;
    }
    if (this.descripcion.valid && this.radioButton == false) {
      if (this.idRadioButton === 's') {
        this.form_descripcion.pagado = 's';
      } else {
        this.form_descripcion.pagado = 'n';
      }
      console.log('abogado', this.act.getDatosAbogado(), Abo);
      this.form_descripcion.abogado_id = this.abogado_id;
      this.form_descripcion.descripcion = this.descripcion.value;
      this.form_descripcion.catalogo_servicio_id = this.catalogo_id;
      this.datos_actuacion.fecha_realizada = moment().format('MM-DD-YYYY');
      this.datos_actuacion.hora_realizada = moment().format('HH:mm:ss');
      this.generalService.Actualizar(this.form_descripcion, 'servicio', id).then((result) => {
        this.generalService.Actualizar(this.datos_actuacion, 'actuacion_servicio', this.datos_actuacion.id).then((result2) => {

        }, (err) => {
          console.log(err);
        });
        this.act.setServicio(servicio);
        this.act.setDatosAbogado(Abo);
        console.log('datos enviados', this.act.getDatosAbogado(), this.act.getServicio());
        this.openSnackBar('Registro Actualizado!', 'Actualizar');
        this.routerLink.navigate(['/recaudo_servicio']);
      }, (err) => {
        console.log(err);
      });
    }
  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }

  obtenerValor(id) {
    this.radioButton = false;
    return this.idRadioButton = id;
  }

}
