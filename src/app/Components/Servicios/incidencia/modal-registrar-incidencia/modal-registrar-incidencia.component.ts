import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import {MatSnackBar} from '@angular/material';
import { Router } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';
import { GeneralService } from '../../../core/Services/general/general.service';

@Component({
  selector: 'app-modal-registrar-incidencia',
  templateUrl: './modal-registrar-incidencia.component.html',
  styleUrls: ['./modal-registrar-incidencia.component.scss']
})
export class ModalRegistrarIncidenciaComponent implements OnInit {

  form_descripcion = { cliente_id: '', fecha_creado: '', descripcion: '', estatus: 'P', tipo_incidencia_id:''};
  resultado: any;
  id_incidencia;
  listaSugerencia: any[] = [];
  listaIncidencia: any[] = [];
  public loading = false;
  fecha = new Date();
  date;

  constructor(public dialogRef: MatDialogRef<ModalRegistrarIncidenciaComponent>, public generalService: GeneralService, public snackBar: MatSnackBar,private router: Router) {
    this.CargarCombo();
    this.date = (this.fecha.getDate()+'/'+this.fecha.getMonth()+'/'+this.fecha.getFullYear());
      console.log(this.date);
   }

    incidenciaFormControl = new FormControl('',
    [
      Validators.required
    ]);

   descripcionFormControl = new FormControl('', [
    Validators.required
  ]);

  CargarCombo() {
    this.generalService.Obtenertodos('tipo_incidencia').then((result) => {

      this.resultado = result;
      console.log(result);
      for (let i = 0; i < this.resultado.data.length; i++) {
        this.listaIncidencia.push(this.resultado.data[i]);
      }
      console.log(this.listaIncidencia);
    }, (err) => {
      console.log(err);
    });
  }
  ObtenerIdIncidencia(id) {
    console.log(id);
    return this.id_incidencia = id;

  }
  ngOnInit() {
  }

 openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }

  onNoClick(){
    this.dialogRef.close();
  }
  Registrar() {

    if (this.descripcionFormControl.valid) {
      let datos;
      this.generalService.ObtenerUno('cliente/usuario',Number.parseInt(localStorage.getItem('id'))).then((result) => {
        datos = result;
        this.form_descripcion.fecha_creado= ((this.fecha.getMonth()+1) + '-'+ this.fecha.getDate() + '-' + this.fecha.getFullYear());
        this.form_descripcion.descripcion = this.descripcionFormControl.value;
        this.form_descripcion.tipo_incidencia_id = this.id_incidencia;
        this.form_descripcion.cliente_id = datos.data.id;
        console.log(this.form_descripcion);
        
        this.loading = true;
        this.generalService.Registrar(this.form_descripcion, 'incidencia').then((result) => {
          this.openSnackBar('Incidencia registrada exitosamente!', "Registro exitoso");
          this.loading = false;
          this.dialogRef.close(this.form_descripcion);
        }, (err) => {
          console.log(err);
          this.loading = false;
        });
      }, (err) => {
        console.log(err);
      });

    }
  }
}
