import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalRegistrarIncidenciaComponent } from './modal-registrar-incidencia.component';

describe('ModalRegistrarIncidenciaComponent', () => {
  let component: ModalRegistrarIncidenciaComponent;
  let fixture: ComponentFixture<ModalRegistrarIncidenciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalRegistrarIncidenciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalRegistrarIncidenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
