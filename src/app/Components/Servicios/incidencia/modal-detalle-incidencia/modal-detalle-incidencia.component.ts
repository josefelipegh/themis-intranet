import { Component, OnInit, Inject } from '@angular/core';
import {MatSnackBar} from '@angular/material';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-modal-detalle-incidencia',
  templateUrl: './modal-detalle-incidencia.component.html',
  styleUrls: ['./modal-detalle-incidencia.component.scss']
})
export class ModalDetalleIncidenciaComponent implements OnInit {

  form = { fecha:'', descripcion: '', estatus:'', cliente_nombre: '', cliente_apellido:'', cliente_cedula:''};
  constructor(public dialogRef: MatDialogRef<ModalDetalleIncidenciaComponent>,
    public snackBar: MatSnackBar,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public data: any) {

      this.form.fecha = data.fecha;
      this.form.descripcion = data.descripcion;
      this.form.estatus = data.estatus;
      this.form.cliente_nombre = data.cliente_nombre;
      this.form.cliente_cedula = data.cliente_cedula;
      this.form.cliente_apellido = data.cliente_apellido;

     }

  ngOnInit() {
  }


  onNoClick(){
    this.dialogRef.close();
  }

  openSnackBar() {
    this.snackBar.open("Respuesta Enviada!!", "OK", {
      duration: 2000,
    });
    this.router.navigate(['incidencia']);
  }
}
