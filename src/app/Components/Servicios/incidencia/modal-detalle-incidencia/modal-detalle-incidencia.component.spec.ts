import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDetalleIncidenciaComponent } from './modal-detalle-incidencia.component';

describe('ModalDetalleIncidenciaComponent', () => {
  let component: ModalDetalleIncidenciaComponent;
  let fixture: ComponentFixture<ModalDetalleIncidenciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalDetalleIncidenciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDetalleIncidenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
