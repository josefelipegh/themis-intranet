import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
// import { MensajedialogComponent } from '../mensajedialog/mensajedialog.component';
import { Router } from '@angular/router';
import { GeneralService } from '../../core/Services/general/general.service';
import { ModalDetalleIncidenciaComponent } from './modal-detalle-incidencia/modal-detalle-incidencia.component';
import { ModalRegistrarIncidenciaComponent } from './modal-registrar-incidencia/modal-registrar-incidencia.component';


export interface IncidenciaData {
 
  idIncidencia: string;
  estatus:string;
  fecha: string;
  cliente_nombre: string;
  cliente_cedula: string;
  cliente_apellido: string;
  descripcion: string;
  ver: string;
  color: string;
}

let incidencias: any[] = [];

@Component({
  selector: 'app-incidencia',
  templateUrl: './incidencia.component.html',
  styleUrls: ['./incidencia.component.scss']
})
export class IncidenciaComponent implements OnInit {
  resultado: any
  displayedColumns: string[] = ['ver', 'fecha', 'estatus'];
  dataSource: MatTableDataSource<IncidenciaData>;
  datos: IncidenciaData;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  constructor(public generalService: GeneralService, public dialog: MatDialog, private router: Router) {

    this.CargarTabla();
   }


  value="";
  valueInput(){
    this.value="";
    this.applyFilter(this.value);
  } 


  ngOnInit() {
  }

  openDialog() // abre una ventana modal
  {
   this.dialog.open(ModalRegistrarIncidenciaComponent, { width: '600px'});
 }

  openDialogDetalle34334() // abre una ventana modal
  {
   this.dialog.open(ModalDetalleIncidenciaComponent, { width: '500px'});
 }

 // openDialog2(){
 //   this.dialog.open(MensajedialogComponent, { width:'500px'})
 // }

 Estatus(est) {
  let estatus;
  if(est == 'A'){
    estatus = 'Aprobado';
  }else if(est == 'R'){
    estatus = 'Rechazado';
  }else{
    estatus = 'Pendiente';
  }
  return estatus;
}

 inicio(){
   this.router.navigate(['DashJ']);
 }

 applyFilter(filterValue: string) {
   this.dataSource.filter = filterValue.trim().toLowerCase();

   if (this.dataSource.paginator) {
     this.dataSource.paginator.firstPage();
   }
 }
 CargarTabla()
 {
   let client : any;
   this.generalService.ObtenerUno('cliente/usuario',Number.parseInt(localStorage.getItem('id'))).then((result) => {
     client = result;
     console.log(client);
     console.log(client.data.id);
     this.generalService.ObtenerUno('vista_incidencia/cliente',Number.parseInt(client.data.id)).then((result) => {
       this.resultado = result;
       console.log(this.resultado);
       incidencias = [];
       

       for (let i = 0; i < this.resultado.data.length; i++) {
        
        if (this.resultado.data[i].estatus != 'I') {

          let est = this.Estatus(this.resultado.data[i].estatus);
          let colorEst = this.resultado.data[i].estatus;
          let color;
          let ver;

          switch (colorEst) {
            case 'A':
              color = 'green';
              ver = 'check_circle';
              break;
            case 'R':
              color = 'red';
              ver = 'cancel';
              break;
            case 'P':
              color = '#cd9d12';
              ver = 'error';
              break;
          }
         console.log(est);
         this.datos = {
           idIncidencia: this.resultado.data[i].incidencia_id,
           descripcion: this.resultado.data[i].descripcion,
           fecha: this.resultado.data[i].fecha,
           cliente_nombre: this.resultado.data[i].cliente_nombre,
           cliente_cedula: this.resultado.data[i].cliente_cedula,
           cliente_apellido: this.resultado.data[i].cliente_apellido,
           estatus: est,
           ver: ver,
           color: color,
         };
         incidencias.push(this.datos);
       }
      }
       this.dataSource = new MatTableDataSource(incidencias);
       this.dataSource.paginator = this.paginator;
       this.dataSource.sort = this.sort;
     }, (err) => {
       console.log(err);
     });
 

   }, (err) => {
     console.log(err);
   });

   
 }

 
 openDialogDetalle(row) //abre una ventana modal
 {
   const dialogConfig = new MatDialogConfig();

   dialogConfig.disableClose = true;
   dialogConfig.autoFocus = true;
   dialogConfig.width = '600px';
   // PARA PASAR LOS DATOS AL DIALOG
   dialogConfig.data = row;

   const dialogRef = this.dialog.open(ModalDetalleIncidenciaComponent, dialogConfig);
   // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD
   dialogRef.afterClosed().subscribe(
     data => {
       if (data) {
         
       }
     }
   );
 }

}
