import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaEvaluarIncidenciaComponent } from './lista-evaluar-incidencia.component';

describe('ListaEvaluarIncidenciaComponent', () => {
  let component: ListaEvaluarIncidenciaComponent;
  let fixture: ComponentFixture<ListaEvaluarIncidenciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaEvaluarIncidenciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaEvaluarIncidenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
