import { Component, OnInit, Inject } from '@angular/core';
import {MatSnackBar} from '@angular/material';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { GeneralService } from '../../../core/Services/general/general.service';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-modal-evaluar-incidencia',
  templateUrl: './modal-evaluar-incidencia.component.html',
  styleUrls: ['./modal-evaluar-incidencia.component.scss']
})
export class ModalEvaluarIncidenciaComponent implements OnInit {

  form = { fecha:'', descripcion: '', cliente_nombre: '', cliente_apellido:'', cliente_cedula:'', id:''};
  form2 = {tipo_respuesta_id:'', fecha_creacion:'', incidencia_id:'' }
  resultado: any;
  id;
  listaRespuesta: any[] = [];
  public loading = false;
  fecha = new Date();
  date;
  constructor(
    public snackBar: MatSnackBar,
    private router: Router,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<ModalEvaluarIncidenciaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public generalService: GeneralService) { 

      this.CargarCombo();
      this.date = (this.fecha.getDate()+'/'+this.fecha.getMonth()+'/'+this.fecha.getFullYear());
      console.log(this.date);
      this.form.fecha = data.fecha;
      this.form.descripcion = data.descripcion;
      this.form.cliente_nombre = data.cliente_nombre;
      this.form.cliente_apellido = data.cliente_apellido;
      this.form.cliente_cedula = data.cliente_cedula;
      this.form.id = data.id;


    }

    respuestaFormControl = new FormControl('',
    [
      Validators.required
    ]);

    CargarCombo() {
      this.generalService.Obtenertodos('tipo_respuesta').then((result) => {
  
        this.resultado = result;
        console.log(result);
        for (let i = 0; i < this.resultado.data.length; i++) {
          this.listaRespuesta.push(this.resultado.data[i]);
        }
        console.log(this.listaRespuesta);
      }, (err) => {
        console.log(err);
      });
    }
  ngOnInit() {
  }

  ObtenerIdRespuesta(id) {
    console.log(id);
    return this.id = id;

  }

  Registrar() {

    if (this.respuestaFormControl.valid) {
      let datos;
      this.generalService.ObtenerUno('cliente/usuario',Number.parseInt(localStorage.getItem('id'))).then((result) => {
        datos = result;
        this.form2.fecha_creacion= ((this.fecha.getMonth()+1) + '-'+ this.fecha.getDate() + '-' + this.fecha.getFullYear());
        this.form2.tipo_respuesta_id = this.respuestaFormControl.value;
        this.form2.incidencia_id = this.data.id;
  
        console.log(this.form2);
        
        this.loading = true;
        this.generalService.Registrar(this.form2, 'respuesta_incidencia').then((result) => {
          this.openSnackBar('Sugerencia Respondida exitosamente!', "Respuesta exitosa");
          this.loading = false;
          this.dialogRef.close(this.form2);
        }, (err) => {
          console.log(err);
          this.loading = false;
        });
      }, (err) => {
        console.log(err);
      });

    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }

  
  onNoClick(){
    this.dialogRef.close();
  }

}
