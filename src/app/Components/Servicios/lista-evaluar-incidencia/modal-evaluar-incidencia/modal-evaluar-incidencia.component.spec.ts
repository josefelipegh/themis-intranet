import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalEvaluarIncidenciaComponent } from './modal-evaluar-incidencia.component';

describe('ModalEvaluarIncidenciaComponent', () => {
  let component: ModalEvaluarIncidenciaComponent;
  let fixture: ComponentFixture<ModalEvaluarIncidenciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalEvaluarIncidenciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEvaluarIncidenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
