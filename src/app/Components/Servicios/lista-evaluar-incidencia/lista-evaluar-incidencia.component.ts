import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog, MatDialogConfig} from '@angular/material';
import { Router } from '@angular/router';
import { GeneralService } from '../../core/Services/general/general.service';
import { ModalEvaluarIncidenciaComponent } from './modal-evaluar-incidencia/modal-evaluar-incidencia.component';


export interface IncidenciaData {
  cliente_cedula: string;
  fecha  : string;
  descripcion: string;
  cliente_nombre: string;
  cliente_apellido: string;
  id: string;
  servicio: string;
  incidencia: string;
}

/** Constants used to fill up our data base. */
let incidencias: any[] = [];

@Component({
  selector: 'app-lista-evaluar-incidencia',
  templateUrl: './lista-evaluar-incidencia.component.html',
  styleUrls: ['./lista-evaluar-incidencia.component.scss']
})
export class ListaEvaluarIncidenciaComponent implements OnInit {
  resultado: any;
  datos: IncidenciaData;
  displayedColumns: string[] = ['ver', 'cliente_cedula', 'cliente', 'servicio', 'incidencia', 'fecha'];
  dataSource: MatTableDataSource<IncidenciaData>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  value="";
  valueInput(){
    this.value="";
    this.applyFilter(this.value);
  }

  constructor(public dialog: MatDialog, private router: Router, public generalService: GeneralService) {

    this.CargarTabla();
  }

  CargarTabla() {
    this.generalService.Obtenertodos('vista_incidencia').then((result) => {
      this.resultado = result;
      incidencias = [];
      for (let i = 0; i < this.resultado.data.length; i++) {
        if (this.resultado.data[i].estatus=='P') 
        {
        let est = this.Estatus(this.resultado.data[i].estatus);
        this.datos = {
          cliente_cedula: this.resultado.data[i].cliente_cedula,
          fecha: this.resultado.data[i].fecha,
          descripcion: this.resultado.data[i].descripcion,
          cliente_apellido: this.resultado.data[i].cliente_apellido,
          cliente_nombre: this.resultado.data[i].cliente_nombre,
          id: this.resultado.data[i].id,
          servicio: this.resultado.data[i].tipo_servicio,
          incidencia: this.resultado.data[i].tipo_incidencia,

        };
        incidencias.push(this.datos);
      }
    }
      this.dataSource = new MatTableDataSource(incidencias);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, (err) => {
      console.log(err);
    });

  }

  Estatus(est) {
    let estatus;
    return estatus = est == 'P' ? 'Pendiente' : 'Aprobado';
  }
  inicio(){
    this.router.navigate(['DashJ']);
  }

  ngOnInit() {
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openDialogAtenderIncidencia(row) //abre una ventana modal
  {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '600px';
    // PARA PASAR LOS DATOS AL DIALOG
    dialogConfig.data = row;

    const dialogRef = this.dialog.open(ModalEvaluarIncidenciaComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          
        }
      }
    );
  }
  

}
