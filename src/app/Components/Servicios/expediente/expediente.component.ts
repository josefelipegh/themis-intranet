import { Component, OnInit, ViewChild } from '@angular/core';
import {MatDialog, MatSort,  MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig, MatTableDataSource } from '@angular/material';
import { ModalExpedienteComponent } from './modal-expediente/modal-expediente.component';
import { GeneralService } from '../../core/Services/general/general.service';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { ActuacionesService } from '../../core/Services/Actuaciones/actuaciones.service';
import { CasoService } from '../../core/Services/caso/caso.service';
import * as moment from 'moment';


// INTERFACE PARA CREAR UN OBJETO DE Act
export interface Actuacion {
  id: number;
  nombre: string;
  descripcion: string; datosServicio
  estatus: string;
}

export interface clienteobj {
  idCliente:any;
  imagenCliente:any; 
  nombreCliente: any;
  cedulaCliente:any;
}

export interface abogadoobj {
  idAbogado:any;
  imagenAbogado:any; 
  nombreAbogado: any;
  cedulaAbogado:any;
}




@Component({
  selector: 'detallecasoCliente',
  templateUrl: './detallecasoCliente.component.html',
  styleUrls: ['./expediente.component.scss']
})

export class DetalleCasoComponentCliente implements OnInit {
  resultado: any;
  datos: any;
  alternate: boolean = true;
  toggle: boolean = true;
  color: boolean = true;
  size: number = 40;
  expandEnabled: boolean = true;
  entries = [];
  actuaciones : any;
  datosAbogado: abogadoobj;

  // array para cargar los datos de cliente asignado al servicio
  abogado = [];
  idusu: any;

  //atributo para obtener los datos de los abogados desde Casos Activos
  datosabogado: any;
  idAbogado: any;
  nombreAbogado: any;
  cedulaAbogado: any;
  idCliente: any;
  cedulaCliente: any;
  nombreCliente: any;
  idServico: any;
  idUsuario: any;


  
  displayedColumns: string[] = [
  'imagenAbogado',
  'nombreAbogado', 
  'cedulaAbogado'];
    dataSource: MatTableDataSource<abogadoobj>;
  
    @ViewChild(MatSort) sort: MatSort;
  
    constructor( 
      public generalService: GeneralService, 
      public casoService: CasoService,
      public dialog: MatDialog, 
      private location: Location, 
      private router: Router, 
      private planificar: ActuacionesService) {
      this.obtenerDatos();
      this.obtenerImagenUsuario();
        // Assign the data to the data source for the table to render
      this.Actuacion();
      this.CargarTimelineServicio();
      this.dataSource = new MatTableDataSource(this.abogado);
     }

     obtenerDatos(){
         let dato = this.casoService.getServicio();
         this.idAbogado = dato.idabogado;
         this.cedulaAbogado = dato.cedulaabogado;
         this.nombreAbogado = dato.nombreabogado;
         this.idCliente = dato.idcliente;
         this.nombreCliente = dato.nombrecliente;
         this.cedulaCliente = dato.cedulacliente;
         this.idServico = dato.id;
     }
    
     ngOnInit() {

    }

  
    
    openDialogRecaudos()
    {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
      dialogConfig.width = '50%';
      dialogConfig.data = this.idServico;
      console.log(this.idServico);
      console.log('miercoles');
  
      const dialogRef = this.dialog.open(ModalExpedienteComponent, dialogConfig);
      // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD 
      dialogRef.afterClosed().subscribe(
        data => {
          if (data) {
            this.CargarTimelineServicio();
          }
        }
      );
    }
  
    // METEDO QUE OBTIENE UNA LISTA DE TipoActuacion Y SE CARGA EN LA TABLA, donde
     // se le hace un llamado de generalService donde esta el servicio y colocas 
     // la ultima directiva ejemplo "tipo_actuacion" que te lleva la direccion del
     // servicio en especifico, para obtener los resultados.
     Actuacion() {
      this.entries = [];
      this.generalService.Obtenertodos('actuacion').then((result) => {
        this.resultado = result;
        console.log(this.resultado);
        for (let i = 0; i < this.resultado.data.length; i++) {
         let est = this.Estatus(this.resultado.data[i].estatus); 
         if (this.resultado.data[i].id !== 27 ) {
          this.datos = {
            id: this.resultado.data[i].id,
            nombre: this.resultado.data[i].nombre,
            descripcion: this.resultado.data[i].descripcion,
            estatus: est,
          };
                // se le asigna al array TipoActuacion todos los datos obtenidos
                this.entries.push(this.datos);
         }
      
    
        }
        console.log(this.entries);
      }, (err) => {
        console.log(err);
      });
  
    }
  
    // METODO QUE RETORNA ACTIVO O INACTIVO PARA MOSTRAR EN LA TABLA
    Estatus(est) {
      let estatus;
      return estatus = est == 'A' ? 'Activo' : 'Inactivo';
    }

  obtenerImagenUsuario(){
    let res;
    let res2;
    this.generalService.ObtenerUno('empleado',this.idAbogado).then((result) => {
      res = result;
      this.generalService.ObtenerUno('vista_usuario',res.data.usuario_id).then((result) => {
        res2 = result;
        this.abogado = [{
          idabog : this.idAbogado,
          imagenAbogado: res2.data.imagen,
          nombreAbogado : this.nombreAbogado,
          cedulaAbogado: this.cedulaAbogado,
        }];
      this.dataSource = new MatTableDataSource(this.abogado);
      }, (err) => {
        console.log(err);
      });
    }, (err) => {
      console.log(err);
    });
  }

     CargarTimelineServicio() {
       console.log(this.idServico);
      this.generalService.Obtenertodos('vista_agenda/servicio/'+this.idServico).then((result) => {
        this.resultado = result;
        this.actuaciones = [];
        for (let i = 0; i < this.resultado.data.length; i++) {
          this.actuaciones.push({
            id: this.resultado.data[i].actuacion_servicio_id,
            title: this.resultado.data[i].actuacion_nombre,
            descripcion: this.resultado.data[i].descripcion,
            fecha_planificada: moment(this.resultado.data[i].actuacion_fecha,['MM-DD-YYYY']).format('DD-MM-YYYY'),
            hora_inicio_planificada:moment(this.resultado.data[i].hora_inicio, ['HH:mm:ss']).format('LT'),
            hora_fin_planificada:moment(this.resultado.data[i].hora_fin, ['HH:mm:ss']).format('LT'),
            estatusActuacion: this.resultado.data[i].actuacion_estatus,
            idActuacion: this.resultado.data[i].actuacion_estatus,
            color: this.CambiarColor(this.resultado.data[i].actuacion_estatus),
            idActuacion_Servicio: this.resultado.data[i].actuacion_servicio_id,
            realizada: this.Mostrar_fecha_realizada(this.resultado.data[i].actuacion_estatus),
            fecha_realizada: moment(this.resultado.data[i].fecha_realizada, ['MM-DD-YYYY']).format('DD-MM-YYYY'),
            hora_realizada: moment(this.resultado.data[i].hora_realizada, ['HH:mm:ss']).format('LT'),
          });
        }
      }, (err) => {
        console.log(err);
      });
  
    }

    CambiarColor(est){
      let cambiarColor;
      switch(est)
      {
        case 'P':
        cambiarColor= '#FFD700';
        break;
        case 'R':
        cambiarColor = '#008000';
        break;
        case 'F':
        cambiarColor = '#DC143C';
        break;
        case 'I':
        cambiarColor = '#FF8C00';
        break;
      }
      return cambiarColor;
    }

    Mostrar_fecha_realizada(dato){
      if (dato == 'R'){
        return true;
      }else{
        return false;
      }
    }

  
    volver() {
      this.location.back();
    }
  
    removeEntry(){
      this.entries.pop();
    }
  
    onHeaderClick(event) {
      if (!this.expandEnabled) {
        event.stopPropagation();
      }
    }
  
    onDotClick(event) {
      if (!this.expandEnabled) {
        event.stopPropagation();
      }
    }
  
    onExpandEntry(expanded, index) {
      console.log(`Expand status of entry #${index} changed to ${expanded}`)
    }
}




@Component({
  selector: 'detallecasoJefe',
  templateUrl: './detallecasoJefe.component.html',
  styleUrls: ['./expediente.component.scss']
})

export class DetalleCasoComponentJefe implements OnInit {
  resultado: any;
  datos: any;
  alternate: boolean = true;
  toggle: boolean = true;
  color: boolean = true;
  size: number = 40;
  expandEnabled: boolean = true;
  entries = [];
  actuaciones : any;
  datosCliente: clienteobj;

  // array para cargar los datos de cliente asignado al servicio
  cliente : clienteobj[];
  idusu: any;

  //atributo para obtener los datos de los abogados desde Casos Activos
  datosabogado: any;
  idAbogado: any;
  nombreAbogado: any;
  cedulaAbogado: any;
  idCliente: any;
  cedulaCliente: any;
  nombreCliente: any;
  idServico: any;
  idUsuario: any;


  
  displayedColumns: string[] = [
  'imagenCliente',
  'nombreCliente', 
  'cedulaCliente'];
    dataSource: MatTableDataSource<clienteobj>;
  
    @ViewChild(MatSort) sort: MatSort;
  
    constructor( 
      public generalService: GeneralService, 
      public casoService: CasoService,
      public dialog: MatDialog, 
      private location: Location, 
      private router: Router, 
      private planificar: ActuacionesService) {
      this.obtenerDatos();
      this.obtenerImagenUsuario();
        // Assign the data to the data source for the table to render
      this.Actuacion();
      this.CargarTimelineServicio();
      this.dataSource = new MatTableDataSource(this.cliente);
     }

     obtenerDatos(){
         const dato = this.casoService.getServicio();
         this.idAbogado = dato.idabogado;
         this.cedulaAbogado = dato.cedulaabogado;
         this.nombreAbogado = dato.nombreabogado;
         this.idCliente = dato.idcliente;
         this.nombreCliente = dato.nombrecliente;
         this.cedulaCliente = dato.cedulacliente;
         this.idServico = dato.id;
     }
    
     ngOnInit() {

    }
  
    openDialogRegistrar() // abre una ventana modal
    {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
      dialogConfig.width = '50%';
  
      const dialogRef = this.dialog.open(ModalExpedienteComponent, dialogConfig);
      // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD 
      dialogRef.afterClosed().subscribe(
        data => {
          if (data) {
            this.Actuacion()
          }
        }
      );
    }
  
    // METEDO QUE OBTIENE UNA LISTA DE TipoActuacion Y SE CARGA EN LA TABLA, donde
     // se le hace un llamado de generalService donde esta el servicio y colocas 
     // la ultima directiva ejemplo "tipo_actuacion" que te lleva la direccion del
     // servicio en especifico, para obtener los resultados.
     Actuacion() {
      this.entries = [];
      this.generalService.Obtenertodos('actuacion').then((result) => {
        this.resultado = result;
        console.log(this.resultado);
        for (let i = 0; i < this.resultado.data.length; i++) {
         let est = this.Estatus(this.resultado.data[i].estatus); 
         if (this.resultado.data[i].id !== 27) {
          this.datos = {
            id: this.resultado.data[i].id,
            nombre: this.resultado.data[i].nombre,
            descripcion: this.resultado.data[i].descripcion,
            estatus: est,
          };
                // se le asigna al array TipoActuacion todos los datos obtenidos
                this.entries.push(this.datos);
         }
      
    
        }
        console.log(this.entries);
      }, (err) => {
        console.log(err);
      });
  
    }
  
    // METODO QUE RETORNA ACTIVO O INACTIVO PARA MOSTRAR EN LA TABLA
    Estatus(est) {
      let estatus;
      return estatus = est == 'A' ? 'Activo' : 'Inactivo';
    }
  

  obtenerImagenUsuario(){
    let res;
    let res2;
    this.generalService.ObtenerUno('cliente',this.idCliente).then((result) => {
      res = result;
      console.log('res');
      console.log(res);
      this.generalService.ObtenerUno('vista_usuario',res.data.usuario_id).then((result) => {
        res2 = result;
        this.cliente = [{
          idCliente : this.idCliente,
          imagenCliente: res2.data.imagen,
          nombreCliente : this.nombreCliente,
          cedulaCliente: this.cedulaCliente
        }];
      this.dataSource = new MatTableDataSource(this.cliente);
      }, (err) => {
        console.log(err);
      });
    }, (err) => {
      console.log(err);
    });
  }

     CargarTimelineServicio() {
       console.log(this.idServico);
      this.generalService.Obtenertodos('vista_agenda/servicio/'+this.idServico).then((result) => {
        this.resultado = result;
        this.actuaciones = [];
        for (let i = 0; i < this.resultado.data.length; i++) {
          this.actuaciones.push({
            id: this.resultado.data[i].actuacion_servicio_id,
            title: this.resultado.data[i].actuacion_nombre,
            descripcion: this.resultado.data[i].descripcion,
            fecha_planificada: moment(this.resultado.data[i].actuacion_fecha,['MM-DD-YYYY']).format('DD-MM-YYYY'),
            hora_inicio_planificada:moment(this.resultado.data[i].hora_inicio, ['HH:mm:ss']).format('LT'),
            hora_fin_planificada:moment(this.resultado.data[i].hora_fin, ['HH:mm:ss']).format('LT'),
            estatusActuacion: this.resultado.data[i].actuacion_estatus,
            idActuacion: this.resultado.data[i].actuacion_estatus,
            color: this.CambiarColor(this.resultado.data[i].actuacion_estatus),
            idActuacion_Servicio: this.resultado.data[i].actuacion_servicio_id,
            realizada: this.Mostrar_fecha_realizada(this.resultado.data[i].actuacion_estatus),
            fecha_realizada: moment(this.resultado.data[i].fecha_realizada, ['MM-DD-YYYY']).format('DD-MM-YYYY'),
            hora_realizada: moment(this.resultado.data[i].hora_realizada, ['HH:mm:ss']).format('LT'),
          });
        }
      }, (err) => {
        console.log(err);
      });
  
    }


    CambiarColor(est){
      let cambiarColor;
      switch(est)
      {
        case 'P':
        cambiarColor= '#FFD700';
        break;
        case 'R':
        cambiarColor = '#008000';
        break;
        case 'F':
        cambiarColor = '#DC143C';
        break;
        case 'I':
        cambiarColor = '#FF8C00';
        break;
      }
      return cambiarColor;
    }

    Mostrar_fecha_realizada(dato){
      if (dato == 'R'){
        return true;
      }else{
        return false;
      }
    }
  
    volver() {
      this.location.back();
    }
  
    removeEntry(){
      this.entries.pop();
    }
  
    onHeaderClick(event) {
      if (!this.expandEnabled) {
        event.stopPropagation();
      }
    }
  
    onDotClick(event) {
      if (!this.expandEnabled) {
        event.stopPropagation();
      }
    }
  
    onExpandEntry(expanded, index) {
      console.log(`Expand status of entry #${index} changed to ${expanded}`)
    }
}


@Component({
  selector: 'detallecasoAbogado',
  templateUrl: './detallecasoAbogado.component.html',
  styleUrls: ['./expediente.component.scss']
})

export class DetalleCasoComponentAbogado implements OnInit {
  resultado: any;
  datos: any;
  alternate: boolean = true;
  toggle: boolean = true;
  color: boolean = true;
  size: number = 40;
  expandEnabled: boolean = true;
  entries = [];
  actuaciones : any [];
  form = {estatus: '', fecha_realizada: '', hora_realizada: ''};
  datosCliente: clienteobj;

  // array para cargar los datos de cliente asignado al servicio
  cliente: clienteobj[];
  idusu: any;

  //atributo para obtener los datos de los abogados desde Casos Activos
  datosabogado: any;
  idAbogado: any;
  nombreAbogado: any;
  cedulaAbogado: any;
  idCliente: any;
  cedulaCliente: any;
  nombreCliente: any;
  idServico: any;
  idUsuario: any;
  redireccion:boolean = true;
  
  displayedColumns: string[] = [
  'imagenCliente',
  'nombreCliente', 
  'cedulaCliente'];
    dataSource: MatTableDataSource<clienteobj>;
  
    @ViewChild(MatSort) sort: MatSort;
  
    constructor( 
      public generalService: GeneralService, 
      public casoService: CasoService,
      public dialog: MatDialog, 
      private location: Location, 
      private router: Router, 
      private planificar: ActuacionesService) {
      this.obtenerDatosAbogado();
      this.obtenerImagenUsuario();
        // Assign the data to the data source for the table to render
      this.Actuacion();
      this.CargarTimelineServicio();
      this.dataSource = new MatTableDataSource(this.cliente);
     }

     obtenerDatosAbogado(){
         const dato = this.casoService.getServicio();
         console.log(dato);
         this.idAbogado = dato.idabogado;
         this.cedulaAbogado = dato.cedulaabogado;
         this.nombreAbogado = dato.nombreabogado;
         this.idCliente = dato.idcliente;
         this.nombreCliente = dato.nombrecliente;
         this.cedulaCliente = dato.cedulacliente;
         this.idServico = dato.id;
     }
    
     ngOnInit() {

    }
  
    iraPagina(Pagename) {
      this.planificar.insertData(this.entries);
      this.router.navigate([`${Pagename}`]);
    }
    openDialogRegistrar() // abre una ventana modal
    {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
      dialogConfig.width = '50%';
  
      const dialogRef = this.dialog.open(ModalExpedienteComponent, dialogConfig);
      // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD 
      dialogRef.afterClosed().subscribe(
        data => {
          if (data) {
            this.Actuacion()
          }
        }
      );
    }
  
    // METEDO QUE OBTIENE UNA LISTA DE TipoActuacion Y SE CARGA EN LA TABLA, donde
     // se le hace un llamado de generalService donde esta el servicio y colocas 
     // la ultima directiva ejemplo "tipo_actuacion" que te lleva la direccion del
     // servicio en especifico, para obtener los resultados.
     Actuacion() {
      this.entries = [];
      this.generalService.Obtenertodos('actuacion').then((result) => {
        this.resultado = result;
        console.log(this.resultado);
        for (let i = 0; i < this.resultado.data.length; i++) {
         let est = this.Estatus(this.resultado.data[i].estatus); 
         if (this.resultado.data[i].id !== 27 ) {
          this.datos = {
            id: this.resultado.data[i].id,
            nombre: this.resultado.data[i].nombre,
            descripcion: this.resultado.data[i].descripcion,
            estatus: est,
          };
                // se le asigna al array TipoActuacion todos los datos obtenidos
                this.entries.push(this.datos);
         }
      
    
        }
        console.log(this.entries);
      }, (err) => {
        console.log(err);
      });
  
    }
  
    // METODO QUE RETORNA ACTIVO O INACTIVO PARA MOSTRAR EN LA TABLA
    Estatus(est) {
      let estatus;
      return estatus = est == 'A' ? 'Activo' : 'Inactivo';
    }

  obtenerImagenUsuario(){
    let res;
    let res2;
    this.generalService.ObtenerUno('cliente',this.idCliente).then((result) => {
      res = result;
      this.generalService.ObtenerUno('vista_usuario',res.data.usuario_id).then((result) => {
        res2 = result;
        
        this.cliente = [{
          idCliente : this.idCliente,
          imagenCliente: res2.data.imagen,
          nombreCliente : this.nombreCliente,
          cedulaCliente: this.cedulaCliente
        }];
      this.dataSource = new MatTableDataSource(this.cliente);
      }, (err) => {
        console.log(err);
      });
    }, (err) => {
      console.log(err);
    });
  }

     CargarTimelineServicio() {
       console.log(this.idServico);
      this.generalService.Obtenertodos('vista_agenda/servicio/'+this.idServico).then((result) => {
        this.resultado = result;
        this.actuaciones = [];
        for (let i = 0; i < this.resultado.data.length; i++) {
          
          this.actuaciones.push({
            
            id: this.resultado.data[i].actuacion_servicio_id,
            title: this.resultado.data[i].actuacion_nombre,
            descripcion: this.resultado.data[i].descripcion,
            fecha_planificada: moment(this.resultado.data[i].fecha_plan,['MM-DD-YYYY']).format('DD-MM-YYYY'),
            hora_inicio_planificada:moment(this.resultado.data[i].hora_inicio, ['HH:mm:ss']).format('LT'),
            hora_fin_planificada:moment(this.resultado.data[i].hora_fin, ['HH:mm:ss']).format('LT'),
            estatusActuacion: this.resultado.data[i].actuacion_estatus,
            idActuacion: this.resultado.data[i].actuacion_estatus,
            color: this.CambiarColor(this.resultado.data[i].actuacion_estatus),
            idActuacion_Servicio: this.resultado.data[i].actuacion_servicio_id,
            pendiente: this.BotonEjecutar(this.resultado.data[i].actuacion_estatus),
            redireccion: this.resultado.data[i].actuacion_estatus,
            realizada: this.Mostrar_fecha_realizada(this.resultado.data[i].actuacion_estatus),
            fecha_realizada: moment(this.resultado.data[i].fecha_realizada, ['MM-DD-YYYY']).format('DD-MM-YYYY'),
            hora_realizada: moment(this.resultado.data[i].hora_realizada, ['HH:mm:ss']).format('LT'),

          });
          console.log(this.actuaciones);
          this.BotonRedireccion();
        }
      }, (err) => {
        console.log(err);
      });
    }
    

    CambiarColor(est){
      let cambiarColor;
      switch(est)
      {
        case 'P':
        cambiarColor= '#FFD700';
        break;
        case 'R':
        cambiarColor = '#008000';
        break;
        case 'F':
        cambiarColor = '#DC143C';
        break;
        case 'I':
        cambiarColor = '#FF8C00';
        break;
      }
      return cambiarColor;
    }

    BotonRedireccion(){
      let i = this.actuaciones.findIndex( x => x.redireccion == 'F');
      if ( i!= -1){
        this.redireccion = false;
      }else{
        this.redireccion = true;
      }
    }

    BotonEjecutar(dato){
      if (dato == 'P' ){
        return true;
      }else{
        return false;
      }
    }

    Mostrar_fecha_realizada(dato){
      if (dato == 'R'){
        return true;
      }else{
        return false;
      }
    }

    
    EjecutarActuacion(idact_serv) {
      this.form.estatus = 'R';
      this.form.fecha_realizada = moment().format('MM-DD-YYYY');
      this.form.hora_realizada = moment().format('HH:mm:ss');
      this.generalService.Actualizar(this.form,'actuacion_servicio',idact_serv).then((result) => {
      this.actuaciones.find(x => x.idActuacion_Servicio == idact_serv).color=this.CambiarColor(this.form.estatus);
      this.actuaciones.find(x => x.idActuacion_Servicio == idact_serv).pendiente=this.BotonEjecutar(this.form.estatus);
      },
       (err) => {
        console.log(err);
      });
      this.actuaciones.find(x => x.idActuacion_Servicio == idact_serv).realizada=this.Mostrar_fecha_realizada(this.form.estatus);
    }
  
    volver() {
      this.location.back();
    }
  
    removeEntry(){
      this.entries.pop();
    }
  
    onHeaderClick(event) {
      if (!this.expandEnabled) {
        event.stopPropagation();
      }
    }
  
    onDotClick(event) {
      if (!this.expandEnabled) {
        event.stopPropagation();
      }
    }
  
    onExpandEntry(expanded, index) {
      console.log(`Expand status of entry #${index} changed to ${expanded}`)
    }

}


@Component({
  selector: 'app-expediente',
  templateUrl: './expediente.component.html',
  styleUrls: ['./expediente.component.scss']
})



export class ExpedienteComponent implements OnInit {

  Abogado = false;
  Cliente= false;
  Jefe= false;

  constructor( public generalService: GeneralService, 
    public casoService: CasoService,
    public dialog: MatDialog, 
    private location: Location, private router: Router, private planificar: ActuacionesService) {
      this.getRol();
   }
  
   ngOnInit() {
  }

  getRol() {
    const rol = JSON.parse(localStorage.getItem('rol'));
    if (rol.nombre === 'Abogado') {
      this.Abogado = true;
    } else if (rol.nombre === 'Cliente') {
      this.Cliente = true;
    } else if (rol.nombre === 'Jefe') {
      this.Jefe = true;
    }
  }
  
}






