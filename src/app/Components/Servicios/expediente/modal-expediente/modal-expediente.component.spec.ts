import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalExpedienteComponent } from './modal-expediente.component';

describe('ModalExpedienteComponent', () => {
  let component: ModalExpedienteComponent;
  let fixture: ComponentFixture<ModalExpedienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalExpedienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalExpedienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
