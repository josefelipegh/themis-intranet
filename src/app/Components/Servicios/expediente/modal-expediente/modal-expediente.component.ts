import { Component, OnInit, Inject } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { GeneralService } from '../../../core/Services/general/general.service';

@Component({
  selector: 'app-modal-expediente',
  templateUrl: './modal-expediente.component.html',
  styleUrls: ['./modal-expediente.component.scss']
})


export class ModalExpedienteComponent implements OnInit {


  Abogado = false;
  Cliente= false;
  Jefe= false;
  resultado: any;

  recaudos: any []=[];

  idservicio: any;

    
    constructor(public dialogRef: MatDialogRef<ModalExpedienteComponent>,
      public generalService: GeneralService, public snackBar: MatSnackBar,
      @Inject(MAT_DIALOG_DATA) public data: any) {
      // PARA CARGAR LA CAJA DE TEXTO CUANDO SE VA ACTUALIZAR DATOS
      console.log(data);
      console.log('pupu');
      this.idservicio = data;
      this.CargarRecadudos();
    }

    CargarRecadudos() {
      
      this.recaudos = [];
      console.log(this.idservicio);
      console.log('aquii');
     this.generalService.Obtenertodos('vista_recaudo_servicio/servicio/'+this.idservicio).then((result) => {
       this.resultado = result;
       for (let i = 0; i < this.resultado.data.length; i++) {
         this.recaudos.push({
           iddocumento : this.resultado.data[i].documento_id,
           documento: this.resultado.data[i].documento,
           estatus: this.resultado.data[i].estatus,
         });
         console.log(this.recaudos);
       }
     }, (err) => {
       console.log(err);
     });
   }
   

    getRol() {
      const rol = JSON.parse(localStorage.getItem('rol'));
      if (rol.nombre === 'Abogado') {
        this.Abogado = true;
      } else if (rol.nombre === 'Cliente') {
        this.Cliente = true;
      } else if (rol.nombre === 'Jefe') {
        this.Jefe = true;
      }
    }
  
    
  
    ngOnInit() {
    }
    // CERRAR EL MODAL
    onNoClick() {
      this.dialogRef.close()
    }
   
  
}
