import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalRechazarSolicitudComponent } from './modal-rechazar-solicitud.component';

describe('ModalRechazarSolicitudComponent', () => {
  let component: ModalRechazarSolicitudComponent;
  let fixture: ComponentFixture<ModalRechazarSolicitudComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalRechazarSolicitudComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalRechazarSolicitudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
