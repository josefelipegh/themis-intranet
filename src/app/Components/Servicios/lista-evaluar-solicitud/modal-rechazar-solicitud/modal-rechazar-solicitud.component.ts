import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatDialog, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { GeneralService } from '../../../core/Services/general/general.service';

@Component({
  selector: 'app-modal-rechazar-solicitud',
  templateUrl: './modal-rechazar-solicitud.component.html',
  styleUrls: ['./modal-rechazar-solicitud.component.scss']
})
export class ModalRechazarSolicitudComponent implements OnInit {

  form = { nombre: '', apellido: '', servicio: '' };
  form2 = { estatus: '' };
  form3 = { solicitud_id: '', tipo_respuesta_id: '', descripcion: '' };

  idRes;
  idSol;

  resultado: any;
  listaRespuesta: any[] = [];

  public loading = false;
  constructor(public dialog: MatDialog, public snackBar: MatSnackBar, public generalService: GeneralService,
    public dialogRef: MatDialogRef<ModalRechazarSolicitudComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {

    this.CargarCombo();

    this.form.nombre = data.cliente_nombre;
    this.form.apellido = data.cliente_apellido;
    this.form.servicio = data.tipoServicio;

    this.idSol = data.id;
    console.log(this.idSol);
  }

  respuestaFormControl = new FormControl('', [
    Validators.required
  ]);

  causaFormControl = new FormControl('', [
    Validators.required
  ]);

  disableSelect = new FormControl(true);



  ngOnInit() {

  }

  CargarCombo() {
    this.generalService.Obtenertodos('tipo_respuesta').then((result) => {

      this.resultado = result;
      console.log(result);
      for (let i = 0; i < this.resultado.data.length; i++) {
        this.listaRespuesta.push(this.resultado.data[i]);
      }
      console.log(this.listaRespuesta);
    }, (err) => {
      console.log(err);
    });
  }

  ObtIdRespuesta(id) {

    console.log(id);
    return this.idRes = id;

  }




  Rechazar() {



    if (this.respuestaFormControl.valid && this.causaFormControl.valid) {
      this.form2.estatus = 'R';

      this.loading = true;

      this.generalService.Actualizar(this.form2, 'solicitud', this.idSol).then((result) => {

        this.form3.descripcion = this.causaFormControl.value;
        this.form3.tipo_respuesta_id = this.idRes;
        this.form3.solicitud_id = this.idSol;

        console.log(this.form3.solicitud_id);
        console.log(this.form3.tipo_respuesta_id);
        console.log('fewfwe', this.data.cliente_id);
        this.generalService.Registrar(this.form3, 'solicitud_rechazo').then((result) => {
          let idcliente;
          this.generalService.ObtenerUno('cliente', this.data.cliente_id).then((result) => {
            idcliente = result;
            let disp;
            this.generalService.ObtenerUno('dispositivo', idcliente.data.usuario_id).then((result) => {
              disp = result;
              let datos = {
                dispositivos: [disp.data.token],
                titulo: this.respuestaFormControl.value,
                mensaje: this.data.cliente_nombre + ' ' + this.data.cliente_apellido + ', tu solicitud ha sido rechazada debido a ' + this.causaFormControl.value,
                usuario_id: idcliente.data.usuario_id,
                tipo_notificacion_id: 3
              }
              this.generalService.Registrar(datos, 'notificacion/enviar').then((result) => {
                let Body = [
                  {
                    phone_number: "04127926733",
                    message: "hola mi pelu, esto es themis bello :*",
                    device_id: 108618
                  }
                ]
                let body = {
                  Body: Body
                }
                // console.log('holaaa');
                // this.generalService.RegistrarMensaje([
                //   {
                //     "phone_number": "04127926733",
                //     "message": "MI pelu ,te amo <3",
                //     "device_id": 108618
                //   }
                // ]).then((result) => {
                // }, (err) => {
                //   console.log(err);
                // });

                let datosUser;
                this.generalService.ObtenerUno('usuario/findById', idcliente.data.usuario_id).then((result) => {
                  datosUser = result;
                  let u = {
                    correo: datosUser.data.correo,
                    servicio: this.data.tipoServicio
                  }
                  this.generalService.Registrar(u, 'correo/solicitudRechazo').then((result) => {
                    this.openSnackBar('Solicitud Rechazada con éxito!!', "Rechazar Solicitud");
                    this.loading = false;
                    this.dialogRef.close(this.form3);
                  }, (err) => {
                    console.log(err);
                  });
                }, (err) => {
                  console.log(err);
                });
              }, (err) => {
                console.log(err);
              });

            }, (err) => {
              console.log(err);
            });
          }, (err) => {
            console.log(err);
          });

        }, (err) => {
          console.log(err);
        });

      }, (err) => {
        console.log(err);
        this.loading = false;
      });

      this.dialogRef.close(this.data);
    }

  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}