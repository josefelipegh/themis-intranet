import { Component, OnInit, Inject } from '@angular/core';
import { ModalRechazarSolicitudComponent } from '../modal-rechazar-solicitud/modal-rechazar-solicitud.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


@Component({
  selector: 'app-atender-solicitud',
  templateUrl: './atender-solicitud.component.html',
  styleUrls: ['./atender-solicitud.component.scss']
})
export class AtenderSolicitudComponent implements OnInit {

  myFilter = (d: Date): boolean => {
    const day = d.getDay();
    // Prevent Saturday and Sunday from being selected.
    return day !== 0 && day !== 6;
  }


  form = { cedula: '', nombre: '', apellido: '', fecha: '', servicio: '', solicitud_id: '', descripcion: ''};

  constructor(public dialog: MatDialog, public dialogRef: MatDialogRef<AtenderSolicitudComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { 
    this.form.cedula = data.cliente_cedula;
    this.form.nombre = data.cliente_nombre;
    this.form.apellido = data.cliente_apellido;
    this.form.fecha = data.fecha;
    this.form.servicio = data.tipoServicio;
    this.form.descripcion = data.descripcion;
  }

  

  ngOnInit() {
  }

  

  onNoClick(){
    this.dialogRef.close();
  }
}
