import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaEvaluarSolicitudComponent } from './lista-evaluar-solicitud.component';

describe('ListaEvaluarSolicitudComponent', () => {
  let component: ListaEvaluarSolicitudComponent;
  let fixture: ComponentFixture<ListaEvaluarSolicitudComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaEvaluarSolicitudComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaEvaluarSolicitudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
