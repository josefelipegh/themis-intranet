import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, Sort } from '@angular/material';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material';
import { FormControl } from '@angular/forms';
import { AtenderSolicitudComponent } from './atender-solicitud/atender-solicitud.component';
import { ModalRechazarSolicitudComponent } from './modal-rechazar-solicitud/modal-rechazar-solicitud.component';
import { AgendarComponent } from './agendar/agendar.component';
import { ConfirmarAprobarComponent } from './confirmar-aprobar/confirmar-aprobar.component';
import { Router } from '@angular/router';
import { GeneralService } from '../../core/Services/general/general.service';



export interface ReclamoData {
  id: number;
  cliente_id: number;
  cliente_cedula: string;
  cliente_nombre: string;
  cliente_apellido: string;
  catalogo_servicio_id: number;
  tipoServicio: string;
  descripcion: string;
  estatus: string;
  fecha: string;
  color: string;
  ver: string;
  agendar: boolean;

}

/** Constants used to fill up our data base. */
let solicitudesxevaluar: any[] = [];


@Component({
  selector: 'app-lista-evaluar-solicitud',
  templateUrl: './lista-evaluar-solicitud.component.html',
  styleUrls: ['./lista-evaluar-solicitud.component.scss']
})
export class ListaEvaluarSolicitudComponent implements OnInit {

  displayedColumns: string[] = ['ver', 'cliente_cedula', 'nombreCliente', 'catalogo_servicio_id', 'fecha_creacion', 'estatus', 'accion'];
  dataSource: MatTableDataSource<ReclamoData>;
  resultado: any;
  datos: ReclamoData;


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  sortBy: any;

  value = "";

  valueInput() {
    this.value = "";
    this.applyFilter(this.value);
  }

  constructor(public dialog: MatDialog, private router: Router, public generalService: GeneralService) {
    this.CargarTabla();
  }

  inicio() {
    this.router.navigate(['DashJ']);
  }

  ngOnInit() {

  }



  openDialogAtenderSoli(row) //abre una ventana modal
  {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    // PARA PASAR LOS DATOS AL DIALOG
    dialogConfig.data = row;

    const dialogRef = this.dialog.open(AtenderSolicitudComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {

        }
      }
    );
  }

  openDialogConfirmar(row) {
    const dialogConfig3 = new MatDialogConfig();

    dialogConfig3.disableClose = true;
    dialogConfig3.autoFocus = true;
    dialogConfig3.width = '50%';
    // PARA PASAR LOS DATOS AL DIALOG
    dialogConfig3.data = row;

    const dialogRef3 = this.dialog.open(ConfirmarAprobarComponent, dialogConfig3);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD
    dialogRef3.afterClosed().subscribe(
      data => {
        if (data) {
          this.CargarTabla();
        }
      }
    );

  }

  openDialogRechazar(row) {
    const dialogConfig2 = new MatDialogConfig();

    dialogConfig2.disableClose = true;
    dialogConfig2.autoFocus = true;
    dialogConfig2.width = '50%';
    // PARA PASAR LOS DATOS AL DIALOG
    dialogConfig2.data = row;

    const dialogRef2 = this.dialog.open(ModalRechazarSolicitudComponent, dialogConfig2);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD
    dialogRef2.afterClosed().subscribe(
      data => {
        if (data) {
          this.CargarTabla();
        }
      }
    );

  }

  openDialogAgendar(row) {
    const dialogConfig3 = new MatDialogConfig();

    dialogConfig3.disableClose = true;
    dialogConfig3.autoFocus = true;
    dialogConfig3.width = '600px';
    // PARA PASAR LOS DATOS AL DIALOG
    dialogConfig3.data = row;
    console.log(dialogConfig3.data);
    const dialogRef3 = this.dialog.open(AgendarComponent, dialogConfig3);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD
    dialogRef3.afterClosed().subscribe(
      data => {
        if (data) {
          this.CargarTabla();
        }
      }
    );

  }



  CargarTabla() {
    this.generalService.Obtenertodos('vista_solicitud').then((result) => {
      this.resultado = result;
      solicitudesxevaluar = [];
      console.log(this.resultado);
      for (let i = 0; i < this.resultado.data.length; i++) {
        if (this.resultado.data[i].estatus == 'P' || this.resultado.data[i].estatus == 'A') {
          let est = this.Estatus(this.resultado.data[i].estatus);
          let agendar;
          let color;
          let ver;

          switch (this.resultado.data[i].estatus) {
            case 'A':
              color = 'green';
              ver = 'check_circle';
              agendar = false;
              break;
            case 'P':
              color = '#cd9d12';
              ver = 'error';
              agendar = true;
              break;
          }

          this.datos = {
            cliente_id: this.resultado.data[i].cliente_id,
            catalogo_servicio_id: this.resultado.data[i].catalogo_servicio_id,
            id: this.resultado.data[i].solicitud_id,
            cliente_cedula: this.resultado.data[i].cliente_cedula,
            cliente_nombre: this.resultado.data[i].cliente_nombre,
            cliente_apellido: this.resultado.data[i].cliente_apellido,
            tipoServicio: this.resultado.data[i].servicio_nombre,
            descripcion: this.resultado.data[i].descripcion,
            fecha: this.resultado.data[i].fecha,
            estatus: est,
            agendar: agendar,
            color: color,
            ver: ver,
          };
          console.log(agendar);
          solicitudesxevaluar.push(this.datos);
        }
      }
      this.dataSource = new MatTableDataSource(solicitudesxevaluar);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, (err) => {
      console.log(err);
    });

  }

  sortColumn($event:Sort){console.log(event)}
  Estatus(est) {
    let estatus;
    return estatus = est == 'P' ? 'Pendiente' : 'Aprobado';
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
