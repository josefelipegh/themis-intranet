import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmarAprobarComponent } from './confirmar-aprobar.component';

describe('ConfirmarAprobarComponent', () => {
  let component: ConfirmarAprobarComponent;
  let fixture: ComponentFixture<ConfirmarAprobarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmarAprobarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmarAprobarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
