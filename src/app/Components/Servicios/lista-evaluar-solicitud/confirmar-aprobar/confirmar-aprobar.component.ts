import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatDialog, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { GeneralService } from '../../../core/Services/general/general.service';
import * as moment from 'moment';
import { ActuacionesService } from '../../../core/Services/Actuaciones/actuaciones.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-confirmar-aprobar',
  templateUrl: './confirmar-aprobar.component.html',
  styleUrls: ['./confirmar-aprobar.component.scss']
})
export class ConfirmarAprobarComponent implements OnInit {

  form2 = { estatus: '' };
  id;
  public loading = false;

  constructor(public dialog: MatDialog, public snackBar: MatSnackBar, public generalService: GeneralService,
    public dialogRef: MatDialogRef<ConfirmarAprobarComponent>,
     @Inject(MAT_DIALOG_DATA) public data: any, private actServ: ActuacionesService,
     private router: Router) {

    this.id = data.id;
  }

  ngOnInit() {
  }

    RegistrarServicio() {

      this.loading = true;

      this.form2.estatus = 'A';
      console.log(this.id);
      this.generalService.Actualizar(this.form2, 'solicitud', this.id).then((result) => {
  
        let idcliente;
        this.generalService.ObtenerUno('cliente', this.data.cliente_id).then((result) => {
          idcliente = result;
          let disp;
          this.generalService.ObtenerUno('dispositivo', idcliente.data.usuario_id).then((result) => {
            disp = result;
            let datos = {
              dispositivos: [disp.data.token],
              titulo: 'Solicitud aprobada',
              mensaje: this.data.cliente_nombre + ' ' + this.data.cliente_apellido + ', tu solicitud ha sido aprobada!!',
              usuario_id: idcliente.data.usuario_id,
              tipo_notificacion_id: 1
            }
            this.generalService.Registrar(datos, 'notificacion/enviar').then((result) => {
              let datosUser;
              this.generalService.ObtenerUno('usuario/findById',idcliente.data.usuario_id).then((result) => {
                datosUser = result;
                let u ={
                  correo: datosUser.data.correo,
                  servicio: this.data.tipoServicio
                }
                this.generalService.Registrar(u,'correo/solicitudAceptada').then((result) => {
                  this.openSnackBar('Solicitud Aprobada exitosamente!', "Aprobación exitoso");
                  this.dialogRef.close(this.form2);
                }, (err) => {
                  console.log(err);
                });
              }, (err) => {
                console.log(err);
              });
            }, (err) => {
              console.log(err);
            });
          }, (err) => {
            console.log(err);
            this.loading = false;
          });
        }, (err) => {
          console.log(err);
        });
      }, (err) => {
        console.log(err);
        this.loading = false;
      });
  
}

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }

  onNoClick() {
    this.dialogRef.close();
  }

  goToPage(pageName: string) {
    this.router.navigate([`${pageName}`]);
  }
}