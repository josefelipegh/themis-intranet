import { Component, OnInit, Inject } from '@angular/core';
import { GeneralService } from '../../../core/Services/general/general.service';
import * as moment from 'moment';
import { ActuacionesService } from '../../../core/Services/Actuaciones/actuaciones.service';
import { Router } from '@angular/router';
import { MatDialogRef, MatDialog, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';

export interface Abogado {

  id: string;
  nombre: string;  
}

export interface Actuacion {

  id: string;
  nombre: string;
}

export interface Servicio {

  id: string;
}

let abogados: any[] = [];
let actuaciones: any[] = [];

@Component({
  selector: 'app-agendar',
  templateUrl: './agendar.component.html',
  styleUrls: ['./agendar.component.scss']
})
export class AgendarComponent implements OnInit {

  form = { cliente_id: '', catalogo_servicio_id: '', fecha_creado: '' };
  form2 = {estatus: ''};

  resulAbog: any;
  resulActu: any;
  resulServ: any;

  datos: Abogado;
  datos2: Actuacion;
  datos3: Servicio;

  id;
  public loading = false;

  constructor(public dialog: MatDialog, public snackBar: MatSnackBar, public generalService: GeneralService,
    public dialogRef: MatDialogRef<AgendarComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private actServ: ActuacionesService,
    private router: Router) {

    this.id = data.id;
    this.form.cliente_id = data.cliente_id;
    this.form.catalogo_servicio_id = data.catalogo_servicio_id;
    this.form.fecha_creado = moment().format('MM-DD-YYYY');
  }

  ngOnInit() {
  }

  AgendarServicio() {
    this.loading = true;
    this.generalService.Registrar(this.form, 'servicio').then((result) => {

      this.resulServ = result;

      this.datos3 = {
        id: this.resulServ.data.servicio_id,
      };

      this.form2.estatus = 'G';
      console.log(this.id);
      this.generalService.Actualizar(this.form2, 'solicitud', this.id).then((resultA) => {
        let idCategoria;
        let idEspecialidad;
        this.generalService.ObtenerUno('catalogo_servicio', Number.parseInt(this.form.catalogo_servicio_id)).then((result) => {
          idCategoria = result;
          console.log('categoria', idCategoria.data.categoria_id);
          this.generalService.ObtenerUno('categoria', idCategoria.data.categoria_id).then((result2) => {
            idEspecialidad = result2;
            console.log('especialidad',idEspecialidad.data.especialidad_id);
            this.generalService.Obtenertodos('vista_abogado/categoria/' + idEspecialidad.data.especialidad_id).then((result3) => {
              this.resulAbog = result3;
              abogados = [];
              console.log('abogados', this.resulAbog);
              for (let i = 0; i < this.resulAbog.data.length; i++) {
                this.datos = {
                  id: this.resulAbog.data[i].abogado_id,
                  nombre: this.resulAbog.data[i].abogado_nombre + " " + this.resulAbog.data[i].abogadio_apellido,
                };
                abogados.push(this.datos);
              }
              actuaciones = [];
              this.datos2 = {
                id: '27',
                nombre: 'Cita inicial'
              };
              actuaciones.push(this.datos2);
              console.log('actuaciones', actuaciones);
              console.log('antes set', abogados);
              this.actServ.setDatosAbogado(abogados);
              console.log('despues set', abogados, this.actServ.getDatosAbogado());
              this.actServ.setServicio(this.datos3);
              this.actServ.insertData(actuaciones);
              console.log('datos a pasar', 
                          this.actServ.getData(), 
                          this.actServ.getDatosAbogado(), 
                          this.actServ.getDatosCita(), 
                          this.actServ.getServicio());
              this.goToPage('planificar_servicio');
              this.openSnackBar('Solicitud Agendada exitosamente!', 'Registro exitoso');
              this.dialogRef.close(this.form);
              console.log('arreglo abogados', abogados);
            }, (err) => {
              console.log(err);
            });
          }, (err) => {
            console.log(err);
          });
        }, (err) => {
          console.log(err);
        });
      }, (err) => {
        console.log(err);
        this.loading = false;
      });
    }, (err) => {
      console.log(err);

    });



  }

  onNoClick() {
    this.dialogRef.close();
  }
  
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }

  goToPage(pageName: string) {
    this.router.navigate([`${pageName}`]);
  }
}
