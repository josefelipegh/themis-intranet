import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core';
import {
  CalendarEvent,
  CalendarView,
} from 'angular-calendar';
import { Subject } from 'rxjs';
import {
  addWeeks,
  subWeeks,
} from 'date-fns';
import * as moment from 'moment';
import { GeneralService } from '../../core/Services/general/general.service';
import { ActuacionesService } from '../../core/Services/Actuaciones/actuaciones.service';
import { identifierModuleUrl } from '@angular/compiler';
import { flatMap } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';


type CalendarPeriod = 'day' | 'week';


// combos
export interface Actuacion {
  id: any;
  nombre: any;
}

export interface Horario {
  id_dia: any;
  nombre: any;
  horario: any;
  fecha: any;
}

export interface Bloque {
  id: any;
  inicio: any;
  fin: any;
}
// fin combo

@Component({
  selector: 'app-planificacion',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './planificacion.component.html',
  styleUrls: ['./planificacion.component.scss'],
})
export class PlanificacionComponent implements OnInit {

  public datepicker: Date = new Date();
  week= [];
  actuacion: Actuacion;
  actuaciones: any[] = [];
  abogados: any[] = [];
  resultado: any;
  hora: Horario;
  horario: any[] = [];
  bloque: Bloque;
  bloques: any[] = [];
  Horas: any[] = [];
  opcActuacion: any;
  opcDia: any;
  opcHora: any;
  opcAbogado: any;
  fecha: any;
  inicio: any;
  fin: any;
  cambio: boolean;

  CalendarView = CalendarView;
  
  view: CalendarPeriod = 'week';
  viewDate = new Date();
  auxDate: any;
  locale: string = 'es';
  StartHour= 7;
  EndHour= 18;
  form_descripcion = { actuacion_id: '',
                       fecha_plan: '',
                       horario_id: '',
                       abogado_id: '',
                       id_servicio: '',
                      estatus: ''};
  events: CalendarEvent[] = [];
  activeDayIsOpen = false;
  minDate: Date = subWeeks(new Date(), 0);
  maxDate: Date = addWeeks(new Date(), 1);
  prevBtnDisabled: boolean = false;
  nextBtnDisabled: boolean = false;
  disableServ = true;
  disableDesc = true;
  disableAct = true;
  disableDia = true;
  disableAbo = false;
  idDia: any;
  idBloque: any;
  idServicio: any;

  refresh: Subject<any> = new Subject();

  constructor(public generalService: GeneralService,
              private actserv: ActuacionesService,
              public snackBar: MatSnackBar,
              private router: Router) {
    this.Cargarhorario();
    this.actuaciones = this.actserv.getData();
    this.abogados = this.actserv.getDatosAbogado();
    console.log('servicio', this.actserv.getServicio());
    this.CargarServicio();
  }

  

  // metodo limitar dias calendario

   getWeek(fromDate) { // funcion que retorna el arreglo de las fechas de la semana elegida
    const date = new Date(fromDate);
    const sunday = new Date(date.setDate(date.getDate() - date.getDay()));
    let s = new Date(sunday);
    const result = [moment(s, ['YYYY-MM-DDTHH:mm:ss.SSSZ']).format('L')];
    while (sunday.setDate(sunday.getDate() + 1) && sunday.getDay() !== 0) {
      let s = new Date(sunday);
      result.push(moment(s, ['YYYY-MM-DDTHH:mm:ss.SSSZ']).format('L'));
    }
    this.week = result;
   }

   CargarServicio() {
      const datos = this.actserv.getServicio();
      this.form_descripcion.id_servicio = datos.id;
   }

   CargarAgenda(id) {
     console.log('cargar agenda');
    this.generalService.Obtenertodos('vista_agenda/abogado/' + id).then((result) => {
      this.resultado = result;
      console.log(this.resultado);
      this.events = [];
      console.log(this.events);
      for (let i = 0; i < this.resultado.data.length; i++) {
        this.events.push({
          id: this.resultado.data[i].actuacion_servicio_id,
          title: this.resultado.data[i].actuacion_nombre,
          start: new Date(parseInt(moment(this.resultado.data[i].fecha_plan, ['MM-DD-YYYY']).format('YYYY'), 10),
                          parseInt(moment(this.resultado.data[i].fecha_plan, ['MM-DD-YYYY']).format('MM'), 10) - 1,
                          parseInt(moment(this.resultado.data[i].fecha_plan, ['MM-DD-YYYY']).format('DD'), 10),
                          parseInt(moment(this.resultado.data[i].hora_inicio, ['HH:mm:ss']).format('HH'), 10),
                          parseInt(moment(this.resultado.data[i].hora_inicio, ['HH:mm:ss']).format('mm'), 10),
                          parseInt(moment(this.resultado.data[i].hora_inicio, ['HH:mm:ss']).format('ss'), 10)),
          end: new Date(parseInt(moment(this.resultado.data[i].fecha_plan, ['MM-DD-YYYY']).format('YYYY'), 10),
                        parseInt(moment(this.resultado.data[i].fecha_plan, ['MM-DD-YYYY']).format('MM'), 10) - 1,
                        parseInt(moment(this.resultado.data[i].fecha_plan, ['MM-DD-YYYY']).format('DD'), 10),
                        parseInt(moment(this.resultado.data[i].hora_fin, ['HH:mm:ss']).format('HH'), 10),
                        parseInt(moment(this.resultado.data[i].hora_fin, ['HH:mm:ss']).format('mm'), 10),
                        parseInt(moment(this.resultado.data[i].hora_fin, ['HH:mm:ss']).format('ss'), 10)),
          color: { primary: this.resultado.data[i].color_primario ,  secondary: this.resultado.data[i].color_secundario},
          draggable: false,
          resizable: {
            beforeStart: false,
            afterEnd: false
          }
        });
      }
      this.refresh.next();
      console.log('eventos', this.events,  this.refresh.next());
      this.disableAct = false;
      this.disableAbo = true;

    }, (err) => {
      console.log(err);
    });
    this.refresh.next();
  }
   Cargarhorario() {
    this.generalService.Obtenertodos('vista_horario').then((result) => {
      this.resultado = result;
      this.horario = [];
      for (let i = 0; i < this.resultado.data.length; i++) {

        let val;
        val = this.horario.filter( hora => hora.nombre === this.resultado.data[i].dia_semana_nombre);
        if ( val.length === 0 ) {
          this.bloques = [];
          if (this.resultado.data[i].horario_estatus === 'A') {
            for (let j = 0; j < this.resultado.data.length; j ++) {
              if (this.resultado.data[j].dia_semana_id === this.resultado.data[i].dia_semana_id) {
                if (this.resultado.data[j].horario_estatus === 'A') {
                  this.bloque = {
                    id: this.resultado.data[j].horario_id,
                    inicio: moment(this.resultado.data[j].hora_inicio, ['HH:mm:ss']).format('LT'),
                    fin: moment(this.resultado.data[j].hora_fin, ['HH:mm:ss']).format('LT')
                  };
                  this.bloques.push(this.bloque);
                }
              }
            }
            this.hora = {
              id_dia: this.resultado.data[i].dia_semana_id,
              nombre: this.resultado.data[i].dia_semana_nombre,
              fecha: moment(this.week[this.GetDia(this.resultado
                                                  .data[i]
                                                  .dia_semana_nombre)]
                            , ['MM-DD-YYYY'] ).format('DD-MM-YYYY'),
              horario: this.bloques
            };
            this.horario.push(this.hora);
          }
        }
      }
    }, (err) => {
      console.log(err);
    });

  }
  GetDia(nombre) {
    switch (nombre) {
      case 'Lunes':
        return 1;
      case 'Martes':
        return 2;
      case 'Miercoles':
        return 3;
      case 'Jueves':
        return 4;
      case 'Viernes':
        return 5;
      case 'Sabado':
        return 6;
      case 'Domingo':
        return 0;
    }
  }
  ComboBloqueHora(id) {
    this.Horas = [];
    let index = this.horario.findIndex(x => x.id_dia === id);
    let i = 0;
    while (i < this.horario[index].horario.length ) {
      this.Horas.push(this.horario[index].horario[i]);
      i++;
    }
  }
  ObtenerHora(datos) {
    this.Horas = datos;
  }


  // beforeMonthViewRender({ body }: { body: CalendarWeekView[] }): void {
  //   body.forEach(day => {
  //     if (!this.dateIsValid(day.date)) {
  //       day.cssClass = 'cal-disabled';
  //     }
  //   });
  // }
  ngOnInit() {
    this.getWeek(this.viewDate);
  }

  ObtenerIdDia(id, nombre) {
    this.ComboBloqueHora(id);
    switch (nombre) {
      case 'Lunes':
        this.idDia = 1;
        break;
      case 'Martes':
        this.idDia = 2;
        break;
      case 'Miercoles':
        this.idDia = 3;
        break;
      case 'Jueves':
        this.idDia = 4;
        break;
      case 'Viernes':
        this.idDia = 5;
        break;
      case 'Sabado':
        this.idDia = 6;
        break;
        case 'Domingo':
        this.idDia = 0;
        break;
    }
    this.fecha = this.week[this.idDia];
    this.disableServ = false;
  }
  cargarfecha(fecha, hora_inicio, hora_fin) {
    this.inicio = new Date(parseInt(moment(fecha, ['MM-DD-YYYY']).format('YYYY'), 10),
                            parseInt(moment(fecha, ['MM-DD-YYYY']).format('MM'), 10) - 1,
                            parseInt(moment(fecha, ['MM-DD-YYYY']).format('DD'), 10),
                            parseInt(moment(hora_inicio, ['LT']).format('HH'), 10) - 4,
                            parseInt(moment(hora_inicio, ['LT']).format('mm'), 10),
                            parseInt(moment(hora_inicio, ['LT']).format('ss'), 10));
    this.fin = new Date(parseInt(moment(fecha, ['MM-DD-YYYY']).format('YYYY'), 10),
                          parseInt(moment(fecha, ['MM-DD-YYYY']).format('MM'), 10) - 1,
                          parseInt(moment(fecha, ['MM-DD-YYYY']).format('DD'), 10),
                          parseInt(moment(hora_fin, ['LT']).format('HH'), 10) - 4,
                          parseInt(moment(hora_fin, ['LT']).format('mm'), 10),
                          parseInt(moment(hora_fin, ['LT']).format('ss'), 10) );
  }

  removerEvento(id) {
    let index = this.events.findIndex(x => x.id === id);
    this.events.splice(index, 1);
  }

  ObtenerIdBloque(id) {
    this.idBloque = id;
    let dia = this.horario.find(x => x.id_dia === this.opcDia);
    let bloque = dia.horario.find( x => x.id === this.idBloque);
    let actuacion = this.actuaciones.find( x => x.id === this.opcActuacion);
    this.fecha = this.week[this.idDia];
    if ( this.cambio ) {
      this.removerEvento(this.opcActuacion);
    }
    this.cargarfecha(this.fecha, bloque.inicio, bloque.fin);
    this.addEvent(this.opcActuacion, actuacion.nombre, this.inicio, this.fin);
    this.cambio = true;
  }

  addEvent(id, title, start, end) {
    this.events.push({
      id: id,
      title: title,
      start: new Date(parseInt(moment(start, ['YYYY-MM-DDTHH:mm:ss.SSSZ']).format('YYYY'), 10),
                      parseInt(moment(start, ['YYYY-MM-DDTHH:mm:ss.SSSZ']).format('MM'), 10) - 1,
                      parseInt(moment(start, ['YYYY-MM-DDTHH:mm:ss.SSSZ']).format('DD'), 10),
                      parseInt(moment(start, ['YYYY-MM-DDTHH:mm:ss.SSSZ']).format('HH'), 10) + 4,
                      parseInt(moment(start, ['YYYY-MM-DDTHH:mm:ss.SSSZ']).format('mm'), 10),
                      parseInt(moment(start, ['YYYY-MM-DDTHH:mm:ss.SSSZ']).format('ss'), 10)),
      end: new Date(parseInt(moment(end, ['YYYY-MM-DDTHH:mm:ss.SSSZ']).format('YYYY'), 10),
                    parseInt(moment(end, ['YYYY-MM-DDTHH:mm:ss.SSSZ']).format('MM'), 10) - 1,
                    parseInt(moment(end, ['YYYY-MM-DDTHH:mm:ss.SSSZ']).format('DD'), 10),
                    parseInt(moment(end, ['YYYY-MM-DDTHH:mm:ss.SSSZ']).format('HH'), 10) + 4,
                    parseInt(moment(end, ['YYYY-MM-DDTHH:mm:ss.SSSZ']).format('mm'), 10),
                    parseInt(moment(end, ['YYYY-MM-DDTHH:mm:ss.SSSZ']).format('ss'), 10)),
      color: { primary: '#bf4959',  secondary: '#d4838e'},
      draggable: false,
      resizable: {
        beforeStart: false,
        afterEnd: false
      }
    });
    this.refresh.next();
    console.log(this.events, this.refresh.next());
  }

  comboActuacion() {
    this.disableDesc = false;
    this.disableDia = false;
    this.disableAct = true;
  }

  Cancelar() {
    if ( this.cambio ) {
      this.cambio = false;
      this.removerEvento(this.opcActuacion);
    }
    this.disableAct = true;
    this.disableDesc = true;
    this.disableDia = true;
    this.disableServ = true;
    this.disableAbo = false;
    this.opcActuacion = -1;
    this.opcAbogado = -1;
    this.opcDia = -1;
    this.opcHora = -1;
  }

  Agregar() {
    let resultado;
    let resultado2;
    let resultado3;
    let horario;
    let hora;
    console.log(this.week[this.opcDia], this.week[this.idDia]);
      this.form_descripcion.actuacion_id = this.opcActuacion;
      this.form_descripcion.fecha_plan = this.week[this.opcDia];
      this.form_descripcion.horario_id = this.idBloque;
      this.form_descripcion.abogado_id = this.opcAbogado;
      if (this.opcActuacion === '30') {
        this.form_descripcion.estatus = 'F';
      } else {
        this.form_descripcion.estatus = 'P';
      }
      console.log(this.form_descripcion);
      this.generalService.Registrar(this.form_descripcion, 'servicio/actuaciones').then((result) => {
        if ( this.opcActuacion === '30') {
          this.generalService.Actualizar({estatus: 'F'},
                                          'servicio', this.form_descripcion.id_servicio).then((result3) => {
            this.openSnackBar('Servicio ha sido finalizado!', 'Finalizar');
            this.goToPage('casos');
          }, (err) => {
            console.log(err);
          });
        } else if ( this.opcActuacion === '27') {
          const servicio = { estatus: 'A',
                              abogado_id: this.opcAbogado };
          console.log('entra cita', servicio);
          this.generalService.Actualizar(servicio, 'servicio', this.form_descripcion.id_servicio).then((result3) => {
            this.generalService.ObtenerUno('horario', parseInt(this.form_descripcion.horario_id, 10)).then((result5) => {
              horario = result5;
              this.generalService.ObtenerUno('bloque_hora', parseInt(horario.data.bloque_hora_id, 10)).then((result6) => {
                hora = result6;
                  this.generalService.ObtenerUno('servicio', parseInt(this.form_descripcion.id_servicio, 10)).then((result) => {
                    resultado = result;
                    this.generalService.ObtenerUno('dispositivo', parseInt(resultado.data.cliente.usuario_id, 10)).then((result2) => {
                      resultado2 = result2;
                      let dato = {
                        dispositivos: [resultado2.data.token],
                        titulo: 'Cita agendada',
                        mensaje: 'Su cita ha sido asignada para el día: ' +
                                  this.form_descripcion.fecha_plan +
                                  ' entre las ' +
                                  moment(hora.data.hora_inicio, ['HH:mm:ss']).format('LT') + ' y ' +
                                  moment(hora.data.hora_fin, ['HH:mm:ss']).format('LT'),
                        usuario_id: resultado.data.cliente.usuario_id,
                        tipo_notificacion_id: 1
                      };
                      console.log('noti',dato);
                      this.generalService.Registrar(dato, 'notificacion/enviar').then((result3) => {

                        let datosUser;
                         this.generalService.ObtenerUno('usuario/findById', resultado.data.cliente.usuario_id).then((result4) => {
                           datosUser = result4;
                           const u = {
                             correo: datosUser.data.correo,
                             fecha: this.form_descripcion.fecha_plan,
                             hora: moment(hora.hora_inicio, ['HH:mm:ss']).format('LT')
                           };
                           this.generalService.Registrar(u, 'correo/cita').then((result5) => {
                             this.openSnackBar('Cita asignada con éxito!', 'Finalizar');
                             this.goToPage('evaluar');
                           }, (err) => {
                             console.log(err);
                           });
                         }, (err) => {
                           console.log(err);
                         });
                      }, (err) => {
                        console.log(err);
                        });
                      }, (err) => {
                      console.log(err);
                      });
                    }, (err) => {
                    console.log(err);
                    });
              }, (err) => {
              console.log(err);
              });
            }, (err) => {
            console.log(err);
            });
          }, (err) => {
          console.log(err);
          });
        } else {
          this.openSnackBar('Registro Exitoso!', 'registrar');
          this.Cancelar();
        }

      }, (err) => {
        console.log(err);
      });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }

  goToPage(pageName: string) {
    this.router.navigate([`${pageName}`]);
  }
}
