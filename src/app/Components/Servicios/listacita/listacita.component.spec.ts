import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListacitaComponent } from './listacita.component';

describe('ListacitaComponent', () => {
  let component: ListacitaComponent;
  let fixture: ComponentFixture<ListacitaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListacitaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListacitaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
