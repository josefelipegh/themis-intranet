import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';




export interface DialogData {
  cita: string;
  
}
@Component({
  selector: 'app-detalle-cita',
  templateUrl: './detalle-cita.component.html',
  styleUrls: ['./detalle-cita.component.scss']
})
export class DetalleCitaComponent  {
  registrar = false;
    constructor( public dialogRef: MatDialogRef<DetalleCitaComponent>,
                 @Inject(MAT_DIALOG_DATA) public data: any) {
      console.log(data);
      if ( data.rol.nombre === 'Abogado') {
        this.registrar = true;
      }
    }


  onNoClick(): void {
    this.dialogRef.close();
  }

}
