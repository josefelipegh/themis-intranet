import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DetalleCitaComponent } from './detalle-cita/detalle-cita.component';
import { IncidenciaCitaComponent } from './incidencia-cita/incidencia-cita.component';
import { Router } from '@angular/router';
import { GeneralService } from '../../core/Services/general/general.service';
import * as moment from 'moment';
import { ModalCancelarServicioComponent } from './modal-cancelar-servicio/modal-cancelar-servicio.component';

export interface DetalleCitaData {
  cliente_cedula: string;
  cliente_nombre: string;
  cliente_apellido: string;
  abogado_nombre: string;
  abogado_apellido: string;
  fecha: string;
  actuacion_servicio_id: number;
  cliente_id: number;
  abogado_id: number;
  servicio_id: number;
  catalogo_servicio_id: number;
  catalogo_servicio_nombre: string;
}

let cita: any[] = [];

@Component({
  selector: 'cita-cliente',
  templateUrl: 'CitaCliente.html',
  styleUrls: ['./listacita.component.scss'],
})
export class CitaClienteComponent implements OnInit {
  resultado: any;
  listaCita: DetalleCitaData;
  displayedColumns: string[] = ['estatus',
                                'idCita',
                                'nombreAbogado',
                                'fechaCita',
                                'Cancelar'];
  dataSource: MatTableDataSource<DetalleCitaData>;
  rol: any;
  id: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  value = '';
  valueInput() {
    this.value = '';
    this.applyFilter(this.value);
  }

  constructor(public dialog: MatDialog,
              private router: Router,
              public generalService: GeneralService) {
    this.CargarTabla();
  }

  ngOnInit() {
  }

  openDialogCancelar(row) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    // PARA PASAR LOS DATOS AL DIALOG PREGUNTA ELIMINAR
    dialogConfig.data = row;
    const dialogRef = this.dialog.open(ModalCancelarServicioComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          const index = cita.findIndex(x => x.servicio_id === row.servicio_id);
          cita.splice(index, 1);
          this.dataSource = new MatTableDataSource(cita);
        }
      }
    );
  }

  openDialog(row) {// abre una ventana modal con datos
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '60%';
    // PARA PASAR LOS DATOS AL DIALOG
    dialogConfig.data = {row: row, rol: JSON.parse(localStorage.getItem('rol'))};
    const dialogRef = this.dialog.open(DetalleCitaComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD
    dialogRef.afterClosed().subscribe(
      data => {
      }
    );
  }

  ObtenerCliente() {

  }

  // METEDO QUE OBTIENE UNA LISTA DE DOCUMENTO Y SE CARGA EN LA TABLA
  CargarTabla() {
    this.generalService.ObtenerUno('cliente/usuario', Number.parseInt(localStorage.getItem('id'))).then((result) => {
      this.id = result;
      this.generalService.Obtenertodos('vista_cita/cliente/' + this.id.data.id + '/estatus/P' ).then((result2) => {
        this.resultado = result2;
        cita = [];
        for (let i = 0; i < this.resultado.data.length; i++) {
          this.listaCita = {
            cliente_nombre: this.resultado.data[i].cliente_nombre,
            fecha: moment(this.resultado.data[i].fecha, ['MM-DD-YYYY']).format('DD-MM-YYYY'),
            abogado_apellido: this.resultado.data[i].abogado_apellido,
            abogado_nombre: this.resultado.data[i].abogado_nombre,
            abogado_id: this.resultado.data[i].abogado_id,
            cliente_apellido: this.resultado.data[i].cliente_apellido,
            cliente_id: this.resultado.data[i].cliente_id,
            cliente_cedula: this.resultado.data[i].cliente_cedula,
            catalogo_servicio_nombre: this.resultado.data[i].catalogo_servicio_nombre,
            catalogo_servicio_id: this.resultado.data[i].catalogo_servicio_id,
            servicio_id: this.resultado.data[i].servicio_id,
            actuacion_servicio_id: this.resultado.data[i].actuacion_servicio_id,
          };
          cita.push(this.listaCita);
        }
        this.dataSource = new MatTableDataSource(cita);
        this.dataSource.paginator = this.paginator;
      }, (err) => {
        console.log(err);
      });
    }, (err) => {
      console.log(err);
    });
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}

@Component({
  selector: 'cita-abogado',
  templateUrl: 'CitaAbogado.html',
  styleUrls: ['./listacita.component.scss'],
})
export class CitaAbogadoComponent implements OnInit {
  resultado: any;
  listaCita: DetalleCitaData;
  displayedColumns: string[] = ['estatus',
                                'idCita',
                                'nombreCliente',
                                'fechaCita',
                                'Cancelar'];
  dataSource: MatTableDataSource<DetalleCitaData>;
  rol: any;
  id: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  value = '';
  valueInput() {
    this.value = '';
    this.applyFilter(this.value);
  }

  constructor(public dialog: MatDialog,
              private router: Router,
              public generalService: GeneralService) {
    this.CargarTabla();
  }

  ngOnInit() {
  }

  openDialogCancelar(row) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    // PARA PASAR LOS DATOS AL DIALOG PREGUNTA ELIMINAR
    dialogConfig.data = row;
    const dialogRef = this.dialog.open(ModalCancelarServicioComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          const index = cita.findIndex(x => x.servicio_id === row.servicio_id);
          cita.splice(index, 1);
          this.dataSource = new MatTableDataSource(cita);
        }
      }
    );
  }

  openDialog(row) {// abre una ventana modal con datos
    console.log(row);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '60%';
    // PARA PASAR LOS DATOS AL DIALOG
    dialogConfig.data = {row: row, rol: JSON.parse(localStorage.getItem('rol'))};
    const dialogRef = this.dialog.open(DetalleCitaComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD
    dialogRef.afterClosed().subscribe(
      data => {
      }
    );
  }

  // METEDO QUE OBTIENE UNA LISTA DE DOCUMENTO Y SE CARGA EN LA TABLA
  CargarTabla() {
    this.generalService.ObtenerUno('empleado/user', Number.parseInt(localStorage.getItem('id'))).then((result) => {
      this.id = result;
      this.generalService.Obtenertodos('vista_cita/abogado/' + this.id.data.id + '/estatus/P' ).then((result2) => {
        this.resultado = result2;
        cita = [];
        for (let i = 0; i < this.resultado.data.length; i++) {
          this.listaCita = {
            cliente_nombre: this.resultado.data[i].cliente_nombre,
            fecha: moment(this.resultado.data[i].fecha, ['MM-DD-YYYY']).format('DD-MM-YYYY'),
            abogado_apellido: this.resultado.data[i].abogado_apellido,
            abogado_nombre: this.resultado.data[i].abogado_nombre,
            abogado_id: this.resultado.data[i].abogado_id,
            cliente_apellido: this.resultado.data[i].cliente_apellido,
            cliente_id: this.resultado.data[i].cliente_id,
            cliente_cedula: this.resultado.data[i].cliente_cedula,
            catalogo_servicio_nombre: this.resultado.data[i].catalogo_servicio_nombre,
            catalogo_servicio_id: this.resultado.data[i].catalogo_servicio_id,
            servicio_id: this.resultado.data[i].servicio_id,
            actuacion_servicio_id: this.resultado.data[i].actuacion_servicio_id,
          };
          cita.push(this.listaCita);
        }
        this.dataSource = new MatTableDataSource(cita);
        this.dataSource.paginator = this.paginator;
      }, (err) => {
        console.log(err);
      });
    }, (err) => {
      console.log(err);
    });
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}

@Component({
  selector: 'cita-jefe',
  templateUrl: 'CitaJefe.html',
  styleUrls: ['./listacita.component.scss'],
})
export class CitaJefeComponent implements OnInit {
  resultado: any;
  listaCita: DetalleCitaData;
  displayedColumns: string[] = ['estatus',
                                'idCita',
                                'nombreCliente',
                                'nombreAbogado',
                                'fechaCita',
                                'Cancelar'];
  dataSource: MatTableDataSource<DetalleCitaData>;
  rol: any;
  id: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  value = '';
  valueInput() {
    this.value = '';
    this.applyFilter(this.value);
  }

  constructor(public dialog: MatDialog,
              private router: Router,
              public generalService: GeneralService) {
    this.CargarTabla();
  }

  ngOnInit() {
  }

  openDialogCancelar(row) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    // PARA PASAR LOS DATOS AL DIALOG PREGUNTA ELIMINAR
    dialogConfig.data = row;
    const dialogRef = this.dialog.open(ModalCancelarServicioComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          const index = cita.findIndex(x => x.servicio_id === row.servicio_id);
          cita.splice(index, 1);
          this.dataSource = new MatTableDataSource(cita);
        }
      }
    );
  }

  openDialog(row) {// abre una ventana modal con datos
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '60%';
    // PARA PASAR LOS DATOS AL DIALOG
    dialogConfig.data = {row: row, rol: JSON.parse(localStorage.getItem('rol'))};
    const dialogRef = this.dialog.open(DetalleCitaComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD
    dialogRef.afterClosed().subscribe(
      data => {
      }
    );
  }

  // METEDO QUE OBTIENE UNA LISTA DE DOCUMENTO Y SE CARGA EN LA TABLA
  CargarTabla() {
      this.generalService.Obtenertodos('vista_cita/estatus/P' ).then((result2) => {
        this.resultado = result2;
        cita = [];
        for (let i = 0; i < this.resultado.data.length; i++) {
          this.listaCita = {
            cliente_nombre: this.resultado.data[i].cliente_nombre,
            fecha: moment(this.resultado.data[i].fecha, ['MM-DD-YYYY']).format('DD-MM-YYYY'),
            abogado_apellido: this.resultado.data[i].abogado_apellido,
            abogado_nombre: this.resultado.data[i].abogado_nombre,
            abogado_id: this.resultado.data[i].abogado_id,
            cliente_apellido: this.resultado.data[i].cliente_apellido,
            cliente_id: this.resultado.data[i].cliente_id,
            cliente_cedula: this.resultado.data[i].cliente_cedula,
            catalogo_servicio_nombre: this.resultado.data[i].catalogo_servicio_nombre,
            catalogo_servicio_id: this.resultado.data[i].catalogo_servicio_id,
            servicio_id: this.resultado.data[i].servicio_id,
            actuacion_servicio_id: this.resultado.data[i].actuacion_servicio_id,
          };
          cita.push(this.listaCita);
        }
        this.dataSource = new MatTableDataSource(cita);
        this.dataSource.paginator = this.paginator;
      }, (err) => {
        console.log(err);
      });
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}

@Component({
  selector: 'app-listacita',
  templateUrl: './listacita.component.html',
  styleUrls: ['./listacita.component.scss'],

  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ListacitaComponent implements OnInit {
  resultado: any;
  listaCita: DetalleCitaData;
  displayedColumns: string[] = ['estatus', 'idCita', 'nombreCliente', 'fechaCita', 'Cancelar'];
  dataSource: MatTableDataSource<DetalleCitaData>;
  rol: any;
  mostrar: any;
  id: any;
  Abogado: boolean;
  Cliente: boolean;
  Jefe: boolean;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  value = "";
  valueInput() {
    this.value = "";
    this.applyFilter(this.value);
  }

  constructor(public dialog: MatDialog,
              private router: Router,
              public generalService: GeneralService) {
    this.CargarTabla();
    this.getRol();
    console.log(this.rol);
    console.log(JSON.parse(localStorage.getItem('rol')));
    console.log(localStorage.getItem('id'));
    this.id = localStorage.getItem('id');
    this.mostrar = true;
  }

  getRol() {
    const rol = JSON.parse(localStorage.getItem('rol'));
    if (rol.nombre === 'Abogado') {
      this.Abogado = true;
    } else if (rol.nombre === 'Cliente') {
      this.Cliente = true;
    } else if (rol.nombre === 'Jefe') {
      this.Jefe = true;
    }
  }

  inicio() {
    this.router.navigate(['DashJ']);
  }


  ngOnInit() {
  }
  openDialogCancelar(row) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    // PARA PASAR LOS DATOS AL DIALOG PREGUNTA ELIMINAR
    dialogConfig.data = row;

    const dialogRef = this.dialog.open(ModalCancelarServicioComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          const index = cita.findIndex(x => x.servicio_id === row.servicio_id);
          cita.splice(index, 1);
          this.dataSource = new MatTableDataSource(cita);
        }
      }
    );
  }

  openDialog(row) {// abre una ventana modal con datos
    const dialogConfig = new MatDialogConfig();
    this.mostrar = !this.mostrar;
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '60%';
    // PARA PASAR LOS DATOS AL DIALOG
    dialogConfig.data = row;

    const dialogRef = this.dialog.open(DetalleCitaComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD
    dialogRef.afterClosed().subscribe(
      data => {
      }
    );
    
  }

  openDialog1() { //  abre una ventana modal
    this.dialog.open(IncidenciaCitaComponent, { width: '500px' });
  }

  // METEDO QUE OBTIENE UNA LISTA DE DOCUMENTO Y SE CARGA EN LA TABLA
  CargarTabla() {
    this.generalService.Obtenertodos('vista_cita').then((result) => {
      this.resultado = result;
      cita = [];
      for (let i = 0; i < this.resultado.data.length; i++) {
        this.listaCita = {
          cliente_nombre: this.resultado.data[i].cliente_nombre,
          fecha: moment(this.resultado.data[i].fecha, ['MM-DD-YYYY']).format('DD-MM-YYYY'),
          abogado_apellido: this.resultado.data[i].abogado_apellido,
          abogado_nombre: this.resultado.data[i].abogado_nombre,
          abogado_id: this.resultado.data[i].abogado_id,
          cliente_apellido: this.resultado.data[i].cliente_apellido,
          cliente_id: this.resultado.data[i].cliente_id,
          cliente_cedula: this.resultado.data[i].cliente_cedula,
          catalogo_servicio_nombre: this.resultado.data[i].catalogo_servicio_nombre,
          catalogo_servicio_id: this.resultado.data[i].catalogo_servicio_id,
          servicio_id: this.resultado.data[i].servicio_id,
          actuacion_servicio_id: this.resultado.data[i].actuacion_servicio_id,
        };
        cita.push(this.listaCita);
      }
      this.dataSource = new MatTableDataSource(cita);
      this.dataSource.paginator = this.paginator;
    }, (err) => {
      console.log(err);
    });

  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}


