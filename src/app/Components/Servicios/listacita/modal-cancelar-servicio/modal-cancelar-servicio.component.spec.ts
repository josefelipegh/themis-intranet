import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCancelarServicioComponent } from './modal-cancelar-servicio.component';

describe('ModalCancelarServicioComponent', () => {
  let component: ModalCancelarServicioComponent;
  let fixture: ComponentFixture<ModalCancelarServicioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCancelarServicioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCancelarServicioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
