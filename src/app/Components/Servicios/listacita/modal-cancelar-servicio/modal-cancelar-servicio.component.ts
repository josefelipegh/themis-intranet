import { Component, OnInit, Inject } from '@angular/core';
import { GeneralService } from '../../../core/Services/general/general.service';
import { MatDialogRef, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import * as moment from 'moment';
import { FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-modal-cancelar-servicio',
  templateUrl: './modal-cancelar-servicio.component.html',
  styleUrls: ['./modal-cancelar-servicio.component.scss']
})
export class ModalCancelarServicioComponent implements OnInit {
  datos_servicio = {id: '',
                    fecha_cierre:moment().format('MM-DD-YYYY'),
                    estatus: 'C'};
  datos_actuacion = { actuacion_id: '',
                      fecha_plan: '',
                      horario_id: '',
                      abogado_id: '',
                      id_servicio: '',
                      estatus: 'i'};
  datos_incidencia = {cliente_id: '',
                      descripcion: '',
                      estatus: 'P',
                      tipo_incidencia_id: 4,
                      fecha_creado:  moment().format('MM-DD-YYYY'),
                      actuacion_servicio_id: ''};
  motivoFormControl = new FormControl('', [
    Validators.required
  ]);
  constructor(public dialogRef: MatDialogRef<ModalCancelarServicioComponent>,
              public generalService: GeneralService,
              public snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      // Datos del servicio
      this.datos_servicio.id = data.servicio_id;
      console.log(this.datos_servicio);
      // Datos de la actuacionde cierre
      this.datos_actuacion.abogado_id = String(data.abogado_id);
      this.datos_actuacion.actuacion_id = '30';
      this.datos_actuacion.id_servicio = String(data.servicio_id);
      this.datos_actuacion.horario_id = '44';
      this.datos_actuacion.fecha_plan = moment().format('MM-DD-YYYY');
      // Datos de la incidencia
      this.datos_incidencia.cliente_id = data.cliente_id;
      this.datos_incidencia.fecha_creado = moment().format('MM-DD-YYYY');
      this.datos_incidencia.tipo_incidencia_id = 4;
      this.datos_incidencia.descripcion = this.motivoFormControl.value;
  }

  ngOnInit() {
  }

  // CERRAR EL MODAL
  onNoClick() {
    this.dialogRef.close();
  }

  // ELIMINAR ENTIDAD LOGICAMENTE EN LA BD
  Eliminar() {
    this.datos_incidencia.descripcion = this.motivoFormControl.value;
    console.log(this.datos_actuacion);
    let resultado: any;
    // Agregar la actuacion de Fin del servicio
    this.generalService.Registrar(this.datos_actuacion, 'servicio/actuaciones').then((result) => {
      // Se añade el id de esa actuacion nueva y se le crea la incidencia de cancelacion de servicio
      resultado = result;
      this.datos_incidencia.actuacion_servicio_id = resultado.data.actuacion_id;
      console.log(result);
      this.generalService.Registrar(this.datos_incidencia, 'incidencia').then((result2) => {
        // Se actualiza el estado del servicio a cancelado
        this.generalService.Actualizar(this.datos_servicio, 'servicio', this.datos_servicio.id).then((result3) => {

          this.generalService.Registrar
          this.openSnackBar('El servicio ha sido cancelado con exito!', 'Cancelar');
          this.dialogRef.close();
        }, (err) => {
          console.log(err);
        });
      }, (err) => {
        console.log(err);
      });
    }, (err) => {
      console.log(err);
    });
  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }

}
