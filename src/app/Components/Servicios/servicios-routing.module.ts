import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgendaComponent } from './agenda/agenda.component';
import { CrearServicioComponent } from './crear-servicio/crear-servicio.component';
import { ExpedienteComponent } from './expediente/expediente.component';
import { ListaSolicitudesComponent } from './lista-solicitudes/lista-solicitudes.component';
import { ListacitaComponent } from './listacita/listacita.component';
import { ListaEvaluarSolicitudComponent } from './lista-evaluar-solicitud/lista-evaluar-solicitud.component';
import { RecaudoAbogadoComponent } from './recaudo-abogado/recaudo-abogado.component';
import { PlanificacionComponent } from './planificacion/planificacion.component';
import { CasosComponent } from './casos/casos.component';
import { CalificarComponent } from './calificar/calificar.component';
import { IncidenciaComponent } from './incidencia/incidencia.component';
import { ListaEvaluarIncidenciaComponent } from './lista-evaluar-incidencia/lista-evaluar-incidencia.component';
import { AuthGuardService as AuthGuard } from './../core/Services/AuthGuard/auth-guard.service';
import { ValorarComponent } from './valorar/valorar.component';
import { ValorarServicioComponent } from './valorar/valorar-servicio/valorar-servicio.component';

const routes: Routes = [
  { path: 'agenda', component: AgendaComponent, canActivate: [AuthGuard] },
  { path: 'crear_servicio/:cliente_id/:abogado_id/:servicio_id/:catalogo_servicio_id/:actuacion_id', component: CrearServicioComponent },
  { path: 'actuaciones', component: ExpedienteComponent, canActivate: [AuthGuard] },
  { path: 'solicitudes', component: ListaSolicitudesComponent, canActivate: [AuthGuard] },
  {path: 'citas', component: ListacitaComponent, canActivate: [AuthGuard]},
  {path: 'evaluar', component: ListaEvaluarSolicitudComponent, canActivate: [AuthGuard]},
  {path: 'recaudo_servicio', component: RecaudoAbogadoComponent, canActivate: [AuthGuard] },
  {path: 'planificar_servicio', component: PlanificacionComponent, canActivate: [AuthGuard] },
  {path: 'casos', component: CasosComponent, canActivate: [AuthGuard] },
  {path: 'calificar', component: CalificarComponent, canActivate: [AuthGuard]},
  {path: 'incidencia', component: IncidenciaComponent, canActivate: [AuthGuard]},
  {path: 'evaluar-incidencia', component: ListaEvaluarIncidenciaComponent},
  {path: 'valorar-cliente', component: ValorarComponent},
  {path: 'valoracion', component:ValorarServicioComponent}


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServiciosRoutingModule { }
