import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Router } from '@angular/router';
import { GeneralService } from '../../core/Services/general/general.service';
import { DetallecalificarComponent } from './detallecalificar/detallecalificar.component';
import * as moment from 'moment';

export interface DetalleCasosCerradosData {
  idServicio: any;
  cedulaCliente: any;
  nombreCliente:any;
  cedulaAbogado: any;
  nombreAbogado:any;
  tiposervicio:any;
  fechaInicio:any;
  estatus:any;
}




@Component({
  selector: 'app-calificar',
  templateUrl: './calificar.component.html',
  styleUrls: ['./calificar.component.scss'],

  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class CalificarComponent implements OnInit {

  displayedColumns: string[] = ['estatus','idServicio', 'cedulaCliente','nombreCliente',
  'cedulaAbogado', 'nombreAbogado',
   'tiposervicio', 'fechaInicio'];
  resultado: any; 
  datos: DetalleCasosCerradosData;
  dataSource: MatTableDataSource<DetalleCasosCerradosData>;
  casosCerrados: any = [];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  value="";
  valueInput(){
    this.value="";
    this.applyFilter(this.value);
  }


  constructor(public dialog: MatDialog, private router: Router, public generalService: GeneralService) {
    //llama para Cargar la tabla del los casos cerrados del rol Jefe
    this.CargarTablaJefe()
    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource(this.casosCerrados);
   }

   CargarTablaJefe() {
    this.generalService.Obtenertodos('vista_servicio/estatus/F').then((result) => {
      this.resultado = result;
      this.casosCerrados = [];
      for (let i = 0; i < this.resultado.data.length; i++) {
       let est = this.Estatus(this.resultado.data[i].estatus); 
        this.datos = {
          idServicio: this.resultado.data[i].servicio_id,
          cedulaCliente: this.resultado.data[i].cliente_cedula,
          nombreCliente: this.resultado.data[i].cliente_nombre,
          cedulaAbogado: this.resultado.data[i].abogado_cedula,
          nombreAbogado: this.resultado.data[i].abogado_nombre,
          tiposervicio: this.resultado.data[i].tipo_servicio,
          fechaInicio: moment(this.resultado.data[i].fecha_creado,['MM-DD-YYYY']).format('DD-MM-YYYY'),
          estatus: est,
        };
        // se le asigna al array casos todos los datos obtenidos
        this.casosCerrados.push(this.datos);
      }
      this.dataSource = new MatTableDataSource(this.casosCerrados);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, (err) => {
      console.log(err);
    });

  }
   // METODO QUE RETORNA ACTIVO O INACTIVO PARA MOSTRAR EN LA TABLA
   Estatus(est) {
    let estatus;
    return estatus = est == 'A' ? 'Activo' : 'Inactivo';
  }

   ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openDialogCalificar(){
    this.dialog.open(DetallecalificarComponent, { width: '70%', height: '90%'});
  }
  


}
