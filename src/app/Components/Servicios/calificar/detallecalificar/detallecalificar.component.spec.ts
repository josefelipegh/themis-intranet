import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallecalificarComponent } from './detallecalificar.component';

describe('DetallecalificarComponent', () => {
  let component: DetallecalificarComponent;
  let fixture: ComponentFixture<DetallecalificarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetallecalificarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallecalificarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
