import { Component, OnInit, ViewChild } from '@angular/core';
import {MatDialog, MatSort,  MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig, MatTableDataSource } from '@angular/material';
import { GeneralService } from '../../../core/Services/general/general.service';
import { Location } from '@angular/common';

// INTERFACE PARA CREAR UN OBJETO DE Act
export interface Actuacion {
  id: number;
  nombre: string;
  descripcion: string; 
  estatus: string;
}

export interface clienteobj {
  idCliente:Int32Array;
  imagenCliente:string; 
  nombreCliente: string;
  cedulaCliente:string;
}

let cliente: any[] = [{
  idCliente:1,
  imagenCliente:'assets/images/users/4.jpg',
  nombreCliente:'Juan Lopez',
  cedulaCliente:'23.456.789'
}];


let entries: any[] = [];

@Component({
  selector: 'app-detallecalificar',
  templateUrl: './detallecalificar.component.html',
  styleUrls: ['./detallecalificar.component.scss']
})
export class DetallecalificarComponent implements OnInit {

  resultado: any;
datos: Actuacion;
alternate: boolean = true;
toggle: boolean = true;
color: boolean = true;
size: number = 40;
expandEnabled: boolean = true;
entries = [];

displayedColumns: string[] = ['imagenCliente','nombreCliente', 'cedulaCliente'];
  dataSource: MatTableDataSource<clienteobj>;
  @ViewChild(MatSort) sort: MatSort;
  constructor( public generalService: GeneralService,public dialog: MatDialog, 
    private location: Location, public dialogRef: MatDialogRef<DetallecalificarComponent>) {
      // Assign the data to the data source for the table to render
     this.dataSource = new MatTableDataSource(cliente);
    this.Actuacion();
   }
  
  
   ngOnInit() {
  }

  // METEDO QUE OBTIENE UNA LISTA DE TipoActuacion Y SE CARGA EN LA TABLA, donde
   // se le hace un llamado de generalService donde esta el servicio y colocas 
   // la ultima directiva ejemplo "tipo_actuacion" que te lleva la direccion del
   // servicio en especifico, para obtener los resultados.
   Actuacion() {
    this.entries = [];
    this.generalService.Obtenertodos('actuacion').then((result) => {
      this.resultado = result;
      for (let i = 0; i < this.resultado.data.length; i++) {
       let est = this.Estatus(this.resultado.data[i].estatus); 
        this.datos = {
          id: this.resultado.data[i].id,
          nombre: this.resultado.data[i].nombre,
          descripcion: this.resultado.data[i].descripcion,
          estatus: est,
        };
        // se le asigna al array TipoActuacion todos los datos obtenidos
        this.entries.push(this.datos);
      }
    }, (err) => {
      console.log(err);
    });

  }

  // METODO QUE RETORNA ACTIVO O INACTIVO PARA MOSTRAR EN LA TABLA
  Estatus(est) {
    let estatus;
    return estatus = est == 'A' ? 'Activo' : 'Inactivo';
  }
  volver() {
    this.location.back();
  }

  removeEntry(){
    this.entries.pop();
  }

  onHeaderClick(event) {
    if (!this.expandEnabled) {
      event.stopPropagation();
    }
  }

  onDotClick(event) {
    if (!this.expandEnabled) {
      event.stopPropagation();
    }
  }

  onExpandEntry(expanded, index) {
    console.log(`Expand status of entry #${index} changed to ${expanded}`)
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  

}
