import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {FormControl} from '@angular/forms';
import {MatAutocompleteSelectedEvent, MatChipInputEvent, MatAutocomplete, MatSnackBar} from '@angular/material';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { GeneralService } from '../../core/Services/general/general.service';
import { ActuacionesService } from '../../core/Services/Actuaciones/actuaciones.service';
import { Router } from '@angular/router';

// Recaudos
export interface Recaudos {
  id: number;
  nombre: string;
}

export interface RecaudosServicio {
  id: any;
  nombre: any;
  entrega: any;
}

// Actuaciones
export interface Actuaciones {
  id: number;
  nombre: string;
}

// Abogado

export interface Abogado {
  id: any;
  nombre: string;
}

export interface TablaData {
  id: number;
  nombre: string;
}

@Component({
  selector: 'app-recaudo-abogado',
  templateUrl: './recaudo-abogado.component.html',
  styleUrls: ['./recaudo-abogado.component.scss']
})
export class RecaudoAbogadoComponent implements OnInit {

  resultado: any;
  /*lista que se autocompleta */
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  recaudo: Recaudos;
  actuacion: Actuaciones;
  abogado: Abogado;
  abogadoCtrl = new FormControl();
  filteredAbogados: Observable<string[]>;
  abogados: any[] = [];
  abogadoServicio: any[] = [];
  recaudos: any[] = [];
  recaudoServicio: RecaudosServicio;
  recaudosServicio: any[] = [];
  actuaciones: any[] = [];
  actuacionesServicio: any[] = [];
  idCatalogo;
  idEspecialidad;
  idServicio;
  opcAbogado;
  opcRecaudo;
  opcActuacion;
  DatosCita: any;
  @ViewChild('abogadoInput') abogadoInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;

  constructor(public generalService: GeneralService,
              private actserv: ActuacionesService,
              public snackBar: MatSnackBar,
              private router: Router) {
                this.DatosCita = this.actserv.getDatosCita();
                console.log(this.actserv.getServicio());
                this.CargarServicio();
                this.CargarRecaudos();
                this.CargarActuacion();
                this.CargarAbogados();
                this.CargarActuacionServicio();
                this.CargarRecaudosServicios();
                this.CargarAbogadosServicio();
  }

  ngOnInit() {
  }

  remove(abogado: string): void {
    const index = this.abogados.indexOf(abogado);

    if (index >= 0) {
      this.abogados.splice(index, 1);
    }
  }

  CargarServicio() {
    const datos = this.actserv.getServicio();
    this.idCatalogo = datos.catalogo_servicio_id;
    this.idServicio = datos.id;
  }

  CargarAbogadosServicio() {
    this.abogadoServicio = this.actserv.getDatosAbogado();
    console.log(this.abogadoServicio);
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.abogados.push(event.option.viewValue);
    this.abogadoInput.nativeElement.value = '';
    this.abogadoCtrl.setValue(null);
  }

  CargarRecaudos() {
    this.generalService.Obtenertodos('documento').then((result) => {
      this.resultado = result;
      this.recaudos = [];
      for (let i = 0; i < this.resultado.data.length; i++) {
        this.recaudo = {
          id: this.resultado.data[i].id,
          nombre: this.resultado.data[i].nombre,
        };
        this.recaudos.push(this.recaudo);

      }
      console.log(this.recaudos);
    }, (err) => {
      console.log(err);
    });

  }

  CargarRecaudosServicios() {
    this.generalService.Obtenertodos('vista_recaudo_catalogo/catalogo/' + this.idCatalogo ).then((result) => {
      this.resultado = result;
      this.recaudosServicio = [];
      for (let i = 0; i < this.resultado.data.length; i++) {
        this.recaudoServicio = {
          id: this.resultado.data[i].recaudo_id,
          nombre: this.resultado.data[i].nombre,
          entrega: false
        };
        this.recaudosServicio.push(this.recaudoServicio);

      }
      console.log(this.recaudos);
    }, (err) => {
      console.log(err);
    });

  }

  CargarActuacion() {
    this.generalService.Obtenertodos('actuacion').then((result) => {
      this.resultado = result;
      this.actuaciones = [];
      for (let i = 0; i < this.resultado.data.length; i++) {
        if (this.resultado.data[i].id !== 27 && this.resultado.data[i].id !== 30) {
          this.actuacion = {
            id: this.resultado.data[i].id,
            nombre: this.resultado.data[i].nombre,
          };
          this.actuaciones.push(this.actuacion);
        }
      }
    }, (err) => {
      console.log(err);
    });

  }

  CargarActuacionServicio() {
    this.generalService.Obtenertodos('vista_actuacion_catalogo/catalogo/' + this.idCatalogo).then((result) => {
      this.resultado = result;
      this.actuacionesServicio = [];
      for (let i = 0; i < this.resultado.data.length; i++) {
        this.actuacion = {
          id: this.resultado.data[i].actuacion_id,
          nombre: this.resultado.data[i].nombre,
        };
        this.actuacionesServicio.push(this.actuacion);
      }
    }, (err) => {
      console.log(err);
    });

  }
  CargarAbogados() {
    console.log('entra abogadoss', this.idCatalogo);
    let catalogo;
    let resultado2;
    let especialidad;
    this.generalService.ObtenerUno('catalogo_servicio', this.idCatalogo).then((result) => {
      catalogo = result;
      this.generalService.ObtenerUno('categoria', catalogo.data.categoria_id).then((result2) => {
        resultado2 = result2;
        especialidad = resultado2.data.especialidad_id;
        console.log(especialidad);
        this.generalService.Obtenertodos('vista_abogado/categoria/' + especialidad).then((result3) => {
          this.resultado = result3;
          console.log(this.resultado);
            for (let i = 0; i < this.resultado.data.length; i++) {
              if ( this.resultado.data[i].abogado_id !== this.abogadoServicio[0].id) {
                this.abogados.push(
                  {
                    id: this.resultado.data[i].abogado_id,
                    nombre: this.resultado.data[i].abogado_nombre + ' ' + this.resultado.data[i].abogadio_apellido,
                  }
                );
              }
            }
          }, (err) => {
            console.log(err);
          });
        }, (err) => {
          console.log(err);
        });
    }, (err) => {
      console.log(err);
    });
  }

  addData() {
    const datos = [];
    this.actserv.insertData(this.recaudos);
  }

  AgregarRecaudo() {
    let elemento = this.recaudos.find( x => x.id === this.opcRecaudo);
    this.recaudoServicio = {
      id: elemento.id,
      nombre: elemento.nombre,
      entrega: false
    };
    this.recaudosServicio.push(this.recaudosServicio);
  }
  EliminarRecaudo(id) {
    let index = this.recaudosServicio.findIndex(x => x.id === id);
    this.recaudosServicio.splice(index, 1);
  }

  AgregarActuacion() {
    let elemento = this.actuaciones.find( x => x.id === this.opcActuacion);
    this.actuacion = {
      id: elemento.id,
      nombre: elemento.nombre
    };
    this.actuacionesServicio.push(this.actuacion);
  }
  EliminarActuacion(id) {
    let index = this.actuacionesServicio.findIndex(x => x.id === id);
    this.actuacionesServicio.splice(index, 1);
  }

  AgregarAbogado() {
    let elemento = this.abogados.find( x => x.id === this.opcAbogado);
    this.abogado = {
      id: elemento.id,
      nombre: elemento.nombre
    };
    this.abogadoServicio.push(this.abogado);
  }
  EliminarAbogado(id) {
    let index = this.abogadoServicio.findIndex(x => x.id === id);
    this.abogadoServicio.splice(index, 1);
  }

  ProcesarConfig() {
    // pasar informacion a componente de planificacion
    this.actserv.insertData(this.actuacionesServicio);
    this.actserv.setDatosAbogado(this.abogadoServicio);
    // agregar los documentos solicitados para el servicio y los abogados adicionales
    // documentos
    for (let i = 0; i < this.recaudosServicio.length; i++) {
        const datos = {documento_id: this.recaudosServicio[i].id,
                       id_servicio: this.idServicio,
                       estatus: 'P'};
        this.generalService.Registrar(datos, 'servicio/documentos').then((result) => {
      }, (err) => {
        console.log(err);
      });
    }
    for (let i = 1; i < this.abogadoServicio.length; i++) {
      const datos = {abogado_id: this.abogadoServicio[i].id,
                     id_servicio: this.idServicio};
      this.generalService.Registrar(datos, 'servicio/abogados').then((result) => {
      }, (err) => {
        console.log(err);
      });
    }
    this.openSnackBar('Planifique su servicio', 'Aviso');
    this.goToPage('planificar_servicio');
  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }

  goToPage(pageName: string) {
    this.router.navigate([`${pageName}`]);
  }
}
