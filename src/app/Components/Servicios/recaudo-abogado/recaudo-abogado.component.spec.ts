import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecaudoAbogadoComponent } from './recaudo-abogado.component';

describe('RecaudoAbogadoComponent', () => {
  let component: RecaudoAbogadoComponent;
  let fixture: ComponentFixture<RecaudoAbogadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecaudoAbogadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecaudoAbogadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
