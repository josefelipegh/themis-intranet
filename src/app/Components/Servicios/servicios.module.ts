  import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import 'flatpickr/dist/flatpickr.css';
import { ServiciosRoutingModule } from './servicios-routing.module';
import { AgendaComponent, ModalView, CitaView } from './agenda/agenda.component';
import { CrearServicioComponent } from './crear-servicio/crear-servicio.component';
import { ExpedienteComponent, DetalleCasoComponentCliente, DetalleCasoComponentJefe, DetalleCasoComponentAbogado } from './expediente/expediente.component';
import { ListaSolicitudesComponent } from './lista-solicitudes/lista-solicitudes.component';
import { ModalSolicitudesComponent } from './lista-solicitudes/modal-solicitud/modal-solicitud.component';
import { SharedModule } from '../shared/shared.module';
import { ListaEvaluarSolicitudComponent } from './lista-evaluar-solicitud/lista-evaluar-solicitud.component';
import { ListacitaComponent, CitaAbogadoComponent, CitaClienteComponent, CitaJefeComponent } from './listacita/listacita.component';
import { RecaudoAbogadoComponent } from './recaudo-abogado/recaudo-abogado.component';
import { ModalRechazarSolicitudComponent } from './lista-evaluar-solicitud/modal-rechazar-solicitud/modal-rechazar-solicitud.component';
import { AtenderSolicitudComponent } from './lista-evaluar-solicitud/atender-solicitud/atender-solicitud.component';
import { DetalleCitaComponent } from './listacita/detalle-cita/detalle-cita.component';
import { IncidenciaCitaComponent } from './listacita/incidencia-cita/incidencia-cita.component';
import { ModalDetalleSolicitudComponent } from './lista-solicitudes/modal-detalle-solicitud/modal-detalle-solicitud.component';
import { IncidenciaComponent } from './incidencia/incidencia.component';
import { PlanificacionComponent } from './planificacion/planificacion.component';
import { CasosComponent, CasosJefeComponent, CasosAbogadoComponent, CasosClienteComponent} from './casos/casos.component';
import { ModalExpedienteComponent } from './expediente/modal-expediente/modal-expediente.component';
import { CalificarComponent } from './calificar/calificar.component';
import { DetallecalificarComponent } from './calificar/detallecalificar/detallecalificar.component';
import { MomentModule } from 'angular2-moment';
import { ModalDetalleIncidenciaComponent } from './incidencia/modal-detalle-incidencia/modal-detalle-incidencia.component';
import { ModalRegistrarIncidenciaComponent } from './incidencia/modal-registrar-incidencia/modal-registrar-incidencia.component';
import { ListaEvaluarIncidenciaComponent } from './lista-evaluar-incidencia/lista-evaluar-incidencia.component';
import { ModalEvaluarIncidenciaComponent } from './lista-evaluar-incidencia/modal-evaluar-incidencia/modal-evaluar-incidencia.component';

import { ConfirmarAprobarComponent } from './lista-evaluar-solicitud/confirmar-aprobar/confirmar-aprobar.component';
import { ModalCancelarServicioComponent } from './listacita/modal-cancelar-servicio/modal-cancelar-servicio.component';
import { ValorarComponent } from './valorar/valorar.component';
import { ValorarServicioComponent } from './valorar/valorar-servicio/valorar-servicio.component';
import { AgendarComponent } from './lista-evaluar-solicitud/agendar/agendar.component';
import { CancelarSolicitudComponent } from './lista-solicitudes/cancelar-solicitud/cancelar-solicitud.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ServiciosRoutingModule
  ],
  declarations: [
    AgendaComponent,
    CrearServicioComponent,
    ExpedienteComponent,
    ListaSolicitudesComponent,
    ListaEvaluarSolicitudComponent,
    ListacitaComponent,
    CitaAbogadoComponent,
    CitaClienteComponent,
    CitaJefeComponent,
    CasosComponent,
    RecaudoAbogadoComponent,
    CasosJefeComponent,
    CasosAbogadoComponent,
    CasosClienteComponent,
    DetalleCasoComponentCliente,
    DetalleCasoComponentJefe,
    DetalleCasoComponentAbogado,
    // modales
    ModalSolicitudesComponent,
    AtenderSolicitudComponent,
    ModalRechazarSolicitudComponent,
    DetalleCitaComponent,
    IncidenciaCitaComponent,
    ModalDetalleSolicitudComponent,
    ModalView,
    CitaView,
    IncidenciaComponent,
    PlanificacionComponent,
    ModalExpedienteComponent,
    CalificarComponent,
    DetallecalificarComponent,
    ModalDetalleIncidenciaComponent,
    ModalRegistrarIncidenciaComponent,
    ListaEvaluarIncidenciaComponent,
    ModalEvaluarIncidenciaComponent,
    ConfirmarAprobarComponent,
    ModalCancelarServicioComponent,
    AgendarComponent,
    CancelarSolicitudComponent,
    ValorarComponent,
    ValorarServicioComponent
  ],
  entryComponents: [
    ModalSolicitudesComponent,
    AtenderSolicitudComponent,
    ModalRechazarSolicitudComponent,
    ModalDetalleSolicitudComponent,
    DetalleCitaComponent,
    IncidenciaCitaComponent,
    ModalView,
    CitaView,
    ModalExpedienteComponent,
    DetallecalificarComponent,
    ModalDetalleIncidenciaComponent,
    ModalRegistrarIncidenciaComponent,
    ModalEvaluarIncidenciaComponent,
    ConfirmarAprobarComponent,
    ModalCancelarServicioComponent,
    AgendarComponent,
    CancelarSolicitudComponent,
  ]
})
export class ServiciosModule { }
