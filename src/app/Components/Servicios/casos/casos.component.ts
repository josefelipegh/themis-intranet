import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Router } from '@angular/router';
import { GeneralService } from '../../core/Services/general/general.service';
import { Location } from '@angular/common';
import * as moment from 'moment';
import { CasoService } from '../../core/Services/caso/caso.service';
import { ActuacionesService } from '../../core/Services/Actuaciones/actuaciones.service';

export interface DetalleCasosJefeData {
  idServicio: any;
  idCliente: any;
  cedulaCliente: any;
  nombreCliente:any;
  apellidoCliente: any;
  idAbogado: any;
  cedulaAbogado:any;
  nombreAbogado:any;
  apellidoAbogado:any;
  tiposervicio:any;
  fechaInicio:any;
  estatus:any;
}

export interface DetalleCasosClienteData {
  idServicio: any;
  idCliente: any;
  cedulaCliente: any;
  nombreCliente: any;
  apellidoCliente: any;
  idAbogado: any;
  cedulaAbogado:any;
  apellidoAbogado: any;
  nombreAbogado:any;
  tiposervicio: any;
  fechaInicio: any;
  estatus: any;
}

export interface DetalleCasosAbogadoData {
  idServicio: any;
  idCliente: any;
  cedulaCliente: any;
  nombreCliente: any;
  apellidoCliente: any;
  idAbogado: any;
  cedulaAbogado:any;
  nombreAbogado:any;
  tiposervicio: any;
  fechaInicio: any;
  estatus: any;
}

/*  Componente de todos los servicios activos de la organizacion, lo pude ver el rol Jefe  */

@Component ({
  selector: 'casosAJefe',
  templateUrl: './casosjefe.component.html',
  styleUrls: ['./casos.component.scss'],
})

export class CasosJefeComponent implements OnInit {

  

  //Atributos usados para cargar servicios vistos por el Jefe
  casosJefe: any = [];
  resultado: any;
  datos: DetalleCasosJefeData;
  displayedColumns: string[] = [
  'estatus',
  'idServicio', 
  'cedulaCliente', 
  'nombreCliente', 
  'cedulaAbogado', 
  'nombreAbogado',
  'tiposervicio', 
  'fechaInicio'];

  dataSource: MatTableDataSource<DetalleCasosJefeData>;


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  value= '';
  valueInput() {
    this.value = '';
    this.applyFilter(this.value);
  }


  constructor(
    public dialog: MatDialog, 
    private router: Router, 
    public generalService: GeneralService,
    public casoService: CasoService,
    private location: Location,
    private plan: ActuacionesService) {
    
    // carga la tabla de los servicios activos por rol Jefe
    this.CargarTablaJefe();
   }


   ngOnInit() {

  }

  // carga los servicios Activos, del jefe que este logueado, todos los casos de la organizacion.

    CargarTablaJefe() {
    this.generalService.Obtenertodos('vista_servicio/estatus/A').then((result) => {
      this.resultado = result;
      this.casosJefe = [];
      for (let i = 0; i < this.resultado.data.length; i++) {
       const est = this.Estatus(this.resultado.data[i].estatus);
        this.datos = {
          idServicio: this.resultado.data[i].servicio_id,
          idCliente: this.resultado.data[i].cliente_id,
          cedulaCliente: this.resultado.data[i].cliente_cedula,
          nombreCliente: this.resultado.data[i].cliente_nombre,
          apellidoCliente: this.resultado.data[i].cliente_nombre+' '+this.resultado.data[i].cliente_apellido,
          idAbogado: this.resultado.data[i].abogado_id,
          cedulaAbogado: this.resultado.data[i].abogado_cedula,
          nombreAbogado: this.resultado.data[i].abogado_nombre,
          apellidoAbogado: this.resultado.data[i].abogado_nombre+' '+this.resultado.data[i].abogado_apellido,
          tiposervicio: this.resultado.data[i].tipo_servicio,
          fechaInicio: moment(this.resultado.data[i].fecha_creado, ['MM-DD-YYYY']).format('DD-MM-YYYY'),
          estatus: est,
        };
        // se le asigna al array casos todos los datos obtenidos
        this.casosJefe.push(this.datos);
      }
      this.dataSource = new MatTableDataSource(this.casosJefe);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, (err) => {
      console.log(err);
    });

  }
   // METODO QUE RETORNA ACTIVO O INACTIVO PARA MOSTRAR EN LA TABLA
   Estatus(est) {
    let estatus;
    return estatus = est === 'A' ? 'Activo' : 'Inactivo';
  }


  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  iraPagina(pageName, row) {
    const datosServicio = {
      idabogado: row.idAbogado,
      cedulaabogado: row.cedulaAbogado,
      nombreabogado: row.apellidoAbogado,
      idcliente: row.idCliente,
      cedulacliente: row.cedulaCliente,
      nombrecliente: row.apellidoCliente,
      id: row.idServicio,
    };
    console.log(datosServicio);
    const datosAbogado = [{id: row.idAbogado, nombre: row.apellidoAbogado}];
    this.casoService.setServicio(datosServicio);
    this.plan.setDatosAbogado(datosAbogado);
    this.plan.setServicio(datosServicio);
    this.router.navigate([`${pageName}`]);
  }

}

/* Componente de los abogados asignados a los servicios activos */

@Component ({
  selector: 'casosAAbogados',
  templateUrl: './casosabogado.component.html',
  styleUrls: ['./casos.component.scss'],

  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],

})

export class CasosAbogadoComponent implements OnInit {

  

  //Atributos usados para cargar servicios vistos por el Abogado
  casosAbogado: any = [];
  resultadoAbogado: any;
  datosabogado: DetalleCasosAbogadoData;
  idAbogado: any;

  displayedColumns: string[] = [
  'estatus',
  'idServicio', 
  'cedulaCliente', 
  'nombreCliente',
  'tiposervicio', 
  'fechaInicio'];

  dataSource: MatTableDataSource<DetalleCasosAbogadoData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  value = '';
  valueInput() {
    this.value = '';
    this.applyFilter(this.value);
  }

  constructor(
    public dialog: MatDialog, private router: Router,
    public generalService: GeneralService,
    private location: Location,
    public casoService: CasoService,
    private plan: ActuacionesService) {
    // carga la tabla de los servicios activos por rol Abogado
    this.CargarTablaAbogado();
    }

  // Carga la tabla de servicios activos, unicamente del abogado que esta logueado

  CargarTablaAbogado() {
    let idusuario = Number.parseInt(localStorage.getItem('id'));
    this.generalService.ObtenerUno('empleado/user',idusuario).then((result) => {
    this.idAbogado = result;
    this.generalService.Obtenertodos('vista_servicio_abogado/abogado/'+this.idAbogado.data.id+'/estatus/A').then((result) => {
      this.resultadoAbogado = result;
      this.casosAbogado = [];
      for (let i = 0; i < this.resultadoAbogado.data.length; i++) {
       const est = this.Estatus(this.resultadoAbogado.data[i].estatus);
        this.datosabogado = {
          idServicio: this.resultadoAbogado.data[i].servicio_id,
          idCliente: this.resultadoAbogado.data[i].cliente_id,
          cedulaCliente: this.resultadoAbogado.data[i].cliente_cedula,
          nombreCliente: this.resultadoAbogado.data[i].cliente_nombre,
          apellidoCliente: this.resultadoAbogado.data[i].cliente_nombre+' '+this.resultadoAbogado.data[i].cliente_apellido,
          idAbogado: this.resultadoAbogado.data[i].abogado_id,
          cedulaAbogado: this.resultadoAbogado.data[i].abogado_cedula,
          nombreAbogado: this.resultadoAbogado.data[i].abogado_nombre,
          tiposervicio: this.resultadoAbogado.data[i].tipo_servicio,
          fechaInicio: moment(this.resultadoAbogado.data[i].fecha_creado, ['MM-DD-YYYY']).format('DD-MM-YYYY'),
          estatus: est,
        };
        // se le asigna al array casos todos los datos obtenidos
        this.casosAbogado.push(this.datosabogado);
      }
      this.dataSource = new MatTableDataSource(this.casosAbogado);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, (err) => {
      console.log(err);
    });
  }, (err) => {
    console.log(err);
  });

  }

  ngOnInit() {
  }

  // METODO QUE RETORNA ACTIVO O INACTIVO PARA MOSTRAR EN LA TABLA
  Estatus(est) {
    let estatus;
    return estatus = est === 'A' ? 'Activo' : 'Inactivo';
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  iraPagina(pageName, row) {
    const datosServicio = {
      idabogado: row.idAbogado,
      cedulaabogado: row.cedulaAbogado,
      nombreabogado: row.apellidoAbogado,
      idcliente: row.idCliente,
      cedulacliente: row.cedulaCliente,
      nombrecliente: row.apellidoCliente,
      id: row.idServicio,
    };
    console.log(datosServicio);
    const datosAbogado = [{id: row.idAbogado, nombre: row.apellidoAbogado}];
    this.casoService.setServicio(datosServicio);
    this.plan.setDatosAbogado(datosAbogado);
    this.plan.setServicio(datosServicio);
    this.router.navigate([`${pageName}`]);
  }



}

/* Componente del cliente para sus servicios activos */
@Component ({
  selector: 'casosACliente',
  templateUrl: './casoscliente.component.html',
  styleUrls: ['./casos.component.scss'],

  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],

})

export class CasosClienteComponent implements OnInit {

  // Atributos usados para cargar servicios vistos por el Cliente
  casosCliente: any = [];
  resultadoCliente: any;
  datoscliente: DetalleCasosClienteData;
  idcliente: any;

  displayedColumns: string[] = [
  'estatus',
  'idServicio', 
  'cedulaAbogado', 
  'nombreAbogado',
  'tiposervicio', 
  'fechaInicio'];

  dataSource: MatTableDataSource<DetalleCasosClienteData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  value = '';
  valueInput() {
    this.value = '';
    this.applyFilter(this.value);
  }

  constructor(
    public dialog: MatDialog, 
    private router: Router,
    public generalService: GeneralService,
    private location: Location,
    public casoService: CasoService,
    private plan: ActuacionesService) {
    // carga la tabla de los servicios activos por rol cliente
    this.CargarTablaCliente();
    }

    // Carga la tabla de casos Activos, unicamente del cliente que esta logueado.
  CargarTablaCliente() {
    let idusuario = Number.parseInt(localStorage.getItem('id'));
    this.generalService.ObtenerUno('cliente/usuario',idusuario).then((result) => {
      this.idcliente = result;
      this.generalService.Obtenertodos('vista_servicio/cliente/'+this.idcliente.data.id+'/estatus/A').then((result) => {
        this.resultadoCliente = result;
        this.casosCliente = [];
        for (let i = 0; i < this.resultadoCliente.data.length; i++) {
         const est = this.Estatus(this.resultadoCliente.data[i].estatus); 
          this.datoscliente = {
            idServicio: this.resultadoCliente.data[i].servicio_id,
            idAbogado: this.resultadoCliente.data[i].abogado_id,
            cedulaAbogado: this.resultadoCliente.data[i].abogado_cedula,
            nombreAbogado: this.resultadoCliente.data[i].abogado_nombre,
            apellidoAbogado: this.resultadoCliente.data[i].abogado_nombre+' '+this.resultadoCliente.data[i].abogado_apellido,
            idCliente: this.resultadoCliente.data[i].cliente_id,
            cedulaCliente: this.resultadoCliente.data[i].cliente_cedula,
            nombreCliente: this.resultadoCliente.data[i].cliente_nombre,
            apellidoCliente: this.resultadoCliente.data[i].cliente_apellido,
            tiposervicio: this.resultadoCliente.data[i].tipo_servicio,
            fechaInicio: moment(this.resultadoCliente.data[i].fecha_creado,['MM-DD-YYYY']).format('DD-MM-YYYY'),
            estatus: est,
          };
          // se le asigna al array casos todos los datos obtenidos
          this.casosCliente.push(this.datoscliente);
        }
        this.dataSource = new MatTableDataSource(this.casosCliente);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }, (err) => {
        console.log(err);
      });
    }, (err) => {
      console.log(err);
    });

  }

  ngOnInit() {
  }

  // METODO QUE RETORNA ACTIVO O INACTIVO PARA MOSTRAR EN LA TABLA
  Estatus(est) {
    let estatus;
    return estatus = est === 'A' ? 'Activo' : 'Inactivo';
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  iraPagina(pageName, row) {
    const datosServicio = {
      idabogado: row.idAbogado,
      cedulaabogado: row.cedulaAbogado,
      nombreabogado: row.apellidoAbogado,
      idcliente: row.idCliente,
      cedulacliente: row.cedulaCliente,
      nombrecliente: row.apellidoCliente,
      id: row.idServicio,
    };
    console.log(datosServicio);
    const datosAbogado = [{id: row.idAbogado, nombre: row.apellidoAbogado}];
    this.casoService.setServicio(datosServicio);
    this.plan.setDatosAbogado(datosAbogado);
    this.plan.setServicio(datosServicio);
    this.router.navigate([`${pageName}`]);
  }


}

/* Componente padre */

@Component({
  selector: 'app-casos',
  templateUrl: './casos.component.html',
  styleUrls: ['./casos.component.scss'],

  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})


export class CasosComponent implements OnInit {
  Abogado= false;
  Cliente= false;
  Jefe= false;

  constructor(public dialog: MatDialog,
     private router: Router, 
     public generalService: GeneralService,
    private location: Location) {

    this.getRol();
   }


   getRol() {
    const rol = JSON.parse(localStorage.getItem('rol'));
    if (rol.nombre === 'Abogado') {
      this.Abogado = true;
    } else if (rol.nombre === 'Cliente') {
      this.Cliente = true;
    } else if (rol.nombre === 'Jefe') {
      this.Jefe = true;
    }
  }

   ngOnInit() {}

}



