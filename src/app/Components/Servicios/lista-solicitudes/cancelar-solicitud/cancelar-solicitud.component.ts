import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatDialog, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { GeneralService } from '../../../core/Services/general/general.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cancelar-solicitud',
  templateUrl: './cancelar-solicitud.component.html',
  styleUrls: ['./cancelar-solicitud.component.scss']
})
export class CancelarSolicitudComponent implements OnInit {

  form = { estatus: '' };

  public loading = false;
  id;

  constructor(public dialog: MatDialog, public snackBar: MatSnackBar, public generalService: GeneralService,
    public dialogRef: MatDialogRef<CancelarSolicitudComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router) {

    this.id = data.idSolicitud;
console.log(this.id);
  }

  ngOnInit() {
  }

  CancelarServicio() {


    this.loading = true;

    this.form.estatus = 'I';
    console.log(this.id);
    this.generalService.Actualizar(this.form, 'solicitud', this.id).then((result) => {

      this.openSnackBar('Solicitud Cancelada exitosamente!', "Cancelación exitosa");
    }, (err) => {
      console.log(err);
      this.loading = false;
    });

    this.dialogRef.close(this.form);
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }

  onNoClick() {
    this.dialogRef.close();
  }
}
