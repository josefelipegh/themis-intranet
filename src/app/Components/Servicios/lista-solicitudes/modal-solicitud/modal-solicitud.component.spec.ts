import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSolicitudesComponent } from './modal-solicitud.component';

describe('ModalSolicitudesComponent', () => {
  let component: ModalSolicitudesComponent;
  let fixture: ComponentFixture<ModalSolicitudesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalSolicitudesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSolicitudesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
