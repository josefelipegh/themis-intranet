import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { MatDialogRef, MatSnackBar } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { GeneralService } from '../../../core/Services/general/general.service';
import * as moment from 'moment';

@Component({
  selector: 'app-modal-solicitud',
  templateUrl: './modal-solicitud.component.html',
  styleUrls: ['./modal-solicitud.component.scss'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'es-VE' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ],
})
export class ModalSolicitudesComponent implements OnInit {

  form_descripcion = { cliente_id: '', fecha_creacion: '', descripcion: '', estatus: 'P', catalogo_servicio_id: '' };
  fecha = new Date();
  date;
  idCatalogo;
  public loading = false;
  disableServ = true;

  resultado: any;
  listaCategoria: any[] = [];
  idCat;
  listaCatalogo: any[] = [];
  cliente;

  constructor(public dialogRef: MatDialogRef<ModalSolicitudesComponent>, public generalService: GeneralService,
    public snackBar: MatSnackBar, private router: Router) {
    this.CargarCombo();
    this.date = (this.fecha.getDate() + '/' + this.fecha.getMonth() + '/' + this.fecha.getFullYear());
    console.log(this.date);
  }

  ngOnInit() {
  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }

  categoriaFormControl = new FormControl('',
    [
      Validators.required
    ]);

  descripcionFormControl = new FormControl('', [
    Validators.required
  ]);

  tipoServFormControl = new FormControl('', [
    Validators.required
  ]);


  // OBTIENE EL ID DE LA CATEGORIA DEL COMBO
  ObtenerIdCat(id) {
    console.log(id);
    this.ComboCatalogo(id);
    return this.idCat = id;

  }
  ObtenerIdCatalogo(id) {
    console.log(id);
    return this.idCatalogo = id;

  }

  CargarCombo() {
    this.generalService.Obtenertodos('categoria').then((result) => {

      this.resultado = result;
      console.log(result);
      for (let i = 0; i < this.resultado.data.length; i++) {
        this.listaCategoria.push(this.resultado.data[i]);
      }
      console.log(this.listaCategoria);
    }, (err) => {
      console.log(err);
    });
  }

  ComboCatalogo(id) {
    this.listaCatalogo = [];
    let index = this.listaCategoria.findIndex(x => x.id == id);
    let i = 0;
    while (i < this.listaCategoria[index].catalogo_servicio.length) {
      if(this.listaCategoria[index].catalogo_servicio[i].estatus == 'A'){
        this.listaCatalogo.push(this.listaCategoria[index].catalogo_servicio[i]);
      }      
      i++;
    }
    console.log(this.listaCatalogo);
  }

  Registrar() {
    this.loading = true;
    this.generalService.ObtenerUno('cliente/usuario',Number.parseInt(localStorage.getItem('id'))).then((result) => {
      this.cliente = result;
      console.log(this.cliente);
      console.log(moment().format('MM-dd-yyyy')); 
      this.form_descripcion.fecha_creacion = moment().format('MM-dd-yyyy');
      this.form_descripcion.descripcion = this.descripcionFormControl.value;
      this.form_descripcion.catalogo_servicio_id = this.idCatalogo;

      this.form_descripcion.cliente_id = this.cliente.data.id;
      if (this.categoriaFormControl.valid && this.tipoServFormControl.valid && this.descripcionFormControl.valid) {
  
        
        this.generalService.Registrar(this.form_descripcion, 'solicitud').then((result) => {
          this.openSnackBar('Solicitud registrada exitosamente!', "Registro exitoso");
          this.loading = false;
          this.dialogRef.close(this.form_descripcion);
        }, (err) => {
          console.log(err);
          this.loading = false;
        });
      }
    }, (err) => {
      console.log(err);
    });

  }

  onNoClick(){
    this.dialogRef.close();
  }

  habilitarServicios(selCat){
    if(selCat != ''){
    this.disableServ = false;}
  }
}

