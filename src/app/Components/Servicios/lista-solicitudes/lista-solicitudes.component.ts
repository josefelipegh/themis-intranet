import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ModalSolicitudesComponent } from './modal-solicitud/modal-solicitud.component';
import { ModalDetalleSolicitudComponent } from './modal-detalle-solicitud/modal-detalle-solicitud.component';
import { CancelarSolicitudComponent } from './cancelar-solicitud/cancelar-solicitud.component';
import { Router } from '@angular/router';
import { GeneralService } from '../../core/Services/general/general.service';

export interface SolicitudData {

  idSolicitud: string;
  tipoServicio: string;
  descripcion: string;
  estatus: string;
  fecha: string;
  ver: string;
  color: string;
  cancelar: boolean;
}

let solicitud: any[] = [];


@Component({
  selector: 'app-lista-solicitudes',
  templateUrl: './lista-solicitudes.component.html',
  styleUrls: ['./lista-solicitudes.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ListaSolicitudesComponent implements OnInit {
  resultado: any;
  resultado2: any;
  displayedColumns: string[] = ['ver', 'tipoServicio', 'fecha', 'estatus', 'anular'];
  dataSource: MatTableDataSource<SolicitudData>;
  datos: SolicitudData;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  value = "";
  valueInput() {
    this.value = "";
    this.applyFilter(this.value);
  }

  constructor(public generalService: GeneralService, public dialog: MatDialog, private router: Router) {
    this.CargarTabla();
    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource(solicitud);
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  openDialog() // abre una ventana modal
  {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';

    const dialogRef = this.dialog.open(ModalSolicitudesComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.CargarTabla()
        }
      }
    );
  }

  openDialogActualizar(row) {// abre una ventana modal
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    // PARA PASAR LOS DATOS AL DIALOG
    dialogConfig.data = row;

    this.dialog.open(ModalDetalleSolicitudComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD

  }

  openDialogCancelar(row) {// abre una ventana modal
    const dialogConfig2 = new MatDialogConfig();

    dialogConfig2.disableClose = true;
    dialogConfig2.autoFocus = true;
    dialogConfig2.width = '50%px';
    // PARA PASAR LOS DATOS AL DIALOG
    dialogConfig2.data = row;

    const dialogRef2 = this.dialog.open(CancelarSolicitudComponent, dialogConfig2);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD
    dialogRef2.afterClosed().subscribe(
      data => {
        if (data) {
          this.CargarTabla()
        }
      }
    );

  }

  Estatus(est) {
    let estatus;
    if (est == 'A') {
      estatus = 'Aprobado';
    } else if (est == 'R') {
      estatus = 'Rechazado';
    } else if (est == 'P') {
      estatus = 'Pendiente';
    }
    return estatus;
  }

  CargarTabla() {

    let idCliente;
    
    this.generalService.ObtenerUno('cliente/usuario', Number.parseInt(localStorage.getItem('id'))).then((result) => {
      idCliente = result;
      
      this.generalService.Obtenertodos('vista_solicitud/cliente/' + idCliente.data.id).then((result2) => {
        this.resultado = result2;
        solicitud = [];
        


        for (let i = 0; i < this.resultado.data.length; i++) {

          if (this.resultado.data[i].estatus != 'I' && this.resultado.data[i].estatus != 'G') {

            let est = this.Estatus(this.resultado.data[i].estatus);
            let colorEst = this.resultado.data[i].estatus;
            let color;
            let ver;
            let cancelar;

            switch (colorEst) {
              case 'A':
                color = 'green';
                ver = 'check_circle';
                cancelar = false;
                break;
              case 'R':
                color = 'red';
                ver = 'cancel';
                cancelar = false;
                break;
              case 'P':
                color = '#cd9d12';
                ver = 'error';
                cancelar = true;
                break;
            }

            this.datos = {
              idSolicitud: this.resultado.data[i].solicitud_id,
              tipoServicio: this.resultado.data[i].servicio_nombre,
              descripcion: this.resultado.data[i].descripcion,
              fecha: this.resultado.data[i].fecha,
              estatus: est,
              ver: ver,
              color: color,
              cancelar: cancelar,
            };

            solicitud.push(this.datos);
          }
        }
        this.dataSource = new MatTableDataSource(solicitud);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

      }, (err) => {
        console.log(err);
      });
    }, (err) => {
      console.log(err);

    });


  }
  inicio() {
    this.router.navigate(['DashJ']);
  }



  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}


