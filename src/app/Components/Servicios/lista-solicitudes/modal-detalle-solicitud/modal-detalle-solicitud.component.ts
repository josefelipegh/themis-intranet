import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


@Component({
  selector: 'app-modal-detalle-solicitud',
  templateUrl: './modal-detalle-solicitud.component.html',
  styleUrls: ['./modal-detalle-solicitud.component.scss']
})
export class ModalDetalleSolicitudComponent implements OnInit {

  form = { fecha: '', servicio: '',  descripcion: ''};

  constructor(public dialogRef: MatDialogRef<ModalDetalleSolicitudComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { 
    this.form.fecha = data.fecha;
    this.form.servicio = data.tipoServicio;
    this.form.descripcion = data.descripcion;
  }

  ngOnInit() {
  }

  onNoClick(){
    this.dialogRef.close();
  }
}
