import {
  Component,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef,
  Inject,
  OnDestroy,
} from '@angular/core';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours
} from 'date-fns';
import { Subject } from 'rxjs';
import { NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView,
  CalendarDayViewComponent,
  CalendarWeekViewComponent
} from 'angular-calendar';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ObservableMedia, MediaChange } from '@angular/flex-layout';
import {Subscription} from 'rxjs';
import { GeneralService } from '../../core/Services/general/general.service';


const colors: any = {
  red: {
    primary: '#bf4959',
    secondary: '#d4838e'
  },
  blue: {
    primary: '#fec10b',
    secondary: '#fecb32'
  },
  yellow: {
    primary: '#cd9d12',
    secondary: '#eec140'
  }
};
export interface Empleados {
  id: any;
  nombre: any;
}
let abogados: any[] = [];


export interface Clientes {
  id: any;
  nombre: any;
}
export interface Servicios {
  id: any;
  nombre: any;
}
export interface calendarconfig{
  StartHour: number;
  EndHour: number;
  // StartDay: Date;
  // EndDay: Date;
}
@Component({
  selector: 'app-agenda',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.scss']
})



export class AgendaComponent implements OnDestroy{
  resultado: any;
  abogado: Empleados;
  opcionAbogado: string = '0';
  Clientes: Clientes[] = [
    {
      id: '1',
      nombre: 'Alejandro Alvarez'
    },
    {
      id: '2',
      nombre: 'Gustavo Pérez'
    }
  ];

  model: NgbDateStruct;
  date: {year: number, month: number};

  view: CalendarView = CalendarView.Week;

  CalendarView = CalendarView;

  hora: string;
  minuto: string;
  tiempo: string;

  StartHour= 7;
  EndHour= 18;

  dt: Date = new Date();

  viewDate: Date = new Date();

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-fw fa-pencil"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      }
    },
    {
      label: '<i class="fa fa-fw fa-times"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
        this.handleEvent('Deleted', event);
      }
    }
  ];

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  refresh: Subject<any> = new Subject();

  events: CalendarEvent[] = [
    {
      start: addHours(startOfDay('01/09/2019'), 10),
      end: addHours(startOfDay('01/09/2018'), 11),
      title: 'Cita Juan Lopez',
      color: {
        primary: '#bf4959',
        secondary: '#d4838e'
      }
    }
  ];
  locale: string = 'es';

  activeDayIsOpen: boolean = true;
  activeMediaQuery = '';

  watcher: Subscription;
  constructor(public modal: MatDialog,
     private calendar: NgbCalendar,
     public media: ObservableMedia,
     public generalService: GeneralService) {
      this.watcher = media.subscribe((change: MediaChange) => {
        this.activeMediaQuery = change ? `'${change.mqAlias}' = (${change.mediaQuery})` : '';
        if ( change.mqAlias === 'md') {
           this.view = CalendarView.Day;
        }
        console.log(this.activeMediaQuery);
      });
      this.CargarAbogados();
      console.log(this.events);
     }

  selectToday() {
    this.model = this.calendar.getToday();
  }
  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      this.viewDate = date;
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
    }
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.handleEvent('Dropped or resized', event);
    this.refresh.next();
  }

  // dayView() {
  //   this.media.asObservable()
  //     .subscribe((change: MediaChange) => {
  //       this.state = change ? `'${change.mqAlias}' = (${change.mediaQuery})` : '';
  //   });
  //   console.log(this.state);
  //   if (this.media.asObservable().subscribe('sm')) {
  //     this.view = this.CalendarView.Day;
  //   }
  // }

  cal_hora(fecha: Date) {
    if (fecha.getHours() < 10) {
      this.hora = '0' + fecha.getHours().toString();
      this.tiempo = 'a.m';
    } else if (fecha.getHours() > 12) {
      this.hora = '0' + (fecha.getHours() - 12).toString();
      this.tiempo = 'p.m';
    } else {
      this.hora = fecha.getHours().toString();
    }
    if (fecha.getMinutes() < 10) {
      this.minuto = '0' + fecha.getMinutes().toString();
    } else {
      this.minuto = fecha.getMinutes().toString();
    }
    return this.hora + ':' + this.minuto;
  }
  n_cita(fecha: Date): void {
    this.modal.open(CitaView, {
      width: '65%',
      data: {
        fecha: fecha.getDate() + '/' + (fecha.getMonth() + 1) + '/' + fecha.getFullYear(),
        hora: this.cal_hora(fecha),
        tiempo: this.tiempo
      }
    });
  }
  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };
    this.modal.open(ModalView, {
      width: '50%',
      data: {
        action: action,
        event: event,
      }
    });
  }
  addEvent(): void {
    this.events.push({
      title: 'New event',
      start: startOfDay(new Date()),
      end: endOfDay(new Date()),
      color: colors.red,
      draggable: true,
      resizable: {
        beforeStart: true,
        afterEnd: true
      }
    });
    this.refresh.next();
  }

  CargarAbogados() {
    this.generalService.Obtenertodos('empleado').then((result) => {
      this.resultado = result;
      abogados = [];
      for (let i = 0; i < this.resultado.data.length; i++) {
        this.abogado = {
          id: this.resultado.data[i].id,
          nombre: this.resultado.data[i].nombre,
        };
        abogados.push(this.abogado);
      }
    }, (err) => {
      console.log(err);
    });

  }

  select_abogado() {
    console.log(this.opcionAbogado.toString());
   /* this.generalService.Obtenertodos('empleado').then((result) => {
      this.resultado = result;
      abogados = [];
      for (let i = 0; i < this.resultado.data.length; i++) {
        this.abogado = {
          id: this.resultado.data[i].id,
          nombre: this.resultado.data[i].nombre,
        };
        abogados.push(this.abogado);
      }
    }, (err) => {
      console.log(err);
    });*/
  }

  ngOnDestroy() {
    this.watcher.unsubscribe();
  }
}
@Component({
  templateUrl: 'cita.html'
})
export class CitaView {
  constructor(
    public dialogRef: MatDialogRef<CitaView>,
    @Inject(MAT_DIALOG_DATA) public data: any) {}
}

@Component({
  templateUrl: 'modal.html'
})
export class ModalView {
  constructor(
    public dialogRef: MatDialogRef<ModalView>,
    @Inject(MAT_DIALOG_DATA) public data: any) {}

    afuConfig = {
      uploadAPI: {
        url:"https://example-file-upload-api"
      },
      uploadBtnText: 'Subir',
      attachPinText: 'Seleccionar archivo',
      resetBtnText: 'Reiniciar',
      hideProgressBar: true,
  };
}


