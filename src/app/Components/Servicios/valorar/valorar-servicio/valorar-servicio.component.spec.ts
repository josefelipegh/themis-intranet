import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValorarServicioComponent } from './valorar-servicio.component';

describe('ValorarServicioComponent', () => {
  let component: ValorarServicioComponent;
  let fixture: ComponentFixture<ValorarServicioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValorarServicioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValorarServicioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
