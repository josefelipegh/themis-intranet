import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { GeneralService } from '../../../core/Services/general/general.service';
import { ValorarService } from '../../../core/Services/valoracion/valorar.service';
import * as moment from 'moment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-valorar-servicio',
  templateUrl: './valorar-servicio.component.html',
  styleUrls: ['./valorar-servicio.component.scss']
})
export class ValorarServicioComponent implements OnInit {
  resultado: any;
  resultado2: any;
  resultado3: any;
  listaPregunta: any[] = [];
  id_tipo_servicio: any;
  form_valoracion = { servicio_id: '', tipo_valoracion_id: '', rango_valoracion_id: '', fecha: moment().format('MM-DD-YYYY'), estatus: 'A' };
  ArregloValoracion: any[] = [];
  listaRespuesta: any[] = [];
  verInput: boolean = false;
  public loading = false;
  public listaResp: any[] = [];


  respuestas: any[] = [];
  constructor(public generalService: GeneralService,
    private router: Router,
    private valorarService: ValorarService, public snackBar: MatSnackBar) {
    this.ObtnerDatos();
    this.cargarValorar();
    //this.cargarValorar2()

  }

  ngOnInit() {
  }


  cargarValorar() {
    this.listaPregunta = [];
    let result1;
    this.generalService.ObtenerUno('catalogo_servicio', this.id_tipo_servicio).then((result) => {
      result1 = result;
        this.listaPregunta = result1.data.valoraciones;
   

      // for (let i = 0; i < result1.data.length; i++) {
      //   if (!this.listaPregunta.find(x => x.tipo_valoracion_id === result1.data[i].tipo_valoracion_id)) {
      //     this.generalService.Obtenertodos('vista_valoracion/' + this.id_tipo_servicio + '/' + result1.data[i].tipo_valoracion_id).then((result) => {
      //       result2 = result;
      //       this.listaResp = [];
      //       for (let j = 0; j < result2.data.length; j++) {
      //         this.listaResp.push({
      //           rango_valoracion_id: result2.data[j].rango_valoracion_id,
      //           rango_descripcion: result2.data[j].rango_descripcion
      //         });

      //       }
      //       console.log('hola', this.listaResp);
      //       localStorage.setItem('respuestas', JSON.stringify(this.listaResp));
          
      //       // localStorage.setItem('respuestas', JSON.stringify(this.listaResp));
      //     }, (err) => {
      //       console.log(err);
      //     });


      //   }
      //   this.listaPregunta.push({
      //     tipo_valoracion_id: result1.data[i].tipo_valoracion_id,
      //     tipo_valoracion: result1.data[i].tipo_valoracion,
      //     respuestas: localStorage.getItem('respuestas')
      //   });
      // }
      console.log(this.listaPregunta);
      //console.log(this.listaRespuesta);
    }, (err) => {
      console.log(err);
    });
  }
  // cargarValorar2() {
  //   this.listaPregunta = [];
  //   let result1;
  //   this.generalService.ObtenerUno('vista_valoracion/tipo_servicio', this.id_tipo_servicio).then((result) => {
  //     result1 = result;

  //     for (let i = 0; i < result1.data.length; i++) {
  //       if (!this.listaPregunta.find(x => x.tipo_valoracion_id === result1.data[i].tipo_valoracion_id)) {
  //         this.listaPregunta.push({
  //           tipo_valoracion_id: result1.data[i].tipo_valoracion_id,
  //           tipo_valoracion: result1.data[i].tipo_valoracion,
  //           respuestas: ''
  //         });
  //         this.cargarResp(result1.data[i]);
  //       }
  //     }
  //     console.log(this.listaPregunta);
  //   }, (err) => {
  //     console.log(err);
  //   });

  // }
  cargarResp(pregunta) {
        let result2;
    this.generalService.Obtenertodos('vista_valoracion/' + this.id_tipo_servicio + '/' + pregunta.tipo_valoracion_id).then((result) => {
      result2 = result;
      this.listaResp = [];
      for (let j = 0; j < result2.data.length; j++) {
        this.listaResp.push({
          rango_valoracion_id: result2.data[j].rango_valoracion_id,
          rango_descripcion: result2.data[j].rango_descripcion
        });
      }
      console.log(this.listaResp);
    }, (err) => {
      console.log(err);
    });
    pregunta.respuestas = 'this.listaResp';
    console.log('pregunta', this.listaPregunta);
  }


  Registrar() {

    for (let i = 0; i < this.ArregloValoracion.length; i++) {
      this.loading = true;
      this.form_valoracion.tipo_valoracion_id = this.ArregloValoracion[i].tipo_valoracion_id;
      this.form_valoracion.rango_valoracion_id = this.ArregloValoracion[i].rango_valoracion_id;

      console.log(this.form_valoracion);
      this.generalService.Registrar(this.form_valoracion, 'valoracion').then((result) => {
        this.loading = false;
      }, (err) => {
        this.loading = false;
        console.log(err);
      });
    }
    this.loading = true;
    this.generalService.Actualizar({ estatus: 'V' }, 'servicio', this.form_valoracion.servicio_id).then((result) => {
      this.loading = false;
      this.openSnackBar('Servicio Valorado!', "Valorar");
      this.router.navigate([`${'valorar-cliente'}`]);
    }, (err) => {
      console.log(err);
      this.loading = false;
    });
  }

  obtIdRespuesta(idValoracion: number, idRespuesta: number) {
    let index = this.ArregloValoracion.findIndex(x => x.tipo_valoracion_id == idValoracion);
    console.log(index)
    if (index != -1) {
      this.ArregloValoracion.splice(index, 1, {
        tipo_valoracion_id: idValoracion,
        rango_valoracion_id: idRespuesta
      });
    }
    else {
      this.ArregloValoracion.push(
        {
          tipo_valoracion_id: idValoracion,
          rango_valoracion_id: idRespuesta
        });
    }
    console.log(this.ArregloValoracion);

  }

  //Método que se utiliza para llenar el ID del servicio en el arreglo form_valoracion (que se usa para incluir una valoracion en la bd)
  ObtnerDatos() {
    const dato = this.valorarService.getData();
    this.form_valoracion.servicio_id = dato.serv;
    this.id_tipo_servicio = dato.tipos;
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }
}
