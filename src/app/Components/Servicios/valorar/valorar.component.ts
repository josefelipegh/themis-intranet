import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material'
import { ValorarServicioComponent } from './valorar-servicio/valorar-servicio.component';
import { Router } from '@angular/router';
import { GeneralService } from '../../core/Services/general/general.service';
import { ValorarService } from '../../core/Services/valoracion/valorar.service';
import * as moment from 'moment';


export interface TablaData{
  id : number;
  abogado_nombre: string;
  abogado_id: number;
  tipo_servicio: number;
  servicio_id: number;
  fecha_creado: string;
  tipo_servicio_id: number;
 

}


let valoraciones: any[] = [];
@Component({
  selector: 'app-valorar',
  templateUrl: './valorar.component.html',
  styleUrls: ['./valorar.component.scss'],

  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ValorarComponent implements OnInit {

  displayedColumns: string[] = ['abogado_nombre', 'tipo_Servicio', 'fecha_creado', 'valoracion'];
   dataSource: MatTableDataSource<TablaData>;
  resultado: any;
  // OBJETO PARA CARGAR EN LA TABLA
  datos: TablaData;

  ListaCasos:TablaData;
  
  servicio_id: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  value = '';
  valueInput() {
    this.value = '';
    //this.applyFilter(this.value);
  }
  constructor(public dialog: MatDialog, public generalService: GeneralService,
    public valorarService: ValorarService,
    private router: Router ) {
    this.CargarTabla();
    console.log(localStorage.getItem("id"));
  }

  ngOnInit() {

  }

  CargarTabla() {
    let resultado2;
    this.generalService.ObtenerUno("cliente/usuario", Number.parseInt(localStorage.getItem("id"))).then((result) => {
    resultado2 = result



    this.generalService.Obtenertodos('vista_servicio/cliente/'+resultado2.data.id+'/estatus/F').then((result) => {
      this.resultado = result;
      valoraciones = [];
      for (let i = 0; i < this.resultado.data.length; i++) {
        //let est = this.Estatus(this.resultado.data[i].estatus);
        this.ListaCasos = {
          id: this.resultado.data[i].id,
          abogado_nombre: this.resultado.data[i].abogado_nombre+' '+ this.resultado.data[i].abogado_apellido,
          abogado_id: this.resultado.data[i].abogado_id,
          tipo_servicio: this.resultado.data[i].tipo_servicio,
          servicio_id: this.resultado.data[i].servicio_id,
          fecha_creado: moment(this.resultado.data[i].fecha_creado,['MM-DD-YYYY']).format('DD-MM-YYYY'),
          tipo_servicio_id: this.resultado.data[i].tipo_servicio_id,
         // estatus: est,
        };
       
        valoraciones.push(this.ListaCasos);
      }
      this.dataSource = new MatTableDataSource(valoraciones);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, (err) => {
      console.log(err);
    });
  }, (err) => {
    console.log(err);
  });

  }

   // METODO QUE RETORNA ACTIVO O INACTIVO PARA MOSTRAR EN LA TABLA
   Estatus(est) {
    let estatus;
    return estatus = est == 'A' ? 'Activo' : 'Inactivo';
  }

  openDialogValorar(row) //abre una ventana modal con datos
  {
   
 
        const ObtenerDatos = {
          tipos : row.tipo_servicio_id,
          serv : row.servicio_id,
         }
         this.valorarService.insertData(ObtenerDatos);
         this.router.navigate([`${'valorarservicio'}`]);
  }
  
}
