import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PromocionesRoutingModule } from './promociones-routing.module';
import { DifusionComponent } from './difusion/difusion.component';
import { ConfigurarPromocionComponent } from './configurar-promocion/configurar-promocion.component';
import { SharedModule } from '../shared/shared.module';
import { PromocionComponent } from './configurar-promocion/promocion/promocion.component';
import { MAT_DATE_LOCALE } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    PromocionesRoutingModule
  ],
  declarations: [
    DifusionComponent,
    ConfigurarPromocionComponent,
    PromocionComponent
  ],
  entryComponents: [
    PromocionComponent
  ],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'es-Ve'},
  ],
})
export class PromocionesModule { }
