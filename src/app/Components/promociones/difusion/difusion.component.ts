import { Component, OnInit } from '@angular/core';
import { GeneralService } from '../../core/Services/general/general.service';
import { FormControl, Validators } from '@angular/forms';
import * as moment from 'moment';
import { MatSnackBar } from '@angular/material';
import { ListaCaracteristicaBaseComponent } from '../../Maestros/lista-caracteristica-base/lista-caracteristica-base.component';
export interface Objeto {
  id: number;
  nombre: string;
  caracteristicas: any[];
  
}
@Component({
  selector: 'app-difusion',
  templateUrl: './difusion.component.html',
  styleUrls: ['./difusion.component.css'],

})
export class DifusionComponent implements OnInit {
  resultado: any;
  resultado2: any;
  resultado3: any;
  resultado4: any;
  nombre: string;
  imagen: string;
  descripcion: string;
  idpro: number;
  listaCaracteristicaB: any[] = [];
  listaCaracteristica: any[] = [];
  listaPromocion: any[] = [];
  caracteristicas: any;
  datos: any[] = [];
  ids: any[] = [];
  constructor(public generalService: GeneralService, public snackBar: MatSnackBar) {
    this.CargarCaracteristicasBase();
    this.CargarPromocion();
    this.myControlFecha.disable();
    this.myControlFechaF.disable();
    this.myControlDescripcion.disable();
  }

  ngOnInit() {
  }

  myControlFecha = new FormControl('',
    [
      Validators.required
    ]);

  myControlFechaF = new FormControl('',
    [
      Validators.required
    ]);


  myControlDescripcion = new FormControl('',
    [
      Validators.required
    ]);

  CargarCaracteristicasBase() {
    this.generalService.Obtenertodos('caracteristica_base').then((result) => {
      this.resultado = result;
      for (let i = 0; i < this.resultado.data.length; i++) {
        this.generalService.Obtenertodos('caracteristica/base/' + this.resultado.data[i].id).then((result) => {
          this.resultado2 = result;
          this.listaCaracteristica = [];
          for (let i = 0; i < this.resultado2.data.length; i++) {
            this.listaCaracteristica.push({
              id: this.resultado2.data[i].id,
              nombre: this.resultado2.data[i].nombre,
              seleccion: false
            });
          }
          let carac: Objeto = {
            id: this.resultado.data[i].id,
            nombre: this.resultado.data[i].nombre,
            caracteristicas: this.listaCaracteristica            
          }
          this.listaCaracteristicaB.push(carac);
        }, (err) => {
          console.log(err);
        });
      }
    }, (err) => {
      console.log(err);
    });
  }

  CargarDatosPromocion(idPromocion) {
    this.generalService.ObtenerUno('promocion', idPromocion).then((result) => {
      this.resultado4 = result

      this.myControlFecha.setValue(moment(this.resultado4.data.fecha_inicio, ['llll']).format('DD-MM-YYYY'));
      this.myControlFechaF.setValue(moment(this.resultado4.data.fecha_fin, ['llll']).format('DD-MM-YYYY'));
      this.myControlDescripcion.setValue(this.resultado4.data.descripcion);
      this.idpro = idPromocion;
      this.imagen = this.resultado4.data.imagen;
      this.nombre = this.resultado4.data.nombre;
      this.descripcion = this.resultado4.data.descripcion;

    }, (err) => {
      console.log(err);
    });

  }

  CargarPromocion() {
    this.generalService.Obtenertodos('promocion').then((result) => {
      this.resultado3 = result;
      for (let i = 0; i < this.resultado3.data.length; i++) {
        this.listaPromocion.push({
          id: this.resultado3.data[i].id,
          nombre: this.resultado3.data[i].nombre,
        });
      }
    }, (err) => {
      console.log(err);
    });
  }



  difundir() {
    for (let i = 0; i < this.listaCaracteristicaB.length; i++) {
      
      for (let j = 0; j < this.listaCaracteristicaB[i].caracteristicas.length; j++) {

        if (this.listaCaracteristicaB[i].caracteristicas[j].seleccionado == true) {
          this.ids.push(this.listaCaracteristicaB[i].caracteristicas[j].id)
        }
        for (let k = 0; k < this.ids.length; k++) {
          let data = {
            idp: this.idpro,
            idca: this.ids[k]
          }

          this.generalService.RegistrarConImagen(data, 'difusion').then((result) => {
          }, (err) => {
            console.log(err);
          }
          )
        }

      }

    }
    let data2 = {
      nombre: this.nombre,
      imagen: this.imagen,
      descripcion: this.descripcion,
      filtros: this.ids
    }
    this.generalService.Registrar(data2, 'difusion/difundir').then((result) => {

    }, (err) => {
      console.log(err);
    })


  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }
  seleccionar(idcara, idbase) {
    let index = this.listaCaracteristicaB.findIndex(x => x.id == idbase);
    let index2 = this.listaCaracteristicaB[index].caracteristicas.findIndex(y => y.id == idcara);
    if (this.listaCaracteristicaB[index].caracteristicas[index2].seleccion == true) {
      this.listaCaracteristicaB[index].caracteristicas[index2].seleccion = false
    } else
      this.listaCaracteristicaB[index].caracteristicas[index2].seleccion = true;
  }
}
