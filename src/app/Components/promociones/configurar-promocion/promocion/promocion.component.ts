import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar, MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { GeneralService } from '../../../core/Services/general/general.service';
import { FormControl, Validators } from '@angular/forms';
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';
import * as moment from 'moment';

@Component({
  selector: 'app-promocion',
  templateUrl: './promocion.component.html',
  styleUrls: ['./promocion.component.scss'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'es-VE' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },

]
})
export class PromocionComponent implements OnInit {

  titulo;
  incluir: boolean = false;
  imagenDefecto: string = "assets/images/default.jpg";//imagen por defecto que se le pasa al incluir
  imagen;//variable para mostrar las imágenes
  imagenSeleccionada: any = null; // variable tipo File que se utiliza para mostrar una vista previa de la imagen
  resultado: any;
  resultado2: any;
  listaCatalogo: any[] = [];
  form_prom = { catalogo_servicio_id: '', descripcion: '', imagen: null, estatus: 'A', fecha_inicio: '',fecha_fin:'', nombre: '' };
  id;
  public loading = false;


  tituloFormControl = new FormControl('',
    [
      Validators.required
    ]);

  descripcionFormControl = new FormControl('',
    [
      Validators.required
    ]);

  fechaIFormControl = new FormControl('',
    [
      Validators.required
    ]);

  fechaFFormControl = new FormControl('',
    [
      Validators.required
    ]);

  tipoServFormControl = new FormControl('',
    [
      Validators.required
    ]);
  imagenFormControl = new FormControl('',
    [
      Validators.required
    ]);

  constructor(public dialogRef: MatDialogRef<PromocionComponent>, public generalService: GeneralService, public snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      console.log(data);
    if (data) {
      this.tituloFormControl.setValue(data.titulo);
      this.descripcionFormControl.setValue(data.contenido);
      this.fechaIFormControl.setValue(moment(data.fechaI,['MM-DD-YYYY']).format('llll'));
      this.fechaFFormControl.setValue(moment(data.fechaF,['MM-DD-YYYY']).format('llll'));
      this.id = data.id;
      this.tipoServFormControl.setValue(data.catalogo_servicio_id);
      this.imagen = data.imagen;
      this.titulo = 'Actualizar';
    }
    else {
      this.incluir = true;
      this.titulo = 'Nueva';
    }
    this.CargarCombo();
  }

  // Funcion para las imágenes 
  imagenEntrada(img: FileList) {
    this.imagenSeleccionada = img.item(0);
    //Para mostrar un preview de la imagen
    var lector = new FileReader();
    lector.onload = (event: any) => {
      this.imagen = event.target.result;
      this.imagenDefecto = event.target.result;
    }
    lector.readAsDataURL(this.imagenSeleccionada);
  }

  CargarCombo() {
    this.generalService.Obtenertodos('catalogo_servicio').then((result) => {
      this.resultado = result;
      for (let i = 0; i < this.resultado.data.length; i++) {
        this.listaCatalogo.push({
          id: this.resultado.data[i].id,
          nombre: this.resultado.data[i].nombre,
        });
      }
    }, (err) => {
      console.log(err);
    });
  }
  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

   // REGISTRAR UN NUEVO SERVICIO EN LA BASE DE DATOS
   Registrar() {
   
    if (this.descripcionFormControl.valid && this.fechaFFormControl.valid && this.fechaIFormControl.valid && this.imagenFormControl.valid && this.tipoServFormControl.valid && this.tituloFormControl) {
      const datos = new FormData();
      datos.append('imagen', this.imagenSeleccionada, this.imagenSeleccionada.name);
      datos.append('catalogo_servicio_id', this.tipoServFormControl.value);
      datos.append('descripcion', this.descripcionFormControl.value);
      datos.append('fecha_inicio', moment(this.fechaIFormControl.value, ['llll']).format('MM-DD-YYYY'));
      datos.append('fecha_fin', moment(this.fechaFFormControl.value, ['llll']).format('MM-DD-YYYY'));
      datos.append('nombre', this.tituloFormControl.value);
      datos.append('estatus', 'A');
      this.loading = true;

      this.generalService.RegistrarConImagen(datos,'promocion').then((result) => {
        this.openSnackBar('Registro exitoso!', "registrar");
        this.imagenDefecto = "assets/images/default.jpg";
        this.loading = false;
        this.dialogRef.close(datos);
      }, (err) => {
        console.log(err);
        this.loading = false;
      });
    }
  }

  
  // ACTUALIZAR LOS DATOS DEL SERVICIO EN LA BD
  Actualizar() {

    if (this.descripcionFormControl.valid && this.fechaFFormControl.valid && this.fechaIFormControl.valid && this.tipoServFormControl.valid && this.tituloFormControl) {
      const datos = new FormData();
      if (this.imagenSeleccionada != null) {
        datos.append('imagen', this.imagenSeleccionada, this.imagenSeleccionada.name);
      }
      datos.append('nombre', this.tituloFormControl.value);
      datos.append('descripcion', this.descripcionFormControl.value);
      datos.append('catalogo_servicio_id', this.tipoServFormControl.value);
      datos.append('fecha_inicio', moment(this.fechaIFormControl.value, ['llll']).format('MM-DD-YYYY'));
      datos.append('fecha_fin', moment(this.fechaFFormControl.value, ['llll']).format('MM-DD-YYYY'));
      this.loading = true;
      this.generalService.ActualizarConImagen(datos,'promocion', this.id).then(result => {
        console.log(result);
        this.generalService.ObtenerUno('promocion', this.id).then((result) => {
          this.resultado2 = result;
          this.loading = false;
          this.openSnackBar('Registro Actualizado!', "Actualizar");
          this.dialogRef.close(this.resultado2.data);
        }, (err) => {
          this.loading = false;
          console.log(err);
        });
      }, (err) => {
        this.loading = false;
        console.log(err);
      });

    }
  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }
}
