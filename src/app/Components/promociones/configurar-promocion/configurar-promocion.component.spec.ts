import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigurarPromocionComponent } from './configurar-promocion.component';

describe('ConfigurarPromocionComponent', () => {
  let component: ConfigurarPromocionComponent;
  let fixture: ComponentFixture<ConfigurarPromocionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigurarPromocionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigurarPromocionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
