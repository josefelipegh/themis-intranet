import { Component, OnInit } from '@angular/core';
import { GeneralService } from '../../core/Services/general/general.service';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { PromocionComponent } from './promocion/promocion.component';
import { ModalPreguntaEliminarComponent } from '../../shared/modal-pregunta-eliminar/modal-pregunta-eliminar.component';

export interface categoria {
  id: number;
  nombre: string;
}
@Component({
  selector: 'app-configurar-promocion',
  templateUrl: './configurar-promocion.component.html',
  styleUrls: ['./configurar-promocion.component.scss']
})
export class ConfigurarPromocionComponent implements OnInit {

  // variable que se usa para limpiar los valores de la caja de texto
  // con el atributo en la etiqueta input vista que se llama ngModel
  value = '';
  resultado: any;
  nombre: any;
  listaServicios: any[] = [];
  listaServicios2: any[] = [];
  categoria: categoria;
  listaCategoria: categoria[] = [];


  constructor(public dialog: MatDialog, public generalService: GeneralService) {
    this.CargarCombo();
    this.listaServicios2= this.listaServicios;
  }

  openDialogRegistrar() //abre una ventana modal
  {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';

    const dialogRef = this.dialog.open(PromocionComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD 
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.CargarPromocion()
        }
      }
    );

  }

  openDialogActualizar(row) //abre una ventana modal con datos
  {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    // PARA PASAR LOS DATOS AL DIALOG
    dialogConfig.data = row;
    console.log(row);

    const dialogRef = this.dialog.open(PromocionComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD 
    dialogRef.afterClosed().subscribe(
      data => console.log('object')
    );
  }

  ngOnInit() {

  }

  FiltrarCatalogo(idcat){
    this.listaServicios = this.listaServicios2.filter(x => x.catalogo_servicio_id == idcat);
  }
 

  
  FiltrarCatalogoNombre() {
    if (this.value != "") {
      this.listaServicios = this.listaServicios2
        .filter(x => x.titulo.trim()
          .toLowerCase().startsWith(this.value.trim().toLowerCase()) == true);
    } else {
      this.ReiniciarCatalogo();
    }
  }
  ReiniciarCatalogo(){
    this.listaServicios = this.listaServicios2;
  }

  CargarCombo() {
    this.generalService.Obtenertodos('catalogo_servicio').then((result) => {
      this.resultado = result;
      for (let i = 0; i < this.resultado.data.length; i++) {
        this.categoria = {
          id: this.resultado.data[i].id,
          nombre: this.resultado.data[i].nombre,
        }
        this.listaCategoria.push(this.categoria);
      }
      this.CargarPromocion();
    }, (err) => {
      console.log(err);
    });
  }

  valueInput() {
    this.value = '';
    this.ReiniciarCatalogo();
  }
 
  CargarPromocion() {
    this.listaServicios = [];
    this.generalService.Obtenertodos('promocion').then((result) => {
      this.resultado = result;
      for (let i = 0; i < this.resultado.data.length; i++) {
        this.listaServicios.push({
          id: this.resultado.data[i].id,
          titulo: this.resultado.data[i].nombre,
          contenido: this.resultado.data[i].descripcion,
          fechaI:this.resultado.data[i].fecha_inicio,
          fechaF:this.resultado.data[i].fecha_fin,
          catalogo_servicio_id: this.resultado.data[i].catalogo_servicio_id,
          imagen: this.resultado.data[i].imagen
        });
      }
      this.listaServicios2 = this.listaServicios;
    }, (err) => {
      console.log(err);
    });
  }

   // PARA ABRIR MODAL PREGUNTAR ESTÁ SEGURO DE ELIMNIAR ESTE MODAL ES GENERICO USEN EL QUE YA ESTA CREADO
   openDialogEliminar(row) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    // PARA PASAR LOS DATOS AL DIALOG PREGUNTA ELIMINAR 
    dialogConfig.data = {
      id: row,
      nombreMetodo: 'promocion'
    };

    const dialogRef = this.dialog.open(ModalPreguntaEliminarComponent, dialogConfig);
    // RECARGA LA TABLA CUANDO SE INSERTA EN LA BD 
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          let index = this.listaServicios.findIndex(x => x.id == data);
          this.listaServicios.splice(index, 1);
        }
      }
    );
  }
}
