import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConfigurarPromocionComponent } from './configurar-promocion/configurar-promocion.component';
import { DifusionComponent } from './difusion/difusion.component';
import { AuthGuardService as AuthGuard } from './../core/Services/AuthGuard/auth-guard.service';
const routes: Routes = [
  { path: 'config_promocion', component: ConfigurarPromocionComponent, canActivate: [AuthGuard] },
  {path: 'difusion', component: DifusionComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PromocionesRoutingModule { }
