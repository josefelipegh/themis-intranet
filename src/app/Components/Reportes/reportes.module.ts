import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportesRoutingModule } from './reportes-routing.module';
import { ReportesComponent } from './reportes/reportes.component';
import { SharedModule } from '../shared/shared.module';
import { ServicioComponent } from './reportes/estructurados/servicio/servicio.component';
import { PromocionComponent } from './reportes/estructurados/promocion/promocion.component';
import { ServicioEComponent } from './reportes/estadisticos/servicio-e/servicio-e.component';
import { PromocionEComponent } from './reportes/estadisticos/promocion-e/promocion-e.component';
import { CanalEscuchaEComponent } from './reportes/estadisticos/canal-escucha-e/canal-escucha-e.component';
import { IncidenciasEComponent } from './reportes/estadisticos/incidencias-e/incidencias-e.component';
import { IncidenciasComponent } from './reportes/estructurados/incidencias/incidencias.component';
import { CanalEscuchaComponent } from './reportes/estructurados/canal-escucha/canal-escucha.component';
import { TiposIncidenciasComponent } from './reportes/estadisticos/incidencias-e/tipos-incidencias/tipos-incidencias.component';
import { Tipos2IncidenciasComponent } from './reportes/estadisticos/incidencias-e/tipos2-incidencias/tipos2-incidencias.component';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ReportesRoutingModule
  ],
  declarations: [
    ReportesComponent,
    ServicioComponent,
    PromocionComponent,
    ServicioEComponent,
    PromocionEComponent,
    CanalEscuchaEComponent,
    IncidenciasEComponent,
    IncidenciasComponent,
    CanalEscuchaComponent,
    TiposIncidenciasComponent,
    Tipos2IncidenciasComponent
  ]
})
export class ReportesModule { }
