import { Component, OnInit } from '@angular/core';
import * as Chart from "chart.js";
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
//import * as querystring from 'querystring';
//import * as moment from 'moment';

import { GeneralService } from '../../../../core/Services/general/general.service';

@Component({
  selector: 'app-servicio-e',
  templateUrl: './servicio-e.component.html',
  styleUrls: ['./servicio-e.component.scss']
})

export class ServicioEComponent implements OnInit {

  chart: any;
  d: any;
  public myChart:any;  
  informe;
  tser: any;
  tmes: any;
  tyear: any;
  mes: any = [];
  year: any = [];
  por: any = [];
  servicios: any = [];
  colors = [];
  resultado;

  constructor(public generalService: GeneralService) {

      this.colors = [ 
        "#f99494",
        "#f66364",
        "#f33334",
        "#dc0d0e",
        "#b90c0d",
        "#930a0a",
        "#f8cc8c",
        "#f5b04d",
        "#f29b1d",
        "#de890d",
        "#c67a0c",
        "#a4650a",
        "#a1dbb1",
        "#71c989",
        "#4cba6b",
        "#3fa45b",
        "#358a4d",
        "#2a6d3c",
        "#d5dadc",
        "#bac1c4",
        "#9ea8ad",
        "#848f94",
        "#69767c",
        "#596468",
        "#b2d4f5",
        "#93bfeb",
        "#74abe2",
        "#5899DA",
        "#367dc4",
        "#1866b4",
        "#fcc3a7",
        "#f5aa85",
        "#ef8d5d",
        "#E8743B",
        "#da5a1b",
        "#cc4300",
        "#8fd1bb",
        "#66c2a3",
        "#3fb68e",
        "#19A979",
        "#0e8c62",
        "#03734d",
        "#d5dadc",
        "#bac1c4",
        "#9ea8ad",
        "#848f94",
        "#69767c",
        "#596468",
        "#5e2129",
        "#e6294b",
        "#1eacbe",
        "#e9ab2e",
        "#6352ce",
        "#028ee1",
        "#d61f1f",
        "#232a37",
        "#bf4959",
        "#85660c"],

        this.year= [ 
          "2019",
          "2020",
          "2021",
          "2022",
          "2023",
          "2024",
          "2025",
          "2026"]
   }

  ngOnInit() {
   // this.Cargar();

  }

barra(){
  this.por = [];
  this.servicios = [];

  this.generalService.Obtenertodos('estadistico_servicio').then((result) => {
    this.resultado = result;
    console.log(result);
    for (let i = 0; i < this.resultado.data.length; i++) {
      if( this.resultado.data[i].mes == this.tmes
        && this.resultado.data[i].y == this.tyear){
      this.servicios.push(this.resultado.data[i].tipo_servicio);
      this.por.push(parseInt(this.resultado.data[i].parcial));}
    }
    if (this.myChart) this.myChart.destroy(); //destroy prev chart instance
    this.myChart = new Chart("canvas-barra", {
      type: 'bar',
      pieceLabel: {
        fontColor: '#000'
      },
      
      data: {
        labels: this.servicios,
        datasets: [{
          label:'# de servicios',
          data: this.por,
          backgroundColor : this.colors,
         
        },]
      },

      options: {
        scaleShowValues: true,
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }],
            xAxes: [{
              ticks: {
                autoSkip: false
              }
            }]
          }
       /* onClick: (e) => {
            var element = this.myChart.getElementAtEvent(e);
            if (!element.length) {
              return; // return if not clicked on element
            }
            var label = element[0]._model.label;
            //this.router.navigate(['/detalle'], { queryParams: {estados: label, tipoentidad: 1} });
          }*/
      }
    });   }, err => {
      console.log(err);
    });        

  }

  torta(){
    this.por = [];
    this.servicios = [];
  
    this.generalService.Obtenertodos('estadistico_servicio').then((result) => {
      this.resultado = result;
      console.log(result);
      for (let i = 0; i < this.resultado.data.length; i++) {
        if( this.resultado.data[i].mes == this.tmes
          && this.resultado.data[i].y == this.tyear){
        this.servicios.push(this.resultado.data[i].tipo_servicio);
        this.por.push(parseInt(this.resultado.data[i].parcial));}
      }
      if (this.myChart) this.myChart.destroy(); //destroy prev chart instance

    this.myChart = new Chart("canvas-barra", {
      type: 'pie',
      pieceLabel: {
        fontColor: '#000'
      },
      
      data: {
        labels: this.servicios,
        datasets: [{
          label:'# de servicios',
          data: this.por,
          backgroundColor : this.colors,
         
        },]
      },

      options: {
           
        plugins: {
          datalabels: {
            color: '#000000',
            formatter: function(value) {
              return Math.round(value);
            },
            font: {
              weight: 'bold',
              size: 16,
            }
          },
     
      },

      legend: {
        position: 'left',
        fullWidth: true
      },

       /* onClick: (e) => {
            var element = this.myChart.getElementAtEvent(e);
            if (!element.length) {
              return; // return if not clicked on element
            }
            var label = element[0]._model.label;
            //this.router.navigate(['/detalle'], { queryParams: {estados: label, tipoentidad: 1} });
          }*/
      }
    });    }, err => {
      console.log(err);
    });      

  }

  downloadImagePDF(){
    var doc = new jspdf()
    var data = document.getElementById('content');
    html2canvas(data).then(canvas => {
      // Few necessary setting options
      var imgWidth = 208;
      var pageHeight = 295;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL('image/png')
      //let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF
      var position = 0;
      //pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
     doc.addImage(contentDataURL, 'PNG', 0, 40, imgWidth, imgHeight)
      doc.setFontSize(30)
      doc.text(55, 25, ' Servicios mas solicitados')
     var img = new Image();
      img.src = "../../../../../../assets/images/pic.png"
      doc.addImage(img, 'PNG', 0,3,30,30)
      doc.addImage(img, 'PNG', 180,3,30,30)
      doc.save("Reporte.pdf")
    });

      }

/*Cargar(){
  this.generalService.Obtenertodos('estadistico_servicio').then((result) => {
  this.resultado = result;
  console.log(result);
  for (let i = 0; i < this.resultado.data.length; i++) {
    this.servicios.push(this.resultado.data[i].tipo_servicio);
    this.por.push(parseInt(this.resultado.data[i].parcial));
  }
  
    console.log(this.servicios);
  console.log(this.por);
}, (err) => {
  console.log(err);
});
}


fitarfecha(tmes,tyear){
  this.por = [];
  this.servicios = [];

  this.generalService.Obtenertodos('estadistico_servicio').then((result) => {
    this.resultado = result;
    console.log(result);
    for (let i = 0; i < this.resultado.data.length; i++) {
      if( this.resultado.data[i].mes == tmes
        && this.resultado.data[i].y == tyear){
      this.servicios.push(this.resultado.data[i].tipo_servicio);
      this.por.push(parseInt(this.resultado.data[i].parcial));}
    }

    console.log(this.servicios);
    console.log(this.por);
  }, (err) => {
    console.log(err);
  });
}*/


}


