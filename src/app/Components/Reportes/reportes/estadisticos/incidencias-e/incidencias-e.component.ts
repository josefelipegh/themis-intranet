import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-incidencias-e',
  templateUrl: './incidencias-e.component.html',
  styleUrls: ['./incidencias-e.component.scss']
})
export class IncidenciasEComponent implements OnInit {

  constructor(private router: Router) { }

  goToPage(pageName: string) {
    this.router.navigate([`${pageName}`]);
  }


  ngOnInit() {
  }

}
