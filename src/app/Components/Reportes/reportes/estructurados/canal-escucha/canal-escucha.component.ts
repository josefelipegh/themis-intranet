import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import * as moment from 'moment';
import { GeneralService } from '../../../../core/Services/general/general.service';

export interface TipoS {
  value: string;
  viewValue: string;
}
export interface Estatus {
  value: string;
  viewValue: string;
}
export interface sugerencia {

  tipo_sugerencia: any;
  sugerencia: any;
  fecha: any;
  cedula: any;
  cliente_nombre: any;
  cliente_apellido?: any;
  respuesta: any;

}

let solicitud: any[] = [];


@Component({
  selector: 'app-canal-escucha',
  templateUrl: './canal-escucha.component.html',
  styleUrls: ['./canal-escucha.component.scss']
})
export class CanalEscuchaComponent implements OnInit {


  tablasReport: any[] = [];
            //tipo_sugerencia,  sugerencia, fecha cedula, cliente_nombre, cliente_apellido, respuesta
  titulos = ['Tipo Sugerencia', 'Sugerencia', 'Fecha', 'Cédula', 'Cliente Nombre', 'Cliente Apellido', 'Respuesta'];
  resultado;
  TotalServicioClientes: any;
  datos: sugerencia;
  reportdata: sugerencia;
  tipoSugerencia: any[] = [];
  arr: any[] = [];
  datosTabla: any[] = [];
  dataSource: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  value = '';
  valueInput() {
    this.value = '';
    this.applyFilter(this.value);
  }

  //tipo_sugerencia, sugerencia, fecha, cedula, cliente_nombre, cliente_apellido, respuesta
  displayedColumns: string[] = ['tipo_sugerencia', 'sugerencia', 'fecha', 'cedula', 'cliente_nombre', 'respuesta'];

  estatus: Estatus[] = [
    { value: 'A', viewValue: 'Aprobado' },
    { value: 'R', viewValue: 'Rechazado' },
    { value: 'V', viewValue: 'Valorado' },


  ];

  constructor(public generalService: GeneralService) {
    this.CargarTablaS();
    this.CargarCombo();

  }
  //finconstructor

  ngOnInit() {
  }

  respValid(resp:any) {

  let result;
    if (resp == null) {
      result = '----';
      return result;
    }else{
      result = resp;
    }

  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  // METODO QUE RETORNA ACTIVO O INACTIVO PARA MOSTRAR EN LA TABLA
  Estatus(est) {
    let estatus;
    return estatus = est == 'A' ? 'Activo' : 'Rechazado';
  }

  FechaCierre(fech) {

    let fecha: any;
    if (fech == null) {
      fecha = '----';
      return fecha;
    } else {

      fecha = moment(fecha, ['MM-DD-YYYY']).format('DD-MM-YYYY');

      return fecha;
    }

  }

  CargarTablaS() {
    this.generalService.Obtenertodos('reporte_sugerencia').then((result) => {
      this.resultado = result;

      let nomb = '';

      for (let i = 0; i < this.resultado.data.length; i++) {
        let resp = this.respValid(this.resultado.data[i].respuesta);
        nomb = this.resultado.data[i].cliente_nombre + ' ' + this.resultado.data[i].cliente_apellido;
        this.datos = {
          tipo_sugerencia: this.resultado.data[i].tipo_sugerencia,
          sugerencia: this.resultado.data[i].sugerencia,
          fecha: moment(this.resultado.data[i].fecha,['MM-DD-YYYY']).format('DD-MM-YYYY'),
          cedula: this.resultado.data[i].cedula,
          cliente_nombre: nomb,
          respuesta: resp,
        }
        solicitud.push(this.datos);
      }
      this.dataSource = new MatTableDataSource(solicitud);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    }, (err) => {
      console.log(err);
    });
  }

  CargarCombo() {
    this.generalService.Obtenertodos('reporte_sugerencia').then((result) => {

      this.resultado = result;
      for (let i = 0; i < this.resultado.data.length; i++) {
        this.tipoSugerencia.push({
          value: this.resultado.data[i].tipo_servicio_id,
          descripcion: this.resultado.data[i].tipo_servicio,
        });
      }
    }, (err) => {
      console.log(err);
    });
  }

  filtrarCateria(id: any) {
    this.dataSource.filter = id;
  }

  GenerarReporte() {

    const win = window.open('', '_blank');
    this.tablasReport = [];
    this.tablasReport.push(this.titulos);
    for (let i = 0; i < this.dataSource.data.length; i++) {
      const nombre = this.dataSource.data[i].cliente_nombre + ' ' + this.dataSource.data[i].cliente_apellido;
      this.tablasReport.push([
        //tipo_sugerencia, sugerencia, fecha, cedula, cliente_nombre, cliente_apellido
        this.dataSource.data[i].tipo_sugerencia,
        this.dataSource.data[i].sugerencia,
        this.dataSource.data[i].fecha,
        this.dataSource.data[i].cedula,
        nombre,
        this.dataSource.data[i].respuesta
      ]);

    }
    let TotalServicio = this.tablasReport.length;
    //console.log('Debe imprimir un total', TotalServicio ); 
    pdfMake.vfs = pdfFonts.pdfMake.vfs;

    let dd = {
      // by default we use portrait, you can change it to landscape if you wish
      pageOrientation: 'landscape',
      content: [
        { text: 'Bufete de abogados y asesoría Legal Illueca - BI', fontSize: 16, bold: true, alignment: 'center', style: 'header' },
        { text: 'Barquisimeto - Estado Lara. \n\n', fontSize: 15, bold: true, alignment: 'center', style: 'subheader' },
        { text: 'Reporte de sugerencias.', fontSize: 13, bold: true, alignment: 'center', style: 'subheader'  },
        {
          layout: 'headerLineOnly', // optional
          margin: [30, 10, 10, 30],
          fontSize: 10,
          table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            // margin: [left, top, right, bottom]
            headerRows: 1,

            widths: ['*', 80, 'auto', '*', 60, 60],

            body:
              this.tablasReport
          }
        },
        { text: 'Total sugerencias: ' + TotalServicio },
      ]
    };
    pdfMake.createPdf(dd).open({}, win);
    TotalServicio = 0;
  }

}