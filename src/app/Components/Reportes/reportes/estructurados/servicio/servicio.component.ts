import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import * as moment from 'moment';
import { GeneralService } from '../../../../core/Services/general/general.service';

export interface TipoS {
  value: string;
  viewValue: string;
}
export interface Estatus {
  value: string;
  viewValue: string;
}
export interface categoria {

  servicioid?: any;
  tiposervicioid?: any;
  tiposervicio: any;
  clienteid?: any;
  clientecedula?: any;
  clientenombre: any;
  clienteapellido?: any;
  telef: any;
  abogadoid?: any;
  abogadocedula?: any;
  abogadonombre: any;
  abogadoapellido?: any;
  fechacreado: any;
  fechacierre: any;
  estatus: any;
  incidencias: any;

}

let solicitud: any[] = [];


@Component({
  selector: 'app-servicio',
  templateUrl: './servicio.component.html',
  styleUrls: ['./servicio.component.scss']
})
export class ServicioComponent implements OnInit {


  tablasReport: any[] = [];
  titulos=['Nombre Cliente', 'Telefono Cliente', 'Tipo Servicio', 'Nombre Abogado', 'Fecha Creación', 'Fecha Cierre', 'Estatus', 'Incidencias'];
  resultado;
  TotalServicioClientes:any;
  datos: categoria;
  reportdata: categoria;
  tipoServicio: any[] = [];
  arr: any[] = [];
  datosTabla: any[] = [];
  dataSource: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  value = '';
  valueInput() {
    this.value = '';
    this.applyFilter(this.value);
  }
  //servicioid, tiposervicioid, tiposervicio, clienteid, clientecedula, clientenombre, clienteapellido, telef, abogadoid, abogadocedula, abogadonombre, abogadoapellido, fechacreado, fechacierre, estatus, incidencias,
  displayedColumns: string[] = ['clientenombre', 'telef', 'tiposervicio', 'abogadonombre', 'fechacreado', 'fechacierre', 'estatus', 'incidencias'];

  estatus: Estatus[] = [
    { value: 'A', viewValue: 'Aprobado' },
    { value: 'R', viewValue: 'Rechazado' },
    { value: 'V', viewValue: 'Valorado' },


  ];

  constructor(public generalService: GeneralService) {
    this.CargarTablaS();
    this.CargarCombo();

  }
  //finconstructor

  ngOnInit() {
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  // METODO QUE RETORNA ACTIVO O INACTIVO PARA MOSTRAR EN LA TABLA
  Estatus(est) {
    let estatus;
    return estatus = est == 'A' ? 'Activo' : 'Rechazado';
  }

  FechaCierre(fech) {

    let fecha: any;
    if (fech == null) {
      fecha = '----';
      return fecha;
    } else {

      fecha = moment(fecha, ['MM-DD-YYYY']).format('DD-MM-YYYY');

      return fecha;
    }

  }

  CargarTablaS() {
    this.generalService.Obtenertodos('reporte_servicio').then((result) => {
      this.resultado = result;

      let nomb = '';

      for (let i = 0; i < this.resultado.data.length; i++) {

        let est = this.Estatus(this.resultado.data[i].estatus);
        let fech = this.FechaCierre(this.resultado.data[i].fecha_cierre);
        nomb = this.resultado.data[i].cliente_nombre + ' ' + this.resultado.data[i].cliente_apellido
        this.datos = {

          servicioid: this.resultado.data[i].servicio_id,
          tiposervicioid: this.resultado.data[i].tipo_servicio_id,
          tiposervicio: this.resultado.data[i].tipo_servicio,
          clienteid: this.resultado.data[i].cliente_id,
          clientecedula: this.resultado.data[i].cliente_cedula,
          clientenombre: nomb,
          clienteapellido: this.resultado.data[i].cliente_apellido,
          telef: this.resultado.data[i].telefono,
          abogadoid: this.resultado.data[i].abogado_id,
          abogadocedula: this.resultado.data[i].abogado_cedula,
          abogadonombre: this.resultado.data[i].abogado_nombre,
          abogadoapellido: this.resultado.data[i].abogado_apellido,
          fechacreado: moment(this.resultado.data[i].fecha_creado, ['MM-DD-YYYY']).format('DD-MM-YYYY'),
          fechacierre: fech,
          incidencias: this.resultado.data[i].incidencia,
          estatus: est,
        }

        solicitud.push(this.datos);
      }
      this.dataSource = new MatTableDataSource(solicitud);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    }, (err) => {
      console.log(err);
    });
  }

  CargarCombo() {
    this.generalService.Obtenertodos('reporte_servicio').then((result) => {

      this.resultado = result;
      for (let i = 0; i < this.resultado.data.length; i++) {
        this.tipoServicio.push({
          value: this.resultado.data[i].tipo_servicio_id,
          descripcion: this.resultado.data[i].tipo_servicio,
        });
      }
    }, (err) => {
      console.log(err);
    });
  }

  filtrarCateria(id: any) {
    this.dataSource.filter = id;
  }

  GenerarReporte() {

    const win = window.open('', '_blank');
    this.tablasReport = [];
    this.tablasReport.push(this.titulos);
    for (let i = 0; i < this.dataSource.data.length; i++) {
      const abogado = this.dataSource.data[i].abogadonombre + ' ' + this.dataSource.data[i].abogadoapellido;
      this.tablasReport.push([
        //'clientenombre', 'telef', 'tiposervicio', 'abogadonombre', 'fechacreado', 'fechacierre', 'estatus', 'incidencias'
        this.dataSource.data[i].clientenombre,
        this.dataSource.data[i].telef,
        this.dataSource.data[i].tiposervicio,
        abogado,
        this.dataSource.data[i].fechacreado,
        this.dataSource.data[i].fechacierre,
        this.dataSource.data[i].estatus,
        this.dataSource.data[i].incidencias,
      ]);

    }
   let TotalServicio = this.tablasReport.length;
    //console.log('Debe imprimir un total', TotalServicio ); 
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
    let dd = {
      // by default we use portrait, you can change it to landscape if you wish
      pageOrientation: 'landscape',
      content: [
        { text: 'Bufete de abogados y asesoría Legal Illueca - BI', fontSize: 16, bold: true, alignment: 'center', style: 'header' },
        { text: 'Barquisimeto - Estado Lara. \n\n', fontSize: 15, bold: true, alignment: 'center', style: 'subheader' },
        { text: 'Reporte de servicios.', fontSize: 13, bold: true, alignment: 'center' },
        {
          layout: 'headerLineOnly', // optional
          margin: [30, 10, 10, 30],
          fontSize: 10,
          table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            // margin: [left, top, right, bottom]
            headerRows: 1,

            widths: ['*', 80, 'auto', '*', 60, 60, 60, 60],

            body:
              this.tablasReport
          }
        },
        {text:'Total tipos de servicios prestados: '+TotalServicio } ,
      ]
    };
    pdfMake.createPdf(dd).open({}, win);
    TotalServicio = 0;
  }

}
