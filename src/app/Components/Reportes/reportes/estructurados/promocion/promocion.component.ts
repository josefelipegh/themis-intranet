import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import * as moment from 'moment';

import { GeneralService } from '../../../../core/Services/general/general.service';

export interface promocion {
  caracteristicas: any;
  nombrepromocion: any;
  descripcionpromocion: any;
  tiposervicio: any;
  fechainicio: any;
  fechafin: any;
  estatspromocion?: any;
}

export interface TipoS {
  value: string;
  viewValue: string;
}
export interface Estatus {
  value: string;
  viewValue: string;
}

let solicitud: any[] = [];


@Component({
  selector: 'app-promocion',
  templateUrl: './promocion.component.html',
  styleUrls: ['./promocion.component.scss']
})
export class PromocionComponent implements OnInit {

  tablasReport: any[] = [];
  titulos=['Caracteristica', 'Nombre Promocion' , 'Descripcion Promocion' , 'Tipo Servicio' , 'Fecha Inicio' , 'Fecha Cirre'];
  //caracteristica, nombre_promocion , descripcion_promocion , tipo_servicio , fecha_inicio , fecha_fin , estats_promocion ,
  displayedColumns: String[] = ['caracteristica', 'nombre_promocion' , 'descripcion_promocion' , 'tipo_servicio' , 'fecha_inicio' , 'fecha_fin'];

  datos: promocion;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  value = '';
  valueInput() {
    this.value = '';
    this.applyFilter(this.value);
  }
  dataSource: any;
  tipoServicio: any[] = [];

  constructor(public generalService: GeneralService) {

    this.CargarTablaS();
    this.CargarCombo();
   }

  resultado;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit() {
  }

  // METODO QUE RETORNA ACTIVO O INACTIVO PARA MOSTRAR EN LA TABLA
  Estatus(est) {
    let estatus;
    return estatus = est == 'A' ? 'Activo' : 'Rechazado';
  }

  FechaCierre(fech) {

    let fecha: any;
    if (fech == null) {
      fecha = '----';
      return fecha;
    } else {

      fecha = moment(fecha, ['MM-DD-YYYY']).format('DD-MM-YYYY');

      return fecha;
    }

  }

  CargarCombo() {
    this.generalService.Obtenertodos('reporte_promocion').then((result) => {

      this.resultado = result; console.log('Combo', this.resultado)
      for (let i = 0; i < this.resultado.data.length; i++) {
        this.tipoServicio.push({
          value: this.resultado.data[i].tipo_servicio_id,
          descripcion: this.resultado.data[i].tipo_servicio,
        });
      }
    }, (err) => {
      console.log(err);
    });
  }


  CargarTablaS() {
    this.generalService.Obtenertodos('reporte_promocion').then((result) => {
      this.resultado = result; console.log('Resultados ',this.resultado)

      for (let i = 0; i < this.resultado.data.length; i++) {

        this.datos = {
          caracteristicas: this.resultado.data[i].caracteristica, 
          nombrepromocion: this.resultado.data[i].nombre_promocion,
          descripcionpromocion: this.resultado.data[i].descripcion_promocion,
          tiposervicio: this.resultado.data[i].tipo_servicio,
          fechainicio: moment(this.resultado.data[i].fecha_inicio,  ['MM-DD-YYYY']).format('DD-MM-YYYY'),
          fechafin: moment(this.resultado.data[i].fecha_fin,  ['MM-DD-YYYY']).format('DD-MM-YYYY'), 
        }
  
        solicitud.push(this.datos);
      } 
      this.dataSource = new MatTableDataSource(solicitud);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    }, (err) => {
      console.log(err);
    });
  }

  filtrarCateria(id: any) {
    this.dataSource.filter = id;
  }

  GenerarReporte() {

    const win = window.open('', '_blank');
    this.tablasReport = [];
    this.tablasReport.push(this.titulos);
    for (let i = 0; i < this.dataSource.data.length; i++) {
      //const abogado = this.dataSource.data[i].abogadonombre + ' ' + this.dataSource.data[i].abogadoapellido;
      this.tablasReport.push([
        //caracteristica, nombre_promocion , descripcion_promocion , tipo_servicio , fecha_inicio , fecha_fin , estats_promocion
        this.dataSource.data[i].caracteristicas,
        this.dataSource.data[i].nombrepromocion,
        this.dataSource.data[i].descripcionpromocion,
        this.dataSource.data[i].tiposervicio,
        this.dataSource.data[i].fechainicio,
        this.dataSource.data[i].fechafin,
      ]); 
      console.log('Data', this.dataSource.data)

    }
   let TotalServicio = this.tablasReport.length;
    //console.log('Debe imprimir un total', TotalServicio ); 
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
    let dd = {
      // by default we use portrait, you can change it to landscape if you wish
      pageOrientation: 'landscape',
      content: [
        { text: 'Bufete de abogados y asesoría Legal Illueca - BI', fontSize: 16, bold: true, alignment: 'center', style: 'header' },
        { text: 'Barquisimeto - Estado Lara. \n\n', fontSize: 15, bold: true, alignment: 'center', style: 'subheader' },
        { text: 'Reporte de servicios.', fontSize: 13, bold: true, alignment: 'center' },
        {
          layout: 'headerLineOnly', // optional
          margin: [30, 10, 10, 30],
          fontSize: 10,
          table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            // margin: [left, top, right, bottom]
            headerRows: 1,

            widths: ['*', 80, 'auto', '*', 60, 60],

            body:
              this.tablasReport
          }
        },
        {text:'Total de promociones difundidas: '+TotalServicio } 
      ]
    };
    pdfMake.createPdf(dd).open({}, win);
    TotalServicio = 0;
  }

}
