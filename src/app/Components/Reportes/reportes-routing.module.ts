import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportesComponent } from './reportes/reportes.component';
import { AuthGuardService as AuthGuard } from './../core/Services/AuthGuard/auth-guard.service';
import { ServicioComponent } from './reportes/estructurados/servicio/servicio.component';
import { PromocionComponent } from './reportes/estructurados/promocion/promocion.component';
import { IncidenciasComponent } from './reportes/estructurados/incidencias/incidencias.component';
import { CanalEscuchaComponent } from './reportes/estructurados/canal-escucha/canal-escucha.component';
import { ServicioEComponent } from './reportes/estadisticos/servicio-e/servicio-e.component';
import { PromocionEComponent } from './reportes/estadisticos/promocion-e/promocion-e.component';
import { IncidenciasEComponent } from './reportes/estadisticos/incidencias-e/incidencias-e.component';
import { CanalEscuchaEComponent } from './reportes/estadisticos/canal-escucha-e/canal-escucha-e.component';
import { TiposIncidenciasComponent } from './reportes/estadisticos/incidencias-e/tipos-incidencias/tipos-incidencias.component';
import { Tipos2IncidenciasComponent } from './reportes/estadisticos/incidencias-e/tipos2-incidencias/tipos2-incidencias.component';

const routes: Routes = [
//   {
//     path: '',
//     redirectTo: 'reportes',
//     pathMatch: 'full'
//   },
//   {
//     path: 'reportes',
//     component: ReportesComponent,
//     children: [
//       { path: '', component: ReportesComponent }
//     ]
// }
{ path: 'reportes2', component: ReportesComponent, canActivate: [AuthGuard] },
{ path: 'reportes', component: ReportesComponent },
//Estructurados
{ path: 'report-canal', component: CanalEscuchaComponent },
{ path: 'report-incidencia', component: IncidenciasComponent  },
{ path: 'report-promocion', component: PromocionComponent },
{ path: 'report-servicio', component: ServicioComponent },
//Estadisticos
{ path: 'report-est-canal', component: CanalEscuchaEComponent },
{ path: 'report-est-incidencia', component: IncidenciasEComponent },
{ path: 'report-est-incidencia-tipo', component: TiposIncidenciasComponent },
{ path: 'report-est-incidencia-tipo2', component: Tipos2IncidenciasComponent },
{ path: 'report-est-promocion', component: PromocionEComponent },
{ path: 'report-est-servicio', component: ServicioEComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportesRoutingModule { }
